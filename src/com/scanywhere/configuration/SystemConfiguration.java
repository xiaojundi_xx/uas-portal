package com.scanywhere.configuration;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

public class SystemConfiguration
{
	private static final Logger logger = Logger.getLogger(SystemConfiguration.class);

	private Configuration configuration;

	public SystemConfiguration()
	{
		init();
	}

	public void init()
	{
		CompositeConfiguration conf = new CompositeConfiguration();
		try
		{
			Enumeration<URL> resources = SystemConfiguration.class.getClassLoader()
					.getResources("config/system.properties");
			while (resources.hasMoreElements())
			{
				URL u = resources.nextElement();
				if (logger.isDebugEnabled())
				{
					logger.debug("Loading config from " + u);
				}

				conf.addConfiguration(new PropertiesConfiguration(u));
			}
			configuration = conf;
		}
		catch (ConfigurationException e)
		{
			e.printStackTrace();
			logger.error("Cannot load the configuration file", e);
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			logger.error("Unable to load system.propeties from classpath", e);
			throw new RuntimeException(e);
		}
	}

	public Configuration getConfiguration() throws IllegalStateException
	{
		if (configuration == null)
		{
			init();
		}

		return configuration;
	}
}

package com.scanywhere.configuration;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

public class ConfigurationFactory
{
	private static final Logger logger = Logger.getLogger(ConfigurationFactory.class);

	private static SystemConfiguration systemConfiguration;

	public static Configuration getSystemConfiguration()
	{
		if (systemConfiguration == null)
		{
			try
			{
				Class<?> c = Class.forName("com.scanywhere.configuration.SystemConfiguration");
				systemConfiguration = (SystemConfiguration) c.newInstance();
			}
			catch (Exception e)
			{
				logger.error("System Configuration Init Error!");
				e.printStackTrace();
			}
		}
		return systemConfiguration.getConfiguration();
	}
}

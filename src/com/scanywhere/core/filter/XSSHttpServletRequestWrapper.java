package com.scanywhere.core.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang3.StringEscapeUtils;

public class XSSHttpServletRequestWrapper extends HttpServletRequestWrapper
{
	/** Exclude Parameter Names */
	public String[] excludeParameterNames;

	public XSSHttpServletRequestWrapper(HttpServletRequest request, String[] excludeParameterNames)
	{
		super(request);
		this.excludeParameterNames = excludeParameterNames;
	}

	public String getHeader(String name)
	{
		return StringEscapeUtils.escapeHtml4(super.getHeader(name));
	}

	public String getQueryString()
	{
		return StringEscapeUtils.escapeHtml4(super.getQueryString());
	}

	public String getParameter(String name)
	{
		if (this.excludeParameterNames != null && this.excludeParameterNames.length > 0)
		{
			for (int i = 0; i < this.excludeParameterNames.length; i++)
			{
				if (name.equals(excludeParameterNames[i]))
				{
					return super.getParameter(name);
				}
			}
		}
		return StringEscapeUtils.escapeHtml4(super.getParameter(name));
	}

	public String[] getParameterValues(String name)
	{
		String[] values = super.getParameterValues(name);
		if (values != null)
		{
			int length = values.length;
			String[] escapseValues = new String[length];
			for (int i = 0; i < length; i++)
			{
				escapseValues[i] = StringEscapeUtils.escapeHtml4(values[i]);
			}
			return escapseValues;
		}
		return super.getParameterValues(name);
	}
}

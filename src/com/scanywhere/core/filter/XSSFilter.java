package com.scanywhere.core.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.scanywhere.common.lang.StringUtils;

public class XSSFilter implements Filter
{
	/** Default path separator: ";" */
	protected static final String DEFAULT_PATH_SEPARATOR = ";";

	/** Exclude Parameter Names */
	public String[] excludeParameterNames;

	public void init(FilterConfig filterConfig) throws ServletException
	{
		String value = filterConfig.getInitParameter("excludeParameterNames");
		if (StringUtils.isNotEmpty(value))
		{
			this.excludeParameterNames = StringUtils.tokenizeToStringArray(value, DEFAULT_PATH_SEPARATOR);
		}
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException
	{
		XSSHttpServletRequestWrapper xssRequest = new XSSHttpServletRequestWrapper((HttpServletRequest) servletRequest,
				this.excludeParameterNames);
		filterChain.doFilter(xssRequest, servletResponse);
	}

	public void destroy()
	{
	}
}

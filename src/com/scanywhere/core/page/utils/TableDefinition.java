package com.scanywhere.core.page.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;

import com.scanywhere.common.utils.ConfigurationUtil;
import com.scanywhere.core.page.xml.DataTables;
import com.scanywhere.core.page.xml.Table;

public class TableDefinition
{
	public static final String DEFAULT_CONFIG_LOCATION = "plugin/table/*.xml";

	static final Logger logger = Logger.getLogger(TableDefinition.class);

	private static volatile TableDefinition tableDefinition;

	private final Map<String, Table> tables = new HashMap<String, Table>();

	private final Map<Resource, Long> cachedFiles = new HashMap<Resource, Long>();

	private int cachedFilesCount;

	private TableDefinition()
	{
		initQuery();
	}
	
	public static TableDefinition getInstance()
	{
		if (tableDefinition == null)
		{
			synchronized (TableDefinition.class)
			{
				if (tableDefinition == null)
				{
					tableDefinition = new TableDefinition();
				}
			}
		}
		return tableDefinition;
	}

	public void initQuery()
	{
		cachedFilesCount = 0;
		Resource resources[] = ConfigurationUtil.getAllResources(DEFAULT_CONFIG_LOCATION);
		if (resources != null)
		{
			for (int i = 0; i < resources.length; i++)
			{
				Resource resource = resources[i];
				logger.info("Loading query from {" + resource.toString() + "}");
				try
				{
					DataTables dataTables = (DataTables) ConfigurationUtil.parseXMLObject(DataTables.class,
							resource);
					List<Table> list = dataTables.getTables();
					for (Iterator<Table> it = list.iterator(); it.hasNext();)
					{
						Table table = (Table) it.next();
						Table previous = (Table) tables.put(table.getId(), table);
					}
					
					if (resource.getURL().getProtocol().equals("file"))
					{
						cachedFiles.put(resource, Long.valueOf(resource.getFile().lastModified()));
					}
				}
				catch (IOException e)
				{
					logger.error("Could not load table from {" + resource.toString() + "}, reason:", e);
				}
				catch (RuntimeException e)
				{
					logger.error("Fail to digester table from {" + resource + "}, reason:", e);
				}
			}

			cachedFilesCount = cachedFiles.size();
		}
	}

	public Table getTable(String tableId)
	{
		tableDefinition.update();
		return (Table) tableDefinition.tables.get(tableId);
	}

	public Map<String, Table> getTables()
	{
		return tableDefinition.tables;
	}

	public void update()
	{
		if (cachedFilesCount > 0)
		{
			for (Iterator<Resource> i = cachedFiles.keySet().iterator(); i.hasNext();)
			{
				Resource resource = (Resource) i.next();
				synchronized (cachedFiles)
				{
					try
					{
						if (resource.getFile().lastModified() > ((Long) cachedFiles.get(resource)).longValue())
						{
							DataTables dataTables = (DataTables) ConfigurationUtil.parseXMLObject(DataTables.class, resource);
							List<Table> list = dataTables.getTables();
							for (Iterator<Table> it = list.iterator(); it.hasNext();)
							{
								Table table = (Table) it.next();
								tableDefinition.tables.put(table.getId(), table);
							}
							cachedFiles.put(resource, Long.valueOf(resource.getFile().lastModified()));
						}
					}
					catch (IOException e)
					{
						logger.error("Could not load query from {" + resource.toString() + "}, reason:", e);
					}
					catch (RuntimeException e)
					{
						logger.error("Fail to digester query from {" + resource.toString() + "}, reason:", e);
					}
				}
			}
		}
	}
}

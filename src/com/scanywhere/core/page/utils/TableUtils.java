package com.scanywhere.core.page.utils;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.exception.PageQueryException;
import com.scanywhere.core.page.pojo.Page;
import com.scanywhere.core.page.pojo.QueryCondition;
import com.scanywhere.core.page.xml.Table;

public class TableUtils
{
	/**
	 * 根据TableCondition获取Table
	 *
	 * @param queryCondition 查询条件
	 * @return
	 * @throws PageQueryException
	 */
	public static Table getTable(QueryCondition queryCondition) throws PageQueryException
	{
		if (queryCondition == null)
		{
			throw new PageQueryException("QueryCondition为空!");
		}
		
		if (queryCondition.getTable() != null)
		{
			return queryCondition.getTable();
		}
		
		String tableId = queryCondition.getTableId();
		if (StringUtils.isNotEmpty(tableId))
		{
			Table table = TableDefinition.getInstance().getTable(tableId);
			if (table == null)
			{
				throw new PageQueryException("TableId为{" + tableId + "}的XML配置不存在!");
			}
			else
			{
				return table;
			}
		}
		else
		{
			throw new PageQueryException("TableId为空, 请指定[tableId]!");
		}
	}
	
	/**
	 * 获取分页信息
	 *
	 * @param queryCondition 查询条件
	 * 
	 * @param table 表数据配置
	 * 
	 * @return 分页信息
	 */
	public static Page getPage(QueryCondition queryCondition, Table table)
	{
		Page page = new Page();
		if (queryCondition.getPage() == null)
		{
			page.setPageSize(table.getPageSize());
		}
		else
		{
			page = queryCondition.getPage();
		}
		return page;
	}
}

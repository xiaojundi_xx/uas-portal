package com.scanywhere.core.page.xml;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.digester3.annotations.rules.ObjectCreate;
import org.apache.commons.digester3.annotations.rules.SetNext;

@ObjectCreate(pattern = "dataTables")
public class DataTables
{
	private int cacheSize;

	private List<Table> tables;

	public DataTables()
	{
		cacheSize = 20;
		tables = new ArrayList<Table>();
	}

	public int getCacheSize()
	{
		return cacheSize;
	}

	public void setCacheSize(int cacheSize)
	{
		this.cacheSize = cacheSize;
	}

	@SetNext
	public void addTable(Table table)
	{
		this.tables.add(table);
	}

	public List<Table> getTables()
	{
		return this.tables;
	}
}

package com.scanywhere.core.page.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.digester3.annotations.rules.ObjectCreate;
import org.apache.commons.digester3.annotations.rules.SetNext;
import org.apache.commons.digester3.annotations.rules.SetProperty;

@ObjectCreate(pattern = "dataTables/table")
public class Table
{
	@SetProperty(attributeName = "id", pattern = "dataTables/table")
	private String id;

	@SetProperty(attributeName = "key", pattern = "dataTables/table")
	private String key;

	@SetProperty(attributeName = "pageSize", pattern = "dataTables/table")
	private Integer pageSize;

	@SetProperty(attributeName = "allowPaging", pattern = "dataTables/table")
	private Boolean allowPaging;

	private List<Column> columnList;

	private Map<String, Column> columnMap;

	public Table()
	{
		key = "id";
		allowPaging = true;
		pageSize = 10;
		columnList = new ArrayList<Column>();
		columnMap = new HashMap<String, Column>();
	}

	@SetNext
	public void addColumn(Column queryColumn)
	{

		columnList.add(queryColumn);
		columnMap.put(queryColumn.getId() != null ? queryColumn.getId() : queryColumn.getKey(), queryColumn);
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(Integer pageSize)
	{
		this.pageSize = pageSize;
	}

	public Boolean getAllowPaging()
	{
		return allowPaging;
	}

	public void setAllowPaging(Boolean allowPaging)
	{
		this.allowPaging = allowPaging;
	}

	public List<Column> getColumnList()
	{
		return columnList;
	}

	public Map<String, Column> getColumnMap()
	{
		return columnMap;
	}

	public Column getColumn(String key)
	{
		return (Column) columnMap.get(key);
	}

	public void setColumnList(List<Column> columnList)
	{
		this.columnList = columnList;
	}

	public void setColumnMap(Map<String, Column> columnMap)
	{
		this.columnMap = columnMap;
	}
}

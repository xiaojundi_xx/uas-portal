package com.scanywhere.core.page.xml;

import org.apache.commons.digester3.annotations.rules.ObjectCreate;
import org.apache.commons.digester3.annotations.rules.SetProperty;

/**
 * 列
 */
@ObjectCreate(pattern = "dataTables/table/column")
public class Column
{
	/**
	 * 实体名
	 */
	@SetProperty(attributeName = "key", pattern = "dataTables/table/column")
	private String key;

	/**
	 * id
	 */
	@SetProperty(attributeName = "id", pattern = "dataTables/table/column")
	private String id;

	/**
	 * 显示值
	 */
	@SetProperty(attributeName = "header", pattern = "dataTables/table/column")
	private String header;

	/**
	 * 对齐方式 left right center
	 */
	@SetProperty(attributeName = "align", pattern = "dataTables/table/column")
	private String align;

	/**
	 * 是否在列表显示
	 */
	@SetProperty(attributeName = "hidden", pattern = "dataTables/table/column")
	private Boolean hidden;

	/**
	 * 是否显示提示信息
	 */
	@SetProperty(attributeName = "enableTooltip", pattern = "dataTables/table/column")
	private Boolean enableTooltip;

	/**
	 * 显示列的长度
	 */
	@SetProperty(attributeName = "width", pattern = "dataTables/table/column")
	private String width;

	/**
	 * 列的格式化显示（服务器端调用）
	 */
	@SetProperty(attributeName = "dateFormat", pattern = "dataTables/table/column")
	private String dateFormat;

	/**
	 * 客户端 重写该列的方法
	 */
	@SetProperty(attributeName = "fnRender", pattern = "dataTables/table/column")
	private String fnRender;

	/**
	 * 截取长度
	 */
	@SetProperty(attributeName = "maxLen", pattern = "dataTables/table/column")
	private Integer maxLen;

	/**
	 * 提示文本
	 */
	@SetProperty(attributeName = "tooltip", pattern = "dataTables/table/column")
	private String tooltip;

	/**
	 * render简化fnRender配置 eq对应 redner="type=eq,1=可用,0=停用" 默认type可不配
	 * render="type=link,url=/base/user/userAddUpdate,param=id,target=_self"
	 * type="link" render=
	 * "type=window,url=/base/user/userAddUpdate,param=id,winId=id,winHeader=value,width=600,height=400,isModel=true"
	 * winId winHeader isModel可默认不配
	 */
	@SetProperty(attributeName = "render", pattern = "dataTables/table/column")
	private String render;

	public Column()
	{
		width = "50";
		align = "center";
		hidden = false;
		enableTooltip = false;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public String getAlign()
	{
		return align;
	}

	public void setAlign(String align)
	{
		this.align = align;
	}

	public Boolean getHidden()
	{
		return hidden;
	}

	public void setHidden(Boolean hidden)
	{
		this.hidden = hidden;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
	}

	public String getFnRender()
	{
		return fnRender;
	}

	public void setFnRender(String fnRender)
	{
		this.fnRender = fnRender;
	}

	public Boolean getEnableTooltip()
	{
		return enableTooltip;
	}

	public void setEnableTooltip(Boolean enableTooltip)
	{
		this.enableTooltip = enableTooltip;
	}

	public String getDateFormat()
	{
		return dateFormat;
	}

	public void setDateFormat(String dateFormat)
	{
		this.dateFormat = dateFormat;
	}

	public Integer getMaxLen()
	{
		return maxLen;
	}

	public void setMaxLen(Integer maxLen)
	{
		this.maxLen = maxLen;
	}

	public String getTooltip()
	{
		return tooltip;
	}

	public void setTooltip(String tooltip)
	{
		this.tooltip = tooltip;
	}

	public String getRender()
	{
		return render;
	}

	public void setRender(String render)
	{
		this.render = render;
	}
}

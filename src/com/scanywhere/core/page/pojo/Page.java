package com.scanywhere.core.page.pojo;

import java.util.List;

import com.scanywhere.common.utils.PageUtils;

@SuppressWarnings("rawtypes")
public class Page
{
	// 默认页码
	public static final int DEFAULT_PAGE_NO = 1;

	// 默认页面记录数
	public static final int DEFAULT_PAGE_SIZE = 20;

	// 当前页
	private int pageNo = DEFAULT_PAGE_NO;

	// 每页显示的数据条数
	private int pageSize = DEFAULT_PAGE_SIZE;

	// 总记录条数
	private int totalRecord;

	// 结果集
	private List dataList;

	public Page()
	{

	}

	public Page(int totalRecord, int pageSize, List dataList)
	{
		if (totalRecord < 0 || pageSize < 0L)
		{
			throw new RuntimeException("Illegal Arguments to Initiate Page Object");
		}
		else
		{
			this.totalRecord = totalRecord;
			this.pageSize = pageSize;
			this.dataList = dataList;
		}
	}

	/**
	 * 总页数
	 * 
	 * @return
	 */
	public int getTotalPages()
	{
		return PageUtils.totalPages(totalRecord, pageSize);
	}

	/**
	 * 计算当前页开始记录
	 * 
	 * @param currentPageNo 当前第几页
	 * @param pageSize 每页记录数
	 * @return 当前页开始记录号
	 */
	public int countStartIndex(int currentPageNo, int pageSize)
	{
		int offset = pageSize * (currentPageNo - 1);
		return offset;
	}

	/**
	 * 首页
	 * 
	 * @return
	 */
	public int getTopPageNo()
	{
		return 1;
	}

	/**
	 * 上一页
	 * 
	 * @return
	 */
	public int getPreviousPageNo()
	{
		if (pageNo <= 1)
		{
			return 1;
		}
		return pageNo - 1;
	}

	/**
	 * 下一页
	 * 
	 * @return
	 */
	public int getNextPageNo()
	{
		if (pageNo >= getBottomPageNo())
		{
			return getBottomPageNo();
		}
		return pageNo + 1;
	}

	/**
	 * 最后一页
	 * 
	 * @return
	 */
	public int getBottomPageNo()
	{
		return getTotalPages();
	}

	public int getPageNo()
	{
		return pageNo;
	}

	public void setPageNo(int pageNo)
	{
		this.pageNo = pageNo;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public int getTotalRecord()
	{
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord)
	{
		this.totalRecord = totalRecord;
	}

	public List getDataList()
	{
		return dataList;
	}

	public void setDataList(List dataList)
	{
		this.dataList = dataList;
	}
}

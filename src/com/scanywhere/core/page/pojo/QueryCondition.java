package com.scanywhere.core.page.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.page.xml.Table;

public class QueryCondition
{
	// 表ID
	private String tableId;
		
	// 分页数据定义
	private Table table;
	
	// 分页信息
	private Page page;
	
	// 查询条件
	private List<Map<String, Object>> conditions;


	public String getTableId()
	{
		return tableId;
	}

	public void setTableId(String tableId)
	{
		this.tableId = tableId;
	}

	public Table getTable()
	{
		return table;
	}

	public void setTable(Table table)
	{
		this.table = table;
	}

	public Page getPage()
	{
		return page;
	}

	public void setPage(Page page)
	{
		this.page = page;
	}

	public List<Map<String, Object>> getConditions()
	{
		return conditions;
	}

	public void setConditions(List<Map<String, Object>> conditions)
	{
		this.conditions = conditions;
	}
	
	public String getConditionParam(String paramName)
	{
		if (CollectionUtils.isNotEmpty(conditions))
		{
			for (Map<String, Object> condition : conditions)
			{
				String key = condition.get("key").toString();
				if (key.equals(paramName))
				{
					return condition.get("value") != null ? condition.get("value").toString() : null;
				}
			}
		}
		return null;
	}

	public Map<String, Object> getConditionMap()
	{
		Map<String, Object> conditionValues = new HashMap<String, Object>();
		if (CollectionUtils.isNotEmpty(conditions))
		{
			for (Map<String, Object> condition : conditions)
			{
				String key = condition.get("key").toString();
				String value = condition.get("value") != null ? condition.get("value").toString() : null;
				
				if (!conditionValues.containsKey(key))
				{
					conditionValues.put(key, value);
				}
			}
		}
		return conditionValues;
	}
	
	public Map<String, Object> getEqualConditionMap()
	{
		Map<String, Object> conditionValues = new HashMap<String, Object>();
		if (CollectionUtils.isNotEmpty(conditions))
		{
			for (Map<String, Object> condition : conditions)
			{
				String key = condition.get("key").toString();
				String value = condition.get("value") != null ? condition.get("value").toString() : null;
				String operator = condition.get("operator") != null ? condition.get("operator").toString() : null;
				
				if (StringUtils.isEmpty(operator) || !operator.equals("equal")) 
				{
					continue;
				}
				
				if (StringUtils.isEmpty(value))
				{
					continue;
				}
				
				if (!conditionValues.containsKey(key))
				{
					conditionValues.put(key, value);
				}
			}
		}
		return conditionValues;
	}
	
	public Map<String, String> getLikeConditionMap()
	{
		Map<String, String> conditionValues = new HashMap<String, String>();
		if (CollectionUtils.isNotEmpty(conditions))
		{
			for (Map<String, Object> condition : conditions)
			{
				String key = condition.get("key").toString();
				String value = condition.get("value") != null ? condition.get("value").toString() : null;
				String operator = condition.get("operator") != null ? condition.get("operator").toString() : null;
				
				if (StringUtils.isEmpty(operator) || !operator.equals("like")) 
				{
					continue;
				}
				
				if (StringUtils.isEmpty(value))
				{
					continue;
				}
				
				if (!conditionValues.containsKey(key))
				{
					conditionValues.put(key, value);
				}
			}
		}
		return conditionValues;
	}
	
	public Map<String, String> getTimeConditionMap()
	{
		Map<String, String> conditionValues = new HashMap<String, String>();
		if (CollectionUtils.isNotEmpty(conditions))
		{
			for (Map<String, Object> condition : conditions)
			{
				String key = condition.get("key").toString();
				String value = condition.get("value") != null ? condition.get("value").toString() : null;
				String operator = condition.get("operator") != null ? condition.get("operator").toString() : null;
				
				if (StringUtils.isEmpty(operator) || !operator.equals("time")) 
				{
					continue;
				}
				
				if (StringUtils.isEmpty(value))
				{
					continue;
				}
				
				if (!conditionValues.containsKey(key))
				{
					conditionValues.put(key, value);
				}
			}
		}
		return conditionValues;
	}
}
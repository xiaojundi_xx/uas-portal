package com.scanywhere.core.cache;

public interface IProjectCache
{
	/**
	 * 查询要放入缓存的对象
	 * 
	 * @return
	 */
	public void init();
}

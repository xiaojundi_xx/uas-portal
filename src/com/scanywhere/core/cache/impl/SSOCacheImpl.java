package com.scanywhere.core.cache.impl;

import java.util.Iterator;
import java.util.Set;

import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.reflect.ClassUtils;
import com.scanywhere.core.cache.IProjectCache;
import com.scanywhere.sso.handler.SSOHandler;

public class SSOCacheImpl implements IProjectCache
{
	public void init()
	{
		Set<Class<?>> caches = ClassUtils.scanPackageBySuper("com.scanywhere.sso.handler.support", SSOHandler.class);
		Iterator<Class<?>> it = caches.iterator();
		while (it.hasNext()) 
		{
			Class<?> clazz = it.next();
			SSOHandler handler = (SSOHandler) ClassUtils.newInstance(clazz);
			handler.init();
			
			CacheUtils.put(ProjectConstants.SSO_CACHE, handler.getSSOType(), handler);
		}
	}
}
package com.scanywhere.core.cache.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.scanywhere.authentication.handler.AuthenticationHandler;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.reflect.ClassUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.configuration.ConfigurationFactory;
import com.scanywhere.core.cache.IProjectCache;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.authentication.entity.AuthConfig;
import com.scanywhere.modules.authentication.service.AuthConfigService;
import com.scanywhere.modules.authentication.service.AuthParameterService;

public class AuthenticationCacheImpl implements IProjectCache
{
	public void init()
	{
		AuthConfigService authConfigService = (AuthConfigService) SpringContextUtils.getBean("authConfigService");
		AuthParameterService authParameterService = (AuthParameterService) SpringContextUtils.getBean("authParameterService");
		
		// 查询认证方式
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("acStatus", CommonConstants.STATUS_ENABLED);
		List<AuthConfig> authList = authConfigService.find(params, "acSort");
		if (CollectionUtils.isNotEmpty(authList))
		{
			for (AuthConfig authConfig : authList)
			{
				// 排除停用的认证方式
				if (authConfig.getAcStatus() == CommonConstants.STATUS_DISABLED)
				{
					continue;
				}
				
				// 获取认证方式实现类
				String clazz = ConfigurationFactory.getSystemConfiguration().getString("authentication." + authConfig.getAcCode());
				if (StringUtils.isEmpty(clazz))
				{
					continue;
				}
				
				// 认证方式参数配置
				Map<String, Object> authParameter = authParameterService.findAuthParameter(authConfig.getId());
				authParameter.put("id", authConfig.getId());
				authParameter.put("powerCode", authConfig.getAcPowerCode());
				authParameter.put("updateDateTime", authConfig.getUpdateDateTime());
				
				// 实例化对象
				AuthenticationHandler handler = ClassUtils.newInstance(clazz);
				handler.init();
				handler.setAuthParameter(authParameter);
				
				CacheUtils.put(ProjectConstants.AUTHENTICATION_CACHE, authConfig.getAcCode(), handler);
			}
			
			CacheUtils.put(ProjectConstants.AUTHENTICATION_CACHE, "authList", authList);
		}
	}
}

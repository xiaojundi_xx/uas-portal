package com.scanywhere.core.constant;

public class LogConstant
{
	/** 成功 */
	public final static int SUCCESS = 0;
	
	/** 失败 */
	public final static int FAILURE = 1;
	
	
	/** 用户行为日志-行为类型-密码登录 */
	public final static int ACTION_TYPE_LOGIN_LOGOUT = 1;
	
	/** 用户行为日志-行为类型-新增数据 */
	public final static int ACTION_TYPE_INSERT_DATA = 2;
	
	/** 用户行为日志-行为类型-删除数据 */
	public final static int ACTION_TYPE_DELETE_DATA = 3;
	
	/** 用户行为日志-行为类型-修改数据 */
	public final static int ACTION_TYPE_UPDATE_DATA = 4;
	
	/** 用户行为日志-行为类型-修改密码*/
	public final static int ACTION_TYPE_UPDATE_PASSWORD = 5;
	
	/** 用户行为日志-行为类型-证书登录 */
	public final static int ACTION_TYPE_LOGIN_CERT = 6;
	
}

package com.scanywhere.core.constant;

public class CommonConstants
{
	/** 管理员登录信息Session名 */
	public static final String LOGIN_INFO = "_login_info";

	/** 登录验证码Session名 */
	public static final String LOGIN_CAPTCHA = "_login_captcha";

	
	/** Long型0 : [ <b>0L</b> ] */
	public static final long LONG_VALUE_ZERO = 0L;

	/** Integer型0 : [ <b>0</b> ] */
	public static final int INT_VALUE_ZERO = 0;

	/** Short型0 : [ <b>0</b> ] */
	public static final short SHORT_VALUE_ZERO = 0;

	
	/** 简单状态标识 -- 是/正常/有效/有 : [ <b>0</b> ] */
	public static final int STATUS_0 = 0;

	/** 简单状态标识 -- 否/异常/无效/无 : [ <b>1</b> ] */
	public static final int STATUS_1 = 1;

	
	/** 状态标识 -- 正常 : [ <b>0</b> ] */
	public static final int STATUS_ENABLED = 0;

	/** 状态标识 -- 停用 : [ <b>1</b> ] */
	public static final int STATUS_DISABLED = 1;
	
	/** 状态标识 -- 锁定 : [ <b>2</b> ] */
	public static final int STATUS_LOCKED = 2;
	
	/** 状态标识 -- 过期 : [ <b>3</b> ] */
	public static final int STATUS_EXPIRED = 3;
	
	
	/** 分页返回页面数据 Table */
	public static final String MAP_KEY_TABLE = "table";
	
	/** 分页返回页面数据 Page */
	public static final String MAP_KEY_PAGE = "page";
}

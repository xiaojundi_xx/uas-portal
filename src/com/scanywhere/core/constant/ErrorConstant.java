package com.scanywhere.core.constant;

public class ErrorConstant
{
	/**
	 * [CSRF_TOKEN_ERRPR] 无效的Token或Token已过期!
	 */
	public final static String CSRF_TOKEN_ERROR = "CSRF_TOKEN_ERRPR";
	
	public final static String ERROR_DETAIL = "如果不能解决问题, 请联系管理员!";
}

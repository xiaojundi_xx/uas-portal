package com.scanywhere.core.exception;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.web.http.ServletUtils;
import com.scanywhere.core.constant.ErrorConstant;

/**
 * 异常处理
 */
public class CustomExceptionResolver implements HandlerExceptionResolver
{
	public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object o, Exception ex)
	{
		ex.printStackTrace();
		
		String errorName = "出现错误";
		String message = ex.getMessage().toString();
		
		ModelAndView model = new ModelAndView();
		if (ex instanceof DataOperationException)
		{
			errorName = "数据操作出现错误!";
		}
		else if (ex instanceof PageQueryException)
		{
			errorName = "分页查询出现错误!";
		}
		else if (ex instanceof ToolsException)
		{
			errorName = "工具类出现错误!";
		}
		/*
		else
		{
			errorName = ex.getClass().getSimpleName();
			message = StringUtils.isNotEmpty(ex.getMessage()) ? ex.getMessage().toString() : "";
		}
		*/
		// AJAX调用返回
		if (ServletUtils.isAjaxRequest(httpServletRequest))
		{
			FastJsonJsonView view = new FastJsonJsonView();
	        Map<String, Object> attributes = new HashMap<String, Object>();
	        attributes.put("code", "1");
	        attributes.put("message", "服务端异常, 请联系管理员!");
	        view.setAttributesMap(attributes);
	        model.setView(view);
	        return model;
		}
		
		model.addObject("errorName", errorName);
		model.addObject("message", message);
		model.addObject("detail", ErrorConstant.ERROR_DETAIL);
		model.setViewName("error/info.jsp");
		return model;
	}
}

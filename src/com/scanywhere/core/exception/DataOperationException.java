package com.scanywhere.core.exception;

@SuppressWarnings("serial")
public class DataOperationException extends Exception
{
	public DataOperationException(String message)
	{
		super(message);
	}
}

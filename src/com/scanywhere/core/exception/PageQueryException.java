package com.scanywhere.core.exception;

@SuppressWarnings("serial")
public class PageQueryException extends Exception
{
	public PageQueryException(String message)
	{
		super(message);
	}
}

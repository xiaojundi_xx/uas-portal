package com.scanywhere.core.exception;

import com.scanywhere.common.lang.StringUtils;

@SuppressWarnings("serial")
public class ToolsException extends RuntimeException
{
	public ToolsException(Throwable e)
	{
		super(e.getMessage(), e);
	}

	public ToolsException(String message)
	{
		super(message);
	}

	public ToolsException(String messageTemplate, Object... params)
	{
		super(StringUtils.format(messageTemplate, params));
	}

	public ToolsException(String message, Throwable throwable)
	{
		super(message, throwable);
	}

	public ToolsException(Throwable throwable, String messageTemplate, Object... params)
	{
		super(StringUtils.format(messageTemplate, params), throwable);
	}
}

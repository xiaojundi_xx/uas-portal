package com.scanywhere.core.base.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.reflect.GenericsUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.common.utils.PageUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.entity.BaseEntity;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.page.pojo.Page;

@Repository("baseDao")
@SuppressWarnings({ "hiding", "rawtypes", "unchecked" })
@Transactional
public abstract class BaseDaoImpl<T extends BaseEntity> implements BaseDao<T>
{
	private Class<T> entityClass;

	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;

	public BaseDaoImpl()
	{
		this.entityClass = GenericsUtils.getSuperClassGenricType(this.getClass());
	}

	public Session getSession()
	{
		return this.sessionFactory.getCurrentSession();
	}

	public <T> Serializable save(T entity)
	{
		return this.getSession().save(entity);
	}

	public <T> void delete(T entity)
	{
		this.getSession().delete(entity);
	}

	public <T> void update(T entity)
	{
		this.getSession().update(entity);
	}

	public <T> void saveOrUpdate(T entity)
	{
		this.getSession().saveOrUpdate(entity);
	}

	public <T> void saveBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		Session session = this.getSession();
		for (int i = 0; i < entityList.size(); i++)
		{
			T entity = entityList.get(i);
			session.save(entity);

			if (i % 20 == 0)
			{
				session.flush();
				session.clear();
			}
		}
	}

	public <T> void deleteBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		Session session = this.getSession();
		for (int i = 0; i < entityList.size(); i++)
		{
			T entity = entityList.get(i);
			session.delete(entity);

			if (i % 20 == 0)
			{
				session.flush();
				session.clear();
			}
		}
	}

	public <T> void updateBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		Session session = this.getSession();
		for (int i = 0; i < entityList.size(); i++)
		{
			T entity = entityList.get(i);
			session.update(entity);

			if (i % 20 == 0)
			{
				session.flush();
				session.clear();
			}
		}
	}

	public <T> void saveOrUpdateBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		Session session = this.getSession();
		for (int i = 0; i < entityList.size(); i++)
		{
			T entity = entityList.get(i);
			session.saveOrUpdate(entity);

			if (i % 20 == 0)
			{
				session.flush();
				session.clear();
			}
		}
	}

	public <T> T getById(Serializable id)
	{
		if (id == null)
		{
			return null;
		}

		return (T) this.getSession().get(this.entityClass, id);
	}

	public <T> T getByHql(String hql)
	{
		if (StringUtils.isEmpty(hql))
		{
			return null;
		}

		Query query = this.getSession().createQuery(hql);
		query.setMaxResults(1);
		return (T) query.uniqueResult();
	}

	public <T> T getByHql(String hql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(hql))
		{
			return null;
		}

		Query query = this.getSession().createQuery(hql);
		query = this.getQueryByMap(query, params);
		query.setMaxResults(1);
		return (T) query.uniqueResult();
	}
	
	public <T> T getBySql(String sql)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.addEntity(this.entityClass);
		sqlQuery.setMaxResults(1);
		return (T) sqlQuery.uniqueResult();
	}

	public <T> T getBySql(String sql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		sqlQuery.addEntity(this.entityClass);
		sqlQuery.setMaxResults(1);
		return (T) sqlQuery.uniqueResult();
	}
	
	public <T> T getBySql(Class<T> clazz, String sql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}
		
		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		sqlQuery.addEntity(clazz);
		sqlQuery.setMaxResults(1);
		return (T) sqlQuery.uniqueResult();
	}

	public <T> List<T> find()
	{
		Criteria criteria = this.getSession().createCriteria(this.entityClass);
		return criteria.list();
	}

	public <T> List<T> find(DetachedCriteria criteria)
	{
		return criteria.getExecutableCriteria(getSession()).setProjection(null)
				.setResultTransformer(CriteriaSpecification.ROOT_ENTITY).list();
	}

	public <T> List<T> find(DetachedCriteria criteria, int pageNo, int pageSize)
	{
		return criteria.getExecutableCriteria(getSession()).setProjection(null)
                .setResultTransformer(CriteriaSpecification.ROOT_ENTITY).setFirstResult(pageNo).setMaxResults(pageSize).list();
	}

	public <T> List<T> findByHql(String hql)
	{
		if (StringUtils.isEmpty(hql))
		{
			return null;
		}

		Query query = this.getSession().createQuery(hql);
		return query.list();
	}

	public <T> List<T> findByHql(String hql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(hql))
		{
			return null;
		}

		Query query = this.getSession().createQuery(hql);
		query = this.getQueryByMap(query, params);
		return query.list();
	}

	public <T> List<T> findByHql(String hql, Map<String, Object> params, int pageNo, int pageSize)
	{
		if (StringUtils.isEmpty(hql))
		{
			return null;
		}

		Query query = this.getSession().createQuery(hql);
		query = this.getQueryByMap(query, params);
		query.setFirstResult(PageUtils.countStartIndex(pageNo, pageSize));
		query.setMaxResults(pageSize);
		return query.list();
	}

	public <T> List<T> findBySql(String sql)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.addEntity(this.entityClass);
		return sqlQuery.list();
	}

	public <T> List<T> findBySql(String sql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		sqlQuery.addEntity(this.entityClass);
		return sqlQuery.list();
	}
	
	public <T> List<T> findBySql(Class<T> clazz, String sql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		if (clazz != null)
		{
			sqlQuery.addEntity(clazz);
		}
		return sqlQuery.list();
	}

	public <T> List<T> findBySql(String sql, Map<String, Object> params, int pageNo, int pageSize)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		sqlQuery.addEntity(this.entityClass);
		sqlQuery.setFirstResult(PageUtils.countStartIndex(pageNo, pageSize));
		sqlQuery.setMaxResults(pageSize);
		return sqlQuery.list();
	}

	public List<Map<String, Object>> findMapBySql(String sql)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return sqlQuery.list();
	}

	public List<Map<String, Object>> findMapBySql(String sql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return sqlQuery.list();
	}

	public List<Map<String, Object>> findMapBySql(String sql, int pageNo, int pageSize)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.setFirstResult(PageUtils.countStartIndex(pageNo, pageSize));
		sqlQuery.setMaxResults(pageSize);
		sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return sqlQuery.list();
	}

	public List<Map<String, Object>> findMapBySql(String sql, Map<String, Object> params, int pageNo, int pageSize)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = getSqlQueryByMap(sqlQuery, params);
		sqlQuery.setFirstResult(PageUtils.countStartIndex(pageNo, pageSize));
		sqlQuery.setMaxResults(pageSize);
		sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return sqlQuery.list();
	}

	public <T> List<T> findMapBySql(String sql, Object[] params, Type[] types)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.setParameters(params, types);
		sqlQuery.setResultTransformer(Transformers.aliasToBean(this.entityClass));
		return sqlQuery.list();
	}

	public <T> List<T> findMapBySql(String sql, Object[] params, Type[] types, int pageNo, int pageSize)
	{
		if (StringUtils.isEmpty(sql))
		{
			return null;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.setParameters(params, types);
		sqlQuery.setFirstResult(PageUtils.countStartIndex(pageNo, pageSize));
		sqlQuery.setMaxResults(pageSize);
		sqlQuery.setResultTransformer(Transformers.aliasToBean(this.entityClass));
		return sqlQuery.list();
	}

	public Long findCountByHql(String hql)
	{
		if (StringUtils.isEmpty(hql))
		{
			return CommonConstants.LONG_VALUE_ZERO;
		}

		Query query = this.getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}

	public Long findCountByHql(String hql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(hql))
		{
			return CommonConstants.LONG_VALUE_ZERO;
		}

		Query query = this.getSession().createQuery(hql);
		query = this.getQueryByMap(query, params);
		return (Long) query.uniqueResult();
	}

	public Long findCountBySql(String sql)
	{
		if (StringUtils.isEmpty(sql))
		{
			return CommonConstants.LONG_VALUE_ZERO;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		return (Long) sqlQuery.uniqueResult();
	}

	public Long findCountBySql(String sql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(sql))
		{
			return CommonConstants.LONG_VALUE_ZERO;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		return (Long) sqlQuery.uniqueResult();
	}

	public Long findCountBySql(String sql, Object[] params, Type[] types)
	{
		if (StringUtils.isEmpty(sql))
		{
			return CommonConstants.LONG_VALUE_ZERO;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.setParameters(params, types);
		return (Long) sqlQuery.uniqueResult();
	}

	public int executeByHql(String hql)
	{
		if (StringUtils.isEmpty(hql))
		{
			return CommonConstants.INT_VALUE_ZERO;
		}

		Query query = this.getSession().createQuery(hql);
		return query.executeUpdate();
	}

	public int executeByHql(String hql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(hql) || (params == null || params.isEmpty()))
		{
			return CommonConstants.INT_VALUE_ZERO;
		}

		Query query = this.getSession().createQuery(hql);
		query = this.getQueryByMap(query, params);
		return query.executeUpdate();
	}

	public int executeBySql(String sql)
	{
		if (StringUtils.isEmpty(sql))
		{
			return CommonConstants.INT_VALUE_ZERO;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		return sqlQuery.executeUpdate();
	}

	public int executeBySql(String sql, Map<String, Object> params)
	{
		if (StringUtils.isEmpty(sql) || (params == null || params.isEmpty()))
		{
			return CommonConstants.INT_VALUE_ZERO;
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery = this.getSqlQueryByMap(sqlQuery, params);
		return sqlQuery.executeUpdate();
	}

	public Query getQueryByMap(Query query, Map<String, Object> params)
	{
		if (CollectionUtils.isNotEmpty(params))
		{
			for (String key : params.keySet())
			{
				Object obj = params.get(key);
				if (obj instanceof Collection<?>)
				{
					query.setParameterList(key, (Collection<?>) obj);
				}
				else if (obj instanceof Object[])
				{
					query.setParameterList(key, (Object[]) obj);
				}
				else
				{
					query.setParameter(key, obj);
				}
			}
		}
		return query;
	}

	public SQLQuery getSqlQueryByMap(SQLQuery sqlQuery, Map<String, Object> params)
	{
		if (CollectionUtils.isNotEmpty(params))
		{
			for (String key : params.keySet())
			{
				Object obj = params.get(key);
				if (obj instanceof Collection<?>)
				{
					sqlQuery.setParameterList(key, (Collection<?>) obj);
				}
				else if (obj instanceof Object[])
				{
					sqlQuery.setParameterList(key, (Object[]) obj);
				}
				else
				{
					sqlQuery.setParameter(key, obj);
				}
			}
		}
		return sqlQuery;
	}

	public <T> List<T> find(Map<String, Object> params)
	{
		return this.find(params, null, null, null, null);
	}

	public <T> List<T> find(Map<String, Object> params, String orderBy)
	{
		return this.find(params, null, null, null, orderBy, null, null);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, String orderBy)
	{
		return this.find(params, likeParams, null, null, orderBy, null, null);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy)
	{
		return this.find(params, likeParams, timeParams, null, orderBy, null, null);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy)
	{
		return this.find(params, likeParams, timeParams, fields, orderBy, null, null);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.find(params, likeParams, timeParams, fields, orderBy, null, null, true);
	}

	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize, boolean ascend)
	{
		// HQL语句
		String hql = "FROM " + this.entityClass.getSimpleName() + " AS entity ";

		// 查询参数不为空追加WHERE语句
		if (CollectionUtils.isNotEmpty(params) || CollectionUtils.isNotEmpty(likeParams) || CollectionUtils.isNotEmpty(timeParams))
		{
			hql = hql + "WHERE 1=1 ";
		}
		
		// 查询条件值
		List<Object> valueList = new ArrayList<Object>();
				
		// 查询条件
		if (CollectionUtils.isNotEmpty(params))
		{
			for (Map.Entry<String, Object> item : params.entrySet())
			{
				String key = item.getKey();
				Object obj = item.getValue();
				if (obj instanceof Object[])
				{
					Object[] values = (Object[]) obj;
					String[] condition = new String[values.length];
					for (int i = 0; i < condition.length; i++)
					{
						condition[i] = "?";
						valueList.add(values[i]);
					}
					hql = hql + "AND entity." + key + " IN (" + org.apache.commons.lang.StringUtils.join(condition, ",")
							+ ") ";
				}
				else
				{
					hql = hql + "AND entity." + key + " = ? ";
					valueList.add(obj);
				}
			}
		}
		
		// 模糊查询条件
		if (CollectionUtils.isNotEmpty(likeParams))
		{
			for (Map.Entry<String, String> item : likeParams.entrySet())
			{
				String key = item.getKey();
				String value = item.getValue();
				hql = hql + "AND entity." + key + " LIKE ? ";
				valueList.add("%" + value + "%");
			}
		}
		
		// 时间查询条件
		if (CollectionUtils.isNotEmpty(timeParams))
	    {
			for (Map.Entry<String, String> item : timeParams.entrySet())
			{
				String key = item.getKey();
				String value = item.getValue();
				
				if (StringUtils.isNotEmpty(key) && key.startsWith("begin_") && StringUtils.isNotEmpty(value))
				{
					hql = hql + "AND entity." + key.substring("begin_".length(), key.length()) + " >= ? ";
					if(!DateUtils.isValidDate(value)){
						value = value + " 00:00:00";
					}
					valueList.add(DateUtils.parseTime(value));
				}
				else if (StringUtils.isNotEmpty(key) && key.startsWith("end_") && StringUtils.isNotEmpty(value))
				{
					hql = hql + "AND entity." + key.substring("end_".length(), key.length()) + " <= ? ";
					if(!DateUtils.isValidDate(value)){
						value = value + " 23:59:59";
					}
					valueList.add(DateUtils.parseTime(value));
				}
			}
	    }

		// 返回字段
		if (fields != null && fields.length != 0)
		{
			String selectHql = "SELECT ";
			for (int i = 0; i < fields.length; i++)
			{
				selectHql = selectHql + fields[i] + ", ";
			}

			selectHql = selectHql.substring(0, selectHql.length() - 1);
			hql = selectHql + " " + hql;
		}

		// 排序
		if (StringUtils.isNotEmpty(orderBy))
		{
			hql = hql + "ORDER BY entity." + orderBy;
		}
		
		if (!ascend)
		{
			hql = hql + " desc";
		}

		Query query = this.getSession().createQuery(hql);
		for (int i = 0; i < valueList.size(); i++)
		{
			query.setParameter(i, valueList.get(i));
		}
		
		List dataList = null;
		if (pageNo != null && pageSize != null)
		{
			// 计算当前页开始记录
			int startIndex = PageUtils.countStartIndex(pageNo, pageSize);
			
			// 查询分页数据
			dataList = query.setFirstResult(startIndex).setMaxResults(pageSize).list();
		}
		else
		{
			dataList = query.list();
		}
		return dataList;
	}

	public <T> Long findCount()
	{
		String hql = "SELECT COUNT(*) FROM " + this.entityClass.getSimpleName() + " AS entity ";
		Query query = this.getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}

	public <T> Long findCount(Map<String, Object> params)
	{
		return this.findCount(params, null, null);
	}
	
	public <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams)
	{
		return this.findCount(params, likeParams, null);
	}
	
	public <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams)
	{
		// HQL语句
		String hql = "SELECT COUNT(*) FROM " + this.entityClass.getSimpleName() + " AS entity ";
		
		// 查询参数不为空追加WHERE语句
		if (CollectionUtils.isNotEmpty(params) || CollectionUtils.isNotEmpty(likeParams) || CollectionUtils.isNotEmpty(timeParams))
		{
			hql = hql + "WHERE 1=1 ";
		}
		
		// 查询条件值
		List<Object> valueList = new ArrayList<Object>();
		
		// 查询条件
		if (CollectionUtils.isNotEmpty(params))
		{
			for (Map.Entry<String, Object> item : params.entrySet())
			{
				String key = item.getKey();
				Object obj = item.getValue();
				if (obj instanceof Object[])
				{
					Object[] values = (Object[]) obj;
					String[] condition = new String[values.length];
					for (int i = 0; i < condition.length; i++)
					{
						condition[i] = "?";
						valueList.add(values[i]);
					}
					hql = hql + "AND entity." + key + " IN (" + org.apache.commons.lang.StringUtils.join(condition, ",")
							+ ") ";
				}
				else
				{
					hql = hql + "AND entity." + key + " = ? ";
					valueList.add(obj);
				}
			}
		}
		
		// 模糊查询条件
		if (CollectionUtils.isNotEmpty(likeParams))
		{
			for (Map.Entry<String, String> item : likeParams.entrySet())
			{
				String key = item.getKey();
				Object obj = item.getValue();
				hql = hql + "AND entity." + key + " LIKE ? ";
				valueList.add("%" + obj + "%");
			}
		}
		
		// 时间查询条件
		if (CollectionUtils.isNotEmpty(timeParams))
	    {
			for (Map.Entry<String, String> item : timeParams.entrySet())
			{
				String key = item.getKey();
				String value = item.getValue();
				
				if (StringUtils.isNotEmpty(key) && key.startsWith("begin_") && StringUtils.isNotEmpty(value))
				{
					hql = hql + "AND entity." + key.substring("begin_".length(), key.length()) + " >= ? ";
					if(!DateUtils.isValidDate(value)){
						value = value + " 00:00:00";
					}
					valueList.add(DateUtils.parseTime(value));
				}
				else if (StringUtils.isNotEmpty(key) && key.startsWith("end_") && StringUtils.isNotEmpty(value))
				{
					hql = hql + "AND entity." + key.substring("end_".length(), key.length()) + " <= ? ";
					if(!DateUtils.isValidDate(value)){
						value = value + " 23:59:59";
					}
					valueList.add(DateUtils.parseTime(value));
				}
			}
	    }

		Query query = this.getSession().createQuery(hql);
		for (int i = 0; i < valueList.size(); i++)
		{
			query.setParameter(i, valueList.get(i));
		}

		return (Long) query.uniqueResult();
	}

	public Long findCount(DetachedCriteria criteria)
	{
		return (Long) criteria.getExecutableCriteria(getSession()).setProjection(Projections.rowCount()).uniqueResult();
	}
	
	public Page queryPage(Map<String, Object> params, Integer pageNo, Integer pageSize)
	{
		return this.queryPage(params, null, null, null, null, pageNo, pageSize);
	}

	public Page queryPage(Map<String, Object> params, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.queryPage(params, null, null, null, orderBy, pageNo, pageSize);
	}
	
	public Page queryPage(Map<String, Object> params, Map<String, String> likeParams, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.queryPage(params, likeParams, null, null, orderBy, pageNo, pageSize);
	}
	
	public Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.queryPage(params, likeParams, timeParams, null, orderBy, pageNo, pageSize);
	}

	public Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize)
	{
		// 查询记录数
		Long totalRecord = this.findCount(params, likeParams, timeParams);
		if (totalRecord.longValue() < 1L)
		{
			return new Page();
		}

		// HQL语句
		String hql = "FROM " + this.entityClass.getSimpleName() + " AS entity ";

		// 查询参数不为空追加WHERE语句
		if (CollectionUtils.isNotEmpty(params) || CollectionUtils.isNotEmpty(likeParams) || CollectionUtils.isNotEmpty(timeParams))
		{
			hql = hql + "WHERE 1=1 ";
		}
		
		// 查询条件值
		List<Object> valueList = new ArrayList<Object>();
		
		// 查询条件
		if (CollectionUtils.isNotEmpty(params))
		{
			for (Map.Entry<String, Object> item : params.entrySet())
			{
				String key = item.getKey();
				Object obj = item.getValue();
				if (obj instanceof Object[])
				{
					Object[] values = (Object[]) obj;
					String[] condition = new String[values.length];
					for (int i = 0; i < condition.length; i++)
					{
						condition[i] = "?";
						valueList.add(values[i]);
					}
					hql = hql + "AND entity." + key + " IN (" + org.apache.commons.lang.StringUtils.join(condition, ",")
							+ ") ";
				}
				else
				{
					hql = hql + "AND entity." + key + " = ? ";
					valueList.add(obj);
				}
			}
		}
		
		// 模糊查询条件
		if (CollectionUtils.isNotEmpty(likeParams))
		{
			for (Map.Entry<String, String> item : likeParams.entrySet())
			{
				String key = item.getKey();
				Object obj = item.getValue();
				hql = hql + "AND entity." + key + " LIKE ? ";
				valueList.add("%" + obj + "%");
			}
		}

		// 时间查询条件
		if (CollectionUtils.isNotEmpty(timeParams))
	    {
			for (Map.Entry<String, String> item : timeParams.entrySet())
			{
				String key = item.getKey();
				String value = item.getValue();
				
				if (StringUtils.isNotEmpty(key) && key.startsWith("begin_") && StringUtils.isNotEmpty(value))
				{
					hql = hql + "AND entity." + key.substring("begin_".length(), key.length()) + " >= ? ";
					valueList.add(DateUtils.parseTime(value));
				}
				else if (StringUtils.isNotEmpty(key) && key.startsWith("end_") && StringUtils.isNotEmpty(value))
				{
					hql = hql + "AND entity." + key.substring("end_".length(), key.length()) + " <= ? ";
					valueList.add(DateUtils.parseTime(value));
				}
			}
	    }
				
		// 返回字段
		if (fields != null && fields.length != 0)
		{
			String selectHql = "SELECT ";
			for (int i = 0; i < fields.length; i++)
			{
				selectHql = selectHql + fields[i] + ", ";
			}

			selectHql = selectHql.substring(0, selectHql.length() - 1);
			hql = selectHql + " " + hql;
		}

		// 排序
		if (StringUtils.isNotEmpty(orderBy))
		{
			hql = hql + "ORDER BY entity." + orderBy;
		}

		Query query = this.getSession().createQuery(hql);
		for (int i = 0; i < valueList.size(); i++)
		{
			query.setParameter(i, valueList.get(i));
		}

		List dataList = null;
		if (pageNo != null && pageSize != null)
		{
			// 计算当前页开始记录
			int startIndex = PageUtils.countStartIndex(pageNo, pageSize);
					
			// 查询分页数据
			dataList = query.setFirstResult(startIndex).setMaxResults(pageSize).list();
		}
		else
		{
			dataList = query.list();
		}
		return new Page(totalRecord.intValue(), pageSize, dataList);
	}

	public <T> void deleteById(Serializable id)
	{
		if (id == null)
		{
			return;
		}

		this.getSession().delete(this.getById(id));
	}

	public <T> void deleteBatch(Serializable[] ids)
	{
		if (ids == null || ids.length == 0)
		{
			return;
		}

		for (int i = 0; i < ids.length; i++)
		{
			Serializable id = ids[i];
			this.delete(this.getById(id));
		}
	}
	
	public <T> void delete(Map<String, Object> params)
	{
		if (CollectionUtils.isEmpty(params))
		{
			return;
		}

		// HQL语句
		String hql = "DELETE FROM " + this.entityClass.getSimpleName() + " AS entity WHERE 1=1 ";
		
		// 查询条件
		List<Object> valueList = new ArrayList<Object>();
		for (Map.Entry<String, Object> item : params.entrySet())
		{
			String key = item.getKey();
			Object obj = item.getValue();
			if (obj instanceof Object[])
			{
				Object[] values = (Object[]) obj;
				String[] condition = new String[values.length];
				for (int i = 0; i < condition.length; i++)
				{
					condition[i] = "?";
					valueList.add(values[i]);
				}
				hql = hql + "AND entity." + key + " IN (" + org.apache.commons.lang.StringUtils.join(condition, ",")
						+ ") ";
			}
			else
			{
				hql = hql + "AND entity." + key + " = ? ";
				valueList.add(obj);
			}
		}
		
		Query query = this.getSession().createQuery(hql);
		for (int i = 0; i < valueList.size(); i++)
		{
			query.setParameter(i, valueList.get(i));
		}
		query.executeUpdate();
	}

	public boolean isUnique(String name, String value, String condition)
	{
		Criteria criteria = getSession().createCriteria(this.entityClass);
		criteria.add(Restrictions.eq(name, value));
		if (StringUtils.isNotEmpty(condition))
		{
			condition = condition.replaceAll("`", "'");
			criteria.add(Restrictions.sqlRestriction(condition));
		}
		return criteria.list().isEmpty();
	}
}

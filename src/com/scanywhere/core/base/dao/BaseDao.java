package com.scanywhere.core.base.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.type.Type;

import com.scanywhere.core.base.entity.BaseEntity;
import com.scanywhere.core.page.pojo.Page;

@SuppressWarnings("hiding")
public abstract interface BaseDao<T extends BaseEntity>
{
	/**
	 * 提供CurrentSession使用
	 * 
	 * @return
	 */
	public Session getSession();

	/**
	 * 保存对象
	 *
	 * @param entity 保存的对象
	 * @return 唯一主键
	 */
	public abstract <T> Serializable save(T entity);

	/**
	 * 删除对象
	 *
	 * @param entity 删除的对象
	 */
	public abstract <T> void delete(T entity);

	/**
	 * 修改对象
	 *
	 * @param entity 修改的对象
	 */
	public abstract <T> void update(T entity);

	/**
	 * 保存或修改
	 *
	 * @param entity 保存或修改的对象
	 */
	public abstract <T> void saveOrUpdate(T entity);

	/**
	 * 批量保存
	 * 
	 * @param entityList 保存的对象集合
	 */
	public abstract <T> void saveBatch(List<T> entityList);

	/**
	 * 批量删除
	 * 
	 * @param entityList 删除的对象集合
	 */
	public abstract <T> void deleteBatch(List<T> entityList);

	/**
	 * 批量更新
	 * 
	 * @param entityList 更新的对象集合
	 */
	public abstract <T> void updateBatch(List<T> entityList);

	/**
	 * 批量保存或修改
	 * 
	 * @param entityList 保存或修改的对象集合
	 */
	public abstract <T> void saveOrUpdateBatch(List<T> entityList);

	/**
	 * 根据主键获取对象
	 *
	 * @param id 主键ID
	 * @return
	 */
	public abstract <T> T getById(Serializable id);

	/**
	 * HQL语句查询单个实体对象
	 *
	 * @param hql 查询语句
	 * @return
	 */
	public abstract <T> T getByHql(String hql);

	/**
	 * HQL语句带条件查询单个实体对象
	 * 
	 * HQL语句格式示例：SELECT * FROM User WHERE loginName=:loginName AND
	 * status=:status
	 * 
	 * @param hql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract <T> T getByHql(String hql, Map<String, Object> params);

	/**
	 * SQL查询获取实体对象
	 *
	 * @param sql 查询语句
	 * @return
	 */
	public abstract <T> T getBySql(String sql);

	/**
	 * SQL带条件查询获取实体对象
	 *
	 * SQL语句格式示例：SELECT * FROM T_USER WHERE LOGIN_NAME=:loginName AND
	 * STATUS=:status
	 * 
	 * @param sql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract <T> T getBySql(String sql, Map<String, Object> params);

	/**
	 * SQL带条件查询获取实体对象
	 *
	 * SQL语句格式示例：SELECT * FROM T_USER WHERE LOGIN_NAME=:loginName AND
	 * STATUS=:status
	 * 
	 * @param clazz 类对象
	 * @param sql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract <T> T getBySql(Class<T> clazz, String sql, Map<String, Object> params);

	/**
	 * 查询此对象所有数据
	 *
	 * @return
	 */
	public abstract <T> List<T> find();

	/**
	 * Criteria查询实体结果集
	 * 
	 * @param criteria 查询条件
	 * @return
	 */
	public abstract <T> List<T> find(DetachedCriteria criteria);

	/**
	 * Criteria分页查询实体结果集
	 * 
	 * @param criteria 查询条件
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> find(DetachedCriteria criteria, int pageNo, int pageSize);

	/**
	 * HQL语句查询实体集合
	 *
	 * @param hql 查询语句
	 * @return
	 */
	public abstract <T> List<T> findByHql(String hql);

	/**
	 * HQL语句带条件查询实体集合
	 * 
	 * HQL语句格式示例：SELECT * FROM User WHERE loginName=:loginName AND
	 * status=:status
	 * 
	 * @param hql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract <T> List<T> findByHql(String hql, Map<String, Object> params);

	/**
	 * HQL语句带条件分页查询实体集合
	 *
	 * @param hql 查询语句
	 * @param params 条件参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> findByHql(String hql, Map<String, Object> params, int pageNo, int pageSize);

	/**
	 * SQL查询实体结果集
	 *
	 * @param sql 查询语句
	 * @return
	 */
	public abstract <T> List<T> findBySql(String sql);

	/**
	 * SQL带条件查询实体结果集
	 *
	 * @param sql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract <T> List<T> findBySql(String sql, Map<String, Object> params);

	/**
	 * SQL带条件查询实体结果集
	 *
	 * @param clazz 对象类
	 * @param sql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract <T> List<T> findBySql(Class<T> clazz, String sql, Map<String, Object> params);

	/**
	 * SQL带条件分页查询实体结果集
	 *
	 * @param sql 查询语句
	 * @param params 条件参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> findBySql(String sql, Map<String, Object> params, int pageNo, int pageSize);

	/**
	 * SQL查询Map结果集
	 *
	 * @param sql 查询语句
	 * @return
	 */
	public abstract List<Map<String, Object>> findMapBySql(String sql);

	/**
	 * SQL带条件查询Map结果集
	 *
	 * @param sql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract List<Map<String, Object>> findMapBySql(String sql, Map<String, Object> params);

	/**
	 * SQL分页查询Map结果集
	 *
	 * @param sql 查询语句
	 * @param pageNo 当前页号
	 * @param pageSize 行数
	 * @return
	 */
	public abstract List<Map<String, Object>> findMapBySql(String sql, int pageNo, int pageSize);

	/**
	 * SQL分页带条件查询Map结果集
	 * 
	 * @param sql 查询语句
	 * @param params 查询参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract List<Map<String, Object>> findMapBySql(String sql, Map<String, Object> params, int pageNo, int pageSize);

	/**
	 * 查询实体结果集
	 * 
	 * @param sql 查询语句
	 * @param params 查询参数
	 * @param types 查询参数类型
	 * @return
	 */
	public abstract <T> List<T> findMapBySql(String sql, Object[] params, Type[] types);

	/**
	 * 查询实体结果集
	 * 
	 * @param sql 查询语句
	 * @param params 查询参数
	 * @param types 查询参数类型
	 * @param pageNo 开始记录数
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> findMapBySql(String sql, Object[] params, Type[] types, int pageNo, int pageSize);

	/**
	 * HQL语句查询记录数
	 *
	 * @param hql 查询语句
	 * @return
	 */
	public abstract Long findCountByHql(String hql);

	/**
	 * HQL语句带条件查询记录数
	 *
	 * @param hql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract Long findCountByHql(String hql, Map<String, Object> params);

	/**
	 * SQL查询记录数
	 *
	 * @param sql 查询语句
	 * @return
	 */
	public abstract Long findCountBySql(String sql);

	/**
	 * SQL带条件查询记录数
	 *
	 * @param sql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract Long findCountBySql(String sql, Map<String, Object> params);

	/**
	 * 
	 * @param sql 查询语句
	 * @param params 查询参数
	 * @param types 查询参数类型
	 * @return
	 */
	public abstract Long findCountBySql(String sql, Object[] params, Type[] types);

	/**
	 * 执行HQL语句（可带事务）
	 *
	 * @param hql 查询语句
	 * @return
	 */
	public abstract int executeByHql(String hql);

	/**
	 * 执行HQL语句（可带事务）
	 *
	 * @param hql 查询语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract int executeByHql(String hql, Map<String, Object> params);

	/**
	 * 执行SQL语句（带事务）
	 *
	 * @param sql 执行语句
	 * @return
	 */
	public abstract int executeBySql(String sql);

	/**
	 * 执行SQL语句（带事务）
	 * 
	 * @param sql 执行语句
	 * @param params 条件参数
	 * @return
	 */
	public abstract int executeBySql(String sql, Map<String, Object> params);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, String orderBy);
	
	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, String orderBy);
	
	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy);
	
	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @param ascend 是否为升序
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize, boolean ascend);
	
	/**
	 * 查询记录数
	 * 
	 * @return
	 */
	public abstract <T> Long findCount();

	/**
	 * 查询记录数
	 * 
	 * @param params 查询参数
	 * @return
	 */
	public abstract <T> Long findCount(Map<String, Object> params);
	
	/**
	 * 查询记录数
	 * 
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @return
	 */
	public abstract <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams);
			
	/**
	 * 查询记录数
	 * 
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @return
	 */
	public abstract <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams);

	/**
	 * Criteria查询记录数
	 * 
	 * @param criteria 查询条件
	 * @return
	 */
	public abstract Long findCount(DetachedCriteria criteria);

	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Integer pageNo, Integer pageSize);

	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, String orderBy, Integer pageNo, Integer pageSize);

	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Map<String, String> likeParams, String orderBy, Integer pageNo, Integer pageSize);
	
	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize);
	
	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize);

	/**
	 * 根据主键删除对象
	 *
	 * @param id 主键ID
	 */
	public abstract <T> void deleteById(Serializable id);

	/**
	 * 批量删除对象
	 *
	 * @param ids 主键ID集合
	 */
	public abstract <T> void deleteBatch(Serializable[] ids);
	
	/**
	 * 批量删除对象
	 * 
	 * @param params
	 */
	public abstract <T> void delete(Map<String, Object> params);

	/**
	 * 判断对象属性的值在数据库中是否唯一
	 * 
	 * @param name 属性名
	 * @param value 值
	 * @param condition 查询条件
	 * @return
	 */
	public abstract boolean isUnique(String name, String value, String condition);
}

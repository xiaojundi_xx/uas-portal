package com.scanywhere.core.base.controller;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.support.BeanInjector;
import com.scanywhere.common.support.Convert;
import com.scanywhere.common.utils.CharsetUtils;
import com.scanywhere.common.utils.URLUtils;
import com.scanywhere.core.constant.ErrorConstant;
import com.scanywhere.core.web.http.wrapper.GetHttpServletRequestWrapper;

/**
 * 控制器封装类
 */
public abstract class BaseController
{
	protected Logger logger = LogManager.getLogger(this.getClass());

	@Resource
	private HttpServletRequest request;

	/**
	 * 获取request
	 */
	public HttpServletRequest getRequest()
	{
		return new GetHttpServletRequestWrapper(this.request);
	}

	/**
	 * 获取String参数
	 */
	public String getParameter(String name)
	{
		return getRequest().getParameter(name);
	}

	/**
	 * 获取String参数
	 */
	public String getParameter(String name, String defaultValue)
	{
		return Convert.toStr(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取Integer参数
	 */
	public Integer getParameterToInt(String name)
	{
		return Convert.toInt(getRequest().getParameter(name));
	}

	/**
	 * 获取Integer参数
	 */
	public Integer getParameterToInt(String name, Integer defaultValue)
	{
		return Convert.toInt(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取Long参数
	 */
	public Long getParameterToLong(String name)
	{
		return Convert.toLong(getRequest().getParameter(name));
	}

	/**
	 * 获取Long参数
	 */
	public Long getParameterToLong(String name, Long defaultValue)
	{
		return Convert.toLong(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取Float参数
	 */
	public Float getParameterToFloat(String name)
	{
		return Convert.toFloat(getRequest().getParameter(name));
	}

	/**
	 * 获取Float参数
	 */
	public Float getParameterToFloat(String name, Float defaultValue)
	{
		return Convert.toFloat(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取BigDecimal参数
	 */
	public BigDecimal getParameterToBigDecimal(String name)
	{
		return Convert.toBigDecimal(getRequest().getParameter(name));
	}

	/**
	 * 获取BigDecimal参数
	 */
	public BigDecimal getParameterToBigDecimal(String name, BigDecimal defaultValue)
	{
		return Convert.toBigDecimal(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取Encode参数
	 */
	public String getParameterToEncode(String para)
	{
		return URLUtils.encode(getRequest().getParameter(para), CharsetUtils.UTF_8);
	}

	/**
	 * 获取Decode参数
	 */
	public String getParameterToDecode(String para)
	{
		return URLUtils.decode(getRequest().getParameter(para), CharsetUtils.UTF_8);
	}

	/**
	 * 获取ContextPath
	 */
	public String getContextPath()
	{
		return getRequest().getContextPath();
	}

	/**
	 * 页面跳转
	 * 
	 * @param 路径
	 */
	public String redirect(String url)
	{
		return StringUtils.format("redirect:{}", url);
	}

	/**
	 * 返回AjaxResult
	 * 
	 * @return AjaxResult
	 */
	public AjaxResult success()
	{
		return new AjaxResult().success();
	}

	/**
	 * 返回AjaxResult
	 * 
	 * @param message
	 * @return AjaxResult
	 */
	public AjaxResult success(String message)
	{
		return new AjaxResult().success(message);
	}
	
	/**
	 * 返回AjaxResult
	 * 
	 * @param data
	 * @return AjaxResult
	 */
	public AjaxResult success(Object data)
	{
		return new AjaxResult().success(data);
	}
	
	/**
	 * 返回AjaxResult
	 * 
	 * @param message
	 * @param data
	 * @return AjaxResult
	 */
	public AjaxResult success(String message, Object data)
	{
		return new AjaxResult().success(message, data);
	}
	
	/**
	 * 返回AjaxResult
	 * 
	 * @return AjaxResult
	 */
	public AjaxResult error()
	{
		return new AjaxResult().error();
	}
	
	/**
	 * 返回AjaxResult
	 * 
	 * @param message
	 * @return AjaxResult
	 */
	public AjaxResult error(String message)
	{
		return new AjaxResult().error(message);
	}

	/**
	 * 返回AjaxResult
	 * 
	 * @param data
	 * @return AjaxResult
	 */
	public AjaxResult error(Object data)
	{
		return new AjaxResult().error(data);
	}
	
	/**
	 * 返回AjaxResult
	 * 
	 * @param message
	 * @param data
	 * @return AjaxResult
	 */
	public AjaxResult error(String message, Object data)
	{
		return new AjaxResult().error(message, data);
	}
	
	/**
	 * 表单值映射为JavaBean
	 * 
	 * @param prefix name前缀
	 * @param beanClass javabean.class
	 * @return T
	 */
	public <T> T mappingJavaBean(String prefix, Class<T> beanClass)
	{
		return (T) BeanInjector.inject(beanClass, prefix, getRequest());
	}
	
	/**
	 * 表单值映射为JavaBean
	 * 
	 * @param beanClass javabean.class
	 * @return T
	 */
	public <T> T mappingJavaBean(Class<T> beanClass)
	{
		return (T) BeanInjector.inject(beanClass, getRequest());
	}
	
	/**
	 * 跳转错误页面
	 * 
	 * @param errorName
	 * @param message
	 * @return
	 */
	public String error(String errorName, String message)
	{
		this.getRequest().setAttribute("errorName", errorName);
		this.getRequest().setAttribute("message", message);
		return "/error/info";
	}
}

package com.scanywhere.core.base.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

import com.scanywhere.core.annotation.Comment;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class BaseEntity implements Serializable
{
	/**
	 * 列名常量
	 */
	// 序号
	public static final String COLUMN_ID = "ID";
	
	/**
	 * 属性
	 */
	@Comment(label = "序号")
	@Id
	@GeneratedValue(generator = "id")
	@GenericGenerator(name = "id", strategy = "uuid")
	@Column(name = COLUMN_ID, unique = true, nullable = false, length = 32)
	private String id;
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}
}

package com.scanywhere.core.base.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.reflect.GenericsUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.entity.BaseEntity;
import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.core.page.pojo.Page;

@Service("baseService")
@SuppressWarnings({ "hiding", "unchecked" })
public abstract class BaseServiceImpl<T extends BaseEntity> implements BaseService<T>
{
	private Class<T> entityClass;
	
	private BaseDao<T> dao;
	
	@Resource
	public void setDao(BaseDao<T> dao)
	{
		this.dao = dao;
	}
	
	public BaseDao<T> getDao()
	{
		return this.dao;
	}

	public BaseServiceImpl()
	{
		this.entityClass = GenericsUtils.getSuperClassGenricType(this.getClass());
	}

	public <T> Serializable save(T entity)
	{
		if (entity == null)
		{
			return null;
		}

		return this.dao.save(entity);
	}

	public <T> void delete(T entity)
	{
		if (entity == null)
		{
			return;
		}

		this.dao.delete(entity);
	}

	public <T> void update(T entity)
	{
		if (entity == null)
		{
			return;
		}

		this.dao.update(entity);
	}

	public <T> void saveOrUpdate(T entity)
	{
		if (entity == null)
		{
			return;
		}

		this.dao.saveOrUpdate(entity);
	}

	public <T> void saveBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		this.dao.saveBatch(entityList);
	}

	public <T> void deleteBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		this.dao.deleteBatch(entityList);
	}

	public <T> void updateBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		this.dao.updateBatch(entityList);
	}

	public <T> void saveOrUpdateBatch(List<T> entityList)
	{
		if (CollectionUtils.isEmpty(entityList))
		{
			return;
		}

		this.dao.saveOrUpdateBatch(entityList);
	}

	public <T> T getById(Serializable id)
	{
		if (id == null)
		{
			return null;
		}

		return (T) this.dao.getById(id);
	}

	public <T> List<T> find()
	{
		return this.dao.find();
	}

	public <T> List<T> find(Map<String, Object> params)
	{
		return this.dao.find(params);
	}

	public <T> List<T> find(Map<String, Object> params, String orderBy)
	{
		return this.dao.find(params, orderBy);
	}

	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, String orderBy)
	{
		return this.dao.find(params, likeParams, orderBy);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.dao.find(params, likeParams, null, null, orderBy, pageNo, pageSize);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy)
	{
		return this.dao.find(params, likeParams, timeParams, orderBy);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.dao.find(params, likeParams, timeParams, null, orderBy, pageNo, pageSize);
	}

	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize, boolean ascend)
	{
		return this.dao.find(params, likeParams, timeParams, null, orderBy, pageNo, pageSize, ascend);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy)
	{
		return this.dao.find(params, likeParams, timeParams, fields, orderBy);
	}
	
	public <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.dao.find(params, likeParams, timeParams, fields, orderBy, pageNo, pageSize);
	}

	public <T> Long findCount()
	{
		return this.dao.findCount();
	}

	public <T> Long findCount(Map<String, Object> params)
	{
		return this.dao.findCount(params);
	}
	
	public <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams)
	{
		return this.dao.findCount(params, likeParams);
	}
	
	public <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams)
	{
		return this.dao.findCount(params, likeParams, timeParams);
	}
	
	public Page queryPage(Map<String, Object> params, Integer pageNo, Integer pageSize)
	{
		return this.dao.queryPage(params, pageNo, pageSize);
	}

	public Page queryPage(Map<String, Object> params, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.dao.queryPage(params, orderBy, pageNo, pageSize);
	}

	public Page queryPage(Map<String, Object> params, Map<String, String> likeParams, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.dao.queryPage(params, likeParams, orderBy, pageNo, pageSize);
	}
	
	public Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.dao.queryPage(params, likeParams, timeParams, orderBy, pageNo, pageSize);
	}
	
	public Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize)
	{
		return this.dao.queryPage(params, likeParams, timeParams, fields, orderBy, pageNo, pageSize);
	}

	public <T> void deleteById(Serializable id)
	{
		if (id == null)
		{
			return;
		}

		this.dao.deleteById(id);
	}

	public <T> void deleteBatch(Serializable[] ids)
	{
		if (ids == null || ids.length == 0)
		{
			return;
		}

		this.dao.deleteBatch(ids);
	}
	
	public <T> void delete(Map<String, Object> params)
	{
		if (CollectionUtils.isEmpty(params))
		{
			return;
		}

		this.dao.delete(params);
	}

	public boolean isUnique(String name, String value, String condition)
	{
		return this.dao.isUnique(name, value, condition);
	}
}

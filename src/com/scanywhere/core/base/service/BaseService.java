package com.scanywhere.core.base.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.scanywhere.core.base.entity.BaseEntity;
import com.scanywhere.core.page.pojo.Page;

@SuppressWarnings({ "hiding" })
public abstract interface BaseService<T extends BaseEntity>
{
	/**
	 * 保存对象
	 *
	 * @param entity 保存的对象
	 * @return 唯一主键
	 */
	public abstract <T> Serializable save(T entity);

	/**
	 * 删除对象
	 *
	 * @param entity 删除的对象
	 */
	public abstract <T> void delete(T entity);

	/**
	 * 修改对象
	 *
	 * @param entity 修改的对象
	 */
	public abstract <T> void update(T entity);

	/**
	 * 保存或修改
	 *
	 * @param entity 保存或修改的对象
	 */
	public abstract <T> void saveOrUpdate(T entity);

	/**
	 * 批量保存
	 * 
	 * @param entityList 保存的对象集合
	 */
	public abstract <T> void saveBatch(List<T> entityList);

	/**
	 * 批量删除
	 * 
	 * @param entityList 删除的对象集合
	 */
	public abstract <T> void deleteBatch(List<T> entityList);

	/**
	 * 批量更新
	 * 
	 * @param entityList 更新的对象集合
	 */
	public abstract <T> void updateBatch(List<T> entityList);

	/**
	 * 批量保存或修改
	 * 
	 * @param entityList 保存或修改的对象集合
	 */
	public abstract <T> void saveOrUpdateBatch(List<T> entityList);

	/**
	 * 根据主键获取对象
	 *
	 * @param id 主键ID
	 * @return
	 */
	public abstract <T> T getById(Serializable id);

	/**
	 * 查询此对象所有数据
	 *
	 * @return
	 */
	public abstract <T> List<T> find();

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, String orderBy);
	
	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, String orderBy);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, String orderBy, Integer pageNo, Integer pageSize);
	
	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize);

	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @param ascend 是否为升序
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize, boolean ascend);
	
	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy);
	
	/**
	 * 带条件查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract <T> List<T> find(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize);

	/**
	 * 查询记录数
	 * 
	 * @return
	 */
	public abstract <T> Long findCount();

	/**
	 * 查询记录数
	 * 
	 * @param params 查询参数
	 * @return
	 */
	public abstract <T> Long findCount(Map<String, Object> params);

	/**
	 * 查询记录数
	 * 
	 * @param params 查询参数
	 * @param likeParams 查询参数
	 * @return
	 */
	public abstract <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams);
	
	/**
	 * 查询记录数
	 * 
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @return
	 */
	public abstract <T> Long findCount(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams);

	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Integer pageNo, Integer pageSize);

	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, String orderBy, Integer pageNo, Integer pageSize);

	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Map<String, String> likeParams, String orderBy, Integer pageNo, Integer pageSize);
	
	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String orderBy, Integer pageNo, Integer pageSize);
	
	/**
	 * 分页查询获取实体对象
	 *
	 * @param params 查询参数
	 * @param likeParams 模糊查询参数
	 * @param timeParams 时间查询参数
	 * @param fields 返回字段
	 * @param orderBy 排序参数
	 * @param pageNo 当前页号
	 * @param pageSize 每页记录数
	 * @return
	 */
	public abstract Page queryPage(Map<String, Object> params, Map<String, String> likeParams, Map<String, String> timeParams, String[] fields, String orderBy, Integer pageNo, Integer pageSize);

	/**
	 * 根据主键删除对象
	 *
	 * @param id 主键ID
	 */
	public abstract <T> void deleteById(Serializable id);

	/**
	 * 批量删除对象
	 *
	 * @param ids 主键ID集合
	 */
	public abstract <T> void deleteBatch(Serializable[] ids);

	/**
	 * 删除对象
	 * 
	 * @param params
	 */
	public abstract <T> void delete(Map<String, Object> params);
	
	/**
	 * 判断对象属性的值在数据库中是否唯一
	 * 
	 * @param name 属性名
	 * @param value 值
	 * @param condition 查询条件
	 * @return
	 */
	public abstract boolean isUnique(String name, String value, String condition);
}

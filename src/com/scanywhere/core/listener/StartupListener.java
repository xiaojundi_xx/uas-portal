package com.scanywhere.core.listener;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.web.context.WebApplicationContext;

import com.scanywhere.common.reflect.ClassUtils;
import com.scanywhere.configuration.ConfigurationFactory;
import com.scanywhere.core.cache.IProjectCache;

public class StartupListener implements ApplicationListener<ContextRefreshedEvent>
{
	private final Logger logger = Logger.getLogger(StartupListener.class);
	
	public void onApplicationEvent(ContextRefreshedEvent event)
	{
		ApplicationContext applicationContext = event.getApplicationContext();
		if (applicationContext.getParent() == null) 
		{
			logger.info("开始缓存初始化......");
			// 缓存初始化
			Set<Class<?>> caches = ClassUtils.scanPackageBySuper("com.scanywhere.core.cache.impl", IProjectCache.class);
			Iterator<Class<?>> it = caches.iterator();
			while (it.hasNext()) 
			{
				Class<?> clazz = it.next();
				Object obj = ClassUtils.newInstance(clazz);
				ClassUtils.invoke(obj, "init", new Object[]{});
			}
			
	        // 设置系统信息
	        WebApplicationContext webApplicationContext = (WebApplicationContext) applicationContext;
	        ServletContext servletContext = webApplicationContext.getServletContext();
	        servletContext.setAttribute("portal.system.name", ConfigurationFactory.getSystemConfiguration().getString("portal.system.name"));
	        servletContext.setAttribute("portal.company.name", ConfigurationFactory.getSystemConfiguration().getString("portal.company.name"));
	        servletContext.setAttribute("portal.company.url", ConfigurationFactory.getSystemConfiguration().getString("portal.company.url"));
		}
	}
}

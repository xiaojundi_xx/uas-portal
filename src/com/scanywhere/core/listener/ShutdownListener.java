package com.scanywhere.core.listener;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

public class ShutdownListener implements ApplicationListener<ContextClosedEvent>
{
	private final Logger logger = Logger.getLogger(ShutdownListener.class);
	
	public void onApplicationEvent(ContextClosedEvent event)
	{
		if (event.getApplicationContext().getParent() == null) 
		{
		}
	}
}

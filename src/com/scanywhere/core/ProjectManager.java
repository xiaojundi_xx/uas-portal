package com.scanywhere.core;

import com.scanywhere.common.cache.EhcacheFactory;
import com.scanywhere.common.cache.ICache;

public class ProjectManager
{
	/**
	 * 默认缓存工厂类
	 */
	private ICache defaultCacheFactory = new EhcacheFactory();

	private static volatile ProjectManager projectManager;

	private ProjectManager()
	{
	}

	public static ProjectManager getInstance()
	{
		if (projectManager == null)
		{
			synchronized (ProjectManager.class)
			{
				if (projectManager == null)
				{
					projectManager = new ProjectManager();
				}
			}
		}
		return projectManager;
	}

	public ICache getDefaultCacheFactory()
	{
		return defaultCacheFactory;
	}

	public void setDefaultCacheFactory(ICache defaultCacheFactory)
	{
		this.defaultCacheFactory = defaultCacheFactory;
	}
}

package com.scanywhere.core.annotation;

public @interface Comment
{
	/**
	 * 字段说明
	 * 
	 * @return
	 */
	String label() default "";
}

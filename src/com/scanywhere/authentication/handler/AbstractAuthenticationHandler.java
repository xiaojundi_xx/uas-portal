package com.scanywhere.authentication.handler;

import java.io.Serializable;
import java.util.Map;

import com.scanywhere.common.support.Convert;

@SuppressWarnings("serial")
public abstract class AbstractAuthenticationHandler implements AuthenticationHandler, Serializable
{
	// 认证参数配置
	private Map<String, Object> authParameter;

	/**
	 * 获取认证配置参数
	 */
	public String getParameter(String key)
	{
		if (authParameter.containsKey(key))
		{
			return authParameter.get(key).toString();
		}
		return null;
	}

	/**
	 * 获取认证配置参数
	 */
	public String getParameter(String key, String defaultValue)
	{
		if (authParameter.containsKey(key))
		{
			return Convert.toStr(authParameter.get(key).toString(), defaultValue);
		}
		return null;
	}

	/**
	 * 获取认证配置参数
	 */
	public Integer getParameterToInt(String key)
	{
		if (authParameter.containsKey(key))
		{
			return Convert.toInt(authParameter.get(key).toString());
		}
		return null;
	}

	/**
	 * 获取认证配置参数
	 */
	public Integer getParameterToInt(String key, Integer defaultValue)
	{
		if (authParameter.containsKey(key))
		{
			return Convert.toInt(authParameter.get(key).toString(), defaultValue);
		}
		return defaultValue;
	} 
	
	/**
	 * 获取所有认证配置参数
	 * 
	 * @return
	 */
	public Map<String, Object> getAuthParameter()
	{
		return authParameter;
	}

	/**
	 * 设置认证配置参数
	 * 
	 * @param authParameter
	 */
	public void setAuthParameter(Map<String, Object> authParameter)
	{
		this.authParameter = authParameter;
	}
}

package com.scanywhere.authentication.handler.support;

import java.util.Date;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.connector.ADConfig;
import com.scanywhere.authentication.connector.ADConnector;
import com.scanywhere.authentication.connector.ADConnectorPool;
import com.scanywhere.authentication.connector.ADUtils;
import com.scanywhere.authentication.exception.AccountDisabledException;
import com.scanywhere.authentication.exception.AccountExpiredException;
import com.scanywhere.authentication.exception.AccountLockedException;
import com.scanywhere.authentication.exception.AccountNotFoundException;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.authentication.exception.CaptchaExpiredException;
import com.scanywhere.authentication.exception.FirstLoginException;
import com.scanywhere.authentication.exception.IncorrectCredentialsException;
import com.scanywhere.authentication.exception.PasswordExpiredException;
import com.scanywhere.authentication.handler.AbstractAuthenticationHandler;
import com.scanywhere.authentication.principal.AuthCredentials;
import com.scanywhere.authentication.principal.Principal;
import com.scanywhere.authentication.principal.SimplePrincipal;
import com.scanywhere.authentication.principal.UsernamePasswordCredentials;
import com.scanywhere.common.codec.AESUtils;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.network.IpUtils;
import com.scanywhere.common.web.http.ServletUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.constant.LogConstant;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.authentication.entity.AuthFailLocks;
import com.scanywhere.modules.authentication.service.AuthFailLocksService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.service.UserService;

@SuppressWarnings("serial")
public class UsernamePasswordAuthenticationHandler extends AbstractAuthenticationHandler
{
	private UserService userService;
	
	private AuthFailLocksService authFailLocksService;
	
	public void init()
	{
		userService = (UserService) SpringContextUtils.getBean("userService");
		authFailLocksService = (AuthFailLocksService) SpringContextUtils.getBean("authFailLocksService");
	}
	
	public void preHandler(HttpServletRequest request, HttpServletResponse response, JSONObject result) throws AuthenticationException
	{
		// 获取认证配置 -- 是否启用验证码
		int enableCaptcha = super.getParameterToInt("enableCaptcha", CommonConstants.STATUS_DISABLED);
		if (enableCaptcha == CommonConstants.STATUS_ENABLED)
		{
			// 失败几次后出现验证码
			int failDisplayCaptcha = super.getParameterToInt("failDisplayCaptcha", 0);
			if (failDisplayCaptcha == CommonConstants.INT_VALUE_ZERO)
			{
				result.put("enableCaptcha", CommonConstants.STATUS_ENABLED);
			}
			else
			{
				// 从会话中获取失败次数
				String failCount = (String) request.getSession().getAttribute("failCount");
				if (StringUtils.isNotEmpty(failCount))
				{
					if (Integer.parseInt(failCount) >= failDisplayCaptcha)
					{
						result.put("enableCaptcha", CommonConstants.STATUS_ENABLED);
					}
					else
					{
						result.put("enableCaptcha", CommonConstants.STATUS_DISABLED);
					}
				}
				else
				{
					result.put("enableCaptcha", CommonConstants.STATUS_DISABLED);
				}
			}
		}
		else
		{
			result.put("enableCaptcha", CommonConstants.STATUS_DISABLED);
		}
	}

	public AuthCredentials postHandle(HttpServletRequest request, HttpServletResponse response, JSONObject result) throws AuthenticationException
	{
		// 获取用户名
		String username = request.getParameter("username");
		
		// 获取密码
		String password = request.getParameter("password");
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
		{
			throw new AuthenticationException("认证信息不完整!");
		}
		
		// 认证随机数
		String verifyRandom = request.getParameter("verifyRandom");
		
		// 会话中保存的认证随机数
		String sessionRandom = (String) request.getSession().getAttribute("sessionRandom");
		if (StringUtils.isEmpty(verifyRandom) || StringUtils.isEmpty(sessionRandom) || !verifyRandom.equals(sessionRandom)) 
		{
			throw new AuthenticationException("认证信息已过期, 请重新登录!");
		}
		
		// 获取验证码
		String captcha = request.getParameter("captcha");
		int enableCaptcha = super.getParameterToInt("enableCaptcha", CommonConstants.STATUS_DISABLED);
		if (enableCaptcha == CommonConstants.STATUS_ENABLED)
		{
			// 失败几次后出现验证码
			int failDisplayCaptcha = super.getParameterToInt("failDisplayCaptcha", 0);
			if (failDisplayCaptcha == CommonConstants.INT_VALUE_ZERO)
			{
				if (StringUtils.isEmpty(captcha))
				{
					throw new AuthenticationException("认证信息不完整!");
				}
				
				// 校验验证码
				String validateCode = (String) request.getSession().getAttribute(CommonConstants.LOGIN_CAPTCHA);
				if (StringUtils.isEmpty(validateCode) || !validateCode.equals(captcha.toUpperCase()))
				{
					throw new CaptchaExpiredException("验证码不正确!");
				}
			}
			else
			{
				// 从会话中获取失败次数
				String failCount = (String) request.getSession().getAttribute("failCount");
				if (StringUtils.isNotEmpty(failCount))
				{
					if (Integer.parseInt(failCount) >= failDisplayCaptcha)
					{
						if (StringUtils.isEmpty(captcha))
						{
							throw new AuthenticationException("认证信息不完整!");
						}
						
						// 校验验证码
						String validateCode = (String) request.getSession().getAttribute(CommonConstants.LOGIN_CAPTCHA);
						if (StringUtils.isEmpty(validateCode) || !validateCode.equals(captcha.toUpperCase()))
						{
							throw new CaptchaExpiredException("验证码不正确!");
						}
					}
				}
			}
		}

		String key = username;
		int length = key.length();
		if(length > 16)
		{
			key = key.substring(0,16);
		}
		else if(length < 16)
		{
			while (length < 16) 
			{
				key = key + "0";
				length++;
			}
		}
		
		String aesDecrypt = AESUtils.decrypt(password, key);
		request.getSession().setAttribute("userPassword", aesDecrypt);
		
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials();
		credentials.setUsername(username);
		credentials.setPassword(password);
		credentials.setCaptcha(captcha);
		return credentials;
	}
	
	public Principal authenticate(AuthCredentials credentials) throws AuthenticationException
	{
		UsernamePasswordCredentials authCredentials = (UsernamePasswordCredentials) credentials;
		
		// 查询用户
		String username = authCredentials.getUsername();
		User user = userService.findByLoginName(username);
		if (user == null)
		{
			// 记录日志
			LogUtils.me().saveUserLog(null, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "无效的帐号!");
			throw new AccountNotFoundException();
		}
		
		// 判断帐号状态
		if (user.getUserStatus() == CommonConstants.STATUS_DISABLED)
		{
			// 记录日志
			LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "帐号已停用!");
			throw new AccountDisabledException();
		}
		else if (user.getUserStatus() == CommonConstants.STATUS_LOCKED)
		{
			// 记录日志
			LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "帐号已锁定!");
			throw new AccountLockedException();
		}
		else if (user.getUserStatus() == CommonConstants.STATUS_EXPIRED)
		{
			// 记录日志
			LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "帐号已过期!");
			throw new AccountExpiredException();
		}
		
		// 判断有效期
		if (user.getExpiredTime() != null)
		{
			Date now = DateUtils.date();
			if (now.after(user.getExpiredTime()))
			{
				user.setUserStatus(CommonConstants.STATUS_EXPIRED);
				userService.update(user);
				
				// 记录日志
				LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "帐号已失效!");
				throw new AccountExpiredException();
			}
		}
		
		String key = username;
		int length = key.length();
		if(length > 16)
		{
			key = key.substring(0,16);
		}
		else if(length < 16)
		{
			while (length < 16) 
			{
				key = key + "0";
				length++;
			}
		}
		
		String encryptpassword = authCredentials.getPassword();
		String aesDecrypt = AESUtils.decrypt(encryptpassword, key);
		
		// 获取认证源
		boolean pwdVerifySuccess = false;
		int authSource = super.getParameterToInt("authSource", 0);
		if (authSource == 0)
		{
			// 本地认证
			String password = new SimpleHash("MD5", aesDecrypt, ByteSource.Util.bytes(user.getLoginName()), 2).toHex();
			if (password.equals(user.getUserPassword()))
			{
				pwdVerifySuccess = true;
			}
		}
		else if (authSource == 1)
		{
			// 域认证
			String protocol = super.getParameter("protocol");
			String host = super.getParameter("host");
			int port = super.getParameterToInt("port");
			String adPrincipal = super.getParameter("principal");
			String adCredentials = super.getParameter("credentials");
			String baseDN = super.getParameter("baseDN");
			
			// 初始化域连接信息
			String configCode = "domain";
			ADConfig config = new ADConfig(configCode, host, port, adPrincipal, adCredentials, protocol.equals("ldaps") ? true : false);
			ADConnectorPool pool = ADConnectorPool.getInstance();
			ADConnector connector = null;
			
			// 查询域帐号是否存在
			try
			{
				// 获取连接
				connector = pool.getConnector(config);
				
				DirContext context = connector.getDirContext();
				if (!ADUtils.getInstance().checkAccountExists(context, baseDN, username))
				{
					// 记录日志
					LogUtils.me().saveUserLog(null, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "无效的域帐号!");
					throw new AccountNotFoundException();
				}
				
				String accountDN = ADUtils.getInstance().searchAccountDN(context, baseDN, username);
				if (StringUtils.isEmpty(accountDN))
				{
					// 记录日志
					LogUtils.me().saveUserLog(null, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "无效的域帐号!");
					throw new AccountNotFoundException();
				}
				
				// 验证密码是否正确
				boolean ret = ADUtils.getInstance().validateAccount(host, accountDN, aesDecrypt);
				if (ret)
				{
					pwdVerifySuccess = true;
				}
			}
			catch (NamingException e) 
			{
				e.printStackTrace();
				throw new AuthenticationException("请联系管理员确认是否正确配置域信息!");
			}
			finally
			{
				pool.setConnectorFree(configCode, connector);
			}
		}
		else
		{
			throw new AuthenticationException("认证源配置错误!");
		}
		
		// 验证密码
		if (!pwdVerifySuccess)
		{
			// 获取认证配置 -- 是否启用验证码
			int enableCaptcha = super.getParameterToInt("enableCaptcha", CommonConstants.STATUS_DISABLED);
			if (enableCaptcha == CommonConstants.STATUS_ENABLED)
			{
				// 失败几次后出现验证码
				int failDisplayCaptcha = super.getParameterToInt("failDisplayCaptcha", 0);
				if (failDisplayCaptcha != CommonConstants.INT_VALUE_ZERO)
				{
					// 从会话中获取失败次数
					String failCount = (String) ServletUtils.getRequest().getSession().getAttribute("failCount");
					if (StringUtils.isEmpty(failCount))
					{
						ServletUtils.getRequest().getSession().setAttribute("failCount", "1");
					}
					else
					{
						ServletUtils.getRequest().getSession().setAttribute("failCount", String.valueOf(Integer.parseInt(failCount) + 1));
					}
				}
			}
			
			// 密码尝试次数
			int failNum = super.getParameterToInt("failNum", CommonConstants.INT_VALUE_ZERO);
			if (failNum != CommonConstants.INT_VALUE_ZERO)
			{
				AuthFailLocks record = authFailLocksService.findFailureRecord(super.getParameter("id"), user.getId());
				if (record == null)
				{
					authFailLocksService.saveFailureRecord(super.getParameter("id"), user.getId(), 1, CommonConstants.STATUS_1, DateUtils.date(), null);
					
					// 记录日志
					String message = "密码错误,您还可以尝试" + (failNum - 1) + "次登录!";
					LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), message);
					throw new IncorrectCredentialsException(message);
				}
				else
				{
					// 用户锁定时间
					int lockTime = super.getParameterToInt("lockTime", 5);
					
					if (record.getLoginFailCount() <= failNum - 1)
					{
						if ((failNum - record.getLoginFailCount() - 1) == 0)
						{
							// 下次登录时间
							Date loginNextTime = DateUtils.getMinute(DateUtils.date(), lockTime);
							authFailLocksService.saveFailureRecord(super.getParameter("id"), user.getId(), record.getLoginFailCount() + 1, CommonConstants.STATUS_0, DateUtils.date(), loginNextTime);
							
							// 记录日志
							String message = "密码错误,失败次数达到上限,帐号被临时锁定" + lockTime + "分钟!";
							LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), message);
							throw new IncorrectCredentialsException(message);
						}
						
						authFailLocksService.saveFailureRecord(super.getParameter("id"), user.getId(), record.getLoginFailCount() + 1, CommonConstants.STATUS_0, DateUtils.date(), null);
						
						// 记录日志
						String message = "密码错误,您还可以尝试" + (failNum - record.getLoginFailCount() - 1) + "次登录!";
						LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), message);
						throw new IncorrectCredentialsException(message);
					}
					else
					{
						// 下次登录时间
						Date loginNextTime = record.getLoginNextTime();
						if (loginNextTime.after(DateUtils.date()))
						{
							// 记录日志
							String message = "用户已锁定,请于" + DateUtils.getTimeDifference(loginNextTime, DateUtils.date()) + "后再次登录!";
							LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), message);
							throw new AuthenticationException(message);
						}
						else
						{
							authFailLocksService.saveFailureRecord(super.getParameter("id"), user.getId(), 1, CommonConstants.STATUS_1, DateUtils.date(), null);
							
							// 记录日志
							String message = "密码错误,您还可以尝试" + (failNum - record.getLoginFailCount() - 1) + "次登录!";
							LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), message);
							throw new IncorrectCredentialsException();
						}
					}
				}
			}
			
			// 记录日志
			LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "错误的认证信息!");
			throw new AuthenticationException();
		}
		else
		{
			// 密码尝试次数
			int failNum = super.getParameterToInt("failNum", CommonConstants.INT_VALUE_ZERO);
			if (failNum != CommonConstants.INT_VALUE_ZERO)
			{
				AuthFailLocks record = authFailLocksService.findFailureRecord(super.getParameter("id"), user.getId());
				if (record != null)
				{
					if (record.getLoginFailCount() >= failNum)
					{
						// 下次登录时间
						Date loginNextTime = record.getLoginNextTime();
						if (loginNextTime.after(DateUtils.date()))
						{
							// 记录日志
							String message = "用户已锁定,请于" + DateUtils.getTimeDifference(loginNextTime, DateUtils.date()) + "后再次登录!";
							LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), message);
							throw new AuthenticationException(message);
						}
						else
						{
							authFailLocksService.removeFailureRecord(super.getParameter("id"), user.getId());
						}
					}
					else
					{
						authFailLocksService.removeFailureRecord(super.getParameter("id"), user.getId());
					}
				}
			}
			
			// 获取认证配置 -- 是否启用验证码
			int enableCaptcha = super.getParameterToInt("enableCaptcha", CommonConstants.STATUS_DISABLED);
			if (enableCaptcha == CommonConstants.STATUS_ENABLED)
			{
				ServletUtils.getRequest().getSession().removeAttribute("failCount");
			}
		}
		
		// 本地密码认证时验证密码有效期, 首次登录
		if (authSource == 0)
		{
			// 验证密码有效期
			int pwdValidity = super.getParameterToInt("pwdValidity", CommonConstants.INT_VALUE_ZERO);
			if (pwdValidity != CommonConstants.INT_VALUE_ZERO)
			{
				Date now = DateUtils.date();
				if (now.after(DateUtils.getDate(user.getPwdModifyTime(), pwdValidity)))
				{
					// 记录日志
					LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "密码已过期!");
					throw new PasswordExpiredException();
				}
			}
			
			// 首次登录是否修改密码
			int firstLogin = super.getParameterToInt("firstLogin", CommonConstants.STATUS_DISABLED);
			if (firstLogin == CommonConstants.STATUS_ENABLED)
			{
				if (user.getFirstLogin() == CommonConstants.STATUS_0)
				{
					// 记录日志
					LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, IpUtils.getRemoteAddr(ServletUtils.getRequest()), "首次登录强制修改密码!");
					throw new FirstLoginException();
				}
			}
		}
		else if(authSource == 1)
		{
			int pwdSync = super.getParameterToInt("pwdSync", CommonConstants.STATUS_ENABLED);
			if(pwdSync == CommonConstants.STATUS_ENABLED ) 
			{
				String password = new SimpleHash("MD5", aesDecrypt, ByteSource.Util.bytes(user.getLoginName()), 2).toHex();
				if (!password.equals(user.getUserPassword()) || !AESUtils.encode(aesDecrypt).equals(user.getFormPassword()))
				{
					//密码同步到本地数据库
					user.setUserPassword(password);
					user.setPwdModifyTime(DateUtils.date());
					user.setFirstLogin(CommonConstants.STATUS_1);
					user.setUpdateDateTime(DateUtils.date());
					user.setUpdateTimeStamp(DateUtils.current(false));
					user.setFormPassword(AESUtils.encode(aesDecrypt));
					userService.update(user);
				}
			}
		}

		return new SimplePrincipal(user.getId(), user, true);
	}

	public boolean supports(AuthCredentials credentials)
	{
		return credentials != null && UsernamePasswordCredentials.class.isAssignableFrom(credentials.getClass());
	}
}

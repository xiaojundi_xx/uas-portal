package com.scanywhere.authentication.handler.support;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.exception.AccountDisabledException;
import com.scanywhere.authentication.exception.AccountExpiredException;
import com.scanywhere.authentication.exception.AccountLockedException;
import com.scanywhere.authentication.exception.AccountNotFoundException;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.authentication.handler.AbstractAuthenticationHandler;
import com.scanywhere.authentication.principal.AuthCredentials;
import com.scanywhere.authentication.principal.Principal;
import com.scanywhere.authentication.principal.SMSCredentials;
import com.scanywhere.authentication.principal.SimplePrincipal;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.RandomUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.service.UserService;

@SuppressWarnings("serial")
public class SMSAuthenticationHandler extends AbstractAuthenticationHandler
{
	private UserService userService;
	
	public void init()
	{
		userService = (UserService) SpringContextUtils.getBean("userService");
	}
	
	public void preHandler(HttpServletRequest request, HttpServletResponse response,JSONObject result) throws AuthenticationException
	{
		// 产生随机数
		String random = RandomUtils.generateString(8);
		
		// 设置到会话中
		request.getSession().setAttribute("sessionRandom", random);
		
		// 设置到请求中
		request.setAttribute("verifyRandom", random);
		
		// 获取认证配置 -- 是否启用验证码
		int enableCaptcha = super.getParameterToInt("enableCaptcha", CommonConstants.STATUS_DISABLED);
		request.setAttribute("enableCaptcha", enableCaptcha);
	}

	public AuthCredentials postHandle(HttpServletRequest request, HttpServletResponse response,JSONObject result) throws AuthenticationException
	{
		// 获取用户名
		String username = request.getParameter("username");
		
		// 获取手机号
		String mobilePhone = request.getParameter("mobilePhone");

		// 短信验证码
		String smsCode = request.getParameter("smsCode");
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(mobilePhone) || StringUtils.isEmpty(smsCode))
		{
			throw new AuthenticationException("认证信息不完整!");
		}
		
		// 认证随机数
		String verifyRandom = request.getParameter("verifyRandom");
		
		// 会话中保存的认证随机数
		String sessionRandom = (String) request.getSession().getAttribute("sessionRandom");
		if (StringUtils.isEmpty(verifyRandom) || StringUtils.isEmpty(sessionRandom) || !verifyRandom.equals(sessionRandom)) 
		{
			throw new AuthenticationException("认证信息已过期!");
		}

		SMSCredentials credentials = new SMSCredentials();
		credentials.setUsername(username);
		credentials.setMobilePhone(mobilePhone);
		credentials.setSmsCode(smsCode);
		return credentials;
	}
	
	public Principal authenticate(AuthCredentials credentials) throws AuthenticationException
	{
		SMSCredentials authCredentials = (SMSCredentials) credentials;
		
		// 查询用户
		User user = userService.findByLoginName(authCredentials.getUsername());
		if (user == null)
		{
			throw new AccountNotFoundException();
		}
		
		// 判断帐号状态
		if (user.getUserStatus() == CommonConstants.STATUS_DISABLED)
		{
			throw new AccountDisabledException();
		}
		else if (user.getUserStatus() == CommonConstants.STATUS_LOCKED)
		{
			throw new AccountLockedException();
		}
		
		// 判断有效期
		if (user.getExpiredTime() != null)
		{
			Date now = DateUtils.date();
			if (now.after(user.getExpiredTime()))
			{
				user.setUserStatus(CommonConstants.STATUS_EXPIRED);
				userService.update(user);
				
				throw new AccountExpiredException();
			}
		}
		
		return new SimplePrincipal(user.getId(), user, true);
	}

	public boolean supports(AuthCredentials credentials)
	{
		return credentials != null && SMSCredentials.class.isAssignableFrom(credentials.getClass());
	}
}

package com.scanywhere.authentication.handler.support;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.util.encoders.Base64;

import com.alibaba.fastjson.JSONObject;
import com.sca.cert.CertAuth;
import com.sca.cert.CertValid;
import com.scanywhere.authentication.exception.AccountDisabledException;
import com.scanywhere.authentication.exception.AccountExpiredException;
import com.scanywhere.authentication.exception.AccountLockedException;
import com.scanywhere.authentication.exception.AccountNotFoundException;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.authentication.handler.AbstractAuthenticationHandler;
import com.scanywhere.authentication.principal.AuthCredentials;
import com.scanywhere.authentication.principal.CertificateCredentials;
import com.scanywhere.authentication.principal.Principal;
import com.scanywhere.authentication.principal.SimplePrincipal;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CAUtils;
import com.scanywhere.common.utils.FileUtils;
import com.scanywhere.common.utils.PrintUtils;
import com.scanywhere.common.utils.TimerManager;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.entity.UserCert;
import com.scanywhere.modules.user.service.UserCertService;
import com.scanywhere.modules.user.service.UserService;

@SuppressWarnings("serial")
public class CertificateAuthenticationHandler extends AbstractAuthenticationHandler
{
	private static X509CertificateHolder ca = null;
	private static X509CRLHolder crl = null;
	private static String issuer = null;
	
	static {
		initCA();
		new TimerManager();
	}
	
	private UserService userService;
	private UserCertService userCertService;
	
	public void init()
	{
		userService = (UserService) SpringContextUtils.getBean("userService");
		userCertService = (UserCertService) SpringContextUtils.getBean("userCertService");
	}
	
	public static void initCA(){
		if(ca == null) {
			ca = CAUtils.initCARoot();
		}
		
		if(ca != null){
			if(issuer == null)
				try {
					issuer = (new JcaX509CertificateConverter().setProvider( "BC" )
							.getCertificate( ca )).getIssuerX500Principal().getName();
				} catch (CertificateException e) {
					PrintUtils.printError("获取CRL列表", "获取根证书颁发者失败");
				}
			crl = CAUtils.initCRL(issuer);
		}
	}
	
	public void preHandler(HttpServletRequest request, HttpServletResponse response, JSONObject result) throws AuthenticationException
	{
	}

	public AuthCredentials postHandle(HttpServletRequest request, HttpServletResponse response,JSONObject result) throws AuthenticationException
	{
		// 获取用户名
		String cn = request.getParameter("cn");
				
		// 获取证书
		String cert = request.getParameter("cert");
		
		// 获取签名
		String signature = request.getParameter("signature");
		if (StringUtils.isEmpty(cn) || StringUtils.isEmpty(cert) || StringUtils.isEmpty(signature))
		{
			throw new AuthenticationException("认证信息不完整!");
		}
		
		// 认证随机数
		String verifyRandom = request.getParameter("verifyRandom");
		
		// 会话中保存的认证随机数
		String sessionRandom = (String) request.getSession().getAttribute("sessionRandom");
		if (StringUtils.isEmpty(verifyRandom) || StringUtils.isEmpty(sessionRandom) || !verifyRandom.equals(sessionRandom)) 
		{
			throw new AuthenticationException("认证信息已过期!");
		}
		
		CertificateCredentials credentials = new CertificateCredentials();
		credentials.setCn(cn);
		credentials.setCert(cert);
		credentials.setSignature(signature);
		credentials.setVerifyRandom(verifyRandom);
		return credentials;
	}
	
	public Principal authenticate(AuthCredentials credentials) throws AuthenticationException
	{
		CertificateCredentials authCredentials = (CertificateCredentials) credentials;
		
		byte[] bytes = Base64.decode(authCredentials.getCert().getBytes());

		X509CertificateHolder crt;
		boolean ret;
		try {
			crt = CertValid.parseCertificate(bytes);
			ret = CertAuth.CheckSignature(crt, authCredentials.getVerifyRandom(), authCredentials.getSignature(), 3);
		} catch (IOException e) {
			throw new AuthenticationException("用户证书异常!");
		}

		if(!ret){
			throw new AuthenticationException("证书签名错误!");
		}

		if(ca == null){
			if (!FileUtils.exist(new File(CAUtils.CAROOTPATH))) 
				throw new AuthenticationException("根证书文件缺失!");
			try {
				byte[] caRootBytes = FileUtils.readBytes(new File(CAUtils.CAROOTPATH));
				ca = CertValid.parseCertificate(caRootBytes);
			} catch (Throwable e) {
				throw new AuthenticationException("根证书文件异常!");
			}
		}
		
		if(crl == null){
			if (FileUtils.exist(new File(CAUtils.CAROOTPATH))) {
				try {
					InputStream crlInputStream = FileUtils.getInputStream(CAUtils.CACRLPATH);
					crl = CertValid.parseCRL(crlInputStream);
				} catch (Throwable e) {
					PrintUtils.printError("CRL校验","CRL文件异常");
				}
			}
		}
		
		int iRet = CertValid.CheckCertificate(crt, ca, crl);

		if(iRet == -1)
			throw new AuthenticationException("证书已过期!");
		if(iRet == -2)
			throw new AuthenticationException("证书未生效!");
		if(iRet == -3)
			throw new AuthenticationException("证书颁发者无效!");
		if(iRet == -4)
			throw new AuthenticationException("证书已作废!");
		if(iRet == -5)
			throw new AuthenticationException("证书签名无效!");
		if(iRet == -99)
			throw new AuthenticationException("证书有误!");

		// 查询用户
		User user = userService.findByLoginName(authCredentials.getCn());
		if (user == null)
		{
			throw new AccountNotFoundException("byCert");
		}

		// 查询证书用户
		/* 证书流程完善后打开
		UserCert userCert = userCertService.findByLoginName(authCredentials.getCn());
		if (userCert == null)
		{
			throw new AuthenticationException("系统未授予该用户证书权限!");
		}
		*/
		
		// 判断帐号状态
		if (user.getUserStatus() == CommonConstants.STATUS_DISABLED)
		{
			throw new AccountDisabledException();
		}
		else if (user.getUserStatus() == CommonConstants.STATUS_LOCKED)
		{
			throw new AccountLockedException();
		}
		
		// 判断有效期
		if (user.getExpiredTime() != null)
		{
			Date now = DateUtils.date();
			if (now.after(user.getExpiredTime()))
			{
				user.setUserStatus(CommonConstants.STATUS_EXPIRED);
				userService.update(user);
				
				throw new AccountExpiredException();
			}
		}
		
		return new SimplePrincipal(user.getId(), user, true);
	}
	
	/**
	 * 读取文件
	 * @param filePath
	 * @return
	 */
	/*
	private static byte[] readFile(String filePath){
		byte[] byt = null;
		InputStream cain = null;
		try {
			cain = new FileInputStream(filePath);
		    byt = new byte[cain.available()];
		    cain.read(byt);
		} catch (FileNotFoundException e) {
		    System.out.println("file not find!");
		} catch (IOException e) {
			System.out.println("IOException :" + e);
		}finally {
			try {
				cain.close();
			} catch (IOException e) {
				System.out.println("IOException :" + e);
			} catch (NullPointerException e) {
				System.out.println("NullPointerException :" + e);
			}
		}
		return byt;
	}
	*/
	
	public boolean supports(AuthCredentials credentials)
	{
		return credentials != null && CertificateCredentials.class.isAssignableFrom(credentials.getClass());
	}
}

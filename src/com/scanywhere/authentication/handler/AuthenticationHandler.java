package com.scanywhere.authentication.handler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.authentication.principal.AuthCredentials;
import com.scanywhere.authentication.principal.Principal;

public interface AuthenticationHandler
{
	/**
	 * 初始化
	 */
	public void init();
	
	/**
	 * 认证登录请求处理前调用
	 * 
	 * @param request
	 * @param response
	 * @throws AuthException
	 */
	public void preHandler(HttpServletRequest request, HttpServletResponse response, JSONObject result) throws AuthenticationException;
	
	/**
	 * 获取认证登录请求
	 * 
	 * @param request
	 * @param response
	 * @param credentials
	 * @throws AuthException
	 */
	public AuthCredentials postHandle(HttpServletRequest request, HttpServletResponse response, JSONObject result) throws AuthenticationException;
	
	/**
	 * 认证请求处理
	 * 
	 * @param credentials
	 * @return
	 * @throws AuthenticationException
	 */
	public Principal authenticate(AuthCredentials credentials) throws AuthenticationException;
	
	/**
	 * 验证认证方式是否支持此凭证
	 * 
	 * @param credentials
	 * @return
	 */
	public boolean supports(AuthCredentials credentials);
	
	/**
	 * 获取认证方式参数配置
	 */
	public String getParameter(String key);
	
	/**
	 * 获取认证方式参数配置
	 */
	public String getParameter(String key, String defaultValue);
	
	/**
	 * 获取认证方式参数配置
	 */
	public Integer getParameterToInt(String key);
	
	/**
	 * 获取认证方式参数配置
	 */
	public Integer getParameterToInt(String key, Integer defaultValue);
	
	/**
	 * 获取认证方式参数配置
	 */
	public Map<String, Object> getAuthParameter();
	
	/**
	 * 设置认证方式参数配置
	 * 
	 * @param authParameter
	 */
	public void setAuthParameter(Map<String, Object> authParameter);
}

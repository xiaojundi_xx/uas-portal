package com.scanywhere.authentication;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.exception.AccountDisabledException;
import com.scanywhere.authentication.exception.AccountExpiredException;
import com.scanywhere.authentication.exception.AccountLockedException;
import com.scanywhere.authentication.exception.AccountNotFoundException;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.authentication.exception.CaptchaExpiredException;
import com.scanywhere.authentication.exception.FirstLoginException;
import com.scanywhere.authentication.exception.IncorrectCredentialsException;
import com.scanywhere.authentication.exception.PasswordExpiredException;
import com.scanywhere.authentication.handler.AuthenticationHandler;
import com.scanywhere.authentication.principal.AuthCredentials;
import com.scanywhere.authentication.principal.Principal;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.network.IpUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.common.utils.GetBrowserUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.constant.LogConstant;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.authentication.entity.AuthConfig;
import com.scanywhere.modules.system.service.SysConfigService;
import com.scanywhere.security.subject.UserSubject;

public class AuthenticationManagerImpl implements AuthenticationManager
{
	@Resource
	private SysConfigService sysConfigService;
	
	public void loadAuthCode(HttpServletRequest request, HttpServletResponse response, JSONObject result) throws AuthenticationException{
		// 获取缓存的认证方式
		List<AuthConfig> authList = CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, "authList");

		if (CollectionUtils.isEmpty(authList))
		{
			throw new AuthenticationException("平台未启用任何认证方式, 请联系管理员!");
		}

		List<String> authCodeList = new ArrayList<String>();
		for(AuthConfig auth : authList)
			authCodeList.add(auth.getAcCode());
		
		int noAuthWay = 0;
		String authCode = null;
		String noAuthCode = null;
		if(!authCodeList.contains("password_auth")){
			noAuthCode = "password_auth";
			authCode = "certificate_auth";
			noAuthWay++;
		}
		if(!authCodeList.contains("certificate_auth")){
			noAuthCode = "certificate_auth";
			authCode = "password_auth";
			noAuthWay++;
		}
		
		if(noAuthWay == 2){
			throw new AuthenticationException("平台未启用任何认证方式, 请联系管理员!");
		}
		
		if(noAuthWay ==1){
			result.put("noAuthCode", noAuthCode);
			result.put("authCode", authCode);
			return;
		}
		
		Cookie cookies[]=request.getCookies();//声明一个cookie对象

		// 获取认证方式
		if(cookies!=null){
			for (int i = 0; i < cookies.length; i++){   
				if (cookies[i].getName().equals("authCode")){
					authCode = cookies[i].getValue();
					break;
				}
			}
		}

		if(StringUtils.isEmpty(authCode)){
			// 取默认第一个认证方式
			authCode = (String) authList.get(0).getAcCode();
		}

		result.put("authCode", authCode);
		return;
	}
	
	public void loginPage(HttpServletRequest request, HttpServletResponse response, String authCode, JSONObject result) throws AuthenticationException
	{
		// 获取认证方式实现
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, authCode);
		if (handler != null)
		{
			handler.preHandler(request, response, result);
		}
		else
		{
			throw new AuthenticationException("平台未启用密码认证方式与认证方式");
		}
		
		
		// 获取登录用户名
		String loginName = request.getParameter("username");
		Cookie cookies[]=request.getCookies();
		if(cookies == null){
			
		}else if (!StringUtils.isBlank(loginName))
		{
			result.put("username", loginName);
		}else{
			for (int i = 0; i < cookies.length; i++){   
			    if (cookies[i].getName().equals("username")){
			    	loginName = cookies[i].getValue();
			         break;
			    }
			}
			result.put("username", loginName);
		}
	}
	
	public boolean doAuthentication(HttpServletRequest request, HttpServletResponse response, String authCode, JSONObject result) throws AuthenticationException
	{
		if (StringUtils.isEmpty(authCode))
		{
			throw new AuthenticationException("未选择认证方式, 请重新访问!");
		}
		
		// 获取认证方式实现
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, authCode);
		if (handler == null)
		{
			throw new AuthenticationException("认证方式未启用, 请重新访问!");
		}

		// 获取登录用户名
		String loginName = request.getParameter("username");
		try
		{
			// 获取认证登录请求
			AuthCredentials credentials = handler.postHandle(request, response, result);
			if (credentials == null)
			{
				throw new AuthenticationException("获取认证信息失败, 请重新访问!");
			}
			
			// 判断实例是否支持凭证
			if (!handler.supports(credentials))
			{
				throw new AuthenticationException("认证实例不支持此认证凭证, 请重新访问!");
			}
			// 认证请求处理
			Principal principal = handler.authenticate(credentials);
			if (principal != null && principal.isAuthenticated())
			{
				// 认证成功
				UserSubject subject = new UserSubject(principal);
				subject.setAuthCode(authCode);
				subject.setLoginMode(ProjectConstants.LOGIN_MODE_INTRANET);
				subject.setClientIp(IpUtils.getRemoteAddr(request));
				
				subject.setBrowserType(GetBrowserUtils.getBrowserType(request));
				//将登录信息存入cookie
				Cookie authCodeCookie =  new Cookie("authCode", authCode);
				
				authCodeCookie.setMaxAge(60*60*24*30*12*10);
				authCodeCookie.setPath(request.getContextPath()+"/");
				response.addCookie(authCodeCookie);
				Cookie usernameCookie =  new Cookie("username", loginName);
				usernameCookie.setMaxAge(60*60*24*30*12*10);
				usernameCookie.setPath(request.getContextPath()+"/");
				response.addCookie(usernameCookie);
				
				// 设置会话
				this.sysConfigService = (SysConfigService) SpringContextUtils.getBean("sysConfigService");
				String userSessionTimeout = sysConfigService.findValueByCode(ProjectConstants.SYS_CONFIG_MODULE_SECURITY, "userSessionTimeout");
				if (StringUtils.isEmpty(userSessionTimeout))
				{
					userSessionTimeout = "240";
				}
				request.getSession().setMaxInactiveInterval(Integer.parseInt(userSessionTimeout) * 60);
				request.getSession().setAttribute(CommonConstants.LOGIN_INFO, subject);
				
				// 记录日志
				if(authCode.equals("password_auth")){
					LogUtils.me().saveUserLog(subject, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "密码登录", LogConstant.SUCCESS, null);
				}else{
					LogUtils.me().saveUserLog(subject, LogConstant.ACTION_TYPE_LOGIN_CERT, "证书登录", LogConstant.SUCCESS, null);
				}
				return true;
			}
			/*else
			{
				// 认证失败 记录日志
				UserSubject subject = new UserSubject(principal);
				subject.setClientIp(IpUtils.getRemoteAddr(request));
				LogUtils.me().saveUserLog(subject, LogConstant.ACTION_TYPE_LOGIN_LOGOUT, "用户登录", LogConstant.FAILURE, principal.getAuthenticationError());
			}*/
		}
		catch (AccountNotFoundException e)
		{
			e.printStackTrace();
			result.put("errorCode", "username");
			result.put("authError", "无效的帐号!");
			/*
			if("byCert".equals(e.getMessage())){
				result.put("authError", "无效的帐号!");
			}else{
				result.put("authError", "无效的帐号或密码!");
			}
			*/
		}
		catch (AccountDisabledException e)
		{
			e.printStackTrace();
			result.put("errorCode", "auth");
			result.put("authError", "帐号已停用!");
		}
		catch (AccountLockedException e)
		{
			e.printStackTrace();
			result.put("errorCode", "auth");
			result.put("authError", "帐号已锁定!");
		}
		catch (AccountExpiredException e)
		{
			e.printStackTrace();
			result.put("errorCode", "auth");
			result.put("authError", "帐号已过期!");
		}
		catch (IncorrectCredentialsException e)
		{
			e.printStackTrace();
			result.put("errorCode", StringUtils.isEmpty(e.getMessage()) ? "login" : "password");
			result.put("authError", StringUtils.isEmpty(e.getMessage()) ? "错误的认证信息!" : e.getMessage());
		}
		catch (CaptchaExpiredException e)
		{
			e.printStackTrace();
			result.put("errorCode", "captcha");
			result.put("authError", StringUtils.isEmpty(e.getMessage()) ? "错误的认证信息!" : e.getMessage());
		}
		catch (PasswordExpiredException e)
		{
			e.printStackTrace();
			Cookie authCodeCookie =  new Cookie("authCode", authCode);
			authCodeCookie.setMaxAge(60*60*24*30*12*10);
			authCodeCookie.setPath(request.getContextPath()+"/");
			response.addCookie(authCodeCookie);
			Cookie usernameCookie =  new Cookie("username", loginName);
			usernameCookie.setMaxAge(60*60*24*30*12*10);
			usernameCookie.setPath(request.getContextPath()+"/");
			response.addCookie(usernameCookie);
			result.put("errorCode", "changePwd");
			result.put("authError", "密码已过期!");
		}
		catch (FirstLoginException e)
		{
			e.printStackTrace();
			Cookie authCodeCookie =  new Cookie("authCode", authCode);
			authCodeCookie.setMaxAge(60*60*24*30*12*10);
			authCodeCookie.setPath(request.getContextPath()+"/");
			response.addCookie(authCodeCookie);
			Cookie usernameCookie =  new Cookie("username", loginName);
			usernameCookie.setMaxAge(60*60*24*30*12*10);
			usernameCookie.setPath(request.getContextPath()+"/");
			response.addCookie(usernameCookie);
			result.put("errorCode", "changePwd");
			result.put("authError", "首次登录请修改密码!");
		}
		catch (AuthenticationException e)
		{
			e.printStackTrace();
			result.put("errorCode", "auth");
			result.put("authError", StringUtils.isEmpty(e.getMessage()) ? "错误的认证信息!" : e.getMessage());
		}

		return false;
	}
}

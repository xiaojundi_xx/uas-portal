package com.scanywhere.authentication.principal;

@SuppressWarnings("serial")
public class UsernamePasswordCredentials implements AuthCredentials
{
	/** 用户名 */
    private String username;

    /** 密码 */
    private String password;
    
    /** 验证码 */
    private String captcha;

	public String getUsername()
	{
		return username;
	}

	public String getPassword()
	{
		return password;
	}

	public String getCaptcha()
	{
		return captcha;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public void setCaptcha(String captcha)
	{
		this.captcha = captcha;
	}
}

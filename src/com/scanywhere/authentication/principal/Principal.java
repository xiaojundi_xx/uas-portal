package com.scanywhere.authentication.principal;

import java.io.Serializable;

public interface Principal extends Serializable
{
	/**
	 * ID
	 * 
	 * @return
	 */
	public String getId();
	
	/**
	 * 获取认证对象
	 * 
	 * @return
	 */
	public Object getInfo();
	
	/**
	 * 认证结果
	 * 
	 * @return
	 */
	public boolean isAuthenticated();
	
	/**
	 * 获取认证错误
	 * 
	 * @return
	 */
	public String getAuthenticationError();
	
	
}

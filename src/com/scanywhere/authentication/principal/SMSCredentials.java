package com.scanywhere.authentication.principal;

@SuppressWarnings("serial")
public class SMSCredentials implements AuthCredentials
{
	/** 用户名 */
    private String username;
    
	/** 手机号 */
    private String mobilePhone;

    /** 短信验证码 */
    private String smsCode;

	public String getUsername()
	{
		return username;
	}

	public String getMobilePhone()
	{
		return mobilePhone;
	}

	public String getSmsCode()
	{
		return smsCode;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}

	public void setSmsCode(String smsCode)
	{
		this.smsCode = smsCode;
	}
}

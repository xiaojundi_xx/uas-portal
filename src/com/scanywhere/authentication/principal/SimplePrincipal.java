package com.scanywhere.authentication.principal;

@SuppressWarnings("serial")
public class SimplePrincipal implements Principal
{
	/** 认证对象唯一标识 */
    private String id;
    
    /** 认证对象 */
	private Object info;
	
    /** 是否认证成功 */
	private boolean authenticated = false;
	
	/** 认证错误 */
	private String error;
	
	public SimplePrincipal(String id, Object info, boolean authenticated)
    {
    	this.id = id;
    	this.info = info;
    	this.authenticated = authenticated;
    }
	
    public SimplePrincipal( String id, Object info, String error)
    {
    	this.id = id;
    	this.info = info;
    	this.error = error;
    }
    
	public String getId()
	{
		return id;
	}
	
	public Object getInfo()
	{
		return info;
	}
	
	public boolean isAuthenticated() 
	{
		return authenticated;
	}

	public String getAuthenticationError()
	{
		return error;
	}
	
	public void setAuthenticationError(String error)
	{
		this.error = error;
	}
}
package com.scanywhere.authentication.principal;

@SuppressWarnings("serial")
public class CertificateCredentials implements AuthCredentials
{
	/** CN */
    private String cn;
    
	/** 证书 */
    private String cert;
    
	/** 签名 */
    private String signature;

    /** 原文 */
    private String verifyRandom;

    public String getCn()
	{
		return cn;
	}

	public void setCn(String cn)
	{
		this.cn = cn;
	}
	
	public String getCert()
	{
		return cert;
	}

	public String getSignature()
	{
		return signature;
	}

	public String getVerifyRandom()
	{
		return verifyRandom;
	}

	public void setCert(String cert)
	{
		this.cert = cert;
	}

	public void setSignature(String signature)
	{
		this.signature = signature;
	}

	public void setVerifyRandom(String verifyRandom)
	{
		this.verifyRandom = verifyRandom;
	}
}

package com.scanywhere.authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.exception.AuthenticationException;

public interface AuthenticationManager
{
	/**
	 * 读取缓存中的登录方式
	 * @param request
	 * @param response
	 * @param result
	 * @return
	 * @throws AuthenticationException
	 */
	public void loadAuthCode(HttpServletRequest request, HttpServletResponse response, JSONObject result) throws AuthenticationException;
	
	/**
	 * 登录页跳转
	 * 
	 * @param request
	 * @param response
	 * @param authCode
	 * @return
	 * @throws AuthenticationException
	 */
	public void loginPage(HttpServletRequest request, HttpServletResponse response, String authCode, JSONObject result) throws AuthenticationException;
	
	/**
	 * 认证
	 * 
	 * @param request
	 * @param response
	 * @param authCode
	 * @throws AuthenticationException
	 */
	public boolean doAuthentication(HttpServletRequest request, HttpServletResponse response, String authCode, JSONObject result) throws AuthenticationException;
}

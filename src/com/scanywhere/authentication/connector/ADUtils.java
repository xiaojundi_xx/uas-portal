package com.scanywhere.authentication.connector;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class ADUtils 
{
	private ADUtils()
	{}
	
	private static ADUtils adUtils;
	
	public static ADUtils getInstance()
	{
		if (adUtils == null)
		{
			adUtils = new ADUtils();
		}
		return adUtils;
	}
	
	/**
	 * 检查连接
	 * @param context
	 * @throws NamingException
	 */
	public void checkConnection(DirContext context) throws NamingException
	{
		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.OBJECT_SCOPE);
		constraints.setReturningAttributes(new String[]{"rootDomainNamingContext"});
		constraints.setReturningObjFlag(false);
		context.search("", "(objectclass=*)", constraints);
	}
	
	/**
	 * 检查域用户是否存在
	 * @param context
	 * @param name
	 * @param account
	 * @return
	 * @throws NamingException
	 */
	public boolean checkAccountExists(DirContext context, String name, String account) throws NamingException
	{
		if (context == null) {
			return false;
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String filter = "(&(sAMAccountType=805306368)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(sAMAccountName=" + account + "))";  
		NamingEnumeration<SearchResult> result = context.search(name, filter, constraints);
		if (result.hasMoreElements()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 查询用户DN
	 * @param context
	 * @param name
	 * @param account
	 * @return
	 * @throws NamingException
	 */
	public String searchAccountDN(DirContext context, String name, String account) throws NamingException
	{
		if (context == null) {
			return "";
		}
		
		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String attrList[] = {"distinguishedName"};
		constraints.setReturningAttributes(attrList);
		String filter = "(&(sAMAccountType=805306368)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(sAMAccountName=" + account + "))";
		NamingEnumeration<SearchResult> result = context.search(name, filter, constraints);
		if (result.hasMoreElements())
		{
			SearchResult sr = (SearchResult) result.next();
			Attributes attrs = sr.getAttributes();
			if (attrs != null)
			{
				Attribute attr = attrs.get("distinguishedName");
				NamingEnumeration vals = attr.getAll();
				String value = (String) vals.nextElement();
				return value;
			}
		}
		return "";
	}
	
	/**
	 * 修改域用户密码
	 * @param context
	 * @param accountDN
	 * @param password
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NamingException
	 */
	public boolean updateAccountPassword(DirContext context, String accountDN, String password) throws UnsupportedEncodingException, NamingException
	{
		if (context == null) {
			return false;
		}
		
		ModificationItem[] items = new ModificationItem[1];
		password = "\"" + password + "\"";
		items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("unicodePwd", password.getBytes("UTF-16LE")));
		context.modifyAttributes(accountDN, items);
		return true;
	}
	
	/**
	 * 验证域用户帐号/密码
	 * @param host
	 * @param principal
	 * @param credentials
	 * @return
	 */
	public boolean validateAccount(String host, String principal, String credentials)
	{
		try
		{
			Hashtable<String, String> mEnv = new Hashtable<String, String>();
			mEnv.put(Context.AUTHORITATIVE, "true");
			mEnv.put("com.sun.jndi.ldap.connect.pool", "false");
			mEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			mEnv.put(Context.PROVIDER_URL, "ldap://"+host+":389");
			mEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
			mEnv.put(Context.SECURITY_PRINCIPAL, principal);
			mEnv.put(Context.SECURITY_CREDENTIALS, credentials);
			DirContext context = new InitialDirContext(mEnv);
			if (context != null) {
				return true;
			} else {
				return false;
			}
		} catch (NamingException e) {
			return false;
		}
	}
}

package com.scanywhere.authentication.connector;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ConnectorConfig 
{
	private static final String BUNDLE_NAME = "com.scanywhere.authentication.connector.config";

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	private ConnectorConfig() {
	}
	
	public static String getString(String key) 
	{
		try 
		{
			return RESOURCE_BUNDLE.getString(key);
		}
		catch (MissingResourceException e) 
		{
			return '!' + key + '!';
		}
	}
	
	public static String getString(String key, Object args[])
    {
		Object[] castArgs = null;
		if (args != null) {
			castArgs = new Object[args.length];
			for (int i = 0; i < args.length; i ++) {
				Object arg = args[i];
				if (arg == null) {
					castArgs[i] = " ";
				} else {
					castArgs[i] = arg;
				}
			}
		}
        return MessageFormat.format(getString(key), castArgs);
    }
}

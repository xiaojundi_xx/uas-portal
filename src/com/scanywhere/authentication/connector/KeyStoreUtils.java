package com.scanywhere.authentication.connector;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class KeyStoreUtils 
{
	private final Logger logger = Logger.getLogger(KeyStoreUtils.class);
	
	public static final String KEYSTORE_ALIAS_PREFIX = "UAS-";
	
	public static final String KEYSTORE_PASSWORD = "scanywhere"; 
	
	public static final String KEYSTORE_JAVA_PASSWORD = "changeit"; 

	public final static String KEYSTORE_FILE;
	static 
	{
		File keystore = new File(KeyStoreUtils.class.getResource("/").getPath(), "keystore");
		if (!keystore.exists())
		{
			keystore.mkdirs();
		}
		String path = keystore.getAbsolutePath();
		if (!path.endsWith("/") && !path.endsWith("\\"))
		{
			path = path + File.separator;
		}
		KEYSTORE_FILE = path;
	}
	
	public KeyStoreUtils() {
	}
	
	public void importKeystore(String code, String host, int port) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableEntryException, KeyManagementException
	{
		/*
		 * 此类表示密钥和证书的存储设施
		 * KeyStore 管理不同类型的项。每种类型的项都实现 KeyStore.Entry 接口
		 * 从密钥库中直接读取证书

		 * 请求 KeyStore 对象的典型方式包括使用默认类型和提供一个特定的 keystore 类型
		 * 根据默认类型：

		 * KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		 * 提供特定的 keystore 类型： 
		 * KeyStore ks = KeyStore.getInstance("JKS");
		 * 
		 * */
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		
		// 从指定的输入流中加载此 KeyStore
		ks.load(null);
		
		/*
		 * 此类的实例表示安全套接字协议的实现，它充当用于安全套接字工厂或 SSLEngine 的工厂

		 * 生成实现指定安全套接字协议的 SSLContext 对象
		 * 如果默认的提供程序包提供了请求的密钥管理算法实现，则返回包含该实现的 SSLContext 实例
		 * 如果默认提供程序包中未提供算法，则搜索其他的包

		 * */
		SSLContext context = SSLContext.getInstance("TLS");
		
		/*
		 * 此类充当基于信任材料源的信任管理器的工厂
		 * 每个信任管理器管理特定类型的由安全套接字使用的信任材料。信任材料是基于 KeyStore 和/或提供程序特定的源。
		 * getInstance(String algorithm) 生成实现指定的信任管理算法的 TrustManagerFactory 对象。 
		 * algorithm - 请求的信任管理算法的标准名称。
		 * */
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		
		/*
		 * 用证书授权源和相关的信任材料初始化此工厂。
		 * 提供程序通常使用 KeyStore 作为基础做出信任决定。
		 * */
		tmf.init(ks);
		
		/*
		 * 此接口的实例管理使用哪一个 X509 证书来验证远端的安全套接字。 
		 * 决定是根据信任的证书授权、证书撤消列表、在线状态检查或其他方式做出的。 
		 */
		X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
		SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);
		
		/*
		 * 初始化此上下文。前两个参数都可以为 null，在这种情况下将搜索装入的安全提供程序来寻找适当工厂的最高优先级实现。
		 * 同样，安全的 random 参数也可以为 null，在这种情况下将使用默认的实现。
		 * 只有数组中的第一个特定密钥和/或信任管理器实现类型的实例被使用。（例如，只有数组中的第一个 javax.net.ssl.X509KeyManager 被使用。）
		 * 参数：
		 * km - 身份验证密钥源或 null
		 * tm - 同位体身份验证信任决策源或 null
		 * random - 此生成器的随机源或 null
		 * */
		context.init(null, new TrustManager[] { tm }, null);

		/*
		 * 返回此上下文的 SocketFactory 对象。
		 * */
		SSLSocketFactory factory = context.getSocketFactory();

		/*
		 * SSLSocketFactory 创建 SSLSocket
		 * 此类扩展 Socket 并提供使用协议（如 "Secure Sockets Layer" (SSL) 或 IETF "Transport Layer Security" (TLS) 协议）的安全套接字。
		 * */
		SSLSocket socket = (SSLSocket) factory.createSocket(host, port);

		/*
		 * 启用/禁用带有指定超时值的 SO_TIMEOUT，以毫秒为单位。
		 * 将此选项设为非零的超时值时，在与此 Socket 关联的 InputStream 上调用 read() 将只阻塞此时间长度。
		 * 如果超过超时值，将引发 java.net.SocketTimeoutException，虽然 Socket 仍旧有效。
		 * 选项必须在进入阻塞操作前被启用才能生效。超时值必须是 > 0 的数。超时值为 0 被解释为无穷大超时值。 
		 * */
		socket.setSoTimeout(10000);

		try 
		{
			/*
			 * 在此连接上开始 SSL 握手。常见的原因包括使用新的加密密钥的需要、改变密码套件的需要或开始新的会话的需要。为了强制进行重新身份验证，可以在开始握手之前使当前的会话无效。
			 * 如果数据已经在连接上发送，则在握手期间数据继续流动。当握手结束时，将使用事件来通知。 
			 * 此方法对于连接上的初始握手是同步的并且在协商的握手结束时返回。
			 * 一些协议可能不支持在一个已经存在的套接字上的多次握手，可能抛出 SSLException。 
			 * */
			socket.startHandshake();
			
			/*
			 * 关闭此套接字。
			 * */
			socket.close();
		} 
		catch (IOException e) 
		{
		}
		
		X509Certificate chain[] = tm.chain;
		if (chain == null) 
		{
			throw new IOException();
		}
		else 
		{
			X509Certificate cert = chain[0];
			
			/* 证书信息 begin */
			logger.info("Version [" + cert.getVersion() + "]");  
			logger.info("SerialNumber ["+ cert.getSerialNumber().toString(16) + "]");  
			logger.info("SubjectDN ["+ cert.getSubjectDN() + "]");  
			logger.info("IssuerDN ["+ cert.getIssuerDN() + "]");  
			logger.info("NotBefore ["+ cert.getNotBefore() + "]");  
			logger.info("SigAlgName ["+ cert.getSigAlgName() + "]");
			logger.info("[" +  Base64.encodeBase64String(cert.getEncoded()) + "]");
			/* 证书信息 end */
			
			/*
			 * 将给定可信证书分配给给定别名。
			 * 如果给定的别名标识通过调用 setCertificateEntry 创建的现有项，或者标识通过调用使用 TrustedCertificateEntry 作为参数的 setEntry 创建的现有项，则现有项中的可信证书被给定证书重写。 
			 * */
			String alias = new StringBuilder(KEYSTORE_ALIAS_PREFIX).append(code).toString();
			
			/* JDK添加证书 */
			try
			{
				char passphrase[] = KEYSTORE_JAVA_PASSWORD.toCharArray();
				char SEP = File.separatorChar;
				File jkeystore = new File((new StringBuilder(String.valueOf(System.getProperty("java.home")))).append(SEP).append("lib").append(SEP).append("security").toString(), "cacerts");
				
				InputStream jin = new FileInputStream(jkeystore);
				KeyStore jks = KeyStore.getInstance(KeyStore.getDefaultType());
				jks.load(jin, passphrase);
				jin.close();
				jks.setCertificateEntry(alias, cert);
				OutputStream jout = new FileOutputStream(jkeystore);
				jks.store(jout, passphrase);
				jout.flush();
				jout.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			// end
			
			// 生成keystore文件
			char passphrase[] = KEYSTORE_PASSWORD.toCharArray();
			ks.setCertificateEntry(alias, cert);
			
			// 将此 keystore 存储到给定输出流中，并用给定的密码保护其完整性
			File ksfile = new File(KEYSTORE_FILE + "/" + alias + ".keystore");
			if(!ksfile.exists())
			{
				FileOutputStream fos = new FileOutputStream(ksfile);
				fos.flush();
				fos.close();
			}
			
			OutputStream out = new FileOutputStream(ksfile);
			ks.store(out, passphrase);
			out.close();
		}
	}
	
	public void makeKeystore(String code, String keystore) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
	{
		/*
		 * 此类表示密钥和证书的存储设施
		 * KeyStore 管理不同类型的项。每种类型的项都实现 KeyStore.Entry 接口
		 * 从密钥库中直接读取证书

		 * 请求 KeyStore 对象的典型方式包括使用默认类型和提供一个特定的 keystore 类型
		 * 根据默认类型：

		 * KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		 * 提供特定的 keystore 类型： 
		 * KeyStore ks = KeyStore.getInstance("JKS");
		 * 
		 * */
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		
		// 从指定的输入流中加载此 KeyStore
		ks.load(null);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(Base64.decodeBase64(keystore));
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509Certificate cert = (X509Certificate) cf.generateCertificate(bais);
		
		/* 证书信息 begin */
		logger.info("Version [" + cert.getVersion() + "]");  
		logger.info("SerialNumber ["+ cert.getSerialNumber().toString(16) + "]");  
		logger.info("SubjectDN ["+ cert.getSubjectDN() + "]");  
		logger.info("IssuerDN ["+ cert.getIssuerDN() + "]");  
		logger.info("NotBefore ["+ cert.getNotBefore() + "]");  
		logger.info("SigAlgName ["+ cert.getSigAlgName() + "]");
		logger.info("[" + Base64.encodeBase64String(cert.getEncoded()) + "]");
		/* 证书信息 end */
		
		/*
		 * 将给定可信证书分配给给定别名。
		 * 如果给定的别名标识通过调用 setCertificateEntry 创建的现有项，或者标识通过调用使用 TrustedCertificateEntry 作为参数的 setEntry 创建的现有项，则现有项中的可信证书被给定证书重写。 
		 * */
		String alias = new StringBuilder(KEYSTORE_ALIAS_PREFIX).append(code).toString();
		
		/* JDK添加证书 */
		try
		{
			char passphrase[] = KEYSTORE_JAVA_PASSWORD.toCharArray();
			char SEP = File.separatorChar;
			File jkeystore = new File((new StringBuilder(String.valueOf(System.getProperty("java.home")))).append(SEP).append("lib").append(SEP).append("security").toString(), "cacerts");
			
			InputStream jin = new FileInputStream(jkeystore);
			KeyStore jks = KeyStore.getInstance(KeyStore.getDefaultType());
			jks.load(jin, passphrase);
			jin.close();
			jks.setCertificateEntry(alias, cert);
			OutputStream jout = new FileOutputStream(jkeystore);
			jks.store(jout, passphrase);
			jout.flush();
			jout.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		// end
		
		// 生成keystore文件
		char passphrase[] = KEYSTORE_PASSWORD.toCharArray();
		ks.setCertificateEntry(alias, cert);
		
		// 将此 keystore 存储到给定输出流中，并用给定的密码保护其完整性
		File ksfile = new File(KEYSTORE_FILE + "/" + alias + ".keystore");
		if(!ksfile.exists())
		{
			FileOutputStream fos = new FileOutputStream(ksfile);
			fos.flush();
			fos.close();
		}
		
		OutputStream out = new FileOutputStream(ksfile);
		ks.store(out, passphrase);
		out.close();
	}
	
	private static class SavingTrustManager implements X509TrustManager 
	{
		private final X509TrustManager tm;
		
		private X509Certificate chain[];

		SavingTrustManager(X509TrustManager tm) {
			this.tm = tm;
		}
		
		public X509Certificate[] getAcceptedIssuers() {
			throw new UnsupportedOperationException();
		}

		public void checkClientTrusted(X509Certificate chain[], String authType) throws CertificateException {
			throw new UnsupportedOperationException();
		}

		public void checkServerTrusted(X509Certificate chain[], String authType) throws CertificateException {
			this.chain = chain;
			tm.checkServerTrusted(chain, authType);
		}
	}
}

package com.scanywhere.authentication.connector;

import javax.naming.NamingException;

public class ADConnectorFactory 
{
	public static ADConnector createLDAPConection(ADConfig conf) throws NamingException
	{
		return new ADConnector(conf.getConfigCode(), conf.getHost(), conf.getPort(), conf.getPrincipal(), conf.getCredentials(), conf.isSsl());
	}
}

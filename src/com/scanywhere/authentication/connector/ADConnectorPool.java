package com.scanywhere.authentication.connector;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

public class ADConnectorPool 
{
	private final Logger logger = Logger.getLogger(ADConnectorPool.class);
	
	private Hashtable<String, List<ADConnector>> connectors = new Hashtable<String, List<ADConnector>>();

	private static ADConnectorPool connectorPool = null;
	
	private int maxPoolSize = 100;

	private ADConnectorPool() 
	{
		if (ConnectorConfig.getString("maxPoolSize") != null && !ConnectorConfig.getString("maxPoolSize").trim().equals(""))
		{
			maxPoolSize = Integer.parseInt(ConnectorConfig.getString("maxPoolSize"));
		}
	}

	public static synchronized ADConnectorPool getInstance() 
	{
		if (connectorPool == null) 
		{
			connectorPool = new ADConnectorPool();
		}
		return connectorPool;
	}

	public void put(String configCode, ADConnector connector)
	{
		connector.setConetIsFree(false);
		if(this.connectors.get(configCode) == null)
		{
			if (logger.isInfoEnabled())
			{
				logger.info("创建域连接, 域配置编码[" + configCode + "], 连接唯一编码[" + connector.getContextId() + "]");
			}
			
			List<ADConnector> list = new ArrayList<ADConnector>();
			list.add(connector);
			this.connectors.put(configCode, list);
		}
		else
		{
			if (logger.isInfoEnabled())
			{
				logger.info("创建域连接, 域配置编码[" + configCode + "], 连接唯一编码[" + connector.getContextId() + "]");
			}
			
			this.connectors.get(configCode).add(connector);
		}
	}
	
	public void remove(String configCode, ADConnector connector)
	{
		if (logger.isInfoEnabled())
		{
			logger.info("删除域连接, 域配置编码[" + configCode + "], 连接唯一编码[" + connector.getContextId() + "]");
		}
		
		List<ADConnector> pool = connectors.get(configCode);
		for (Iterator<ADConnector> iterator = pool.iterator(); iterator.hasNext();)
		{
    		try
			{
    			ADConnector context = (ADConnector) iterator.next();
        		/*
        		 * 	此处产生产生ConcurrentModificationException异常.
        		 *	执行remove(Object o)方法之后,modCount和expectedModCount不相等了.
        		 *	然后当代码执行到next()方法时,判断了checkForComodification(),发现两个数值不等,就抛出了该Exception.
        		 *	添加 iterator.remove()方法
        		 */
    			if(connector.getContextId().equals(context.getContextId()))
    			{
    				context.closeConnector();
    				iterator.remove();
            		break;
    			}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public synchronized ADConnector getConnector(ADConfig config) throws NamingException
	{
		String configCode = config.getConfigCode();
		// 调用该方法返回一个ADConnector
		ADConnector connector = hasFree(configCode);
		if (connector != null)
		{
			if (logger.isInfoEnabled())
			{
				logger.info("获取域连接, 域配置编码[" + configCode + "], 连接唯一编码[" + connector.getContextId() + "]");
			}
			return connector;
		}
		
		if (getSize(configCode) < maxPoolSize)
		{
			// 如果产生的ADConnector还没有达到最大连接数,则可以从工厂里获取.
			connector = ADConnectorFactory.createLDAPConection(config);
			if(connector.getDirContext() != null)
			{
				put(configCode, connector);
			}
		}
		else
		{
			if (logger.isInfoEnabled())
			{
				logger.info("等待空闲域连接, 域配置编码[" + configCode + "]");
			}
			
			// 如果已经达到最大连接数了.那么就的等待,直到有空闲的ADConnector可以使用.
			try 
			{
				wait();
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
				return null;
			}
			
			List<ADConnector> list = this.connectors.get(configCode);
			if(list != null && !list.isEmpty())
			{
				for (ADConnector context : list) 
				{
					if (context.isConetIsFree() && !context.isClosed())
					{
						if (logger.isInfoEnabled())
						{
							logger.info("已有空闲域连接, 获取域连接, 域配置编码[" + configCode + "], 连接唯一编码[" + context.getContextId() + "]");
						}
						
						connector = context;
						break;
					}
				}
			}
		}
		return connector;
	}
	
	public synchronized void setConnectorFree(String configCode, ADConnector connector) 
	{
		if (connector == null)
			return;
		
		// 用完后设置该连接为空闲状态,以便其他线程重用,并把阻塞的线程唤醒.
		if(this.connectors.get(configCode) != null && !this.connectors.get(configCode).isEmpty())
		{
			for (ADConnector context : this.connectors.get(configCode)) 
			{
				if (context.getContextId().equals(connector.getContextId()))
				{
					if (logger.isInfoEnabled())
					{
						logger.info("释放域连接, 域配置编码[" + configCode + "], 连接唯一编码[" + connector.getContextId() + "]");
					}
					
					// 该链接为空闲连接, 设置成功
					connector.setConetIsFree(true);
					notifyAll();
					break;
				}
			}
		}
	}
	
	public int getSize(String configCode) 
	{
		if(this.connectors.get(configCode) == null)
		{
			return 0;
		}
		else 
		{
			if (logger.isInfoEnabled())
			{
				logger.info("获取域连接数, 域配置编码[" + configCode + "], 已建连接[" + this.connectors.get(configCode).size() + "]");
			}
			return this.connectors.get(configCode).size();
		}
	}

	public ADConnector hasFree(String configCode) 
	{
		if(getSize(configCode) == 0)
			return null;
		
		ADConnector context = null;
		List<ADConnector> removeList = new ArrayList<ADConnector>();
		if(this.connectors.get(configCode) != null && !this.connectors.get(configCode).isEmpty())
		{
			for (ADConnector connector : this.connectors.get(configCode)) 
			{
				if (connector == null)
				{
					removeList.add(connector);
					continue;
				}
				else if (connector.isConetIsFree())
				{
					try 
					{
						if(connector.getDirContext() == null)
						{
							if (logger.isInfoEnabled())
							{
								logger.info("获取空闲域连接, 域配置编码[" + configCode + "], 失效连接[" + connector.getContextId() + "]");
							}
							removeList.add(connector);
							continue;
						}
						
						ADUtils.getInstance().checkConnection(connector.getDirContext());
					} 
					catch (NamingException e) 
					{
						if (logger.isInfoEnabled())
						{
							logger.info("获取空闲域连接, 域配置编码[" + configCode + "], 失效连接[" + connector.getContextId() + "]");
						}
						
						removeList.add(connector);
						continue;
					}
					
					if (logger.isInfoEnabled())
					{
						logger.info("获取空闲域连接, 域配置编码[" + configCode + "], 已建连接[" + connector.getContextId() + "]");
					}
					context = connector;
					break;
				}
				else if (connector.isClosed())
				{
					if (logger.isInfoEnabled())
					{
						logger.info("获取空闲域连接, 域配置编码[" + configCode + "], 已关闭连接[" + connector.getContextId() + "]");
					}
					
					removeList.add(connector);
				}
			}
		}
		
		// 删除无效链接
		if(removeList != null && !removeList.isEmpty())
		{
			for (ADConnector connector : removeList) 
			{
				remove(configCode, connector);
			}
		}
		return context;
	}
	
	public void closeAll() 
	{
		try
		{
			if (connectors != null && !connectors.isEmpty())
			{
				if (logger.isInfoEnabled())
				{
					logger.info("关闭所有连接!");
				}
				
				for (Map.Entry<String, List<ADConnector>> item : connectors.entrySet()) 
				{   
					for (ADConnector connector : item.getValue()) 
					{
						connector.closeConnector();
					}
					connectors.remove(item.getKey());
				} 
			}
		}
		catch(Exception e)
		{
			System.out.println("关闭域控连接出现错误!");
		}
	}
	
	public void removeAll()
	{
		closeAll();
		this.connectors.clear();
	}
}

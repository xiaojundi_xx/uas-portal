package com.scanywhere.authentication.connector;

public class ADConfig 
{
	private String configCode;
	private String host;
	private int port;
	private String principal;
	private String credentials;
	private boolean ssl;

	public ADConfig(String configCode, String host, int port, String principal, String credentials, boolean ssl) 
	{
		this.configCode = configCode;
		this.host = host;
		this.port = port;
		this.principal = principal;
		this.credentials = credentials;
		this.ssl = ssl;
	}

	public String getConfigCode() {
		return configCode;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public String getPrincipal() {
		return principal;
	}

	public String getCredentials() {
		return credentials;
	}

	public boolean isSsl() {
		return ssl;
	}
}

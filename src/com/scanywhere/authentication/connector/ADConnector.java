package com.scanywhere.authentication.connector;

import java.io.File;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import com.scanywhere.common.utils.RandomUtils;

public class ADConnector 
{
	private String contextId;
	
	// 判断该链接是否被占用
	private boolean conetIsFree = true;

	// 判断链接是否关闭
	private boolean isClosed = false;
	
	// 链接
	private DirContext context = null;

	public ADConnector(String code, String host, int port, String principal, String credentials) throws NamingException
	{
		this(code, host, port, principal, credentials, false);
	}
	
	public ADConnector(String code, String host, int port, String principal, String credentials, boolean ssl) throws NamingException
	{
		this.contextId = code + "-" + RandomUtils.generateString(8);
		Hashtable<String, String> env = new Hashtable<String, String>();
		//env.put(Context.AUTHORITATIVE, "true");
		env.put("com.sun.jndi.ldap.connect.pool", "true");
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		if(ssl) 
		{
			env.put(Context.PROVIDER_URL, "ldaps://"+host+":"+port);
			env.put(Context.SECURITY_PROTOCOL, "ssl");
			
			File keystore = new File(KeyStoreUtils.KEYSTORE_FILE, KeyStoreUtils.KEYSTORE_ALIAS_PREFIX + code + ".keystore");
			if (!keystore.exists())
			{
				KeyStoreUtils ksa = new KeyStoreUtils();
				try
				{
					ksa.importKeystore(code, host, port);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			
			// Java
			//char SEP = File.separatorChar;
			//keystore = new File((new StringBuilder(String.valueOf(System.getProperty("java.home")))).append(SEP).append("lib").append(SEP).append("security").toString(), "cacerts");
			// Java
			//System.setProperty("javax.net.ssl.trustStorePassword", KeyStoreUtils.KEYSTORE_JAVA_PASSWORD);
			
			
			System.setProperty("javax.net.ssl.trustStore", keystore.getAbsolutePath());
		
			// Keystore File
			System.setProperty("javax.net.ssl.trustStorePassword", KeyStoreUtils.KEYSTORE_PASSWORD);
		}
		else
		{
			env.put(Context.PROVIDER_URL, "ldap://"+host+":"+port);
		}
		
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, principal);
		env.put(Context.SECURITY_CREDENTIALS, credentials);
		
		DirContext initctx = new InitialDirContext(env);
		this.context = initctx;
	}

	public String getContextId() {
		return contextId;
	}

	public boolean isConetIsFree() {
		return conetIsFree;
	}

	public void setConetIsFree(boolean conetIsFree) {
		this.conetIsFree = conetIsFree;
	}
	
	public boolean isClosed() {
		return isClosed;
	}
	
	public DirContext getDirContext() 
	{
		return this.context;
	}

	public void closeConnector() 
	{
		if (this.context != null) 
		{
			try 
			{
				isClosed = true;
				context.close();
			} 
			catch (NamingException e) 
			{
				e.printStackTrace();
			}
		}
	}
}

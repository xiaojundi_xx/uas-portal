package com.scanywhere.authentication.connector;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Map;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.HttpUtils;

public class ADAdminPwd {
	private String adminURL; // iisAdminPwd的URL地址
	private String domain; // iisAdminPwd的domain域名
	private String data; // 返回的提交结果
	
	private static String FLAG_ADMPWD_SUCCESS = "成功";
	

	public ADAdminPwd(String adminURL,String domain) {
		this.adminURL = adminURL;
		this.domain = domain;
	}

	public String changeUserPassword(String userAccount, String oldPassword, String newPassword) {

		if (StringUtils.isEmpty(adminURL) || StringUtils.isEmpty(domain)) {
			return "参数配置错误，请联系管理员!"; // 参数错误；
		}
		
		if ( StringUtils.isEmpty(userAccount) || StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword)) {
			return "参数不能为空!"; // 参数错误；
		}

		// 请求URL
		Map<String, Object> params = new Hashtable<String, Object>();
		params.put("domain", domain);
		params.put("acct", userAccount);
		params.put("old", oldPassword);
		params.put("new", newPassword);
		params.put("new2", newPassword);

		// 请求数据
		try {
			data = HttpUtils.post(adminURL, params);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "服务器请求出错："+e.getMessage();  //页面提交出错
			
		}
		
	
		if (StringUtils.isEmpty(data)) {
			return "服务器返回信息为空!";
		}
		
		if(data.indexOf("gb2312")!=-1 || data.indexOf("GB2312")!=-1 || data.indexOf("GBK")!=-1 || data.indexOf("gbk")!=-1) {
		try {
				data = new String (data.getBytes("iso-8859-1"),"GBK");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (data.indexOf(FLAG_ADMPWD_SUCCESS)!=-1)
		{
			return "OK";
		}
		else
		{
			return "新密码不符合AD密码策略！";
		}
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}

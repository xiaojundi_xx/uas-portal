package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class AccountLockedException extends AuthenticationException
{
	public AccountLockedException() {
	}
	
	public AccountLockedException(String message)
	{
		super(message);
	}
	
	public AccountLockedException(Throwable cause)
	{
		super(cause);
	}
	  
	public AccountLockedException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

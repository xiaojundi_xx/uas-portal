package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class AccountExpiredException extends AuthenticationException
{
	public AccountExpiredException() {
	}
	
	public AccountExpiredException(String message)
	{
		super(message);
	}
	
	public AccountExpiredException(Throwable cause)
	{
		super(cause);
	}
	  
	public AccountExpiredException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

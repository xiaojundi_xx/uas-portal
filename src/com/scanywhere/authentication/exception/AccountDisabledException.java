package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class AccountDisabledException extends AuthenticationException
{
	public AccountDisabledException() {
	}
	
	public AccountDisabledException(String message)
	{
		super(message);
	}
	
	public AccountDisabledException(Throwable cause)
	{
		super(cause);
	}
	  
	public AccountDisabledException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

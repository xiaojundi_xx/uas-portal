package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class ExcessiveAttemptsException extends AuthenticationException
{
	public ExcessiveAttemptsException() {
	}
	
	public ExcessiveAttemptsException(String message)
	{
		super(message);
	}
	
	public ExcessiveAttemptsException(Throwable cause)
	{
		super(cause);
	}
	  
	public ExcessiveAttemptsException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

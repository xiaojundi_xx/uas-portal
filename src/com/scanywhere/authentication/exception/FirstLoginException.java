package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class FirstLoginException extends AuthenticationException
{
	public FirstLoginException() {
	}
	
	public FirstLoginException(String message)
	{
		super(message);
	}
	
	public FirstLoginException(Throwable cause)
	{
		super(cause);
	}
	  
	public FirstLoginException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

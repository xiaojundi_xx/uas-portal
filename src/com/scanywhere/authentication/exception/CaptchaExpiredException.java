package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class CaptchaExpiredException extends AuthenticationException
{
	public CaptchaExpiredException() {
	}
	
	public CaptchaExpiredException(String message)
	{
		super(message);
	}
	
	public CaptchaExpiredException(Throwable cause)
	{
		super(cause);
	}
	  
	public CaptchaExpiredException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

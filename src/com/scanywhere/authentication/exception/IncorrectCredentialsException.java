package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class IncorrectCredentialsException extends AuthenticationException
{
	public IncorrectCredentialsException() {
	}
	
	public IncorrectCredentialsException(String message)
	{
		super(message);
	}
	
	public IncorrectCredentialsException(Throwable cause)
	{
		super(cause);
	}
	  
	public IncorrectCredentialsException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

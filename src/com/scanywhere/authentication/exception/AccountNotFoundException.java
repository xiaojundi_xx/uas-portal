package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class AccountNotFoundException extends AuthenticationException
{
	public AccountNotFoundException() {
	}
	
	public AccountNotFoundException(String message)
	{
		super(message);
	}
	
	public AccountNotFoundException(Throwable cause)
	{
		super(cause);
	}
	  
	public AccountNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

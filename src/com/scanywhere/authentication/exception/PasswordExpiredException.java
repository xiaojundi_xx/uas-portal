package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class PasswordExpiredException extends AuthenticationException
{
	public PasswordExpiredException() {
	}
	
	public PasswordExpiredException(String message)
	{
		super(message);
	}
	
	public PasswordExpiredException(Throwable cause)
	{
		super(cause);
	}
	  
	public PasswordExpiredException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

package com.scanywhere.authentication.exception;

@SuppressWarnings("serial")
public class UnAuthorizedException extends AuthenticationException
{
	public UnAuthorizedException() {
	}
	
	public UnAuthorizedException(String message)
	{
		super(message);
	}
	
	public UnAuthorizedException(Throwable cause)
	{
		super(cause);
	}
	  
	public UnAuthorizedException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

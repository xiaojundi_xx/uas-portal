package com.scanywhere.security.subject;

import com.scanywhere.authentication.principal.Principal;

public interface Subject
{
	/**
	 * 登录用户信息
	 * 
	 * @return
	 */
	public Principal getPrincipal();
	
	/**
	 * 认证结果
	 * 
	 * @return
	 */
	public boolean isAuthenticated();
}

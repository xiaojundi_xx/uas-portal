package com.scanywhere.security.subject;

import com.scanywhere.authentication.principal.Principal;
import com.scanywhere.common.constant.ProjectConstants;

/**
 * 登录用户信息
 */
public class UserSubject implements Subject
{
	/** 登录信息 */
	private Principal principal;
	
	/** 是否认证成功 */
	private boolean authenticated = false;
	
	/** 认证方式编码 */
	private String authCode;
	
	/** 登录类型 */
	private int loginMode = ProjectConstants.LOGIN_MODE_INTRANET;
	
	/** 登录IP地址 */
	private String clientIp;
	
	/** 浏览器类型 */
	private String browserType;
	
	public UserSubject(Principal principal)
	{
		this.principal = principal;
		this.authenticated = principal == null ? false : principal.isAuthenticated();
	}
	
	public Principal getPrincipal()
	{
		return principal;
	}
	
	public boolean isAuthenticated()
	{
		return authenticated;
	}
	
	public String getAuthCode()
	{
		return authCode;
	}

	public void setAuthCode(String authCode)
	{
		this.authCode = authCode;
	}
	
	public int getLoginMode()
	{
		return loginMode;
	}

	public void setLoginMode(int loginMode)
	{
		this.loginMode = loginMode;
	}
	
	public String getClientIp()
	{
		return clientIp;
	}

	public void setClientIp(String clientIp)
	{
		this.clientIp = clientIp;
	}

	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}
	
	
	
}

package com.scanywhere.security.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.authorization.dao.RoleDao;
import com.scanywhere.modules.authorization.dao.RoleOrgDao;
import com.scanywhere.modules.authorization.dao.RoleResourceDao;
import com.scanywhere.modules.authorization.dao.RoleUserDao;
import com.scanywhere.modules.resource.dao.ResourceDao;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.user.dao.UserAccountDao;
import com.scanywhere.modules.user.entity.UserAccount;
import com.scanywhere.security.service.PortalService;

@Service("portalService")
public class PortalServiceImpl implements PortalService
{
	@Resource
	private RoleDao roleDao;
	
	@Resource
	private ResourceDao resourceDao;
	
	@Resource
	private RoleResourceDao roleResourceDao;
	
	@Resource
	private RoleUserDao roleUserDao;
	
	@Resource
	private RoleOrgDao roleOrgDao;
	
	@Resource
	private UserAccountDao userAccountDao;

	@SuppressWarnings("unchecked")
	public List<AppResource> findAuthorizedResourceList(String userId, String orgId)
	{
		// 授权的角色集合
		List<String> roleIdList = new ArrayList<String>();
		
		// 加入所有人
		roleIdList.add("4028e0816430a47f016430a47fef0000");
		
		// 查询用户授权的角色
		List<String> userRoleIdList = roleUserDao.findAuthorizedRoleList(userId);
		if (CollectionUtils.isNotEmpty(userRoleIdList))
		{
			roleIdList = (List<String>) org.apache.commons.collections.CollectionUtils.union(roleIdList, userRoleIdList);
		}
		
		// 查询机构授权的用户角色
		if (StringUtils.isNotEmpty(orgId))
		{
			List<String> orgRoleIdList = roleOrgDao.findAuthorizedRoleList(orgId);
			if (CollectionUtils.isNotEmpty(orgRoleIdList))
			{
				roleIdList = (List<String>) org.apache.commons.collections.CollectionUtils.union(roleIdList, orgRoleIdList);
			}
		}
		
		// 查询应用资源
		List<AppResource> resourceList = roleResourceDao.findAuthorizedResourceList(roleIdList.toArray(new String[0]));
		return resourceList;
	}
	
	public List<UserAccount> findResourceAccountList(String userId, String orgId, int bindingMode)
	{
		List<UserAccount> accountList = new ArrayList<UserAccount>();
		// 查询应用资源
		List<AppResource> resourceList = this.findAuthorizedResourceList(userId, orgId);
		if (CollectionUtils.isEmpty(resourceList)) return accountList;
		// 移除应用系统
		List<AppResource> removeList = new ArrayList<AppResource>();
		for (AppResource resource : resourceList)
		{
			if (resource.getEnableAccount() == CommonConstants.STATUS_DISABLED)
			{
				removeList.add(resource);
			}

			if (bindingMode == ProjectConstants.ACCOUNT_BINDING_MODE_USER)
			{
				if (StringUtils.isEmpty(resource.getSsoType()) || !resource.getSsoType().equals("form_base"))
				{
					removeList.add(resource);
				}
			}
			else if (bindingMode == ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN)
			{
				if (StringUtils.isEmpty(resource.getSsoType()) || resource.getSsoType().equals("form_base"))
				{
					removeList.add(resource);
				}
			}
		}

		if (CollectionUtils.isNotEmpty(removeList))
		{
			resourceList.removeAll(removeList);
		}

		// 查询用户应用帐号信息
		Map<String, Object> params = new HashMap<String, Object>();

		// 子帐号列表
		for (AppResource resource : resourceList)
		{
			params.put("resourceId", resource.getId());
			params.put("userId", userId);
			if (bindingMode == ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN)
			{
				params.put("bindingMode", ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN);
			}

			List<UserAccount> accList = userAccountDao.find(params);
			if (CollectionUtils.isNotEmpty(accList))
			{
				for (UserAccount userAccount : accList)
				{
					userAccount.setResourceName(resource.getResourceName());
				}
				accountList.addAll(accList);
			}
			else
			{
				UserAccount account = new UserAccount();
				account.setResourceId(resource.getId());
				account.setResourceName(resource.getResourceName());
				account.setBindingMode(bindingMode);
				accountList.add(account);
			}
		}
		return accountList;
	}

	@SuppressWarnings("unchecked")
	public boolean checkUserAuthorizedApp(String userId, String orgId, String appId)
	{
		// 授权的角色集合
		List<String> roleIdList = new ArrayList<String>();
		
		// 加入所有人
		roleIdList.add("4028e0816430a47f016430a47fef0000");
		
		// 查询用户授权的角色
		List<String> userRoleIdList = roleUserDao.findAuthorizedRoleList(userId);
		if (CollectionUtils.isNotEmpty(userRoleIdList))
		{
			roleIdList = (List<String>) org.apache.commons.collections.CollectionUtils.union(roleIdList, userRoleIdList);
		}
		
		// 查询机构授权的用户角色
		if (StringUtils.isNotEmpty(orgId))
		{
			List<String> orgRoleIdList = roleOrgDao.findAuthorizedRoleList(orgId);
			if (CollectionUtils.isNotEmpty(orgRoleIdList))
			{
				roleIdList = (List<String>) org.apache.commons.collections.CollectionUtils.union(roleIdList, orgRoleIdList);
			}
		}
		
		return roleResourceDao.checkAuthorizedApp(roleIdList.toArray(new String[0]), appId);
	}
}

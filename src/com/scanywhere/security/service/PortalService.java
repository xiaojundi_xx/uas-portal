package com.scanywhere.security.service;

import java.util.List;

import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.user.entity.UserAccount;

public interface PortalService
{
	/**
	 * 查询授权资源列表
	 * 
	 * @param userId
	 * @param orgId
	 * @return
	 */
	public List<AppResource> findAuthorizedResourceList(String userId, String orgId);
	
	/**
	 * 查询应用帐号列表
	 * 
	 * @param userId
	 * @param orgId
	 * @param bindingMode
	 * @return
	 */
	public List<UserAccount> findResourceAccountList(String userId, String orgId, int bindingMode);
	
	/**
	 * 检查用户是否有权限单点系统
	 * 
	 * @param userId
	 * @param orgId
	 * @param appId
	 * @return
	 */
	public boolean checkUserAuthorizedApp(String userId, String orgId, String appId);
}

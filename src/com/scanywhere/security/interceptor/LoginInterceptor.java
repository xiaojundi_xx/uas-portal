package com.scanywhere.security.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.scanywhere.security.SecurityLoginUtils;
import com.scanywhere.security.subject.UserSubject;

public class LoginInterceptor extends HandlerInterceptorAdapter
{
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		UserSubject subject = SecurityLoginUtils.getSubject();
		if (subject == null || !subject.isAuthenticated())
		{
			response.sendRedirect(request.getContextPath() + "/");
			return false;
		}
		return true;
	}
}

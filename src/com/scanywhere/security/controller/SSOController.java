package com.scanywhere.security.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.service.ResourceService;
import com.scanywhere.modules.resource.service.SsoParameterService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.security.SecurityLoginUtils;
import com.scanywhere.security.service.PortalService;
import com.scanywhere.security.subject.UserSubject;
import com.scanywhere.sso.SSOManager;
import com.scanywhere.sso.exception.SSOException;

@Controller
public class SSOController extends BaseController
{
	// 模块路径
	private static final String BASE_PATH = "/sso/";
		
	@Resource
	private PortalService portalService;
	
	@Resource
	private ResourceService resourceService;
	
	@Resource
	private SSOManager ssoManager;
	
	@Resource
	private SsoParameterService ssoParameterService;
	
	@RequestMapping(value = "/loadAccount")
	@ResponseBody
	private AjaxResult loadAccount(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "resourceCode") String resourceCode)
	{
		if (StringUtils.isEmpty(resourceCode))
			return super.error("resourceCode为空!");
		
		// 获取登录用户信息
		UserSubject subject = SecurityLoginUtils.getSubject();
		if (subject == null || !subject.isAuthenticated())
			return super.error("用户会话过期,请重新登录");
		
		// 登录用户信息
		User user = (User) subject.getPrincipal().getInfo();
				
		// 查询应用系统信息
		AppResource resource = resourceService.findByCode(resourceCode);
		if (resource == null)
			return super.error("未查询到应用系统信息");
		
		// 判断应用是否启用单点
		if (resource.getEnableSso() != CommonConstants.STATUS_ENABLED)
			return super.error("应用系统未启用单点登录");
		
		// 判断应用是否配置单点
		if (StringUtils.isEmpty(resource.getSsoType()))
			return super.error("应用系统未配置单点登录方式");
		
		// 查询应用单点配置
		Map<String, Object> ssoParams = ssoParameterService.findSsoParameter(resource.getId());
		if (CollectionUtils.isEmpty(ssoParams))
			return super.error("应用系统未配置单点登录参数");
		resource.setSsoParams(ssoParams);
				
		// 权限判断
		boolean authorized = portalService.checkUserAuthorizedApp(user.getId(), user.getOrgId(), resource.getId());
		if (!authorized)
			return super.error("未授权用户访问权限!");
		
		JSONObject result = new JSONObject();
		// 单点登录
		try
		{
			ssoManager.loadAccount(request, response, resource, result);
		}
		catch (SSOException e)
		{
			e.printStackTrace();
			return super.error(e.getMessage());
		}
		return super.success(result);
	}
	
	/**
	 * 单点登录
	 * 
	 * @param nav
	 * @return
	 * @throws SSOException 
	 */
	@RequestMapping(value = "/sso")
	public String sso(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "resourceCode") String resourceCode, @RequestParam(name = "ssoAccount", required = false) String ssoAccount, @RequestParam(name = "accountType", required = false) String accountType)
	{
		if (StringUtils.isEmpty(resourceCode))
		{
			return this.error("单点登录失败!", "resourceCode为空!");
		}
		
		// 获取登录用户信息
		UserSubject subject = SecurityLoginUtils.getSubject();
		if (subject == null || !subject.isAuthenticated())
		{
			super.getRequest().getSession().removeAttribute(CommonConstants.LOGIN_INFO);
			super.getRequest().getSession().invalidate();
			return "redirect:/login";
		}
		
		// 登录用户信息
		User user = (User) subject.getPrincipal().getInfo();
				
		// 查询应用系统信息
		AppResource resource = resourceService.findByCode(resourceCode);
		if (resource == null)
		{
			return this.error("单点登录失败!", "未查询到应用系统信息!");
		}
		
		// 判断应用是否启用单点
		if (resource.getEnableSso() != CommonConstants.STATUS_ENABLED)
		{
			return this.error("单点登录失败!", "应用系统未启用单点登录!");
		}
		
		// 判断应用是否配置单点
		if (StringUtils.isEmpty(resource.getSsoType()))
		{
			return this.error("单点登录失败!", "应用系统未配置单点登录方式!");
		}
		
		// 查询应用单点配置
		Map<String, Object> ssoParams = ssoParameterService.findSsoParameter(resource.getId());
		if (CollectionUtils.isEmpty(ssoParams))
		{
			return this.error("单点登录失败!", "应用系统未配置单点登录参数!");
		}
		resource.setSsoParams(ssoParams);
				
		// 权限判断
		boolean authorized = portalService.checkUserAuthorizedApp(user.getId(), user.getOrgId(), resource.getId());
		if (!authorized)
		{
			return this.error("单点登录失败!", "未授权用户访问权限!");
		}
		
		ssoAccount = StringUtils.isBlank(ssoAccount)?"":ssoAccount;
		
		// 单点登录
		try
		{
			ssoManager.doSSO(request, response, resource, ssoAccount, accountType);
		}
		catch (SSOException e)
		{
			e.printStackTrace();
			throw new SSOException(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 子帐号--注册
	 * 
	 * @return
	 */
	@RequestMapping(value = "/sso/account/reg", method = RequestMethod.GET)
	public String reg(@RequestParam(name = "resourceId", required = false) String resourceId)
	{
		if (StringUtils.isEmpty(resourceId))
		{
			throw new SSOException("应用系统ID传递为空!");
		}
		
		AppResource resourceObj = resourceService.getById(resourceId);
		super.getRequest().setAttribute("resource", resourceObj);
		return BASE_PATH + "account_reg";
	}
	
	/**
	 * 未配置子账号错误提示
	 * 
	 * @return
	 */
	@RequestMapping(value = "/sso/account/info")
	public String info(@RequestParam(name = "t", required = false, defaultValue = "1") int type)
	{
	
		return BASE_PATH + "info";
	}
	
}
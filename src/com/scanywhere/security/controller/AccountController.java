package com.scanywhere.security.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.entity.ResourceAccount;
import com.scanywhere.modules.resource.service.ResourceAccountService;
import com.scanywhere.modules.resource.service.ResourceService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.entity.UserAccount;
import com.scanywhere.modules.user.service.UserAccountService;
import com.scanywhere.security.SecurityLoginUtils;
import com.scanywhere.security.service.PortalService;

@Controller
@RequestMapping(value = "/personal/account")
public class AccountController extends BaseController
{
	// 模块路径
	private static final String BASE_PATH = "/center/account/";
	
	@Resource 
	private ResourceAccountService resourceAccountService;
	
	@Resource
	private UserAccountService userAccountService;
	
	@Resource
	private PortalService portalService;
	
	@Resource
	private ResourceService resourceService;
	
	/**
	 * 应用帐号主界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public AjaxResult list(@RequestParam(name = "bindingMode", required = false, defaultValue = "1") int bindingMode)
	{
		User user = (User) SecurityLoginUtils.getSubject().getPrincipal().getInfo();
		List<UserAccount> accList = portalService.findResourceAccountList(user.getId(), StringUtils.isEmpty(user.getOrgId()) ? null : user.getOrgId(), bindingMode);
		super.getRequest().setAttribute("accList", accList);

		return super.success(accList);
	}
	
	/**
	 * 子帐号--注册
	 * 
	 * @return
	 */
	@RequestMapping(value = "/reg", method = RequestMethod.GET)
	public String reg(@RequestParam(name = "resourceId", required = false) String resourceId)
	{
		// 登录用户信息
		User user = (User) SecurityLoginUtils.getSubject().getPrincipal().getInfo();
		
		// 查询授权的应用系统
		AppResource resourceObj = null;
		List<AppResource> resourceList = portalService.findAuthorizedResourceList(user.getId(), StringUtils.isEmpty(user.getOrgId()) ? null : user.getOrgId());
		if (CollectionUtils.isNotEmpty(resourceList))
		{
			List<AppResource> removeList = new ArrayList<AppResource>();
			for (AppResource resource : resourceList)
			{
				if (resource.getEnableAccount() != CommonConstants.STATUS_ENABLED)
				{
					removeList.add(resource);
				}
			}
			
			if (CollectionUtils.isNotEmpty(removeList))
			{
				resourceList.removeAll(removeList);
			}
			
			// 选择的应用系统
			if (StringUtils.isEmpty(resourceId))
			{
				resourceObj = resourceList.get(0);
			}
			else
			{
				resourceObj = resourceService.getById(resourceId);
			}
			super.getRequest().setAttribute("resource", resourceObj);
		}
		
		super.getRequest().setAttribute("resourceList", resourceList);
		return BASE_PATH + "account_reg";
	}
	
	/**
	 * 子帐号--保存
	 * 
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResult save(@RequestParam(name = "formData") String formData)
	{
		// 将页面提交的数据转换成JSONObject对象
		JSONObject jsonObject = JSON.parseObject(formData);
		
		// 将JSONObject对象转换成实体对象
		ResourceAccount account = jsonObject.toJavaObject(ResourceAccount.class);
		
		// 判断是否为空
		if (account == null)
		{
			return super.error("页面提交数据错误!");
		}
		
		// 保存
		String message = resourceAccountService.saveAccount(account);
		if (StringUtils.isNotEmpty(message))
		{
			return super.error(message);
		}
		return this.success();
	}
	
	/**
	 * 子帐号--详细信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(@RequestParam(name = "id") String id)
	{
		// 判断页面传递参数是否为空
		if (StringUtils.isEmpty(id))
		{
			return this.error("页面提交数据错误!", "ID不许允许传空值!");
		}
		
		// 查询详细配置
		UserAccount userAccount = userAccountService.getById(id);
		if (userAccount == null)
		{
			return this.error("数据错误!", "未查询到子账号信息!");
		}
		
		// 查询子账号
		ResourceAccount account = resourceAccountService.getById(userAccount.getAccountId());
		super.getRequest().setAttribute("account", account);
		
		// 查询子帐号应用信息
		AppResource resource = resourceService.getById(account.getResourceId());
		super.getRequest().setAttribute("resource", resource);
		return BASE_PATH + "account_edit";
	}
	
	/**
	 * 子帐号--更新
	 * 
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResult update(@RequestParam(name = "formData") String formData)
	{
		// 将页面提交的数据转换成JSONObject对象
		JSONObject jsonObject = JSON.parseObject(formData);
		
		// 将JSONObject对象转换成实体对象
		ResourceAccount account = jsonObject.toJavaObject(ResourceAccount.class);
		
		// 判断是否为空
		if (account == null || StringUtils.isEmpty(account.getId()))
		{
			return super.error("页面提交数据错误!");
		}
		
		// 更新
		String message = resourceAccountService.updateAccount(account);
		if (StringUtils.isNotEmpty(message))
		{
			return super.error(message);
		}
		return this.success();
	}
	
	/**
	 * 接触绑定子帐号
	 * 
	 * @param roleId
	 * @param userIds
	 * @return
	 */
	@RequestMapping(value = "/deleteAuthorizeUser", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResult deleteAuthorizeUser(@RequestParam(name = "id") String id)
	{
		// 判断页面传递参数是否为空
		if (StringUtils.isEmpty(id))
		{
			return super.error("参数不许允许传空值!");
		}
		
		// 删除用户授权
		String message = resourceAccountService.deleteAccount(id);
		if (StringUtils.isNotEmpty(message))
		{
			return super.error(message);
		}
		return this.success();
	}
}

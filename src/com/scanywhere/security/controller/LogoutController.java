package com.scanywhere.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.sso.SSORequest;

/**
 * 用户登出
 */
@Controller
public class LogoutController extends BaseController
{
	/**
	 * 处理退出请求
	 * 
	 * @return
	 */
	@RequestMapping(value = "/logout")
	@ResponseBody
	private AjaxResult logout()
	{
		super.getRequest().getSession().removeAttribute(CommonConstants.LOGIN_INFO);
		super.getRequest().getSession().removeAttribute(SSORequest.UAS_SSO_REQUEST_KEY);
		super.getRequest().getSession().invalidate();
		while(true){
			if(super.getRequest().getSession().isNew()){
				System.out.println("销毁session成功");
				break;
			}
			System.out.println("重新销毁session");
			try {
			    Thread.sleep(1000);
			}
			catch (InterruptedException e) {
			    e.printStackTrace();
			}
		}
		return super.success();
	}
}

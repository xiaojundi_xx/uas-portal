package com.scanywhere.security.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.AuthenticationManager;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.authentication.handler.AuthenticationHandler;
import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.captcha.CaptchaUtils;
import com.scanywhere.common.codec.AESUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.network.IpUtils;
import com.scanywhere.common.utils.FileUtils;
import com.scanywhere.common.utils.RandomUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.constant.LogConstant;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.system.service.SysConfigService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.service.UserService;
import com.scanywhere.security.SecurityLoginUtils;
import com.scanywhere.security.subject.UserSubject;
import com.scanywhere.sso.SSORequest;

/**
 * 用户登录
 */
@Controller
public class LoginController extends BaseController
{
	private final static String CLIENTNAME = "USOSetup.exe";
	private final static String FILEPATH = FileUtils.getAbsolutePath("../USOSetup.exe");
	private static long fileModifyTime = 0;
	private static byte[] bytes = null;
	
	static{
		Date lastModifyDate = FileUtils.lastModifiedTime(FILEPATH);
		if(lastModifyDate != null){
			fileModifyTime = lastModifyDate.getTime();
			try {
				bytes = FileUtils.readBytes(new File(FILEPATH));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Resource
	private SysConfigService sysConfigService;
	
	@Resource
	private AuthenticationManager authenticationManager;
	
	@Resource
	private UserService userService;
	

	/**
	 * 获取随机数
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadVerifyRandom", method = RequestMethod.GET)
	@ResponseBody
	private AjaxResult loadVerifyRandom(HttpServletRequest request, HttpServletResponse response)
	{
		// 产生认证随机数
		String random;
		try {
			random = RandomUtils.generateString(8);
		} catch (Exception e) {
			e.printStackTrace();
			return super.error("获取服务器消息失败,请重新访问");
		}

		// 设置到会话中
		request.getSession().setAttribute("sessionRandom", random);

		// 设置到请求中
		return super.success(random);
	}
	
	/**
	 * 获取认证方式
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadAuth", method = RequestMethod.GET)
	@ResponseBody
	private AjaxResult loadAuth(HttpServletRequest request, HttpServletResponse response){
		// 查询访问策略
		Map<String, Object> configMap = sysConfigService.findSysConfig(ProjectConstants.SYS_CONFIG_MODULE_ACCESS_POLICY);

		// 启用时间策略
		int timeAccessPolicy = StringUtils.isEmpty((String) configMap.get("timeAccessPolicy")) ? CommonConstants.STATUS_DISABLED : Integer.parseInt((String) configMap.get("timeAccessPolicy"));
		if (timeAccessPolicy == CommonConstants.STATUS_ENABLED)
		{
			String accessTime = StringUtils.isEmpty((String) configMap.get("accessTime")) ? "" : (String) configMap.get("accessTime");
			if (StringUtils.isNotEmpty(accessTime) && !timeValidate(accessTime))
			{
				return super.error("此时间禁止访问!");
			}
		}

		// 启用地址策略
		int ipAccessPolicy = StringUtils.isEmpty((String) configMap.get("ipAccessPolicy")) ? CommonConstants.STATUS_DISABLED : Integer.parseInt((String) configMap.get("ipAccessPolicy"));
		if (ipAccessPolicy == CommonConstants.STATUS_ENABLED)
		{
			String ipAddress = StringUtils.isEmpty((String) configMap.get("ipAddress")) ? "" : (String) configMap.get("ipAddress");
			String clientIp = IpUtils.getRemoteAddr(super.getRequest());
			if (StringUtils.isNotEmpty(ipAddress) && !IpUtils.isLocalAddr(clientIp))
			{
				String[] ipList = ipAddress.split(";");
				boolean result = false;
				for (int i = 0; i < ipList.length; i++) 
				{
					if (clientIp.equals(ipList[i])) 
					{
						result = true;
						break;
					}
					else if (ipList[i].indexOf("/") != -1)
					{
						if (IpUtils.isInRange(clientIp, ipList[i]))
						{
							result = true;
							break;
						}
					}
				}

				if(!result)
				{
					return super.error("此IP禁止访问!");
				}
			}
		}
		// 获取应用系统信息
		SSORequest ssoRequest = new SSORequest(request);
		request.getSession().setAttribute(SSORequest.UAS_SSO_REQUEST_KEY, ssoRequest);
		JSONObject obj = new JSONObject();
		// 获取登录用户信息
		UserSubject subject = SecurityLoginUtils.getSubject();
		if (subject != null && subject.isAuthenticated())
		{
			// 已登录用户
			if (ssoRequest != null && StringUtils.isNotEmpty(ssoRequest.getResourceCode()))
			{
				obj.put("redirect", "redirect:/sso" + (!StringUtils.isEmpty(ssoRequest.getQueryStr()) ? "?" + ssoRequest.getQueryStr() : ""));
			}else {
				obj.put("redirect", "redirect:/main");
			}
			return super.success(obj);
		}
		
		try {
			authenticationManager.loadAuthCode(request, response, obj);
		} catch (AuthenticationException e) {
			e.printStackTrace();
			return super.error(e.getMessage(), obj);
		}
		
		return super.success(obj);
	}
	
	/**
	 * 登录页入口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	@ResponseBody
	private AjaxResult login(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "authCode", required = false) String authCode)
	{
		JSONObject obj = new JSONObject();
		
		if(obj.size()==0)
		{
			try
			{
				AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, "password_auth");
				if(handler != null){
					obj.put("minLength", handler.getParameterToInt("minLength", 4));
					obj.put("maxLength", handler.getParameterToInt("maxLength", 20));
					obj.put("pwdComplexity", handler.getParameterToInt("pwdComplexity", 1));
				}
				obj.put("authCode", authCode);
				authenticationManager.loginPage( request,  response,  authCode,  obj);
			}
			catch (AuthenticationException e)
			{
				e.printStackTrace();
				return super.error("初始化登录页面失败!");
			}
		}
		
		return super.success(obj);
	}
	
	/**
	 * 处理登录请求
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	private AjaxResult auth(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "authCode") String authCode)
	{
		JSONObject result = new JSONObject();
		boolean loginResult = authenticationManager.doAuthentication(request, response, authCode, result);
		if (loginResult)
		{
			// 认证成功
			SSORequest ssoRequest = (SSORequest) request.getSession().getAttribute(SSORequest.UAS_SSO_REQUEST_KEY);
			if (ssoRequest != null && StringUtils.isNotEmpty(ssoRequest.getResourceCode()))
			{
				result.put("redirect","redirect:/sso" + (!StringUtils.isEmpty(ssoRequest.getQueryStr()) ? "?" + ssoRequest.getQueryStr() : ""));
			}else {
				super.getRequest().getSession().removeAttribute("userPassword");
				result.put("redirect", "redirect:/main");
			}
			return super.success(result);
		}
		return super.error(result);
	}

	/**
	 * 获取验证码图片和文本(验证码文本会保存在HttpSession)
	 */
	@RequestMapping("/captcha")
	public void captcha(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		// 设置页面不缓存
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		// 设置输出的内容的类型为JPEG图像
		response.setContentType("image/jpeg");

		// 生成验证码
		String captcha = CaptchaUtils.generateCaptcha();

		// 将验证码放到HttpSession里面
		request.getSession().setAttribute(CommonConstants.LOGIN_CAPTCHA, captcha);

		BufferedImage bufferedImage = CaptchaUtils.generateImage(captcha, 64, 34);

		ImageIO.write(bufferedImage, "JPEG", response.getOutputStream());
	}

	/**
	 * 获取验证码图片和文本(验证码文本会保存在HttpSession)
	 */
	@RequestMapping("/captchacheck")
	public void captchacheck(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		// 设置页面不缓存
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setCharacterEncoding("UTF-8");

		// 设置输出的内容的类型为JPEG图像
		response.setContentType("application/json");

		// 从HttpSession里面取captcha
	    String captcha = (String)request.getSession().getAttribute(CommonConstants.LOGIN_CAPTCHA);
	    String tocheck = request.getParameter("captcha");
	    String body = "{\"valid\":false}";
	    if(captcha!=null && tocheck!=null && captcha.equalsIgnoreCase(tocheck))
	    {
	      body = "{\"valid\": true}";
	    }
	    response.getOutputStream().write(body.getBytes("UTF-8"));
	}
	/**
	 * 重定向登录页入口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/nologin", method = RequestMethod.GET)
	private String nologin()
	{
		return "";
	}
	
	/**
	 * 检查驱动文件
	 * 
	 * @return
	 */
	@RequestMapping(value = "/downloadClient", method = RequestMethod.GET)
	private ResponseEntity<byte[]> downloadClient(HttpServletRequest request, HttpServletResponse response)
	{
		Date lastModifyDate = FileUtils.lastModifiedTime(FILEPATH);
		if(lastModifyDate == null){
			return null;
		}
		
		if(lastModifyDate.getTime()!=fileModifyTime || bytes==null){
			fileModifyTime = lastModifyDate.getTime();
			try {
				bytes = FileUtils.readBytes(new File(FILEPATH));
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Pragma", "No-cache");
		headers.set("Cache-Control", "No-cache");
		headers.set("Expires", "0");
		headers.setContentDispositionFormData("attachment", CLIENTNAME);
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		return new ResponseEntity<byte[]>(bytes,headers, HttpStatus.OK);
	}
	
	/**
	 * 强制修改密码跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "/updatePwd", method = RequestMethod.GET)
	private String updatePwd(@RequestParam(name = "loginName") String loginName, @RequestParam(name = "force", required = false, defaultValue = "false") Boolean force, @RequestParam(name = "firstLogin", required = false, defaultValue = "false") Boolean firstLogin, @RequestParam(name = "errorMsg", required = false) String errorMsg)
	{
		// 设置登录名, 判断是否强制修改密码及首次登录
		super.getRequest().setAttribute("loginName", loginName);
		super.getRequest().setAttribute("force", force);
		super.getRequest().setAttribute("firstLogin", firstLogin);
		if(!StringUtils.isBlank(errorMsg)){
			super.getRequest().setAttribute("errorMsg", errorMsg);
			/*
			try {
				super.getRequest().setAttribute("errorMsg", new String(errorMsg.getBytes("iso8859-1"),"UTF-8"));
			} catch (UnsupportedEncodingException e) {
				super.getRequest().setAttribute("errorMsg", "请求参数异常!");
				e.printStackTrace();
			}
			*/
		}
		
		// 获取认证方式实现
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, "password_auth");
		if(handler != null){
			super.getRequest().setAttribute("minLength", handler.getParameterToInt("minLength", 4));
			super.getRequest().setAttribute("maxLength", handler.getParameterToInt("maxLength", 20));
			super.getRequest().setAttribute("pwdComplexity", handler.getParameterToInt("pwdComplexity", 1));
		}
		return "updatePwd";
	}
	
	/**
	 * 修改密码
	 * 
	 * @return
	 */
	@RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
	@ResponseBody
	private AjaxResult updatePwd(@RequestParam(name = "loginName") String loginName, @RequestParam(name = "old_pwd") String old_pwd, @RequestParam(name = "new_pwd") String new_pwd)
	{
		String key = loginName;
		int length = key.length();
		if(length > 16)
		{
			key = key.substring(0,16);
		}
		else if(length < 16)
		{
			while (length < 16) 
			{
				key = key + "0";
				length++;
			}
		}
		new_pwd = AESUtils.decrypt(new_pwd, key);
		
		if(StringUtils.isBlank(old_pwd)){
			try {
				old_pwd = super.getRequest().getSession().getAttribute("userPassword").toString();
			} catch (Exception e) {
				e.printStackTrace();
				return super.error("非法进入修改密码页面,禁止修改!");
			}
		}else {
			old_pwd = AESUtils.decrypt(old_pwd, key);
		}
		// 查询用户信息
		User user = userService.findByLoginName(loginName);
		if (user == null)
		{
			return super.error("未查询到用户!");
		}
		
		// 设置帐号
		super.getRequest().setAttribute("loginName", loginName);
		
		if (StringUtils.isEmpty(new_pwd))
		{
			return super.error("新密码不允许为空!");
		}
		
		// 更新密码
		String message = userService.updatePassword(user.getId(), old_pwd, new_pwd);
		if (!StringUtils.isBlank(message))
		{
			return super.error(message);
		}
		
		// 记录日志
		LogUtils.me().saveUserLog(user, LogConstant.ACTION_TYPE_UPDATE_PASSWORD, "用户密码", LogConstant.SUCCESS, IpUtils.getRemoteAddr(super.getRequest()), null);
//		super.getRequest().getSession().removeAttribute("modifyCause");
		super.getRequest().getSession().removeAttribute("userPassword");
		return super.success();
	}
	
	/**
	 * 时间校验
	 * 
	 * @param timeParam
	 * @return
	 */
	private static boolean timeValidate(String timeParam) 
	{
		boolean state = false;
		
		Date now = DateUtils.date();
		String nyr = DateUtils.getDate();
		String week = DateUtils.date().toString().substring(0, 3).toLowerCase();
		if (timeParam.indexOf(week) == -1) 
		{
			state = false;
		} 
		else 
		{
			String timeArr[] = timeParam.split(";");
			for (int i = 0; i < timeArr.length; i++) 
			{
				String tmp = timeArr[i].trim();
				if (tmp.startsWith(week)) 
				{
					String s[] = tmp.split("@");
					String start = s[1];
					String end = s[2];
					if (start.equals("00:00") && end.equals("00:00"))
					{
						return true;
					}
					
					Date sdate = DateUtils.parse(nyr + " " + start + ":00");
					Date edate = DateUtils.parse(nyr + " " + end + ":00");
					
					if (now.after(sdate) && now.before(edate)) 
					{
						state = true;
					} 
					else 
					{
						state = false;
					}
				}
			}
		}
		return state;
	}
}

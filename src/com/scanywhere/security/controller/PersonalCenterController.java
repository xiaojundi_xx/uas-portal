package com.scanywhere.security.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.handler.AuthenticationHandler;
import com.scanywhere.authentication.principal.CertificateCredentials;
import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.codec.AESUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.RandomUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.core.constant.LogConstant;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.service.UserService;
import com.scanywhere.security.SecurityLoginUtils;
import com.scanywhere.security.subject.UserSubject;

@Controller
@RequestMapping(value = "/personal")
public class PersonalCenterController extends BaseController
{
	// 模块路径
	private static final String BASE_PATH = "/center/";
	
	@Resource
	private UserService userService;

	@RequestMapping(value = "/toMain")
	public String toMain(@RequestParam(name = "nav", required = false) String nav)
	{
		super.getRequest().setAttribute("nav", nav);
		return BASE_PATH + "toMain";
	}
	
	/**
	 * 个人中心主界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/main")
	public String main(@RequestParam(name = "nav", required = false) String nav)
	{
		super.getRequest().setAttribute("nav", nav);
		return BASE_PATH + "main";
	}
	
	/**
	 * 个人中心头页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/header")
	public String header()
	{
		return BASE_PATH + "header";
	}
	
	/**
	 * 个人中心头页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/navigation")
	public String navigation(@RequestParam(name = "nav", required = false) String nav)
	{
		super.getRequest().setAttribute("nav", nav);
		return BASE_PATH + "navigation";
	}
	
	/**
	 * 个人中心 -- 个人信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/info/view")
	@ResponseBody
	public AjaxResult view()
	{
		// 登录用户信息
		User user = userService.getById(SecurityLoginUtils.getSubject().getPrincipal().getId());
		if (user==null)
		{
			return super.error("系统状态异常");
		}
		return super.success(user);
	}
	
	/**
	 * 个人中心 -- 修改信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/info/updateInfo", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResult updateInfo(@RequestParam(name = "formData") String formData)
	{
		// 将页面提交的数据转换成JSONObject对象
		JSONObject jsonObject = JSON.parseObject(formData);
		
		// 将JSONObject对象转换成实体对象
		User user = jsonObject.toJavaObject(User.class);
		if (user == null || StringUtils.isEmpty(user.getId()) || !user.getId().equals(SecurityLoginUtils.getSubject().getPrincipal().getId()))
		{
			return super.error("页面提交数据错误!");
		}
		
		// 更新用户信息
		String message = userService.updateUser(user, jsonObject);
		if (StringUtils.isNotEmpty(message))
		{
			return super.error(message);
		}
		return super.success();
	}
	
	/**
	 * 个人中心 -- 修改密码
	 * 
	 * @return
	 */
	@RequestMapping(value = "/info/password")
	@ResponseBody
	public AjaxResult password()
	{
		// 获取登录用户信息
		UserSubject subject = SecurityLoginUtils.getSubject();

		String authCode = "password_auth";
		if(subject.isAuthenticated() && !StringUtils.isBlank(subject.getAuthCode()))
		{
			authCode = subject.getAuthCode();
		}
		// 获取认证方式实现
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, authCode);
		JSONObject info = new JSONObject();
		info.put("minLength", handler.getParameterToInt("minLength", 4));
		info.put("maxLength", handler.getParameterToInt("maxLength", 30));
		info.put("pwdComplexity", handler.getParameterToInt("pwdComplexity", 1));
		info.put("authCode", authCode);
		info.put("loginName", ((User) SecurityLoginUtils.getSubject().getPrincipal().getInfo()).getLoginName());
		return super.success(info);
	}
	@RequestMapping(value = "/info/succeed")
	public String passwordSucceed()
	{
		return BASE_PATH + "/info/succeed";
	}
	
	/**
	 * 个人中心 -- 修改密码
	 * 依据旧密码
	 * @return
	 */
	@RequestMapping(value = "/info/updatePasswordByPWD", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResult updatePasswordByPWD(@RequestParam(name = "old_pwd") String old_pwd, @RequestParam(name = "new_pwd") String new_pwd)
	{
		// 获取登录用户信息
		UserSubject subject = SecurityLoginUtils.getSubject();
		
		// 校验提交数据
		if (StringUtils.isEmpty(old_pwd))
		{
			return super.error("旧密码不允许为空!");
		}
		
		if (StringUtils.isEmpty(new_pwd))
		{
			return super.error("新密码不允许为空!");
		}
		
		String key = null;
		try {
			key = ((User)SecurityLoginUtils.getSubject().getPrincipal().getInfo()).getLoginName();
		} catch (Exception e) {
			e.printStackTrace();
			return super.error("用户信息有误，请重新登录!");
		}
		int length = key.length();
		if(length > 16)
		{
			key = key.substring(0,16);
		}
		else if(length < 16)
		{
			while (length < 16) 
			{
				key = key + "0";
				length++;
			}
		}
		
		old_pwd = AESUtils.decrypt(old_pwd, key);
		new_pwd = AESUtils.decrypt(new_pwd, key);
		
		// 更新密码
		String message = userService.updatePassword(subject.getPrincipal().getId(), old_pwd, new_pwd);
		if (StringUtils.isNotEmpty(message))
		{
			return super.error(message);
		}
		
		// 记录日志
		LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_UPDATE_PASSWORD, "用户密码", LogConstant.SUCCESS, null);
		return super.success();
	}
	
	@RequestMapping(value = "/info/getCode", method = RequestMethod.POST)
	@ResponseBody
	private AjaxResult getCode(HttpServletRequest request, HttpServletResponse response)
	{
		// 产生随机数
		Object random = RandomUtils.generateString(8);

		// 设置到会话中
		request.getSession().setAttribute("sessionRandom", random.toString());

		return super.success(random);
	}
	
	/**
	 * 个人中心 -- 修改密码
	 * 依据证书
	 * @return
	 */
	@RequestMapping(value = "/info/updatePasswordByCert", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResult updatePasswordByCert(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(name = "cn") String cn,
			@RequestParam(name = "cert") String cert,
			@RequestParam(name = "verifyRandom") String verifyRandom,
			@RequestParam(name = "signature") String signature, 
			@RequestParam(name = "new_pwd") String new_pwd)
	{

		if(StringUtils.isBlank(cn) || StringUtils.isBlank(signature) || StringUtils.isBlank(cert) || StringUtils.isBlank(verifyRandom))
		{
			return super.error("请求信息不完整!");
		}
		
		String sessionRandom = (String) request.getSession().getAttribute("sessionRandom");
		if (StringUtils.isEmpty(verifyRandom) || StringUtils.isEmpty(sessionRandom) || !verifyRandom.equals(sessionRandom)) 
		{
			return super.error("身份验证未通过!");
		}
		// 获取登录用户信息
		UserSubject subject = SecurityLoginUtils.getSubject();
		
		// 判断证书认证是否启用
		if (CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, "certificate_auth") == null)
		{
			return super.error("认证方式未启用, 请选择其他方式!");
		}
		CertificateCredentials credentials = new CertificateCredentials();
		credentials.setCert(cert);
		credentials.setCn(cn);
		credentials.setSignature(signature);
		credentials.setVerifyRandom(verifyRandom);
		
		// 校验提交数据
		if (StringUtils.isBlank(new_pwd))
		{
			return super.error("新密码不允许为空!");
		}
		
		// 更新密码
		String message = userService.updatePasswordByCert(subject.getPrincipal().getId(), new_pwd, credentials);
		if (StringUtils.isNotEmpty(message))
		{
			return super.error(message);
		}
		
		// 记录日志
		LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_UPDATE_PASSWORD, "用户密码", LogConstant.SUCCESS, null);
		return super.success();
	}
}

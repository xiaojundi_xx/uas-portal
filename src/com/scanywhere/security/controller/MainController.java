package com.scanywhere.security.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.handler.AuthenticationHandler;
import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.support.Convert;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.authentication.service.AuthConfigService;
import com.scanywhere.modules.authentication.service.AuthParameterService;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.service.ResourceIconService;
import com.scanywhere.modules.resource.service.ResourceService;
import com.scanywhere.modules.resource.service.SsoParameterService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.entity.UserAccount;
import com.scanywhere.modules.user.service.UserAccountService;
import com.scanywhere.security.SecurityLoginUtils;
import com.scanywhere.security.service.PortalService;
import com.scanywhere.security.subject.UserSubject;

@Controller
public class MainController extends BaseController
{
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);

	@Resource
	private ResourceService resourceService;

	@Resource
	private ResourceIconService resourceIconService;

	@Resource
	private PortalService portalService;

	@Resource
	private AuthConfigService authConfigService;

	@Resource
	private AuthParameterService authParameterService;

	@Resource
	private UserAccountService userAccountService;

	@Resource
	private SsoParameterService ssoParameterService;
	
	public final static long EXPRIES_TIME = 1000L * 3600 * 24 * 365 * 20;
	public static Map<String, BufferedImage> iconMap = new HashMap<String,BufferedImage>();
	public static long iconTime = System.currentTimeMillis();
	
	private Map<String, BufferedImage> generate(){
		if(iconMap.isEmpty())
			init();
		return iconMap;
	}
	
	private void init(){
		iconMap = resourceIconService.loadDate();
	}

	/**
	 * 平台主页
	 * 
	 * @return
	 */
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	@ResponseBody
	public AjaxResult index()
	{
		// 获取登录用户信息
		JSONObject result = new JSONObject();
		UserSubject subject = SecurityLoginUtils.getSubject();
		if (subject == null || !subject.isAuthenticated())
		{
			super.getRequest().getSession().removeAttribute(CommonConstants.LOGIN_INFO);
			super.getRequest().getSession().invalidate();
			return super.error("redirect:/login");
		}

		// 会话中的用户信息
		User user = (User) subject.getPrincipal().getInfo();

		// 获取认证方式实现
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, subject.getAuthCode());
		// 密码认证
		if (subject.getAuthCode().equals("password_auth"))
		{
			// 密码有效期
			int pwdValidity = handler.getParameterToInt("pwdValidity", CommonConstants.INT_VALUE_ZERO);
			if (pwdValidity != CommonConstants.INT_VALUE_ZERO && user.getPwdModifyTime() != null)
			{
				// 预提醒时间间隔
				int expirePrompt = handler.getParameterToInt("expirePrompt", 7);

				// 当前时间
				Date now = DateUtils.date();

				// 过期时间差
				int distanceDay = Convert.toInt(DateUtils.getDistanceOfTwoDate(now, DateUtils.getDate(user.getPwdModifyTime(), pwdValidity)));
				if (distanceDay <= expirePrompt)
				{
					JSONObject task = new JSONObject();
					task.put("expirePrompt", true);
					task.put("distanceDay", distanceDay);
					result.put("task", task);
				}
			}
		}

		// 查询授权的应用系统
		List<AppResource> resourceList = portalService.findAuthorizedResourceList(user.getId(), StringUtils.isEmpty(user.getOrgId()) ? null : user.getOrgId());
		if (CollectionUtils.isNotEmpty(resourceList))
		{
			userAccountService = (UserAccountService) SpringContextUtils.getBean("userAccountService");
			Map<String, Object> ssoParams;
			List<UserAccount> accountList = null;
			int bindingMode = 0;
			int usableAccount = 0;
			String accountName = null;
			for (AppResource resource : resourceList)
			{
				// 验证应用访问级别
				int powerCode = handler.getParameterToInt("powerCode");
				if ((resource.getAuthPolicy() & powerCode) == 0)
				{
					resource.setAllowAccess(1);
				}
				// 不允许公网用户访问
				if (resource.getAllowVpnUser() == CommonConstants.STATUS_1 && subject.getLoginMode() == ProjectConstants.LOGIN_MODE_PUBLIC_NETWORK)
				{
					if (resource.getAllowAccess() == 0)
					{
						resource.setAllowAccess(2);
					}
				}

				//获取应用配置
				ssoParams = ssoParameterService.findSsoParameter(resource.getId());
				if (CollectionUtils.isEmpty(ssoParams)) 
					usableAccount = 0;
				else{
					//若应用不启用子账号，直接使用主账号
					if(resource.getEnableAccount() == 1){
						usableAccount = 1;
						accountName = user.getLoginName();
					}else{
						bindingMode = ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN;
						if("form_base".equals(resource.getSsoType()))
							bindingMode = (ProjectConstants.ACCOUNT_BINDING_MODE_USER | ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN);

						//获取子账号列表
						accountList = userAccountService.findUserAccount(resource.getId(), user.getId(), bindingMode);

						usableAccount = accountList.size();
						
						if(usableAccount == 1){
							accountName = accountList.get(0).getAccountName();
						}

						//判断是否强制使用子账号
						if(ssoParams.containsKey("forceResourceAccount") &&
								Convert.toInt(ssoParams.get("forceResourceAccount"), CommonConstants.STATUS_1)==1)
						{
							usableAccount++;
							if(usableAccount == 1){
								accountName = user.getLoginName();
							}
						}
						
					}
				}
				
				//判断是否有可用账号，是否需要选择登录账号
				if(usableAccount > 0){
					resource.setTempField1("usableAccount");
					if(usableAccount == 1){
						resource.setTempField2("singleAccount");
						//将唯一可用账号保存
						resource.setTempField3(accountName);
					}
				}
			}
		}
		
		result.put("iconTime", iconTime);
		String resourceCode = (String) super.getRequest().getParameter("resourceCode");
		result.put("resourceCode", resourceCode);
		result.put("resourceList", resourceList);
		user.setUserPassword(null);
		result.put("user", user);
		return super.success(result);
	}
	
	@RequestMapping("/appIcon")
	public void appIcon(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "resourceCode") String resourceCode) throws IOException
	{
		// 设置页面使用缓存
		response.setHeader("Pragma", "Pragma");
		response.setHeader("Cache-Control", "public");
		response.setDateHeader("Expires", System.currentTimeMillis() + EXPRIES_TIME);
		// 设置页面不使用缓存
		/*
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		 */
		// 设置输出的内容的类型为PNG图像
		response.setContentType("image/png");

		ImageIO.write(generate().get(resourceCode), "png", response.getOutputStream());
	}

}

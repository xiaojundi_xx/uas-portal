package com.scanywhere.security.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scanywhere.common.ajax.AjaxResult;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.modules.audit.entity.SsoLog;
import com.scanywhere.modules.audit.entity.UserLog;
import com.scanywhere.modules.audit.service.SsoLogService;
import com.scanywhere.modules.audit.service.UserLogService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.security.SecurityLoginUtils;

@Controller
@RequestMapping(value = "/personal/log")
public class LogController extends BaseController
{
	// 模块路径
	private static final String BASE_PATH = "/center/log/";
	
	@Resource
	private SsoLogService ssoLogService;
	@Resource
	private UserLogService userLogService;
	/**
	 * 安全日志主界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list")
	public String list()
	{
		return BASE_PATH + "list";
	}
	@RequestMapping(value = "/userList")
	public String userList()
	{
		return BASE_PATH + "userList";
	}
	
	/**
	 * 日志列表--数据
	 * 
	 * @param reqObj
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/loadData")
	@ResponseBody
	public Map<String, Object> loadData(@RequestParam(name = "reqObj") String reqObj) throws Exception
	{
		return ssoLogService.loadData(reqObj);
	}
	
	/**
	 * 日志列表--用户数据
	 * 
	 * @param reqObj
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/userLoadData")
	@ResponseBody
	public Map<String, Object> userLoadData(@RequestParam(name = "reqObj") String reqObj) throws Exception
	{
		return userLogService.loadData(reqObj);
	}
	
	/**
	 * 日志--详细信息
	 * 
	 * @return
	 */
	/*@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable(name = "id") String id)
	{
		// 判断页面传递参数是否为空
		if (StringUtils.isEmpty(id))
		{
			this.error("页面提交数据错误!", "ID不许允许传空值!");
			return "/error/info";
		}
		
		// 查询详细配置
		UserLog log = userLogService.getById(id);
		if (log == null)
		{
			this.error("数据错误!", "未查询到日志详细信息!");
			return "/error/info";
		}
		super.getRequest().setAttribute("log", log);
		return BASE_PATH + "view";
	}*/
	
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@ResponseBody
	public AjaxResult view(@PathVariable(name = "id") String id)
	{
		// 登录用户信息
		UserLog log = userLogService.getById(id);
		if (log==null)
		{
			return super.error("系统状态异常");
		}
		return super.success(log);
	}
	
	/**
	 * 日志--详细信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/viewLogin/{id}", method = RequestMethod.GET)
	@ResponseBody
	public AjaxResult viewLogin(@PathVariable(name = "id") String id)
	{
		/*// 判断页面传递参数是否为空
		if (StringUtils.isEmpty(id))
		{
			this.error("页面提交数据错误!", "ID不许允许传空值!");
			return "/error/info";
		}
		*/
		// 查询详细配置
		SsoLog log = ssoLogService.getById(id);
		if (log==null)
		{
			return super.error("系统状态异常");
		}
		return super.success(log);
	}
	
}

package com.scanywhere.security.controller;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.common.codec.AESUtils;
import com.scanywhere.common.codec.MD5Utils;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.modules.audit.service.SsoLogService;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.entity.SsoToken;
import com.scanywhere.modules.resource.service.ResourceService;
import com.scanywhere.modules.resource.service.SsoTokenService;
import com.scanywhere.modules.user.entity.Org;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.service.OrgService;
import com.scanywhere.modules.user.service.UserService;

@Controller
@RequestMapping(value = "/")
public class TokenValidateController extends BaseController
{
	@Resource
	private ResourceService resourceService;
	
	@Resource
	private SsoTokenService ssoTokenService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private OrgService orgService;
	
	@Resource
	private SsoLogService ssoLogService;
	
	/**
	 * Token验证
	 * 
	 * @return
	 */
	@RequestMapping(value={"/api/sso/validate_token","/portal/api/sso/validate_token"})
	@ResponseBody
	public String validateToken(@RequestParam(name = "resource_code") String resource_code, @RequestParam(name = "token") String token)
	{
		// 验证参数
		if (StringUtils.isEmpty(resource_code))
		{
			return this.buildErrorMsg("10001", "resource_code为空!");
		}
		
		if (StringUtils.isEmpty(token))
		{
			return this.buildErrorMsg("10002", "token为空!");
		}
		
		// 查询应用系统信息
		AppResource resource = resourceService.findByCode(resource_code);
		if (resource == null)
		{
			System.out.println(this.buildErrorMsg("10003", "应用系统未注册!"));
			return this.buildErrorMsg("10003", "应用系统未注册!");
		}
		
		// 查询Token信息
		SsoToken ssoToken = ssoTokenService.findByToken(token);
		if (ssoToken == null)
		{
			return this.buildErrorMsg("10004", "token无效!");
		}
		
		// 验证Token是否过期
		if (DateUtils.date().after(ssoToken.getExpiredTime()))
		{
			ssoTokenService.deleteById(ssoToken.getId());
			return this.buildErrorMsg("10005", "token过期!");
		}
		
		// 校验Token是否被修改
		String hashValue = MD5Utils.md5(token + "&" + ssoToken.getSsoPassport());
		if (!ssoToken.getSsoVerifyHash().equals(hashValue))
		{
			return this.buildErrorMsg("10006", "token被篡改!");
		}
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("accessToken", ssoToken.getAccessToken());
		List<Object> ssoLogList = ssoLogService.find(params);
		
		// 删除SsoToken
		ssoTokenService.deleteById(ssoToken.getId());
		
		// 查询用户
		User user = userService.getById(ssoToken.getUserId());
		if (user == null)
		{
			return this.buildErrorMsg("10007", "无效用户!");
		}
		
		// 查询机构信息
		Org org = null;
		String orgPath = "";
		
		String orgUnitCode = "";
		
		if (StringUtils.isNotEmpty(user.getOrgId()))
		{
			org = orgService.getById(user.getOrgId());
			
			String[] orgIds = org.getOrgLevelCode().split("/");
			if (CollectionUtils.isNotEmpty(orgIds))
			{
				for (String orgId : orgIds)
				{
					Org orgInfo = orgService.getById(orgId);
					if (orgInfo != null)
					{
						orgPath += orgInfo.getOrgName() + "/";
						if (orgInfo.getOrgType() == 1 )
						{
							orgUnitCode = orgInfo.getOrgCode().trim();
						}
					}
				}
				
				if (StringUtils.isNotEmpty(orgPath))
				{
					orgPath = orgPath.substring(0, orgPath.length() - 1);
				}
			}
		}
		
		// 生成JSON数据
		JSONObject dataJSON = new JSONObject();
		dataJSON.put("passport", ssoToken.getSsoPassport());
		dataJSON.put("loginName", user.getLoginName());
		dataJSON.put("userName", user.getUserName());
		dataJSON.put("authMode", ssoToken.getAuthMode());
		dataJSON.put("orgCode", org != null ? org.getOrgCode() : "");
		dataJSON.put("orgUnitCode", StringUtils.isNotEmpty(orgUnitCode) ? orgUnitCode : "");
		dataJSON.put("orgName", org != null ? org.getOrgName() : "");
		dataJSON.put("orgPath", StringUtils.isNotEmpty(orgPath) ? orgPath : "");
		
		// 加密data
		String data = AESUtils.encode(dataJSON.toString(), resource.getResourceKey());
		
		// 生成JSON数据
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("result", 0);
		resultJSON.put("data", data);
		
		String result = resultJSON.toString();
		// 记录日志
		dataJSON.put("result", 0);
		LogUtils.me().updateSsoLog(ssoLogList, dataJSON.toJSONString());
		return result;
	}
	
	public static void main(String[] args)
	{
		JSONObject dataJSON = new JSONObject();
		dataJSON.put("passport", "oa-zhangs");
		dataJSON.put("loginName", "zhangs");
		dataJSON.put("userName", "张三");
		dataJSON.put("authMode", "password_auth");
		dataJSON.put("orgCode", "100001");
		dataJSON.put("orgName", "营业部");
		dataJSON.put("orgPath", "河北分公司/营业部");
		System.out.println(dataJSON.toString());
	}
	
	/**
	 * 错误信息
	 * 
	 * @param errcode
	 * @param errmsg
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private String buildErrorMsg(String errcode, String errmsg)
	{
		System.out.println("*************************************");
		System.out.println("token校验出错");
		System.out.println(DateUtils.getCurrentDateTime()+"  "+"{\"result\": \"-1\", \"errcode\": \"" + errcode + "\", \"errmsg\": \"" + errmsg + "\"}");
		System.out.println("*************************************");
		return "{\"result\": \"-1\", \"errcode\": \"" + errcode + "\", \"errmsg\": \"" + URLEncoder.encode(URLEncoder.encode(errmsg)) + "\"}";
	}
}

package com.scanywhere.security.controller;

import java.util.Iterator;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scanywhere.authentication.handler.AuthenticationHandler;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.reflect.ClassUtils;
import com.scanywhere.core.base.controller.BaseController;
import com.scanywhere.core.cache.IProjectCache;
import com.scanywhere.modules.resource.service.ResourceIconService;

@Controller
@RequestMapping(value = "/api/cache")
public class CacheRefurbishController extends BaseController
{
	/**
	 * 刷新缓存
	 * 
	 * @return
	 */
	@RequestMapping(value = "/refurbish")
	@ResponseBody
	public String refurbish()
	{
		CacheUtils.removeAll(ProjectConstants.AUTHENTICATION_CACHE);
		CacheUtils.removeAll(ProjectConstants.SSO_CACHE);
		
		Set<Class<?>> caches = ClassUtils.scanPackageBySuper("com.scanywhere.core.cache.impl", IProjectCache.class);
		Iterator<Class<?>> it = caches.iterator();
		while (it.hasNext()) 
		{
			Class<?> clazz = it.next();
			Object obj = ClassUtils.newInstance(clazz);
			ClassUtils.invoke(obj, "init", new Object[]{});
		}
		
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, "password_auth");
		return "{\"result\": \"0\"}";
	}
	

	@Resource
	private ResourceIconService resourceIconService;
	/**
	 * 刷新缓存应用图标
	 * 
	 * @return
	 */
	@RequestMapping(value = "/refurbishIcon")
	@ResponseBody
	public String refurbishIcon()
	{
		MainController.iconMap = resourceIconService.loadDate();
		MainController.iconTime = System.currentTimeMillis();
		return "{\"result\": \"0\"}";
	}
}

package com.scanywhere.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.security.subject.UserSubject;

/**
 * 获取当前登录用户信息工具类
 */
public class SecurityLoginUtils
{
	/**
	 * 获取Session中用户信息
	 * 
	 * @return
	 */
	public static UserSubject getSubject()
	{
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return (UserSubject) request.getSession().getAttribute(CommonConstants.LOGIN_INFO);
	}
}

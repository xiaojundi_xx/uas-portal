package com.scanywhere.modules.user.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.user.entity.User;

public interface UserDao extends BaseDao<User>
{
	/**
	 * 根据管理员登录帐号查询
	 * 
	 * @param loginName
	 * @return
	 */
	public User findByLoginName(String loginName);
}

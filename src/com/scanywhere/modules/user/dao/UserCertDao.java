package com.scanywhere.modules.user.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.user.entity.UserCert;

public interface UserCertDao extends BaseDao<UserCert>
{
	/**
	 * 根据管理员登录帐号查询
	 * 
	 * @param loginName
	 * @return
	 */
	public UserCert findByLoginName(String loginName);
}

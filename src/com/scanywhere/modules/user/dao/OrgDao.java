package com.scanywhere.modules.user.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.user.entity.Org;

public interface OrgDao extends BaseDao<Org>
{

}

package com.scanywhere.modules.user.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.user.entity.UserAttr;

public interface UserAttrDao extends BaseDao<UserAttr>
{
	/**
	 * 唯一性校验
	 * 
	 * @param attrName
	 * @param attrValue
	 * @param userId
	 * @return
	 */
	public boolean uniqueValidate(String attrName, String attrValue, String userId);
}

package com.scanywhere.modules.user.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.user.dao.OrgAttrDao;
import com.scanywhere.modules.user.entity.OrgAttr;

@Repository("orgAttrDao")
public class OrgAttrDaoImpl extends BaseDaoImpl<OrgAttr> implements OrgAttrDao
{
	public boolean uniqueValidate(String attrName, String attrValue, String orgId)
	{
		Criteria criteria = super.getSession().createCriteria(OrgAttr.class);
		criteria.add(Restrictions.eq("attrName", attrName));
		criteria.add(Restrictions.eq("attrValue", attrValue));
		if (StringUtils.isNotEmpty(orgId))
		{
			criteria.add(Restrictions.ne("orgId", orgId));
		}
		return criteria.list().isEmpty();
	}
	
}

package com.scanywhere.modules.user.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.user.dao.AttrConfigDao;
import com.scanywhere.modules.user.entity.AttrConfig;

@Repository("attrConfigDao")
public class AttrConfigDaoImpl extends BaseDaoImpl<AttrConfig> implements AttrConfigDao
{
	public AttrConfig findByAttrName(String attrName)
	{
		String sql = "SELECT * FROM UAS_ATTR_CONFIG WHERE ATTR_NAME = :attrName";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("attrName", attrName);
		
		AttrConfig attr = super.getBySql(sql, params);
		return attr;
	}
}

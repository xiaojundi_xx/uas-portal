package com.scanywhere.modules.user.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.user.dao.UserAttrDao;
import com.scanywhere.modules.user.entity.UserAttr;

@Repository("userAttrDao")
public class UserAttrDaoImpl extends BaseDaoImpl<UserAttr> implements UserAttrDao
{
	public boolean uniqueValidate(String attrName, String attrValue, String userId)
	{
		Criteria criteria = getSession().createCriteria(UserAttr.class);
		criteria.add(Restrictions.eq("attrName", attrName));
		criteria.add(Restrictions.eq("attrValue", attrValue));
		if (StringUtils.isNotEmpty(userId))
		{
			criteria.add(Restrictions.ne("userId", userId));
		}
		return criteria.list().isEmpty();
	}
}

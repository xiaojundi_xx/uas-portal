package com.scanywhere.modules.user.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.user.dao.UserAccountDao;
import com.scanywhere.modules.user.entity.UserAccount;

@Repository("userAccountDao")
public class UserAccountDaoImpl extends BaseDaoImpl<UserAccount> implements UserAccountDao
{
	public List<UserAccount> findUserAccount(String resourceId, String userId, int bindingMode)
	{
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT userAccount.* FROM UAS_USER_ACCOUNT userAccount ");
		sql.append("LEFT JOIN UAS_RESOURCE_ACCOUNT resourceAccount ON userAccount.ACCOUNT_ID = resourceAccount.ID ");
		sql.append("WHERE userAccount.RESOURCE_ID = :resourceId AND userAccount.USER_ID = :userId ");
		sql.append("AND resourceAccount.ACCOUNT_STATUS = :accountStatus ");
		if ((bindingMode & (ProjectConstants.ACCOUNT_BINDING_MODE_USER | ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN)) == 0)
		{
			sql.append("AND resourceAccount.BINDING_MODE = :bindingMode ");
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceId", resourceId);
		params.put("userId", userId);
		params.put("accountStatus", CommonConstants.STATUS_ENABLED);
		if ((bindingMode & (ProjectConstants.ACCOUNT_BINDING_MODE_USER | ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN)) == 0)
		{
			params.put("bindingMode", bindingMode);
		}
		List<UserAccount> accountList = super.findBySql(UserAccount.class, sql.toString(), params);
		return accountList;
	}
}

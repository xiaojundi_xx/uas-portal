package com.scanywhere.modules.user.dao.impl;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.user.dao.OrgDao;
import com.scanywhere.modules.user.entity.Org;

@Repository("orgDao")
public class OrgDaoImpl extends BaseDaoImpl<Org> implements OrgDao
{

}

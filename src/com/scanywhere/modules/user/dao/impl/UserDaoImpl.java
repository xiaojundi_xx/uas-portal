package com.scanywhere.modules.user.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.user.dao.UserDao;
import com.scanywhere.modules.user.entity.User;

@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao
{
	public User findByLoginName(String loginName)
	{
		String sql = "SELECT * FROM UAS_USER WHERE LOGIN_NAME = :loginName";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", loginName);
		
		User user = super.getBySql(sql, params);
		return user;
	}
}

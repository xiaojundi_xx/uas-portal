package com.scanywhere.modules.user.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.user.dao.UserCertDao;
import com.scanywhere.modules.user.entity.UserCert;

@Repository("userCertDao")
public class UserCertDaoImpl extends BaseDaoImpl<UserCert> implements UserCertDao
{

	public UserCert findByLoginName(String loginName)
	{
		String sql = "SELECT * FROM UAS_USER_CERT WHERE LOGIN_NAME = :loginName";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", loginName);
		
		UserCert userCert = super.getBySql(sql, params);
		return userCert;
	}
	
}

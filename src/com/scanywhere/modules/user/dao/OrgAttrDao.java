package com.scanywhere.modules.user.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.user.entity.OrgAttr;

public interface OrgAttrDao extends BaseDao<OrgAttr>
{
	/**
	 * 唯一性校验
	 * 
	 * @param attrName
	 * @param attrValue
	 * @param orgId
	 * @return
	 */
	public boolean uniqueValidate(String attrName, String attrValue, String orgId);
}

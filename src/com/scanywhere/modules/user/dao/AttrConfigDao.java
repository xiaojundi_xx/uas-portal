package com.scanywhere.modules.user.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.user.entity.AttrConfig;

public interface AttrConfigDao extends BaseDao<AttrConfig>
{
	/**
	 * 根据属性名查询
	 * 
	 * @param attrName
	 * @return
	 */
	public AttrConfig findByAttrName(String attrName);
}

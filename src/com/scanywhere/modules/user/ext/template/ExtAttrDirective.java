package com.scanywhere.modules.user.ext.template;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.user.entity.AttrConfig;
import com.scanywhere.modules.user.ext.AttrConstants;
import com.scanywhere.modules.user.ext.ExtAttrParameter;
import com.scanywhere.modules.user.ext.impl.UserExtAttrParameter;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class ExtAttrDirective implements TemplateDirectiveModel
{
	/** 动态参数处理类 */
	ExtAttrParameter parameter;
	
	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException
	{
		Writer out = env.getOut();
		
		try 
		{
	        String extId = params.get("extId") != null ? ((TemplateModel) params.get("extId")).toString() : "";
	        String objId = params.get("objId") != null ? ((TemplateModel) params.get("objId")).toString() : "";
			
			if (StringUtils.isNotEmpty(extId) && extId.equals("user"))
			{
				if (extId.equals("user"))
				{
					parameter = new UserExtAttrParameter();
				}
				
				// 查询扩展属性配置
				List<AttrConfig> attrList = parameter.getConfigList();
				if (CollectionUtils.isNotEmpty(attrList))
				{
					// 查询动态参数配置
					Map<String, String> valueMap = null;
					if (StringUtils.isNotEmpty(objId))
					{
						if (CollectionUtils.isEmpty(valueMap)) 
						{
							valueMap = parameter.getValueMap(objId);
						}
					}
					
					// 页面内容
					buildHtml(out, attrList, valueMap);
					
					if (body != null)
					{
						body.render(env.getOut());
					}
				}
			}
		}
		catch (IOException e) 
		{
            e.printStackTrace();
        } 
		/*finally 
		{
            if(out != null) 
            {
                try 
                {
                    out.flush();
                    out.close();
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }
        }*/
	}
	
	private void buildHtml(Writer out, List<AttrConfig> attrList, Map<String, String> valueMap) throws IOException
	{
		for (int i = 0; i < attrList.size(); i++)
		{
			AttrConfig config = attrList.get(i);
			
			if (config.getDisplayType() == 2)
			{
				// 不显示
				continue;
			}
			
			// 属性停用
			if (config.getAttrStatus() == CommonConstants.STATUS_DISABLED)
			{
				continue;
			}
			
			String name = config.getAttrName();
			String fieldName = parameter.getFieldPrefix() + config.getAttrName();
			String defaultValue = config.getAttrDefaultValue();
			String value = (valueMap == null ? (defaultValue != null ? defaultValue : "") : (String) valueMap.get(name));
			if (StringUtils.isEmpty(value))
			{
				value = defaultValue != null ? defaultValue : "";
			}
			
			String[] metaData = {};
			if (config.getAttrContent().length() > 0 && config.getAttrContent().indexOf(",") > 0) 
			{
				metaData = config.getAttrContent().split(",");
			}
			
			if (i % 2 == 0)
			{
				out.write("<div class=\"row\">");
			}
//			out.write("<div class=\"row\">");
			out.write("	<div class=\"col-xs-6\">");
			out.write("		<div class=\"form-group\">");
			out.write("			<label for=\"hireTime\" class=\"control-label col-sm-4\">");
			
			// 是否必填属性
			if (config.getIsRequired() == CommonConstants.STATUS_0)
			{
				out.write("				<span class=\"required\">*</span>" + config.getAttrDispName() + "：");
			}
			else
			{
				out.write("				" + config.getAttrDispName() + "：");
			}
			
			out.write("			</label>");
			out.write("			<div class=\"col-sm-8\">");
			
			if (config.getAttrInputType() == AttrConstants.INPYT_TYPE_TEXT)
			{
				// 是否可编辑
				if (config.getIsEdit() == CommonConstants.STATUS_0)
				{
					out.write("				<input type=\"text\" class=\"form-control\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" value=\"" + value + "\" />");
				}
				else
				{
					out.write("				<input type=\"text\" class=\"form-control\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" value=\"" + value + "\" readonly=\"readonly\"/>");
				}
			}
			else if (config.getAttrInputType() == AttrConstants.INPYT_TYPE_SELECT)
			{
				// 是否可编辑
				if (config.getIsEdit() == CommonConstants.STATUS_0)
				{
					out.write("				<select id=\"" + fieldName + "\" name=\"" + fieldName + "\" class=\"form-control\">");
				}
				else
				{
					out.write("				<select id=\"" + fieldName + "\" name=\"" + fieldName + "\" class=\"form-control\" disabled=\"disabled\">");
				}
				
				for (int j = 0; j < metaData.length; j++)
				{
					String value_ = metaData[j].substring(0, metaData[j].indexOf("@"));
					String label = metaData[j].substring(metaData[j].indexOf("@") + 1);
					boolean checked = (value.equals(value_) || value.indexOf(value_ + ",") != -1 || value.indexOf("," + value_) != -1); 
					if (checked)
					{
						out.write("					<option value=\"" + value_ + "\" selected=\"selected\">" + label + "</option>");
					}
					else
					{
						out.write("					<option value=\"" + value_ + "\">" + label + "</option>");
					}
				}
				out.write("				</select>");
			}
			else if (config.getAttrInputType() == AttrConstants.INPYT_TYPE_RADIO)
			{
				for (int j = 0; j < metaData.length; j++)
				{
					String value_ = metaData[j].substring(0, metaData[j].indexOf("@"));
					String label = metaData[j].substring(metaData[j].indexOf("@") + 1);
					out.write("				<label class=\"control-label\">");
					boolean checked = (value.equals(value_) || value.indexOf(value_ + ",") != -1 || value.indexOf("," + value_) != -1); 
					if (checked)
					{
						// 是否可编辑
						if (config.getIsEdit() == CommonConstants.STATUS_0)
						{
							out.write("					<input type=\"radio\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\" checked=\"checked\"/> " + label); 
						}
						else
						{
							out.write("					<input type=\"radio\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\" checked=\"checked\" disabled=\"disabled\"/> " + label); 
						}
					}
					else
					{
						// 是否可编辑
						if (config.getIsEdit() == CommonConstants.STATUS_0)
						{
							out.write("					<input type=\"radio\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\"/> " + label); 
						}
						else
						{
							out.write("					<input type=\"radio\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\" disabled=\"disabled\"/> " + label); 
						}
					}
					out.write("				</label>&nbsp; ");
				}
			}
			else if (config.getAttrInputType() == AttrConstants.INPYT_TYPE_CHECKBOX)
			{
				for (int j = 0; j < metaData.length; j++)
				{
					String value_ = metaData[j].substring(0, metaData[j].indexOf("@"));
					String label = metaData[j].substring(metaData[j].indexOf("@") + 1);
					out.write("				<label class=\"control-label\">");
					boolean checked = (value.equals(value_) || value.indexOf(value_ + ",") != -1 || value.indexOf("," + value_) != -1); 
					if (checked)
					{
						// 是否可编辑
						if (config.getIsEdit() == CommonConstants.STATUS_0)
						{
							out.write("					<input type=\"checkbox\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\" checked=\"checked\"/> " + label); 
						}
						else
						{
							out.write("					<input type=\"checkbox\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\" checked=\"checked\" disabled=\"disabled\"/> " + label); 
						}
					}
					else
					{
						// 是否可编辑
						if (config.getIsEdit() == CommonConstants.STATUS_0)
						{
							out.write("					<input type=\"checkbox\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\"/> " + label); 
						}
						else
						{
							out.write("					<input type=\"checkbox\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" data-flag=\"icheck\" class=\"minimal-blue\" value=\"" + value_ + "\" disabled=\"disabled\"/> " + label); 
						}
					}
					out.write("				</label>&nbsp; ");
				}
			}
			else if (config.getAttrInputType() == AttrConstants.INPYT_TYPE_DATE)
			{
				out.write("				<div class=\"input-group\">");
				out.write("					<span class=\"input-group-addon\">");
				out.write("						<i class=\"fa fa-calendar\"></i>");
				out.write("					</span>");
				
				// 是否可编辑
				if (config.getIsEdit() == CommonConstants.STATUS_0)
				{
					out.write("					<input type=\"text\" class=\"form-control\" data-flag=\"datetimepicker\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" value=\"" + value + "\" />");
				}
				else
				{
					out.write("					<input type=\"text\" class=\"form-control\" data-flag=\"datetimepicker\" id=\"" + fieldName + "\" name=\"" + fieldName + "\" value=\"" + value + "\" readonly=\"readonly\"/>");
				}
				out.write("				</div>");
			}
			
			out.write("			</div>");
			out.write("		</div>");
			out.write("	</div>");
			if (i % 2 != 0)
			{
				out.write("</div>");
			}
//			out.write("</div>");
		}
	}
}

package com.scanywhere.modules.user.ext.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.user.entity.AttrConfig;
import com.scanywhere.modules.user.entity.OrgAttr;
import com.scanywhere.modules.user.ext.ExtAttrParameter;
import com.scanywhere.modules.user.service.AttrConfigService;
import com.scanywhere.modules.user.service.OrgAttrService;

public class OrgExtAttrParameter implements ExtAttrParameter
{
	/** 参数前缀  [ <b>_org_</b> ]*/
	public final static String FIELD_PREFIX = "_org_";
	
	private AttrConfigService attrConfigService;
	
	private OrgAttrService orgAttrService;
	
	public OrgExtAttrParameter()
	{
		attrConfigService = (AttrConfigService) SpringContextUtils.getBean("attrConfigService");
		orgAttrService = (OrgAttrService) SpringContextUtils.getBean("orgAttrService");
	}
	
	public String getFieldPrefix()
	{
		return FIELD_PREFIX;
	}

	public List<AttrConfig> getConfigList()
	{
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("isOrgAttr", CommonConstants.STATUS_0);
		return attrConfigService.find(params);
	}
	
	public Map<String, String> getValueMap(String objId)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("orgId", objId);
		List<OrgAttr> attrList = orgAttrService.find(params);
		if (CollectionUtils.isNotEmpty(attrList))
		{
			Map<String, String> valueMap = new HashMap<String, String>();
			for (int i = 0; i < attrList.size(); i++)
			{
				OrgAttr attr = attrList.get(i);
				valueMap.put(attr.getAttrName(), attr.getAttrValue());
			}
			return valueMap;
		}
		return null;
	}
	
	public String saveAttrValues(String objId, Map<String, Object> paramValues)
	{
		if (CollectionUtils.isNotEmpty(paramValues))
		{
			return orgAttrService.saveAttrValues(objId, paramValues);
		}
		return null;
	}
	
	public String validate(String objId, Map<String, Object> paramValues)
	{
		if (CollectionUtils.isNotEmpty(paramValues))
		{
			/*Map<String, String> valueMap = null;
			if (objId != null)
			{
				valueMap = this.getValueMap(objId);
			}*/
			
			StringBuffer message = new StringBuffer();
			
			for(Map.Entry<String, Object> paramValue: paramValues.entrySet())
			{
				String attrName = paramValue.getKey();
				if (!attrName.startsWith(OrgExtAttrParameter.FIELD_PREFIX))
				{
					continue;
				}
				attrName = attrName.substring(OrgExtAttrParameter.FIELD_PREFIX.length());
				
				String attrValue = paramValue.getValue() == null ? "" : (String) paramValue.getValue();
				
				// 查询属性配置
				AttrConfig config = attrConfigService.findByAttrName(attrName);
				if (config == null)
				{
					continue;
				}
				
				// 属性停用
				if (config.getAttrStatus() == CommonConstants.STATUS_DISABLED)
				{
					continue;
				}
				
				// 判断是否不允许为空
				if (config.getIsRequired() == CommonConstants.STATUS_0)
				{
					if (StringUtils.isEmpty(attrValue))
					{
						message.append(config.getAttrDispName() + ",");
						continue;
					}
				}
				
				// 判断是否为一
				if (config.getIsUnique() == CommonConstants.STATUS_0)
				{
					// 查询条件
					boolean unique = orgAttrService.uniqueValidate(attrName, attrValue, objId);
					if (!unique)
					{
						message.append(config.getAttrDispName() + ",");
					}
				}
			}
			
			if (StringUtils.isNotEmpty(message.toString()))
			{
				return "机构扩展属性: " + message.substring(0, message.length() - 1) + " 为空或唯一性校验失败!";
			}
		}
		
		return null;
	}
}

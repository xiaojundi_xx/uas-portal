package com.scanywhere.modules.user.ext;

import java.util.List;
import java.util.Map;

import com.scanywhere.modules.user.entity.AttrConfig;

public interface ExtAttrParameter
{
	/** 返回动态扩展属性前缀 */
	public String getFieldPrefix();
	
	/**
	 * 查询扩展属性配置
	 * 
	 * @return
	 */
	public List<AttrConfig> getConfigList();
	
	/**
	 * 查询扩展属性值
	 * 
	 * @param objId
	 * @return
	 */
	public Map<String, String> getValueMap(String objId);
	
	/**
	 * 保存扩展属性
	 * 
	 * @param objId
	 * @param paramValues
	 * @return
	 */
	public String saveAttrValues(String objId, Map<String, Object> paramValues);
}

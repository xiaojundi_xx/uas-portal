package com.scanywhere.modules.user.ext;

public class AttrConstants
{
	/** 输入类型 字符串 */
	public static final int VALUE_TYPE_STRING = 1;
	
	/** 输入类型 数值 */
	public static final int VALUE_TYPE_NUMBER = 2;
	
	/** 输入类型 布尔型 */
	public static final int VALUE_TYPE_BOOLEAN = 3;
	
	/** 输入类型 日期 */
	public static final int VALUE_TYPE_DATE = 4;
	
	/** 输入类型 输入框 */
	public static final int INPYT_TYPE_TEXT = 1;
	
	/** 输入类型 选择框 */
	public static final int INPYT_TYPE_SELECT = 2;
	
	/** 输入类型 单选框 */
	public static final int INPYT_TYPE_RADIO = 3;
	
	/** 输入类型 多选框 */
	public static final int INPYT_TYPE_CHECKBOX = 4;
	
	/** 输入类型 日期 */
	public static final int INPYT_TYPE_DATE = 5;
}

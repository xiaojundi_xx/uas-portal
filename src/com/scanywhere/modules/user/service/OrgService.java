package com.scanywhere.modules.user.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.user.entity.Org;

public interface OrgService extends BaseService<Org>
{

}

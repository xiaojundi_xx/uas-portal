package com.scanywhere.modules.user.service;

import java.util.List;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.user.entity.UserAccount;

public interface UserAccountService extends BaseService<UserAccount>
{
	/**
	 * 查询用户应用子帐号
	 * 
	 * @param resourceId
	 * @param userId
	 * @param bindingMode
	 * @return
	 */
	public List<UserAccount> findUserAccount(String resourceId, String userId, int bindingMode);
}

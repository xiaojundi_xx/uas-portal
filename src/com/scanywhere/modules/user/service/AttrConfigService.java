package com.scanywhere.modules.user.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.user.entity.AttrConfig;

public interface AttrConfigService extends BaseService<AttrConfig>
{
	/**
	 * 根据属性名查询
	 * 
	 * @param attrName
	 * @return
	 */
	public AttrConfig findByAttrName(String attrName);
}

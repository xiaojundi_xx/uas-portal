package com.scanywhere.modules.user.service;

import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.user.entity.OrgAttr;

public interface OrgAttrService extends BaseService<OrgAttr>
{
	/**
	 * 保存扩展属性
	 * 
	 * @param orgId
	 * @param paramValues
	 * @return
	 */
	public String saveAttrValues(String orgId, Map<String, Object> paramValues);
	
	/**
	 * 唯一性校验
	 * 
	 * @param attrName
	 * @param attrValue
	 * @param orgId
	 * @return
	 */
	public boolean uniqueValidate(String attrName, String attrValue, String orgId);
}

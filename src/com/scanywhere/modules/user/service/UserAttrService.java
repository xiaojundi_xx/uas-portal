package com.scanywhere.modules.user.service;

import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.user.entity.UserAttr;

public interface UserAttrService extends BaseService<UserAttr>
{
	/**
	 * 保存扩展属性
	 * 
	 * @param userId
	 * @param paramValues
	 * @return
	 */
	public String saveAttrValues(String userId, Map<String, Object> paramValues);
	
	/**
	 * 唯一性校验
	 * 
	 * @param attrName
	 * @param attrValue
	 * @param userId
	 * @return
	 */
	public boolean uniqueValidate(String attrName, String attrValue, String userId);
}

package com.scanywhere.modules.user.service;

import java.util.Map;

import com.scanywhere.authentication.principal.CertificateCredentials;
import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.user.entity.User;

public interface UserService extends BaseService<User>
{
	/**
	 * 根据帐号查询
	 * 
	 * @param loginName
	 * @return
	 */
	public User findByLoginName(String loginName);
	
	/**
	 * 更新用户
	 * 
	 * @param user
	 * @param paramValues
	 * @return
	 */
	public String updateUser(User user, Map<String, Object> paramValues);
	
	/**
	 * 更新密码
	 * 
	 * @param id
	 * @param origPassword
	 * @param newPassword
	 * @return
	 */
	public String updatePassword(String id, String origPassword, String newPassword);

	/**
	 * 更新密码
	 * 
	 * @param id
	 * @param origPassword
	 * @param newPassword
	 * @return
	 */
	public String updatePasswordByCert(String id, String newPassword, CertificateCredentials credentials);
	
	/**
	 * 重置密码
	 * 
	 * @param id
	 * @return
	 */
	public String resetPwd(String id);
}

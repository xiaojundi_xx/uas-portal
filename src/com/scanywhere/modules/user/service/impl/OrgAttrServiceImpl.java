package com.scanywhere.modules.user.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.user.dao.AttrConfigDao;
import com.scanywhere.modules.user.dao.OrgAttrDao;
import com.scanywhere.modules.user.entity.AttrConfig;
import com.scanywhere.modules.user.entity.OrgAttr;
import com.scanywhere.modules.user.ext.AttrConstants;
import com.scanywhere.modules.user.ext.impl.OrgExtAttrParameter;
import com.scanywhere.modules.user.service.OrgAttrService;

@Service("orgAttrService")
public class OrgAttrServiceImpl extends BaseServiceImpl<OrgAttr> implements OrgAttrService
{
	@Resource
	private AttrConfigDao attrConfigDao;
	
	@Resource
	private OrgAttrDao orgAttrDao;
	
	@Resource(name = "orgAttrDao")
	public void setDao(BaseDao<OrgAttr> dao)
	{
		super.setDao(dao);
	}

	public String saveAttrValues(String orgId, Map<String, Object> paramValues)
	{
		if (CollectionUtils.isNotEmpty(paramValues))
		{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("orgId", orgId);
			List<OrgAttr> attrList = orgAttrDao.find(params);
			
			for(Map.Entry<String, Object> paramValue: paramValues.entrySet())
			{
				String attrName = paramValue.getKey();
				if (!attrName.startsWith(OrgExtAttrParameter.FIELD_PREFIX))
				{
					continue;
				}
				attrName = attrName.substring(OrgExtAttrParameter.FIELD_PREFIX.length());
				
				String attrValue = paramValue.getValue() == null ? "" : (String) paramValue.getValue();
				
				// 查询属性配置
				AttrConfig config = attrConfigDao.findByAttrName(attrName);
				if (config == null)
				{
					continue;
				}
				
				// 属性停用
				if (config.getAttrStatus() == CommonConstants.STATUS_DISABLED)
				{
					continue;
				}
				
				boolean update = false;
				for (OrgAttr attr : attrList)
				{
					if (attrName.equals(attr.getAttrName()))
					{
						// 日期类型转型 yyyy-MM-dd HH:mm:ss
						if (StringUtils.isNotEmpty(attrValue) && config.getAttrType() == AttrConstants.VALUE_TYPE_DATE)
						{
							Calendar calendar = Calendar.getInstance();
							calendar.setTimeInMillis(Long.parseLong(attrValue));
							attrValue = DateUtils.formatDateTime(calendar.getTime());
						}
						
						attr.setAttrValue(attrValue);
						attr.setUpdateDateTime(DateUtils.date());
						attr.setUpdateTimeStamp(DateUtils.current(false));
						orgAttrDao.save(attr);
						
						update = true;
						break;
					}
				}
				
				if (update)
					continue;
				
				OrgAttr attr = new OrgAttr();
				attr.setOrgId(orgId);
				attr.setAttrId(config.getId());
				attr.setAttrName(attrName);
				
				// 日期类型转型 yyyy-MM-dd HH:mm:ss
				if (StringUtils.isNotEmpty(attrValue) && config.getAttrType() == AttrConstants.VALUE_TYPE_DATE)
				{
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(Long.parseLong(attrValue));
					attrValue = DateUtils.formatDateTime(calendar.getTime());
				}
				attr.setAttrValue(attrValue);
				attr.setCreateDateTime(DateUtils.date());
				attr.setUpdateDateTime(DateUtils.date());
				attr.setUpdateTimeStamp(DateUtils.current(false));
				orgAttrDao.save(attr);
			}
		}
		return null;
	}

	public boolean uniqueValidate(String attrName, String attrValue, String orgId)
	{
		if (StringUtils.isEmpty(attrName) || StringUtils.isEmpty(attrValue))
		{
			return false;
		}
		return orgAttrDao.uniqueValidate(attrName, attrValue, orgId);
	}
}

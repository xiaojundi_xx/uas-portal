package com.scanywhere.modules.user.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.user.dao.AttrConfigDao;
import com.scanywhere.modules.user.dao.UserAttrDao;
import com.scanywhere.modules.user.entity.AttrConfig;
import com.scanywhere.modules.user.service.AttrConfigService;

@Service("attrConfigService")
public class AttrConfigServiceImpl extends BaseServiceImpl<AttrConfig> implements AttrConfigService
{
	@Resource 
	private AttrConfigDao attrConfigDao;
	
	@Resource 
	private UserAttrDao userAttrDao;
	
	@Resource(name = "attrConfigDao")
	public void setDao(BaseDao<AttrConfig> dao)
	{
		super.setDao(dao);
	}

	public AttrConfig findByAttrName(String attrName)
	{
		if (StringUtils.isEmpty(attrName))
		{
			return null;
		}
		
		return attrConfigDao.findByAttrName(attrName);
	}
}

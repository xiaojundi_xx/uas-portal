package com.scanywhere.modules.user.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.user.dao.OrgDao;
import com.scanywhere.modules.user.entity.Org;
import com.scanywhere.modules.user.service.OrgService;

@Service("orgService")
public class OrgServiceImpl extends BaseServiceImpl<Org> implements OrgService
{
	@Resource
	private OrgDao orgDao;
	
	@Resource(name = "orgDao")
	public void setDao(BaseDao<Org> dao)
	{
		super.setDao(dao);
	}
}

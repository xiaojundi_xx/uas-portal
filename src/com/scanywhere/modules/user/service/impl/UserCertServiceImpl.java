package com.scanywhere.modules.user.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.user.dao.UserCertDao;
import com.scanywhere.modules.user.entity.UserCert;
import com.scanywhere.modules.user.service.UserCertService;

@Service("userCertService")
public class UserCertServiceImpl extends BaseServiceImpl<UserCert> implements UserCertService
{

	@Resource
	private UserCertDao userCertDao;
	
	@Resource(name = "userCertDao")
	public void setDao(BaseDao<UserCert> dao)
	{
		super.setDao(dao);
	}

	public UserCert findByLoginName(String loginName)
	{
		if (StringUtils.isEmpty(loginName))
		{
			return null;
		}
		
		UserCert userCert = userCertDao.findByLoginName(loginName);
		return userCert;
	}

}

package com.scanywhere.modules.user.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.core.constant.LogConstant;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.resource.dao.ResourceAccountDao;
import com.scanywhere.modules.user.dao.UserAccountDao;
import com.scanywhere.modules.user.dao.UserDao;
import com.scanywhere.modules.user.entity.UserAccount;
import com.scanywhere.modules.user.service.UserAccountService;
import com.scanywhere.security.SecurityLoginUtils;

@Service("userAccountService")
public class UserAccountServiceImpl extends BaseServiceImpl<UserAccount> implements UserAccountService
{
	@Resource 
	private ResourceAccountDao resourceAccountDao;
	
	@Resource 
	private UserAccountDao userAccountDao;
	
	@Resource 
	private UserDao userDao;
	
	@Resource(name = "userAccountDao")
	public void setDao(BaseDao<UserAccount> dao)
	{
		super.setDao(dao);
	}
	
	public List<UserAccount> findUserAccount(String resourceId, String userId, int bindingMode)
	{
		if (StringUtils.isEmpty(resourceId) || StringUtils.isEmpty(userId))
		{
			return null;
		}
		return userAccountDao.findUserAccount(resourceId, userId, bindingMode);
	}
}

package com.scanywhere.modules.user.service.impl;

import static org.dozer.loader.api.TypeMappingOptions.mapEmptyString;
import static org.dozer.loader.api.TypeMappingOptions.mapNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.util.encoders.Base64;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import org.springframework.stereotype.Service;

import com.sca.cert.CertAuth;
import com.sca.cert.CertValid;
import com.scanywhere.authentication.connector.ADAdminPwd;
import com.scanywhere.authentication.connector.ADConfig;
import com.scanywhere.authentication.connector.ADConnector;
import com.scanywhere.authentication.connector.ADConnectorPool;
import com.scanywhere.authentication.connector.ADUtils;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.authentication.handler.AuthenticationHandler;
import com.scanywhere.authentication.principal.CertificateCredentials;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.FileUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.user.dao.OrgDao;
import com.scanywhere.modules.user.dao.UserDao;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.ext.impl.UserExtAttrParameter;
import com.scanywhere.modules.user.service.UserService;

@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService
{

	private final static String CAROOTPATH = FileUtils.getAbsolutePath("../caroot.cer");
	private final static String CACRLPATH = FileUtils.getAbsolutePath("../ca.crl");
	private static long caRootModifyTime = 0;
	private static long crlModifyTime = 0;
	private static byte[] caRootBytes = null;
	private static BufferedInputStream crlInputStream = null;
	
	static{
		Date lastModifyDate = FileUtils.lastModifiedTime(CAROOTPATH);
		if(lastModifyDate != null){
			caRootModifyTime = lastModifyDate.getTime();
			try {
				caRootBytes = FileUtils.readBytes(new File(CAROOTPATH));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		lastModifyDate = FileUtils.lastModifiedTime(CACRLPATH);
		if(lastModifyDate != null){
			crlModifyTime = lastModifyDate.getTime();
			try {
				crlInputStream = FileUtils.getInputStream(CACRLPATH);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Resource
	private UserDao userDao;
	
	@Resource
	private OrgDao orgDao;
	
	@Resource(name = "userDao")
	public void setDao(BaseDao<User> dao)
	{
		super.setDao(dao);
	}

	public User findByLoginName(String loginName)
	{
		if (StringUtils.isEmpty(loginName))
		{
			return null;
		}
		
		User user = userDao.findByLoginName(loginName);
		return user;
	}

	public String updateUser(User user, Map<String, Object> paramValues)
	{
		// 查询原始数据
		User origUser = super.getById(user.getId());
		if (origUser == null)
		{
			return "未查询到用户信息!";
		}
		
		// 对象映射
		DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
		final BeanMappingBuilder builder = new BeanMappingBuilder() {
			protected void configure() {
	            mapping(User.class, User.class, TypeMappingOptions.oneWay(), mapNull(true), mapEmptyString(true))
	            .exclude("id")
	            .exclude("userGuid")
	            .exclude("loginName")
	            .exclude("userName")
	            .exclude("userPassword")
	            .exclude("userSort")
	            .exclude("userStatus")
	            .exclude("workNumber")
	            .exclude("idNumber")
	            .exclude("sex")
	            .exclude("email")
	            .exclude("hireTime")
	            .exclude("expiredTime")
	            .exclude("orgId")
	            .exclude("orgName")
	            .exclude("firstLogin")
	            .exclude("pwdModifyTime")
	            .exclude("creatorName")
	            .exclude("menderName")
	            .exclude("remark")
	            .exclude("createDateTime")
	            .exclude("updateDateTime")
	            .exclude("updateTimeStamp");
			}
		};
		dozerBeanMapper.addMapping(builder);
		dozerBeanMapper.map(user, origUser);
		dozerBeanMapper.destroy();
		
		// 设置属性
		origUser.setUpdateDateTime(DateUtils.date());
		origUser.setUpdateTimeStamp(DateUtils.current(false));
		
		// 未选择机构
		/*if (StringUtils.isEmpty(user.getOrgId()))
		{
			origUser.setOrgId(null);
		}*/
		super.update(origUser);
		
		// 保存扩展属性
		UserExtAttrParameter userExtAttr = new UserExtAttrParameter();
		userExtAttr.saveAttrValues(origUser.getId(), paramValues);
		return null;
	}
	
	public String updatePassword(String id, String origPassword, String newPassword)
	{
		User user = super.getById(id);
		if (user == null)
		{
			return "未查询到用户信息!";
		}
		
		// 获取认证配置
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, "password_auth");
		if (handler == null)
		{
			return "未查询到认证配置信息!";
		}
		
		// 密码认证源
		int authSource = handler.getParameterToInt("authSource");
		
		// 本地认证
		if (authSource == 0)
		{
			if(!StringUtils.isBlank(origPassword))
			{
				// 验证旧密码是否正确
				origPassword = new SimpleHash("MD5", origPassword, ByteSource.Util.bytes(user.getLoginName()), 2).toHex();
				if (!origPassword.equals(user.getUserPassword()))
				{
					// 记录日志
//					String logContext = "修改密码, 旧密码不正确, 用户帐号: " + user.getLoginName() + ", 姓名: " + user.getUserName() + ".";
//					LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_UPDATE_PASSWORD, "用户密码", LogConstant.FAILURE, logContext);
					return "旧密码不正确!";
				}
			}
		}
		else if (authSource == 1)
		{
			// 域认证
			String protocol = handler.getParameter("protocol");
			String host = handler.getParameter("host");
			int port = handler.getParameterToInt("port");
			String adPrincipal = handler.getParameter("principal");
			String adCredentials = handler.getParameter("credentials");
			String baseDN = handler.getParameter("baseDN");
			
			// 初始化域连接信息
			String configCode = "domain";
			ADConfig config = new ADConfig(configCode, host, port, adPrincipal, adCredentials, protocol.equals("ldaps") ? true : false);
			ADConnectorPool pool = ADConnectorPool.getInstance();
			ADConnector connector = null;
			
			// 查询域帐号是否存在
			try
			{
				// 获取连接
				connector = pool.getConnector(config);
				
				DirContext context = connector.getDirContext();
				if (!ADUtils.getInstance().checkAccountExists(context, baseDN, user.getLoginName()))
				{
					return "无效的域帐号!";
				}
				
				String accountDN = ADUtils.getInstance().searchAccountDN(context, baseDN, user.getLoginName());
				if (StringUtils.isEmpty(accountDN))
				{
					return "无效的域帐号!";
				}
				
				// 验证密码修改是否成功
				boolean ret = ADUtils.getInstance().validateAccount(host, accountDN, origPassword);
				if (!ret)
				{
					return "旧密码不正确!";
				}
				
				// ldap密码修改方式
				String pwdMode = handler.getParameter("pwdMode");
				String domain = handler.getParameter("domain");
				
				if ("iisadmin".equalsIgnoreCase(pwdMode))
				{
					//通过iisadmpwd修改密码
					String url  = handler.getParameter("iisURL");
					ADAdminPwd iis = new ADAdminPwd(url,domain);
					String retCode = iis.changeUserPassword(user.getLoginName(), origPassword, newPassword);
					if (!"OK".equals(retCode))
					{
						 return retCode;
					}
					boolean validateAccount = ADUtils.getInstance().validateAccount(host, accountDN, newPassword);
					if(!validateAccount){
						return "修改AD域密码成功，但更新失败";
					}
				}
				
				if ("ldap".equalsIgnoreCase(pwdMode))
				{
					// 修改域帐号密码
					ret = ADUtils.getInstance().updateAccountPassword(context, accountDN, newPassword);
					if (!ret)
					{
						return "修改域帐号密码失败，密码不满足密码策略的要求，检查最小密码长度、密码复杂性的要求";
					}
				}
				
			}
			catch (UnsupportedEncodingException e) 
			{
				return "不支持的密码算法!";
			}
			catch (NamingException e) 
			{
				return "请联系管理员确认是否正确配置域信息!";
			}
			finally
			{
				pool.setConnectorFree(configCode, connector);
			}
		}
		
		// 加密新密码
		newPassword = new SimpleHash("MD5", newPassword, ByteSource.Util.bytes(user.getLoginName()), 2).toHex();
		user.setUserPassword(newPassword);
		user.setPwdModifyTime(DateUtils.date());
		user.setFirstLogin(CommonConstants.STATUS_1);
		user.setUpdateDateTime(DateUtils.date());
		user.setUpdateTimeStamp(DateUtils.current(false));
		super.update(user);
		return null;
	}
	
	public String updatePasswordByCert(String id, String newPassword, CertificateCredentials credentials)
	{
		User user = super.getById(id);
		if (user == null)
		{
			return "未查询到用户信息!";
		}
		
		if(!user.getLoginName().equals(credentials.getCn())){
			return "用户证书不匹配!";
		}
		
		String verifyRandom = credentials.getVerifyRandom();
		String cert = credentials.getCert();
		String signature = credentials.getSignature();
		
		if(StringUtils.isBlank(signature) || StringUtils.isBlank(cert) || StringUtils.isBlank(verifyRandom))
		{
			return "请求信息不完整!";
		}
		
		try {
			byte[] bytes = Base64.decode(cert.getBytes());
			X509CertificateHolder crt = CertValid.parseCertificate(bytes);
			
			boolean ret = CertAuth.CheckSignature(crt, verifyRandom, signature, 3);
			if(!ret){
				return "证书签名错误!";
			}
			
			Date lastModifyDate = FileUtils.lastModifiedTime(CAROOTPATH);
			if(lastModifyDate == null){
				throw new AuthenticationException("根证书文件缺失!");
			}
			
			if(lastModifyDate.getTime()!=caRootModifyTime || caRootBytes==null){
				caRootModifyTime = lastModifyDate.getTime();
				try {
					caRootBytes = FileUtils.readBytes(new File(CAROOTPATH));
				} catch (IOException e) {
					e.printStackTrace();
					caRootBytes = null;
					throw new AuthenticationException("根证书文件异常!");
				}
			}
			
			lastModifyDate = FileUtils.lastModifiedTime(CACRLPATH);
			if(lastModifyDate == null){
				crlModifyTime = 0;
				crlInputStream = null;
			}else{
				if(lastModifyDate.getTime()!=crlModifyTime || crlInputStream==null){
					crlModifyTime = lastModifyDate.getTime();
					try {
						crlInputStream = FileUtils.getInputStream(CACRLPATH);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
						crlInputStream = null;
					}
				}
			}
			
			
			X509CertificateHolder ca = CertValid.parseCertificate(caRootBytes);
			X509CRLHolder crl = crlInputStream==null? null:CertValid.parseCRL(crlInputStream);
			int iRet = CertValid.CheckCertificate(crt, ca, crl);

			if(iRet == -1)
				return "证书已过期!";
			if(iRet == -2)
				return "证书已过期!";
			if(iRet == -3)
				return "证书颁发者无效!";
			if(iRet == -4)
				return "证书已作废!";
			if(iRet == -5)
				return "证书签名无效!";
			if(iRet == -99)
				return "证书有误!";
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "系统证书文件缺失!";
		} catch (IOException e) {
			e.printStackTrace();
			return "系统证书文件异常!";
		} catch (NullPointerException e) {
			e.printStackTrace();
			return "系统证书文件异常!";
		}
		
		// 加密新密码
		newPassword = new SimpleHash("MD5", newPassword, ByteSource.Util.bytes(user.getLoginName()), 2).toHex();
		user.setUserPassword(newPassword);
		user.setPwdModifyTime(DateUtils.date());
		user.setFirstLogin(CommonConstants.STATUS_1);
		user.setUpdateDateTime(DateUtils.date());
		user.setUpdateTimeStamp(DateUtils.current(false));
		super.update(user);
		return null;
	}
	
	public String resetPwd(String id)
	{
		User user = super.getById(id);
		if (user == null)
		{
			return "未查询到用户信息!";
		}
		
		// 查询帐号密码认证方式
		String password = "1234567890";
		
		// 获取认证配置
		AuthenticationHandler handler = (AuthenticationHandler) CacheUtils.get(ProjectConstants.AUTHENTICATION_CACHE, "password_auth");
		if (handler != null)
		{
			// 默认密码
			password = handler.getParameter("defaultPassword");
		}
		
		// 默认密码
		user.setUserPassword(new SimpleHash("MD5", password, ByteSource.Util.bytes(user.getLoginName()), 2).toHex());
		user.setPwdModifyTime(DateUtils.date());
		user.setUpdateTimeStamp(DateUtils.current(false));
		super.update(user);
		return null;
	}

}

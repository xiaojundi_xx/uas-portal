package com.scanywhere.modules.user.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.user.entity.UserCert;

public interface UserCertService extends BaseService<UserCert>
{
	/**
	 * 根据帐号查询
	 * 
	 * @param loginName
	 * @return
	 */
	public UserCert findByLoginName(String loginName);
	
}

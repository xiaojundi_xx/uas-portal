package com.scanywhere.modules.user.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_USER_ACCOUNT")
@SuppressWarnings("serial")
public class UserAccount extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_USER_ACCOUNT";

	/**
	 * 列名常量
	 */
	// 应用系统ID
	public static final String COLUMN_RESOURCE_ID = "RESOURCE_ID";

	// 应用系统账号ID
	public static final String COLUMN_ACCOUNT_ID = "ACCOUNT_ID";

	// 应用系统帐号别名
	public static final String COLUMN_ACCOUNT_ALIAS = "ACCOUNT_ALIAS";

	// 应用系统账号
	public static final String COLUMN_ACCOUNT_NAME = "ACCOUNT_NAME";

	// 应用系统账号绑定模式
	public static final String COLUMN_BINDING_MODE = "BINDING_MODE";
		
	// 用户ID
	public static final String COLUMN_USER_ID = "USER_ID";

	// 用户账号
	public static final String COLUMN_LOGIN_NAME = "LOGIN_NAME";

	// 默认账号
	public static final String COLUMN_IS_DEFAULT = "IS_DEFAULT";

	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "应用系统ID")
	@Column(name = COLUMN_RESOURCE_ID, nullable = false, length = 32)
	private String resourceId;

	@Comment(label = "应用系统账号ID")
	@Column(name = COLUMN_ACCOUNT_ID, nullable = false, length = 32)
	private String accountId;

	@Comment(label = "应用系统帐号别名")
	@Column(name = COLUMN_ACCOUNT_ALIAS, nullable = false, length = 50)
	private String accountAlias;

	@Comment(label = "应用系统账号")
	@Column(name = COLUMN_ACCOUNT_NAME, nullable = false, length = 50)
	private String accountName;

	@Comment(label = "应用系统账号绑定模式")
	@Column(name = COLUMN_BINDING_MODE, nullable = false)
	private Integer bindingMode;
	
	@Comment(label = "用户ID")
	@Column(name = COLUMN_USER_ID, nullable = false, length = 32)
	private String userId;

	@Comment(label = "用户账号")
	@Column(name = COLUMN_LOGIN_NAME, nullable = false, length = 50)
	private String loginName;

	@Comment(label = "默认账号")
	@Column(name = COLUMN_IS_DEFAULT, nullable = false)
	private Integer isDefault;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;
	
	@Transient
	private String resourceName;

	public String getResourceId()
	{
		return resourceId;
	}

	public void setResourceId(String resourceId)
	{
		this.resourceId = resourceId;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public void setAccountId(String accountId)
	{
		this.accountId = accountId;
	}

	public String getAccountAlias()
	{
		return accountAlias;
	}

	public void setAccountAlias(String accountAlias)
	{
		this.accountAlias = accountAlias;
	}

	public String getAccountName()
	{
		if (accountName==null)
			return "";
		return accountName;
	}

	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}
	
	public Integer getBindingMode()
	{
		if(bindingMode==null)
			return 1;
		return bindingMode;
	}

	public void setBindingMode(Integer bindingMode)
	{
		this.bindingMode = bindingMode;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getLoginName()
	{
		return loginName;
	}

	public void setLoginName(String loginName)
	{
		this.loginName = loginName;
	}

	public Integer getIsDefault()
	{
		return isDefault;
	}

	public void setIsDefault(Integer isDefault)
	{
		this.isDefault = isDefault;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
	
	public String getResourceName()
	{
		return resourceName;
	}

	public void setResourceName(String resourceName)
	{
		this.resourceName = resourceName;
	}
}

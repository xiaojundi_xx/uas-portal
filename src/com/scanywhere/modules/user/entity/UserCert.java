package com.scanywhere.modules.user.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_USER_CERT")
@SuppressWarnings("serial")
public class UserCert extends BaseEntity
{

	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_USER_CERT";

	/**
	 * 列名常量
	 */
	// 唯一标识
	public static final String COLUMN_ID = "ID";
	
	// 用户ID
	public static final String COLUMN_USER_ID = "USER_ID";
		
	// 用户帐号
	public static final String COLUMN_LOGIN_NAME = "LOGIN_NAME";
	
	// 用户姓名
	public static final String COLUMN_USER_NAME = "USER_NAME";

	// 所属机构ID
	public static final String COLUMN_ORG_ID = "ORG_ID";

	// 所属机构名称
	public static final String COLUMN_ORG_NAME = "ORG_NAME";

	// 证书请求ID
	public static final String COLUMN_CERT_REQ_ID = "CERT_REQ_ID";

	// 证书请求授权码
	public static final String COLUMN_CERT_REQ_AUTHCODE = "CERT_REQ_AUTHCODE";

	// 证书请求授权码过期时间
	public static final String COLUMN_CERT_REQ_AUTHCODE_EXPIRE = "CERT_REQ_AUTHCODE_EXPIRE";

	// 签名证书序列号
	public static final String COLUMN_SERIAL_NUMBER = "SERIAL_NUMBER";

	// 签名证书
	public static final String COLUMN_CERT_CONTENT = "CERT_CONTENT";

	// 加密证书序列号
	public static final String COLUMN_ENCRYPT_SERIAL_NUMBER = "ENCRYPT_SERIAL_NUMBER";

	// 加密证书
	public static final String COLUMN_ENCRYPT_CERT_CONTENT = "ENCRYPT_CERT_CONTENT";

	// 证书状态
	public static final String COLUMN_CERT_STATUS = "CERT_STATUS";

	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "用户ID")
	@Column(name = COLUMN_USER_ID, nullable = false, length = 32)
	private String userId;
	
	@Comment(label = "用户帐号")
	@Column(name = COLUMN_LOGIN_NAME, nullable = false, length = 50)
	private String loginName;
	
	@Comment(label = "用户姓名")
	@Column(name = COLUMN_USER_NAME, nullable = false, length = 50)
	private String userName;

	@Comment(label = "所属机构ID")
	@Column(name = COLUMN_ORG_ID, length = 32)
	private String orgId;

	@Comment(label = "所属机构名称")
	@Column(name = COLUMN_ORG_NAME, length = 50)
	private String orgName;

	@Comment(label = "证书请求ID")
	@Column(name = COLUMN_CERT_REQ_ID, nullable = false)
	private String certReqId;

	@Comment(label = "证书请求授权码")
	@Column(name = COLUMN_CERT_REQ_AUTHCODE)
	private String certReqAuthCode;

	@Comment(label = "证书请求授权码过期时间")
	@Column(name = COLUMN_CERT_REQ_AUTHCODE_EXPIRE)
	private Date certReqAuthCodeExpire;

	@Comment(label = "签名证书序列号")
	@Column(name = COLUMN_SERIAL_NUMBER, length = 50)
	private String serialNumber;

	@Comment(label = "签名证书")
	@Column(name = COLUMN_CERT_CONTENT)
	private String certContent;

	@Comment(label = "加密证书序列号")
	@Column(name = COLUMN_ENCRYPT_SERIAL_NUMBER, length = 50)
	private String encryptSerialNumber;

	@Comment(label = "加密证书")
	@Column(name = COLUMN_ENCRYPT_CERT_CONTENT)
	private String encryptCertContent;
	
	@Comment(label = "证书状态")
	@Column(name = COLUMN_CERT_STATUS)
	private Integer certStatus;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP)
	private Long updateTimeStamp;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getCertReqId() {
		return certReqId;
	}

	public void setCertReqId(String certReqId) {
		this.certReqId = certReqId;
	}

	public String getCertReqAuthCode() {
		return certReqAuthCode;
	}

	public void setCertReqAuthCode(String certReqAuthCode) {
		this.certReqAuthCode = certReqAuthCode;
	}

	public Date getCertReqAuthCodeExpire() {
		return certReqAuthCodeExpire;
	}

	public void setCertReqAuthCodeExpire(Date certReqAuthCodeExpire) {
		this.certReqAuthCodeExpire = certReqAuthCodeExpire;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getCertContent() {
		return certContent;
	}

	public void setCertContent(String certContent) {
		this.certContent = certContent;
	}

	public String getEncryptSerialNumber() {
		return encryptSerialNumber;
	}

	public void setEncryptSerialNumber(String encryptSerialNumber) {
		this.encryptSerialNumber = encryptSerialNumber;
	}

	public String getEncryptCertContent() {
		return encryptCertContent;
	}

	public void setEncryptCertContent(String encryptCertContent) {
		this.encryptCertContent = encryptCertContent;
	}

	public Integer getCertStatus() {
		return certStatus;
	}

	public void setCertStatus(Integer certStatus) {
		this.certStatus = certStatus;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp() {
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}

}

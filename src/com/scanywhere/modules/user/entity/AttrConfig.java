package com.scanywhere.modules.user.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_ATTR_CONFIG")
@SuppressWarnings("serial")
public class AttrConfig extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_ATTR_CONFIG";

	/**
	 * 列名常量
	 */
	// 属性名
	public static final String COLUMN_ATTR_NAME = "ATTR_NAME";

	// 属性显示名
	public static final String COLUMN_ATTR_DISP_NAME = "ATTR_DISP_NAME";

	// 是否机构属性
	public static final String COLUMN_IS_ORG_ATTR = "IS_ORG_ATTR";

	// 属性类型
	public static final String COLUMN_ATTR_TYPE = "ATTR_TYPE";

	// 属性输入类型
	public static final String COLUMN_ATTR_INPUT_TYPE = "ATTR_INPUT_TYPE";

	// 属性状态
	public static final String COLUMN_ATTR_STATUS = "ATTR_STATUS";

	// 排序
	public static final String COLUMN_ATTR_SORT = "ATTR_SORT";

	// 属性值配置
	public static final String COLUMN_ATTR_CONTENT = "ATTR_CONTENT";

	// 属性值最大长度
	public static final String COLUMN_ATTR_MAX_LENGTH = "ATTR_MAX_LENGTH";

	// 默认值
	public static final String COLUMN_ATTR_DEFAULT_VALUE = "ATTR_DEFAULT_VALUE";

	// 显示类型
	public static final String COLUMN_DISPLAY_TYPE = "DISPLAY_TYPE";

	// 是否必填属性
	public static final String COLUMN_IS_REQUIRED = "IS_REQUIRED";

	// 是否唯一属性
	public static final String COLUMN_IS_UNIQUE = "IS_UNIQUE";

	// 是否允许编辑
	public static final String COLUMN_IS_EDIT = "IS_EDIT";

	// 备注
	public static final String COLUMN_REMARK = "REMARK";
		
	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "属性名")
	@Column(name = COLUMN_ATTR_NAME, nullable = false, length = 50)
	private String attrName;

	@Comment(label = "属性显示名")
	@Column(name = COLUMN_ATTR_DISP_NAME, nullable = false, length = 50)
	private String attrDispName;

	@Comment(label = "是否机构属性")
	@Column(name = COLUMN_IS_ORG_ATTR, nullable = false)
	private Integer isOrgAttr;

	@Comment(label = "属性类型")
	@Column(name = COLUMN_ATTR_TYPE, nullable = false)
	private Integer attrType;

	@Comment(label = "属性输入类型")
	@Column(name = COLUMN_ATTR_INPUT_TYPE, nullable = false)
	private Integer attrInputType;

	@Comment(label = "属性状态")
	@Column(name = COLUMN_ATTR_STATUS, nullable = false)
	private Integer attrStatus;

	@Comment(label = "排序")
	@Column(name = COLUMN_ATTR_SORT, nullable = false)
	private Integer attrSort;

	@Comment(label = "属性值配置")
	@Column(name = COLUMN_ATTR_CONTENT)
	private String attrContent;

	@Comment(label = "属性值最大长度")
	@Column(name = COLUMN_ATTR_MAX_LENGTH, nullable = false)
	private Integer attrMaxLength;

	@Comment(label = "默认值")
	@Column(name = COLUMN_ATTR_DEFAULT_VALUE, length = 50)
	private String attrDefaultValue;

	@Comment(label = "显示类型")
	@Column(name = COLUMN_DISPLAY_TYPE, nullable = false)
	private Integer displayType;

	@Comment(label = "是否必填属性")
	@Column(name = COLUMN_IS_REQUIRED, nullable = false)
	private Integer isRequired;

	@Comment(label = "是否唯一属性")
	@Column(name = COLUMN_IS_UNIQUE, nullable = false)
	private Integer isUnique;

	@Comment(label = "是否允许编辑")
	@Column(name = COLUMN_IS_EDIT, nullable = false)
	private Integer isEdit;

	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK, length = 255)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getAttrName()
	{
		return attrName;
	}

	public void setAttrName(String attrName)
	{
		this.attrName = attrName;
	}

	public String getAttrDispName()
	{
		return attrDispName;
	}

	public void setAttrDispName(String attrDispName)
	{
		this.attrDispName = attrDispName;
	}

	public Integer getIsOrgAttr()
	{
		return isOrgAttr;
	}

	public void setIsOrgAttr(Integer isOrgAttr)
	{
		this.isOrgAttr = isOrgAttr;
	}

	public Integer getAttrType()
	{
		return attrType;
	}

	public void setAttrType(Integer attrType)
	{
		this.attrType = attrType;
	}

	public Integer getAttrInputType()
	{
		return attrInputType;
	}

	public void setAttrInputType(Integer attrInputType)
	{
		this.attrInputType = attrInputType;
	}

	public Integer getAttrStatus()
	{
		return attrStatus;
	}

	public void setAttrStatus(Integer attrStatus)
	{
		this.attrStatus = attrStatus;
	}

	public Integer getAttrSort()
	{
		return attrSort;
	}

	public void setAttrSort(Integer attrSort)
	{
		this.attrSort = attrSort;
	}

	public String getAttrContent()
	{
		return attrContent;
	}

	public void setAttrContent(String attrContent)
	{
		this.attrContent = attrContent;
	}

	public Integer getAttrMaxLength()
	{
		return attrMaxLength;
	}

	public void setAttrMaxLength(Integer attrMaxLength)
	{
		this.attrMaxLength = attrMaxLength;
	}

	public String getAttrDefaultValue()
	{
		return attrDefaultValue;
	}

	public void setAttrDefaultValue(String attrDefaultValue)
	{
		this.attrDefaultValue = attrDefaultValue;
	}

	public Integer getDisplayType()
	{
		return displayType;
	}

	public void setDisplayType(Integer displayType)
	{
		this.displayType = displayType;
	}

	public Integer getIsRequired()
	{
		return isRequired;
	}

	public void setIsRequired(Integer isRequired)
	{
		this.isRequired = isRequired;
	}

	public Integer getIsUnique()
	{
		return isUnique;
	}

	public void setIsUnique(Integer isUnique)
	{
		this.isUnique = isUnique;
	}

	public Integer getIsEdit()
	{
		return isEdit;
	}

	public void setIsEdit(Integer isEdit)
	{
		this.isEdit = isEdit;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

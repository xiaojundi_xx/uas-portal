package com.scanywhere.modules.user.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_USER")
@SuppressWarnings("serial")
public class User extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_USER";

	/**
	 * 列名常量
	 */
	// 唯一标识
	public static final String COLUMN_USER_GUID = "USER_GUID";
		
	// 用户帐号
	public static final String COLUMN_LOGIN_NAME = "LOGIN_NAME";

	// 用户姓名
	public static final String COLUMN_USER_NAME = "USER_NAME";

	// 用户密码
	public static final String COLUMN_USER_PASSWORD = "USER_PASSWORD";
	
	// 表单代填密码
	public static final String COLUMN_FORM_PASSWORD = "FORM_PASSWORD";
	
	// 状态
	public static final String COLUMN_USER_STATUS = "USER_STATUS";

	// 排序
	public static final String COLUMN_USER_SORT = "USER_SORT";

	// 工号
	public static final String COLUMN_WORK_NUMBER = "WORK_NUMBER";

	// 身份证号
	public static final String COLUMN_ID_NUMBER = "ID_NUMBER";

	// 性别
	public static final String COLUMN_SEX = "SEX";

	// 邮箱
	public static final String COLUMN_EMAIL = "EMAIL";

	// 办公电话
	public static final String COLUMN_OFFICE_PHONE = "OFFICE_PHONE";

	// 手机号
	public static final String COLUMN_MOBILE_PHONE = "MOBILE_PHONE";

	// 生日
	public static final String COLUMN_BIRTHDAY = "BIRTHDAY";

	// 入职时间
	public static final String COLUMN_HIRE_TIME = "HIRE_TIME";

	// 有效期
	public static final String COLUMN_EXPIRED_TIME = "EXPIRED_TIME";

	// 所属机构ID
	public static final String COLUMN_ORG_ID = "ORG_ID";

	// 所属机构名称
	public static final String COLUMN_ORG_NAME = "ORG_NAME";

	// 首次登录
	public static final String COLUMN_FIRST_LOGIN = "FIRST_LOGIN";

	// 密码修改时间
	public static final String COLUMN_PWD_MODIFY_TIME = "PWD_MODIFY_TIME";

	// 创建者名称
	public static final String COLUMN_CREATOR_NAME = "CREATOR_NAME";

	// 修改者名称
	public static final String COLUMN_MENDER_NAME = "MENDER_NAME";

	// 备注
	public static final String COLUMN_REMARK = "REMARK";
		
	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "唯一标识")
	@Column(name = COLUMN_USER_GUID, nullable = false, length = 50)
	private String userGuid;
	
	@Comment(label = "用户帐号")
	@Column(name = COLUMN_LOGIN_NAME, nullable = false, length = 50)
	private String loginName;

	@Comment(label = "用户姓名")
	@Column(name = COLUMN_USER_NAME, nullable = false, length = 50)
	private String userName;

	@Comment(label = "用户密码")
	@Column(name = COLUMN_USER_PASSWORD, nullable = false, length = 300)
	private String userPassword;

	@Comment(label = "表单代填密码")
	@Column(name = COLUMN_FORM_PASSWORD, length = 300)
	private String formPassword;

	@Comment(label = "状态")
	@Column(name = COLUMN_USER_STATUS, nullable = false)
	private Integer userStatus;

	@Comment(label = "排序")
	@Column(name = COLUMN_USER_SORT, nullable = false)
	private Integer userSort;

	@Comment(label = "工号")
	@Column(name = COLUMN_WORK_NUMBER, length = 50)
	private String workNumber;

	@Comment(label = "身份证号")
	@Column(name = COLUMN_ID_NUMBER, length = 18)
	private String idNumber;

	@Comment(label = "性别")
	@Column(name = COLUMN_SEX)
	private Integer sex;

	@Comment(label = "邮箱")
	@Column(name = COLUMN_EMAIL, length = 50)
	private String email;

	@Comment(label = "办公电话")
	@Column(name = COLUMN_OFFICE_PHONE, length = 20)
	private String officePhone;

	@Comment(label = "手机号")
	@Column(name = COLUMN_MOBILE_PHONE, length = 20)
	private String mobilePhone;

	@Comment(label = "生日")
	@Column(name = COLUMN_BIRTHDAY)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date birthday;

	@Comment(label = "入职时间")
	@Column(name = COLUMN_HIRE_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date hireTime;

	@Comment(label = "有效期")
	@Column(name = COLUMN_EXPIRED_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date expiredTime;

	@Comment(label = "所属机构ID")
	@Column(name = COLUMN_ORG_ID, length = 32)
	private String orgId;

	@Comment(label = "所属机构名称")
	@Column(name = COLUMN_ORG_NAME, length = 50)
	private String orgName;

	@Comment(label = "首次登录")
	@Column(name = COLUMN_FIRST_LOGIN, nullable = false)
	private Integer firstLogin;

	@Comment(label = "密码修改时间")
	@Column(name = COLUMN_PWD_MODIFY_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date pwdModifyTime;

	@Comment(label = "创建者名称")
	@Column(name = COLUMN_CREATOR_NAME, nullable = false, length = 50)
	private String creatorName;

	@Comment(label = "修改者名称")
	@Column(name = COLUMN_MENDER_NAME, length = 50)
	private String menderName;

	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getUserGuid()
	{
		return userGuid;
	}

	public void setUserGuid(String userGuid)
	{
		this.userGuid = userGuid;
	}
	
	public String getLoginName()
	{
		return loginName;
	}

	public void setLoginName(String loginName)
	{
		this.loginName = loginName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getUserPassword()
	{
		return userPassword;
	}

	public void setFormPassword(String formPassword)
	{
		this.formPassword = formPassword;
	}

	public String getFormPassword()
	{
		return formPassword;
	}

	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}

	public Integer getUserStatus()
	{
		return userStatus;
	}

	public void setUserStatus(Integer userStatus)
	{
		this.userStatus = userStatus;
	}

	public Integer getUserSort()
	{
		return userSort;
	}

	public void setUserSort(Integer userSort)
	{
		this.userSort = userSort;
	}

	public String getWorkNumber()
	{
		return workNumber;
	}

	public void setWorkNumber(String workNumber)
	{
		this.workNumber = workNumber;
	}

	public String getIdNumber()
	{
		return idNumber;
	}

	public void setIdNumber(String idNumber)
	{
		this.idNumber = idNumber;
	}

	public Integer getSex()
	{
		return sex;
	}

	public void setSex(Integer sex)
	{
		this.sex = sex;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getOfficePhone()
	{
		return officePhone;
	}

	public void setOfficePhone(String officePhone)
	{
		this.officePhone = officePhone;
	}

	public String getMobilePhone()
	{
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}

	public Date getBirthday()
	{
		return birthday;
	}

	public void setBirthday(Date birthday)
	{
		this.birthday = birthday;
	}

	public Date getHireTime()
	{
		return hireTime;
	}

	public void setHireTime(Date hireTime)
	{
		this.hireTime = hireTime;
	}

	public Date getExpiredTime()
	{
		return expiredTime;
	}

	public void setExpiredTime(Date expiredTime)
	{
		this.expiredTime = expiredTime;
	}

	public String getOrgId()
	{
		return orgId;
	}

	public void setOrgId(String orgId)
	{
		this.orgId = orgId;
	}

	public String getOrgName()
	{
		return orgName;
	}

	public void setOrgName(String orgName)
	{
		this.orgName = orgName;
	}

	public Integer getFirstLogin()
	{
		return firstLogin;
	}

	public void setFirstLogin(Integer firstLogin)
	{
		this.firstLogin = firstLogin;
	}

	public Date getPwdModifyTime()
	{
		return pwdModifyTime;
	}

	public void setPwdModifyTime(Date pwdModifyTime)
	{
		this.pwdModifyTime = pwdModifyTime;
	}

	public String getCreatorName()
	{
		return creatorName;
	}

	public void setCreatorName(String creatorName)
	{
		this.creatorName = creatorName;
	}

	public String getMenderName()
	{
		return menderName;
	}

	public void setMenderName(String menderName)
	{
		this.menderName = menderName;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

package com.scanywhere.modules.user.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_ORG_ATTR")
@SuppressWarnings("serial")
public class OrgAttr extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_ORG_ATTR";

	/**
	 * 列名常量
	 */
	// 机构ID
	public static final String COLUMN_ORG_ID = "ORG_ID";

	// 扩展属性ID
	public static final String COLUMN_ATTR_ID = "ATTR_ID";
	
	// 扩展属性名
	public static final String COLUMN_ATTR_NAME = "ATTR_NAME";

	// 扩展属性值
	public static final String COLUMN_ATTR_VALUE = "ATTR_VALUE";

	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "机构ID")
	@Column(name = COLUMN_ORG_ID, nullable = false, length = 32)
	private String orgId;

	@Comment(label = "扩展属性ID")
	@Column(name = COLUMN_ATTR_ID, nullable = false, length = 32)
	private String attrId;
	
	@Comment(label = "属性名")
	@Column(name = COLUMN_ATTR_NAME, nullable = false, length = 50)
	private String attrName;

	@Comment(label = "扩展属性值")
	@Column(name = COLUMN_ATTR_VALUE)
	private String attrValue;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getOrgId()
	{
		return orgId;
	}

	public void setOrgId(String orgId)
	{
		this.orgId = orgId;
	}

	public String getAttrId()
	{
		return attrId;
	}

	public void setAttrId(String attrId)
	{
		this.attrId = attrId;
	}
	
	public String getAttrName()
	{
		return attrName;
	}

	public void setAttrName(String attrName)
	{
		this.attrName = attrName;
	}

	public String getAttrValue()
	{
		return attrValue;
	}

	public void setAttrValue(String attrValue)
	{
		this.attrValue = attrValue;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

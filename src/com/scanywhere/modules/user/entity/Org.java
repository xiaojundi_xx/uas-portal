package com.scanywhere.modules.user.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_ORG")
@SuppressWarnings("serial")
public class Org extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_ORG";

	/**
	 * 列名常量
	 */
	// 唯一标识
	public static final String COLUMN_ORG_GUID = "ORG_GUID";
		
	// 机构编码
	public static final String COLUMN_ORG_CODE = "ORG_CODE";

	// 机构名称
	public static final String COLUMN_ORG_NAME = "ORG_NAME";

	// 机构类型
	public static final String COLUMN_ORG_TYPE = "ORG_TYPE";

	// 机构层级
	public static final String COLUMN_ORG_LEVEL = "ORG_LEVEL";

	// 机构层级编码
	public static final String COLUMN_ORG_LEVEL_CODE = "ORG_LEVEL_CODE";

	// 状态
	public static final String COLUMN_ORG_STATUS = "ORG_STATUS";

	// 排序
	public static final String COLUMN_ORG_SORT = "ORG_SORT";

	// 负责人
	public static final String COLUMN_LEADER = "LEADER";

	// 办公电话
	public static final String COLUMN_OFFICE_PHONE = "OFFICE_PHONE";
	
	// 机构邮箱
	public static final String COLUMN_EMAIL = "EMAIL";
	
	// 机构地址
	public static final String COLUMN_ADDRESS = "ADDRESS";

	// 父级机构ID
	public static final String COLUMN_PARENT_ID = "PARENT_ID";

	// 父级机构编码
	public static final String COLUMN_PARENT_CODE = "PARENT_CODE";

	// 父级机构名称
	public static final String COLUMN_PARENT_NAME = "PARENT_NAME";
	
	// 是否叶子机构
	public static final String COLUMN_IS_LEAF_ORG = "IS_LEAF_ORG";

	// 创建者名称
	public static final String COLUMN_CREATOR_NAME = "CREATOR_NAME";

	// 修改者名称
	public static final String COLUMN_MENDER_NAME = "MENDER_NAME";
		
	// 备注
	public static final String COLUMN_REMARK = "REMARK";
		
	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "唯一标识")
	@Column(name = COLUMN_ORG_GUID, nullable = false, length = 50)
	private String orgGuid;
	
	@Comment(label = "机构代码")
	@Column(name = COLUMN_ORG_CODE, nullable = false, length = 50)
	private String orgCode;

	@Comment(label = "机构名称")
	@Column(name = COLUMN_ORG_NAME, nullable = false, length = 50)
	private String orgName;

	@Comment(label = "机构类型")
	@Column(name = COLUMN_ORG_TYPE, nullable = false)
	private Integer orgType;

	@Comment(label = "机构层级")
	@Column(name = COLUMN_ORG_LEVEL, nullable = false)
	private Integer orgLevel;

	@Comment(label = "机构层级编码")
	@Column(name = COLUMN_ORG_LEVEL_CODE, nullable = false, length = 300)
	private String orgLevelCode;

	@Comment(label = "状态")
	@Column(name = COLUMN_ORG_STATUS, nullable = false)
	private Integer orgStatus;

	@Comment(label = "排序")
	@Column(name = COLUMN_ORG_SORT, nullable = false)
	private Integer orgSort;

	@Comment(label = "负责人")
	@Column(name = COLUMN_LEADER, length = 50)
	private String leader;
	
	@Comment(label = "办公电话")
	@Column(name = COLUMN_OFFICE_PHONE, length = 20)
	private String officePhone;
	
	@Comment(label = "机构邮箱")
	@Column(name = COLUMN_EMAIL, length = 50)
	private String email;

	@Comment(label = "机构地址")
	@Column(name = COLUMN_ADDRESS, length = 255)
	private String address;

	@Comment(label = "父级机构ID")
	@Column(name = COLUMN_PARENT_ID, length = 32)
	private String parentId;

	@Comment(label = "父级机构编码")
	@Column(name = COLUMN_PARENT_CODE, length = 50)
	private String parentCode;

	@Comment(label = "父级机构名称")
	@Column(name = COLUMN_PARENT_NAME, length = 50)
	private String parentName;

	@Comment(label = "是否叶子机构")
	@Column(name = COLUMN_IS_LEAF_ORG)
	private int isLeafOrg;
	
	@Comment(label = "创建者名称")
	@Column(name = COLUMN_CREATOR_NAME, nullable = false, length = 50)
	private String creatorName;

	@Comment(label = "修改者名称")
	@Column(name = COLUMN_MENDER_NAME, length = 50)
	private String menderName;
	
	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK, length = 255)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getOrgGuid()
	{
		return orgGuid;
	}

	public void setOrgGuid(String orgGuid)
	{
		this.orgGuid = orgGuid;
	}
	
	public String getOrgCode()
	{
		return orgCode;
	}

	public void setOrgCode(String orgCode)
	{
		this.orgCode = orgCode;
	}

	public String getOrgName()
	{
		return orgName;
	}

	public void setOrgName(String orgName)
	{
		this.orgName = orgName;
	}

	public Integer getOrgType()
	{
		return orgType;
	}

	public void setOrgType(Integer orgType)
	{
		this.orgType = orgType;
	}

	public Integer getOrgLevel()
	{
		return orgLevel;
	}

	public void setOrgLevel(Integer orgLevel)
	{
		this.orgLevel = orgLevel;
	}

	public String getOrgLevelCode()
	{
		return orgLevelCode;
	}

	public void setOrgLevelCode(String orgLevelCode)
	{
		this.orgLevelCode = orgLevelCode;
	}

	public Integer getOrgStatus()
	{
		return orgStatus;
	}

	public void setOrgStatus(Integer orgStatus)
	{
		this.orgStatus = orgStatus;
	}

	public Integer getOrgSort()
	{
		return orgSort;
	}

	public void setOrgSort(Integer orgSort)
	{
		this.orgSort = orgSort;
	}

	public String getLeader()
	{
		return leader;
	}

	public String getOfficePhone()
	{
		return officePhone;
	}

	public void setLeader(String leader)
	{
		this.leader = leader;
	}

	public void setOfficePhone(String officePhone)
	{
		this.officePhone = officePhone;
	}
	
	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getParentId()
	{
		return parentId;
	}

	public void setParentId(String parentId)
	{
		this.parentId = parentId;
	}

	public String getParentCode()
	{
		return parentCode;
	}

	public void setParentCode(String parentCode)
	{
		this.parentCode = parentCode;
	}

	public String getParentName()
	{
		return parentName;
	}

	public void setParentName(String parentName)
	{
		this.parentName = parentName;
	}

	public int getIsLeafOrg()
	{
		return isLeafOrg;
	}

	public void setIsLeafOrg(int isLeafOrg)
	{
		this.isLeafOrg = isLeafOrg;
	}
	
	public String getCreatorName()
	{
		return creatorName;
	}

	public void setCreatorName(String creatorName)
	{
		this.creatorName = creatorName;
	}

	public String getMenderName()
	{
		return menderName;
	}

	public void setMenderName(String menderName)
	{
		this.menderName = menderName;
	}
	
	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

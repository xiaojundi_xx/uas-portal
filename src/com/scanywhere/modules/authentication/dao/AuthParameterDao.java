package com.scanywhere.modules.authentication.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.authentication.entity.AuthParameter;

public interface AuthParameterDao extends BaseDao<AuthParameter>
{

}

package com.scanywhere.modules.authentication.dao.impl;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.authentication.dao.AuthFailLocksDao;
import com.scanywhere.modules.authentication.entity.AuthFailLocks;

@Repository("authFailLocksDao")
public class AuthFailLocksDaoImpl extends BaseDaoImpl<AuthFailLocks> implements AuthFailLocksDao
{

}

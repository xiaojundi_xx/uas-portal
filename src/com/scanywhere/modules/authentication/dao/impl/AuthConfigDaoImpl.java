package com.scanywhere.modules.authentication.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.authentication.dao.AuthConfigDao;
import com.scanywhere.modules.authentication.entity.AuthConfig;

@Repository("authConfigDao")
public class AuthConfigDaoImpl extends BaseDaoImpl<AuthConfig> implements AuthConfigDao
{
	public AuthConfig findByAuthCode(String authCode)
	{
		String sql = "SELECT * FROM UAS_AUTH_CONFIG WHERE AC_CODE = :authCode";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("authCode", authCode);
		
		AuthConfig authConfig = super.getBySql(sql, params);
		return authConfig;
	}

}

package com.scanywhere.modules.authentication.dao.impl;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.authentication.dao.AuthParameterDao;
import com.scanywhere.modules.authentication.entity.AuthParameter;

@Repository("authParameterDao")
public class AuthParameterDaoImpl extends BaseDaoImpl<AuthParameter> implements AuthParameterDao
{

}

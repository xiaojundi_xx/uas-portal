package com.scanywhere.modules.authentication.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.authentication.entity.AuthFailLocks;

public interface AuthFailLocksDao extends BaseDao<AuthFailLocks>
{

}

package com.scanywhere.modules.authentication.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.authentication.entity.AuthConfig;

public interface AuthConfigDao extends BaseDao<AuthConfig>
{
	/**
	 * 根据认证编码查询
	 * 
	 * @param authCode
	 * @return
	 */
	public AuthConfig findByAuthCode(String authCode);
}

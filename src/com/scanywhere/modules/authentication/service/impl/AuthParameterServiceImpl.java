package com.scanywhere.modules.authentication.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.authentication.dao.AuthParameterDao;
import com.scanywhere.modules.authentication.entity.AuthParameter;
import com.scanywhere.modules.authentication.service.AuthParameterService;

@Service("authParameterService")
public class AuthParameterServiceImpl extends BaseServiceImpl<AuthParameter> implements AuthParameterService
{
	@Resource
	private AuthParameterDao authParameterDao;
	
	@Resource(name = "authParameterDao")
	public void setDao(BaseDao<AuthParameter> dao)
	{
		super.setDao(dao);
	}

	public Map<String, Object> findAuthParameter(String authId)
	{
		if (StringUtils.isEmpty(authId))
		{
			return new HashMap<String, Object>();
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("authId", authId);
		List<AuthParameter> configList = authParameterDao.find(params);
		if (CollectionUtils.isNotEmpty(configList))
		{
			Map<String, Object> returnMap = new HashMap<String, Object>();
			for (AuthParameter authParam : configList)
			{
				returnMap.put(authParam.getParamKey(), authParam.getParamValue());
			}
			return returnMap;
		}
		return new HashMap<String, Object>();
	}
	
	public String findValue(String authId, String key)
	{
		if (StringUtils.isEmpty(authId) || StringUtils.isEmpty(key))
		{
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("authId", authId);
		params.put("paramKey", key);
		List<AuthParameter> parameterList = authParameterDao.find(params);
		if (CollectionUtils.isNotEmpty(parameterList))
		{
			return parameterList.get(0).getParamValue();
		}
		return null;
	}
}

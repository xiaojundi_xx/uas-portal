package com.scanywhere.modules.authentication.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.authentication.dao.AuthFailLocksDao;
import com.scanywhere.modules.authentication.entity.AuthFailLocks;
import com.scanywhere.modules.authentication.service.AuthFailLocksService;

@Service("authFailLocksService")
public class AuthFailLocksServiceImpl extends BaseServiceImpl<AuthFailLocks> implements AuthFailLocksService
{
	@Resource
	private AuthFailLocksDao authFailLocksDao;
	
	@Resource(name = "authFailLocksDao")
	public void setDao(BaseDao<AuthFailLocks> dao)
	{
		super.setDao(dao);
	}

	public AuthFailLocks findFailureRecord(String authId, String userId)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("authId", authId);
		params.put("userId", userId);
		List<AuthFailLocks> records = authFailLocksDao.find(params);
		if (CollectionUtils.isNotEmpty(records))
		{
			return records.get(0);
		}
		return null;
	}

	public void saveFailureRecord(String authId, String userId, int failCount, int lockStatus, Date lastLoginTime, Date loginNextTime)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("authId", authId);
		params.put("userId", userId);
		List<AuthFailLocks> records = authFailLocksDao.find(params);
		if (CollectionUtils.isNotEmpty(records))
		{
			AuthFailLocks record = records.get(0);
			record.setLoginFailCount(failCount);
			record.setLockStatus(lockStatus);
			record.setLastLoginTime(lastLoginTime);
			if (loginNextTime != null)
			{
				record.setLoginNextTime(loginNextTime);
			}
			record.setUpdateDateTime(DateUtils.date());
			record.setUpdateTimeStamp(DateUtils.current(false));
			authFailLocksDao.update(record);
		}
		else
		{
			AuthFailLocks record = new AuthFailLocks();
			record.setAuthId(authId);
			record.setUserId(userId);
			record.setLoginFailCount(failCount);
			record.setLockStatus(lockStatus);
			record.setLastLoginTime(lastLoginTime);
			if (loginNextTime != null)
			{
				record.setLoginNextTime(loginNextTime);
			}
			record.setCreateDateTime(DateUtils.date());
			record.setUpdateDateTime(DateUtils.date());
			record.setUpdateTimeStamp(DateUtils.current(false));
			authFailLocksDao.save(record);
		}
	}

	public void removeFailureRecord(String authId, String userId)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("authId", authId);
		params.put("userId", userId);
		authFailLocksDao.delete(params);
	}

}

package com.scanywhere.modules.authentication.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.authentication.dao.AuthConfigDao;
import com.scanywhere.modules.authentication.dao.AuthParameterDao;
import com.scanywhere.modules.authentication.entity.AuthConfig;
import com.scanywhere.modules.authentication.service.AuthConfigService;

@Service("authConfigService")
public class AuthConfigServiceImpl extends BaseServiceImpl<AuthConfig> implements AuthConfigService
{
	@Resource
	private AuthConfigDao authConfigDao;
	
	@Resource
	private AuthParameterDao authParameterDao;
	
	@Resource(name = "authConfigDao")
	public void setDao(BaseDao<AuthConfig> dao)
	{
		super.setDao(dao);
	}

	public AuthConfig findByAuthCode(String authCode)
	{
		if (StringUtils.isEmpty(authCode))
		{
			return null;
		}
		
		return authConfigDao.findByAuthCode(authCode);
	}
}

package com.scanywhere.modules.authentication.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.authentication.entity.AuthConfig;

public interface AuthConfigService extends BaseService<AuthConfig>
{
	/**
	 * 根据认证编码查询
	 * 
	 * @param authCode
	 * @return
	 */
	public AuthConfig findByAuthCode(String authCode);
}

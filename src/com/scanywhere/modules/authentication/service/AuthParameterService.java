package com.scanywhere.modules.authentication.service;

import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.authentication.entity.AuthParameter;

public interface AuthParameterService extends BaseService<AuthParameter>
{
	/**
	 * 查询认证参数配置
	 * 
	 * @param authId
	 * @return
	 */
	public Map<String, Object> findAuthParameter(String authId);
	
	/**
	 * 根据配置编码查询
	 * 
	 * @param authId
	 * @param key
	 */
	public String findValue(String authId, String key);
}

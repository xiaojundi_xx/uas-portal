package com.scanywhere.modules.authentication.service;

import java.util.Date;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.authentication.entity.AuthFailLocks;

public interface AuthFailLocksService extends BaseService<AuthFailLocks>
{
	/**
	 * 根据认证方式, 用户查询失败记录
	 * 
	 * @param authId
	 * @param userId
	 * @return
	 */
	public AuthFailLocks findFailureRecord(String authId, String userId);
	
	/**
	 * 记录失败记录
	 * 
	 * @param authId
	 * @param userId
	 * @param failCount
	 * @param lockStatus
	 * @param lastLoginTime
	 * @param loginNextTime
	 */
	public void saveFailureRecord(String authId, String userId, int failCount, int lockStatus, Date lastLoginTime, Date loginNextTime);
	
	/**
	 * 删除失败记录
	 * 
	 * @param authId
	 * @param userId
	 */
	public void removeFailureRecord(String authId, String userId);
}

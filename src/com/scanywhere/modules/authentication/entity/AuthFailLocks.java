package com.scanywhere.modules.authentication.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_AUTH_FAIL_LOCKS")
@SuppressWarnings("serial")
public class AuthFailLocks extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_AUTH_FAIL_LOCKS";

	/**
	 * 列名常量
	 */
	// 认证方式ID
	public static final String COLUMN_AUTH_ID = "AUTH_ID";

	// 用户ID
	public static final String COLUMN_USER_ID = "USER_ID";

	// 登录失败次数
	public static final String COLUMN_LOGIN_FAIL_COUNT = "LOGIN_FAIL_COUNT";

	// 锁定状态
	public static final String COLUMN_LOCK_STATUS = "LOCK_STATUS";

	// 最后登录时间
	public static final String COLUMN_LAST_LOGIN_TIME = "LAST_LOGIN_TIME";

	// 下次登录时间
	public static final String COLUMN_LOGIN_NEXT_TIME = "LOGIN_NEXT_TIME";

	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "认证方式ID")
	@Column(name = COLUMN_AUTH_ID, nullable = false, length = 32)
	private String authId;

	@Comment(label = "用户ID")
	@Column(name = COLUMN_USER_ID, nullable = false, length = 32)
	private String userId;

	@Comment(label = "登录失败次数")
	@Column(name = COLUMN_LOGIN_FAIL_COUNT, nullable = false)
	private Integer loginFailCount;

	@Comment(label = "锁定状态")
	@Column(name = COLUMN_LOCK_STATUS, nullable = false)
	private Integer lockStatus;

	@Comment(label = "最后登录时间")
	@Column(name = COLUMN_LAST_LOGIN_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date lastLoginTime;

	@Comment(label = "下次登录时间")
	@Column(name = COLUMN_LOGIN_NEXT_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date loginNextTime;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getAuthId()
	{
		return authId;
	}

	public void setAuthId(String authId)
	{
		this.authId = authId;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public Integer getLoginFailCount()
	{
		return loginFailCount;
	}

	public void setLoginFailCount(Integer loginFailCount)
	{
		this.loginFailCount = loginFailCount;
	}

	public Integer getLockStatus()
	{
		return lockStatus;
	}

	public void setLockStatus(Integer lockStatus)
	{
		this.lockStatus = lockStatus;
	}

	public Date getLastLoginTime()
	{
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime)
	{
		this.lastLoginTime = lastLoginTime;
	}

	public Date getLoginNextTime()
	{
		return loginNextTime;
	}

	public void setLoginNextTime(Date loginNextTime)
	{
		this.loginNextTime = loginNextTime;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

package com.scanywhere.modules.authentication.entity;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_AUTH_CONFIG")
@SuppressWarnings("serial")
public class AuthConfig extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_AUTH_CONFIG";

	/**
	 * 列名常量
	 */
	// 认证方式编码
	public static final String COLUMN_AC_CODE = "AC_CODE";

	// 认证方式名称
	public static final String COLUMN_AC_NAME = "AC_NAME";

	// 认证级别
	public static final String COLUMN_AC_POWER_CODE = "AC_POWER_CODE";

	// 状态
	public static final String COLUMN_AC_STATUS = "AC_STATUS";

	// 排序
	public static final String COLUMN_AC_SORT = "AC_SORT";
	
	// 备注
	public static final String COLUMN_REMARK = "REMARK";

	// // 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "认证方式编码")
	@Column(name = COLUMN_AC_CODE, nullable = false, length = 50)
	private String acCode;

	@Comment(label = "认证方式名称")
	@Column(name = COLUMN_AC_NAME, nullable = false, length = 50)
	private String acName;

	@Comment(label = "认证级别")
	@Column(name = COLUMN_AC_POWER_CODE, nullable = false)
	private Integer acPowerCode;

	@Comment(label = "状态")
	@Column(name = COLUMN_AC_STATUS, nullable = false)
	private Integer acStatus;

	@Comment(label = "排序")
	@Column(name = COLUMN_AC_SORT, nullable = false)
	private Integer acSort;

	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK, length = 255)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	/**
	 * 认证方式参数配置
	 */
	@Transient
	private Map<String, Object> authParams;
	
	public String getAcCode()
	{
		return acCode;
	}

	public void setAcCode(String acCode)
	{
		this.acCode = acCode;
	}

	public String getAcName()
	{
		return acName;
	}

	public void setAcName(String acName)
	{
		this.acName = acName;
	}

	public Integer getAcPowerCode()
	{
		return acPowerCode;
	}

	public void setAcPowerCode(Integer acPowerCode)
	{
		this.acPowerCode = acPowerCode;
	}

	public Integer getAcStatus()
	{
		return acStatus;
	}

	public void setAcStatus(Integer acStatus)
	{
		this.acStatus = acStatus;
	}

	public Integer getAcSort()
	{
		return acSort;
	}

	public void setAcSort(Integer acSort)
	{
		this.acSort = acSort;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
	
	public Map<String, Object> getAuthParams()
	{
		return authParams;
	}

	public void setAuthParams(Map<String, Object> authParams)
	{
		this.authParams = authParams;
	}
}

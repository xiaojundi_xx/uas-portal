package com.scanywhere.modules.authorization.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.authorization.entity.RoleResource;

public interface RoleResourceService extends BaseService<RoleResource>
{
	
}

package com.scanywhere.modules.authorization.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.authorization.entity.Role;

public interface RoleService extends BaseService<Role>
{
}

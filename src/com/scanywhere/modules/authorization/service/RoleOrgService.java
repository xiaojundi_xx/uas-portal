package com.scanywhere.modules.authorization.service;

import java.util.List;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.authorization.entity.RoleOrg;

public interface RoleOrgService extends BaseService<RoleOrg>
{
	/**
	 * 查询机构授权的用户角色
	 * 
	 * @param orgId
	 * @return
	 */
	public List<String> findAuthorizedRoleList(String orgId);
}

package com.scanywhere.modules.authorization.service;

import java.util.List;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.authorization.entity.RoleUser;

public interface RoleUserService extends BaseService<RoleUser>
{
	/**
	 * 查询用户授权的用户角色
	 * 
	 * @param userId
	 * @return
	 */
	public List<String> findAuthorizedRoleList(String userId);
}

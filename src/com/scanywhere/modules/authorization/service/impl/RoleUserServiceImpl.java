package com.scanywhere.modules.authorization.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.authorization.dao.RoleDao;
import com.scanywhere.modules.authorization.dao.RoleUserDao;
import com.scanywhere.modules.authorization.entity.RoleUser;
import com.scanywhere.modules.authorization.service.RoleUserService;
import com.scanywhere.modules.user.dao.UserDao;

@Service("roleUserService")
public class RoleUserServiceImpl extends BaseServiceImpl<RoleUser> implements RoleUserService
{
	@Resource
	private RoleDao roleDao;
	
	@Resource
	private UserDao userDao;
	
	@Resource
	private RoleUserDao roleUserDao;
	
	@Resource(name = "roleUserDao")
	public void setDao(BaseDao<RoleUser> dao)
	{
		super.setDao(dao);
	}

	public List<String> findAuthorizedRoleList(String userId)
	{
		if (StringUtils.isEmpty(userId))
		{
			return null;
		}
		return roleUserDao.findAuthorizedRoleList(userId);
	}
}

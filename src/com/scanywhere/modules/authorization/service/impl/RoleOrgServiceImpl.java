package com.scanywhere.modules.authorization.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.authorization.dao.RoleDao;
import com.scanywhere.modules.authorization.dao.RoleOrgDao;
import com.scanywhere.modules.authorization.entity.RoleOrg;
import com.scanywhere.modules.authorization.service.RoleOrgService;

@Service("roleOrgService")
public class RoleOrgServiceImpl extends BaseServiceImpl<RoleOrg> implements RoleOrgService
{
	@Resource
	private RoleDao roleDao;
	
	@Resource
	private RoleOrgDao roleOrgDao;
	
	@Resource(name = "roleOrgDao")
	public void setDao(BaseDao<RoleOrg> dao)
	{
		super.setDao(dao);
	}

	public List<String> findAuthorizedRoleList(String orgId)
	{
		if (StringUtils.isEmpty(orgId))
		{
			return null;
		}
		return roleOrgDao.findAuthorizedRoleList(orgId);
	}
}

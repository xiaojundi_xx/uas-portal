package com.scanywhere.modules.authorization.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.authorization.dao.RoleDao;
import com.scanywhere.modules.authorization.dao.RoleOrgDao;
import com.scanywhere.modules.authorization.dao.RoleResourceDao;
import com.scanywhere.modules.authorization.dao.RoleUserDao;
import com.scanywhere.modules.authorization.entity.Role;
import com.scanywhere.modules.authorization.service.RoleService;

@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl<Role> implements RoleService
{
	@Resource
	private RoleDao roleDao;
	
	@Resource
	private RoleResourceDao roleResourceDao;
	
	@Resource
	private RoleOrgDao roleOrgDao;
	
	@Resource
	private RoleUserDao roleUserDao;
	
	@Resource(name = "roleDao")
	public void setDao(BaseDao<Role> dao)
	{
		super.setDao(dao);
	}
	
}

package com.scanywhere.modules.authorization.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.authorization.dao.RoleResourceDao;
import com.scanywhere.modules.authorization.entity.RoleResource;
import com.scanywhere.modules.authorization.service.RoleResourceService;

@Service("roleResourceService")
public class RoleResourceServiceImpl extends BaseServiceImpl<RoleResource> implements RoleResourceService
{
	@Resource
	private RoleResourceDao roleResourceDao;
	
	@Resource(name = "roleResourceDao")
	public void setDao(BaseDao<RoleResource> dao)
	{
		super.setDao(dao);
	}
}

package com.scanywhere.modules.authorization.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.authorization.dao.RoleOrgDao;
import com.scanywhere.modules.authorization.entity.RoleOrg;

@Repository("roleOrgDao")
public class RoleOrgDaoImpl extends BaseDaoImpl<RoleOrg> implements RoleOrgDao
{
	public List<String> findAuthorizedRoleList(String orgId)
	{
		String sql = "SELECT ROLE_ID FROM UAS_ROLE_ORG WHERE ORG_ID = :orgId";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("orgId", orgId);
		
		List<String> list = super.findBySql(null, sql, params);
		return list;
	}
}

package com.scanywhere.modules.authorization.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.authorization.dao.RoleUserDao;
import com.scanywhere.modules.authorization.entity.RoleUser;

@Repository("roleUserDao")
public class RoleUserDaoImpl extends BaseDaoImpl<RoleUser> implements RoleUserDao
{
	public List<String> findAuthorizedRoleList(String userId)
	{
		String sql = "SELECT ROLE_ID FROM UAS_ROLE_USER WHERE USER_ID = :userId";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		
		List<String> list = super.findBySql(null, sql, params);
		return list;
	}
}

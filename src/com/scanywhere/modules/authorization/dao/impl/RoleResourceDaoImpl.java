package com.scanywhere.modules.authorization.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.modules.authorization.dao.RoleResourceDao;
import com.scanywhere.modules.authorization.entity.RoleResource;
import com.scanywhere.modules.resource.entity.AppResource;

@Repository("roleResourceDao")
public class RoleResourceDaoImpl extends BaseDaoImpl<RoleResource> implements RoleResourceDao
{
	@SuppressWarnings("unchecked")
	public List<AppResource> findAuthorizedResourceList(String[] roleIds)
	{
		// 查询条件值
		List<Object> valueList = new ArrayList<Object>();
		valueList.add(CommonConstants.STATUS_ENABLED);
		
		String sql = "SELECT DISTINCT res.* FROM UAS_ROLE_RESOURCE roleResource ";
		sql = sql + "LEFT JOIN UAS_ROLE role ON roleResource.ROLE_ID = role.ID ";
		sql = sql + "LEFT JOIN UAS_RESOURCE res ON roleResource.RESOURCE_ID = res.ID ";
		sql = sql + "WHERE res.RESOURCE_STATUS = ? ";
		
		if (CollectionUtils.isNotEmpty(roleIds))
		{
			String[] condition = new String[roleIds.length];
			for (int i = 0; i < condition.length; i++)
			{
				condition[i] = "?";
				valueList.add(roleIds[i]);
			}
			sql = sql + "AND role.ID IN (" + org.apache.commons.lang.StringUtils.join(condition, ",") + ") ";
		}
		
		sql = sql + "ORDER BY res.RESOURCE_SORT";
		
		SQLQuery sqlQuery = super.getSession().createSQLQuery(sql);
		for (int i = 0; i < valueList.size(); i++)
		{
			sqlQuery.setParameter(i, valueList.get(i));
		}
		sqlQuery.addEntity(AppResource.class);
		return sqlQuery.list();
	}

	public boolean checkAuthorizedApp(String[] roleIds, String resourceId)
	{
		// 查询条件值
		List<Object> valueList = new ArrayList<Object>();
		valueList.add(CommonConstants.STATUS_ENABLED);
		valueList.add(resourceId);
		
		String sql = "SELECT res.ID FROM UAS_ROLE_RESOURCE roleResource ";
		sql = sql + "LEFT JOIN UAS_ROLE role ON roleResource.ROLE_ID = role.ID ";
		sql = sql + "LEFT JOIN UAS_RESOURCE res ON roleResource.RESOURCE_ID = res.ID ";
		sql = sql + "WHERE res.RESOURCE_STATUS = ? AND res.ID = ? ";
		
		if (CollectionUtils.isNotEmpty(roleIds))
		{
			String[] condition = new String[roleIds.length];
			for (int i = 0; i < condition.length; i++)
			{
				condition[i] = "?";
				valueList.add(roleIds[i]);
			}
			sql = sql + "AND role.ID IN (" + org.apache.commons.lang.StringUtils.join(condition, ",") + ") ";
		}
		
		SQLQuery sqlQuery = super.getSession().createSQLQuery(sql);
		for (int i = 0; i < valueList.size(); i++)
		{
			sqlQuery.setParameter(i, valueList.get(i));
		}
		
		if (sqlQuery.list().isEmpty())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}

package com.scanywhere.modules.authorization.dao;

import java.util.List;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.authorization.entity.RoleOrg;

public interface RoleOrgDao extends BaseDao<RoleOrg>
{
	/**
	 * 查询机构授权的用户角色
	 * 
	 * @param orgId
	 * @return
	 */
	public List<String> findAuthorizedRoleList(String orgId);
}

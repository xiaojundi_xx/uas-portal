package com.scanywhere.modules.authorization.dao;

import java.util.List;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.authorization.entity.RoleResource;
import com.scanywhere.modules.resource.entity.AppResource;

public interface RoleResourceDao extends BaseDao<RoleResource>
{
	/**
	 * 查询角色授权的应用系统
	 * 
	 * @param roleIds
	 * @return
	 */
	public List<AppResource> findAuthorizedResourceList(String[] roleIds);
	
	/**
	 * 检查用户是否有权限单点系统
	 * 
	 * @param roleIds
	 * @param resourceId
	 * @return
	 */
	public boolean checkAuthorizedApp(String[] roleIds, String resourceId);
}

package com.scanywhere.modules.authorization.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.authorization.entity.Role;

public interface RoleDao extends BaseDao<Role>
{

}

package com.scanywhere.modules.authorization.dao;

import java.util.List;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.authorization.entity.RoleUser;

public interface RoleUserDao extends BaseDao<RoleUser>
{
	/**
	 * 查询用户授权的用户角色
	 * 
	 * @param userId
	 * @return
	 */
	public List<String> findAuthorizedRoleList(String userId);
}

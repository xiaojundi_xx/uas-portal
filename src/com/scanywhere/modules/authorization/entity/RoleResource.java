package com.scanywhere.modules.authorization.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_ROLE_RESOURCE")
@SuppressWarnings("serial")
public class RoleResource extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_ROLE_RESOURCE";

	/**
	 * 列名常量
	 */
	// 角色ID
	public static final String COLUMN_ROLE_ID = "ROLE_ID";

	// 应用系统ID
	public static final String COLUMN_RESOURCE_ID = "RESOURCE_ID";

	/**
	 * 属性
	 */
	@Comment(label = "角色ID")
	@Column(name = COLUMN_ROLE_ID, nullable = false, length = 32)
	private String roleId;

	@Comment(label = "应用系统ID")
	@Column(name = COLUMN_RESOURCE_ID, nullable = false, length = 32)
	private String resourceId;

	public String getRoleId()
	{
		return roleId;
	}

	public void setRoleId(String roleId)
	{
		this.roleId = roleId;
	}

	public String getResourceId()
	{
		return resourceId;
	}

	public void setResourceId(String resourceId)
	{
		this.resourceId = resourceId;
	}
}

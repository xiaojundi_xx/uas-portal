package com.scanywhere.modules.authorization.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_ROLE_USER")
@SuppressWarnings("serial")
public class RoleUser extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_ROLE_USER";

	/**
	 * 列名常量
	 */
	// 角色ID
	public static final String COLUMN_ROLE_ID = "ROLE_ID";

	// 用户ID
	public static final String COLUMN_USER_ID = "USER_ID";

	/**
	 * 属性
	 */
	@Comment(label = "角色ID")
	@Column(name = COLUMN_ROLE_ID, nullable = false, length = 32)
	private String roleId;

	@Comment(label = "用户ID")
	@Column(name = COLUMN_USER_ID, nullable = false, length = 32)
	private String userId;

	public String getRoleId()
	{
		return roleId;
	}

	public void setRoleId(String roleId)
	{
		this.roleId = roleId;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}
}

package com.scanywhere.modules.authorization.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_ROLE_ORG")
@SuppressWarnings("serial")
public class RoleOrg extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_ROLE_ORG";

	/**
	 * 列名常量
	 */
	// 角色ID
	public static final String COLUMN_ROLE_ID = "ROLE_ID";

	// 组织机构ID
	public static final String COLUMN_ORG_ID = "ORG_ID";

	/**
	 * 属性
	 */
	@Comment(label = "角色ID")
	@Column(name = COLUMN_ROLE_ID, nullable = false, length = 32)
	private String roleId;

	@Comment(label = "组织机构ID")
	@Column(name = COLUMN_ORG_ID, nullable = false, length = 32)
	private String orgId;

	public String getRoleId()
	{
		return roleId;
	}

	public void setRoleId(String roleId)
	{
		this.roleId = roleId;
	}

	public String getOrgId()
	{
		return orgId;
	}

	public void setOrgId(String orgId)
	{
		this.orgId = orgId;
	}
}

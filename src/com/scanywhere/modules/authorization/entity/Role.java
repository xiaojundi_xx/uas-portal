package com.scanywhere.modules.authorization.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_ROLE")
@SuppressWarnings("serial")
public class Role extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_ROLE";

	/**
	 * 列名常量
	 */
	// 角色编码
	public static final String COLUMN_ROLE_CODE = "ROLE_CODE";

	// 角色名称
	public static final String COLUMN_ROLE_NAME = "ROLE_NAME";

	// 角色类型
	public static final String COLUMN_ROLE_TYPE = "ROLE_TYPE";

	// 状态
	public static final String COLUMN_ROLE_STATUS = "ROLE_STATUS";

	// 排序
	public static final String COLUMN_ROLE_SORT = "ROLE_SORT";

	// 备注
	public static final String COLUMN_REMARK = "REMARK";
		
	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "角色编码")
	@Column(name = COLUMN_ROLE_CODE, nullable = false, length = 50)
	private String roleCode;

	@Comment(label = "角色名称")
	@Column(name = COLUMN_ROLE_NAME, nullable = false, length = 50)
	private String roleName;

	@Comment(label = "角色类型")
	@Column(name = COLUMN_ROLE_TYPE, nullable = false)
	private Integer roleType;

	@Comment(label = "状态")
	@Column(name = COLUMN_ROLE_STATUS, nullable = false)
	private Integer roleStatus;

	@Comment(label = "排序")
	@Column(name = COLUMN_ROLE_SORT, nullable = false)
	private Integer roleSort;

	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK, length = 255)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getRoleCode()
	{
		return roleCode;
	}

	public void setRoleCode(String roleCode)
	{
		this.roleCode = roleCode;
	}

	public String getRoleName()
	{
		return roleName;
	}

	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}

	public Integer getRoleType()
	{
		return roleType;
	}

	public void setRoleType(Integer roleType)
	{
		this.roleType = roleType;
	}

	public Integer getRoleStatus()
	{
		return roleStatus;
	}

	public void setRoleStatus(Integer roleStatus)
	{
		this.roleStatus = roleStatus;
	}

	public Integer getRoleSort()
	{
		return roleSort;
	}

	public void setRoleSort(Integer roleSort)
	{
		this.roleSort = roleSort;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

package com.scanywhere.modules.resource.service;

import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.resource.entity.SsoParameter;

public interface SsoParameterService extends BaseService<SsoParameter>
{
	/**
	 * 查询单点登录参数配置
	 * 
	 * @param resourceId
	 * @return
	 */
	public Map<String, Object> findSsoParameter(String resourceId);
}

package com.scanywhere.modules.resource.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.resource.entity.ResourceAccount;

public interface ResourceAccountService extends BaseService<ResourceAccount>
{
	/**
	 * 检查帐号是否唯一
	 * 
	 * @param resourceId
	 * @param accountId
	 * @param accountName
	 * @return
	 */
	public boolean isAccountUnique(String resourceId, String accountId, String accountName);
	
	/**
	 * 保存子帐号
	 * 
	 * @param account
	 * @return
	 */
	public String saveAccount(ResourceAccount account);
	
	/**
	 * 更新子帐号
	 * 
	 * @param account
	 * @return
	 */
	public String updateAccount(ResourceAccount account);
	
	/**
	 * 解绑帐号
	 * 
	 * @param id
	 * @return
	 */
	public String deleteAccount(String id);
}

package com.scanywhere.modules.resource.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.resource.entity.AppResource;

public interface ResourceService extends BaseService<AppResource>
{
	/**
	 * 根据编码查询应用系统信息
	 * 
	 * @param resourceCode
	 * @return
	 */
	public AppResource findByCode(String resourceCode);
}

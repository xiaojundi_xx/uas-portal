package com.scanywhere.modules.resource.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.resource.dao.ResourceDao;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.service.ResourceService;

@Service("resourceService")
public class ResourceServiceImpl extends BaseServiceImpl<AppResource> implements ResourceService
{
	@Resource
	private ResourceDao resourceDao;
	
	@Resource(name = "resourceDao")
	public void setDao(BaseDao<AppResource> dao)
	{
		super.setDao(dao);
	}

	public AppResource findByCode(String resourceCode)
	{
		if (StringUtils.isEmpty(resourceCode))
		{
			return null;
		}
		return resourceDao.findByCode(resourceCode);
	}
}

package com.scanywhere.modules.resource.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.springframework.stereotype.Service;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.resource.dao.ResourceIconDao;
import com.scanywhere.modules.resource.entity.AppIcon;
import com.scanywhere.modules.resource.service.ResourceIconService;

@Service("resourceIconService")
public class ResourceIconServiceImpl extends BaseServiceImpl<AppIcon> implements ResourceIconService
{
	@Resource
	private ResourceIconDao resourceIconDao;

	@Resource(name = "resourceIconDao")
	public void setDao(BaseDao<AppIcon> dao)
	{
		super.setDao(dao);
	}

	@Override
	public Map<String, BufferedImage> loadDate() {
		List<AppIcon> dataList = super.find();
		
		// 返回数据
		Map<String, BufferedImage> dataMap = new HashMap<String, BufferedImage>();
		ByteArrayInputStream in;
		if(!dataList.isEmpty()){
			for(AppIcon appIcon : dataList){
				in = new ByteArrayInputStream(appIcon.getIconContext());
				try {
					dataMap.put(appIcon.getResourceCode(), ImageIO.read(in));
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("应用图标加载失败，应用编码：" + appIcon.getResourceCode());
				}
			}
		}
		return dataMap;
	}


}

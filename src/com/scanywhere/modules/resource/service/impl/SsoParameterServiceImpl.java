package com.scanywhere.modules.resource.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.resource.dao.ResourceDao;
import com.scanywhere.modules.resource.dao.SsoParameterDao;
import com.scanywhere.modules.resource.entity.SsoParameter;
import com.scanywhere.modules.resource.service.SsoParameterService;

@Service("ssoParameterService")
public class SsoParameterServiceImpl extends BaseServiceImpl<SsoParameter> implements SsoParameterService
{
	@Resource
	private ResourceDao appResourceDao;
	
	@Resource
	private SsoParameterDao ssoParameterDao;
	
	@Resource(name = "ssoParameterDao")
	public void setDao(BaseDao<SsoParameter> dao)
	{
		super.setDao(dao);
	}

	public Map<String, Object> findSsoParameter(String resourceId)
	{
		if (StringUtils.isEmpty(resourceId))
		{
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceId", resourceId);
		List<SsoParameter> configList = ssoParameterDao.find(params);
		if (CollectionUtils.isNotEmpty(configList))
		{
			Map<String, Object> returnMap = new HashMap<String, Object>();
			for (SsoParameter ssoParam : configList)
			{
				returnMap.put(ssoParam.getParamKey(), ssoParam.getParamValue());
			}
			return returnMap;
		}
		return null;
	}

}

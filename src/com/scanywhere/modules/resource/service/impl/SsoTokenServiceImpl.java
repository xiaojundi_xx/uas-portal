package com.scanywhere.modules.resource.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.resource.dao.SsoTokenDao;
import com.scanywhere.modules.resource.entity.SsoToken;
import com.scanywhere.modules.resource.service.SsoTokenService;

@Service("ssoTokenService")
public class SsoTokenServiceImpl extends BaseServiceImpl<SsoToken> implements SsoTokenService
{
	@Resource
	private SsoTokenDao ssoTokenDao;
	
	@Resource(name = "ssoTokenDao")
	public void setDao(BaseDao<SsoToken> dao)
	{
		super.setDao(dao);
	}

	public SsoToken findByToken(String token)
	{
		if (StringUtils.isEmpty(token))
		{
			return null;
		}
		return ssoTokenDao.findByToken(token);
	}

}

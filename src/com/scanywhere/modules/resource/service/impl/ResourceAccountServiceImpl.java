package com.scanywhere.modules.resource.service.impl;

import static org.dozer.loader.api.TypeMappingOptions.mapEmptyString;
import static org.dozer.loader.api.TypeMappingOptions.mapNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import org.springframework.stereotype.Service;

import com.scanywhere.common.codec.AESUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.constant.LogConstant;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.resource.dao.ResourceAccountDao;
import com.scanywhere.modules.resource.dao.ResourceDao;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.entity.ResourceAccount;
import com.scanywhere.modules.resource.service.ResourceAccountService;
import com.scanywhere.modules.user.dao.UserAccountDao;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.entity.UserAccount;
import com.scanywhere.security.SecurityLoginUtils;

@Service("resourceAccountService")
public class ResourceAccountServiceImpl extends BaseServiceImpl<ResourceAccount> implements ResourceAccountService
{
	@Resource
	private ResourceDao resourceDao;
	
	@Resource
	private ResourceAccountDao resourceAccountDao;
	
	@Resource
	private UserAccountDao userAccountDao;
	
	@Resource(name = "resourceAccountDao")
	public void setDao(BaseDao<ResourceAccount> dao)
	{
		super.setDao(dao);
	}

	public boolean isAccountUnique(String resourceId, String accountId, String accountName)
	{
		if (StringUtils.isEmpty(resourceId) || StringUtils.isEmpty(accountName))
		{
			return false;
		}
		return resourceAccountDao.uniqueValidate(resourceId, accountId, accountName);
	}
	
	public String saveAccount(ResourceAccount account)
	{
		// 登录用户信息
		User user = (User) SecurityLoginUtils.getSubject().getPrincipal().getInfo();
		
		// 查询应用信息
		AppResource resource = resourceDao.getById(account.getResourceId());
		if (resource == null)
		{
			return "未查询到应用系统!";
		}
		
		// 判断密码是否必填
		if (resource.getRequiredAccountPassword() == CommonConstants.STATUS_0 && StringUtils.isEmpty(account.getAccountPassword()))
		{
			return "子账号密码为必填项!";
		}
		/*
		//暂时屏蔽子账号唯一性校验
		// 检查子帐号唯一性
		boolean isUnique = resourceAccountDao.uniqueValidate(account.getResourceId(), null, account.getAccountName());
		if (!isUnique)
		{
			// 记录日志
			String logContext = "注册子帐号, 子帐号已被使用, 角色编码: " + account.getAccountName() + ".";
			LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_INSERT_DATA, "注册子帐号", LogConstant.FAILURE, logContext);
			return "该子帐号已被使用!";
		}
		*/
		// 密码为空
		if (StringUtils.isEmpty(account.getAccountPassword()))
		{
			account.setAccountPassword("!@#qwer123");
		}
		
		// 判断别名
		if (StringUtils.isEmpty(account.getAccountAlias()))
		{
			account.setAccountAlias(account.getAccountName());
		}
		
		// 创建子帐号
		String password = AESUtils.encode(account.getAccountPassword());
		account.setAccountPassword(password);
		account.setBindingMode(ProjectConstants.ACCOUNT_BINDING_MODE_USER);
		account.setAccountStatus(CommonConstants.STATUS_ENABLED);
		account.setCreatorName(user.getLoginName());
		account.setCreateDateTime(DateUtils.date());
		account.setUpdateDateTime(DateUtils.date());
		account.setUpdateTimeStamp(DateUtils.current(false));
		String id = (String) super.save(account);
		
		// 绑定关系
		UserAccount userAccount = new UserAccount();
		userAccount.setResourceId(account.getResourceId());
		userAccount.setAccountId(id);
		userAccount.setAccountAlias(account.getAccountAlias());
		userAccount.setAccountName(account.getAccountName());
		userAccount.setBindingMode(ProjectConstants.ACCOUNT_BINDING_MODE_USER);
		userAccount.setUserId(user.getId());
		userAccount.setLoginName(user.getLoginName());
		userAccount.setIsDefault(CommonConstants.STATUS_1);
		userAccount.setCreateDateTime(DateUtils.date());
		userAccount.setUpdateDateTime(DateUtils.date());
		userAccount.setUpdateTimeStamp(DateUtils.current(false));
		userAccountDao.save(userAccount);
		
		// 记录日志
		String logContext = "注册子帐号, 子帐号: " + account.getAccountName() + ", 别名: " + account.getAccountAlias() + ".";
		LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_INSERT_DATA, "注册子帐号", LogConstant.SUCCESS, logContext);
		return null;
	}
	
	public String updateAccount(ResourceAccount account)
	{
		// 查询应用信息
		AppResource resource = resourceDao.getById(account.getResourceId());
		if (resource == null)
		{
			return "未查询到应用系统!";
		}
		
		// 判断密码是否必填
		if (resource.getRequiredAccountPassword() == CommonConstants.STATUS_0 && StringUtils.isEmpty(account.getAccountPassword()))
		{
			return "子账号密码为必填项!";
		}
				
		// 查询原始数据
		ResourceAccount origAccount = super.getById(account.getId());
		if (origAccount == null)
		{
			return "未查询到子帐号信息!";
		}
		
		// 判断子帐号是用户绑定关系
		if (origAccount.getBindingMode() != ProjectConstants.ACCOUNT_BINDING_MODE_USER)
		{
			return "无此子帐号维护权限!";
		}
		
		//++ 暂时屏蔽 子账号唯一性校验 
		// 检查子帐号唯一性
//		boolean isUnique = resourceAccountDao.uniqueValidate(account.getResourceId(), account.getId(), account.getAccountName());
//		if (!isUnique)
//		{
//			// 记录日志
//			String logContext = "编辑子帐号, 子帐号已被使用, 角色编码: " + account.getAccountName() + ".";
//			LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_UPDATE_DATA, "编辑子帐号", LogConstant.FAILURE, logContext);
//			return "该子帐号已被使用!";
//		}
		
		// 登录用户信息
		User user = (User) SecurityLoginUtils.getSubject().getPrincipal().getInfo();
		
		// 对象映射
		DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
		final BeanMappingBuilder builder = new BeanMappingBuilder() {
			protected void configure() {
	            mapping(ResourceAccount.class, ResourceAccount.class, TypeMappingOptions.oneWay(), mapNull(false), mapEmptyString(true))
	            .exclude("id")
	            .exclude("resourceId")
	            .exclude("accountPassword")
	            .exclude("bindingMode")
	            .exclude("expiredTime")
	            .exclude("creatorName")
	            .exclude("userId")
	            .exclude("loginName")
	            .exclude("userName")
	            .exclude("createDateTime");
			}
		};
		dozerBeanMapper.addMapping(builder);
		dozerBeanMapper.map(account, origAccount);
		dozerBeanMapper.destroy();
		
		// 设置属性
		origAccount.setAccountAlias(origAccount.getAccountName());
		if (StringUtils.isNotEmpty(account.getAccountPassword()) && !account.getAccountPassword().equals("********"))
		{
			String password = AESUtils.encode(account.getAccountPassword());
			origAccount.setAccountPassword(password);
		}
		else if (StringUtils.isEmpty(account.getAccountPassword()))
		{
			String password = AESUtils.encode("!@#qwer123");
			origAccount.setAccountPassword(password);
		}
		origAccount.setMenderName(user.getLoginName());
		origAccount.setUpdateDateTime(DateUtils.date());
		origAccount.setUpdateTimeStamp(DateUtils.current(false));
		super.update(origAccount);
		
		// 查询关联的用户账号
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("accountId", account.getId());
		List<UserAccount> accountList = userAccountDao.find(params);
		if (CollectionUtils.isNotEmpty(accountList))
		{
			for (UserAccount userAccount : accountList)
			{
				userAccount.setAccountAlias(origAccount.getAccountAlias());
				userAccount.setAccountName(origAccount.getAccountName());
				userAccount.setUpdateDateTime(DateUtils.date());
				userAccount.setUpdateTimeStamp(DateUtils.current(false));
				userAccountDao.save(userAccount);
			}
		}
		
		// 记录日志
		String logContext = "编辑子帐号, 子帐号: " + account.getAccountName() + ", 别名: " + account.getAccountAlias() + ".";
		LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_UPDATE_DATA, "编辑子帐号", LogConstant.SUCCESS, logContext);
		return null;
	}
	
	public String deleteAccount(String id)
	{
		if (StringUtils.isEmpty(id))
		{
			return "参数不许允许传空值!";
		}
		
		// 查询原始数据
		ResourceAccount account = super.getById(id);
		if (account == null)
		{
			return "未查询到子帐号信息!";
		}
		
		
		// 判断子帐号是用户绑定关系
		if (account.getBindingMode() != ProjectConstants.ACCOUNT_BINDING_MODE_USER)
		{
			return "无此子帐号维护权限!";
		}
		
		// 登录用户信息
		User user = (User) SecurityLoginUtils.getSubject().getPrincipal().getInfo();
				
		// 删除授权
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("accountId", account.getId());
		params.put("userId", user.getId());
		userAccountDao.delete(params);
		
		// 删除帐号
		resourceAccountDao.deleteById(id);
		
		// 记录日志
		String logContext = "解除绑定子账号, 子帐号: " + account.getAccountName() + ", 别名: " + account.getAccountAlias() + ".";
		LogUtils.me().saveUserLog(SecurityLoginUtils.getSubject(), LogConstant.ACTION_TYPE_DELETE_DATA, "解除绑定子账号", LogConstant.SUCCESS, logContext);
		return null;
	}
}

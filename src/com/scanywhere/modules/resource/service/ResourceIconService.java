package com.scanywhere.modules.resource.service;

import java.awt.image.BufferedImage;
import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.resource.entity.AppIcon;

public interface ResourceIconService extends BaseService<AppIcon>
{
	/**
	 * 获取应用图标
	 * 
	 * @param userId
	 * @param orgId
	 * @param appId
	 * @return
	 */
	public Map<String, BufferedImage> loadDate();
}

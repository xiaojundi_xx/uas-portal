package com.scanywhere.modules.resource.service;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.resource.entity.SsoToken;

public interface SsoTokenService extends BaseService<SsoToken>
{
	/**
	 * 查询Token
	 * 
	 * @param token
	 * @return
	 */
	public SsoToken findByToken(String token);
}

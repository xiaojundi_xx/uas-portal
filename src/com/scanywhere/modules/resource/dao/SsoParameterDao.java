package com.scanywhere.modules.resource.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.resource.entity.SsoParameter;

public interface SsoParameterDao extends BaseDao<SsoParameter>
{

}

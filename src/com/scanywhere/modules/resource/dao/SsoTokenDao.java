package com.scanywhere.modules.resource.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.resource.entity.SsoToken;

public interface SsoTokenDao extends BaseDao<SsoToken>
{
	/**
	 * 查询Token
	 * 
	 * @param token
	 * @return
	 */
	public SsoToken findByToken(String token);
}

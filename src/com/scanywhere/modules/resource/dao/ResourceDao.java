package com.scanywhere.modules.resource.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.resource.entity.AppResource;

public interface ResourceDao extends BaseDao<AppResource>
{
	/**
	 * 根据编码查询应用系统
	 * 
	 * @param resourceCode
	 * @return
	 */
	public AppResource findByCode(String resourceCode);
}

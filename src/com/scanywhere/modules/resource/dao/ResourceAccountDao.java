package com.scanywhere.modules.resource.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.resource.entity.ResourceAccount;

public interface ResourceAccountDao extends BaseDao<ResourceAccount>
{
	/**
	 * 检查应用帐号是否唯一
	 * 
	 * @param resourceId
	 * @param accountId
	 * @param accountName
	 * @return
	 */
	public boolean uniqueValidate(String resourceId, String accountId, String accountName);
}

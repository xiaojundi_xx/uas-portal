package com.scanywhere.modules.resource.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.resource.entity.AppIcon;

public interface ResourceIconDao extends BaseDao<AppIcon>
{
	/**
	 * 根据应用编码查询应用图标
	 * 
	 * @param resourceCode
	 * @return
	 */
	public AppIcon findByCode(String resourceCode);
}

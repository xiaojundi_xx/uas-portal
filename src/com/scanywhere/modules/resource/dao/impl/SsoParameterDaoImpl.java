package com.scanywhere.modules.resource.dao.impl;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.resource.dao.SsoParameterDao;
import com.scanywhere.modules.resource.entity.SsoParameter;

@Repository("ssoParameterDao")
public class SsoParameterDaoImpl extends BaseDaoImpl<SsoParameter> implements SsoParameterDao
{

}

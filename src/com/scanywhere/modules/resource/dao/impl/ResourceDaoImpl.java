package com.scanywhere.modules.resource.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.resource.dao.ResourceDao;
import com.scanywhere.modules.resource.entity.AppResource;

@Repository("resourceDao")
public class ResourceDaoImpl extends BaseDaoImpl<AppResource> implements ResourceDao
{
	public AppResource findByCode(String resourceCode)
	{
		String sql = "SELECT * FROM UAS_RESOURCE WHERE RESOURCE_CODE = :resourceCode";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceCode", resourceCode);
		
		AppResource resource = super.getBySql(sql, params);
		return resource;
	}
}

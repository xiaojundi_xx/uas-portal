package com.scanywhere.modules.resource.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.resource.dao.ResourceIconDao;
import com.scanywhere.modules.resource.entity.AppIcon;

@Repository("resourceIconDao")
public class ResourceIconDaoImpl extends BaseDaoImpl<AppIcon> implements ResourceIconDao
{
	public AppIcon findByCode(String resourceCode)
	{
		String sql = "SELECT * FROM UAS_RESOURCE_ICON WHERE RESOURCE_CODE = :resourceCode";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceCode", resourceCode);
		
		AppIcon appIcon = super.getBySql(sql, params);
		return appIcon;
	}
}

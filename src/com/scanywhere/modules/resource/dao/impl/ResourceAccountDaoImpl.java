package com.scanywhere.modules.resource.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.resource.dao.ResourceAccountDao;
import com.scanywhere.modules.resource.entity.ResourceAccount;

@Repository("resourceAccountDao")
public class ResourceAccountDaoImpl extends BaseDaoImpl<ResourceAccount> implements ResourceAccountDao
{
	public boolean uniqueValidate(String resourceId, String accountId, String accountName)
	{
		Criteria criteria = getSession().createCriteria(ResourceAccount.class);
		criteria.add(Restrictions.eq("resourceId", resourceId));
		criteria.add(Restrictions.eq("accountName", accountName));
		if (StringUtils.isNotEmpty(accountId))
		{
			accountId = "{alias}.id!='" + accountId + "'";
			criteria.add(Restrictions.sqlRestriction(accountId));
		}
		return criteria.list().isEmpty();
	}
}

package com.scanywhere.modules.resource.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.resource.dao.SsoTokenDao;
import com.scanywhere.modules.resource.entity.SsoToken;

@Repository("ssoTokenDao")
public class SsoTokenDaoImpl extends BaseDaoImpl<SsoToken> implements SsoTokenDao
{
	public SsoToken findByToken(String token)
	{
		String sql = "SELECT * FROM UAS_SSO_TOKEN WHERE ACCESS_TOKEN = :token";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("token", token);
		
		SsoToken ssoToken = super.getBySql(sql, params);
		return ssoToken;
	}
}

package com.scanywhere.modules.resource.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_RESOURCE_ICON")
@SuppressWarnings("serial")
public class AppIcon extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_RESOURCE_ICON";

	/**
	 * 列名常量
	 */
	// 应用编码
	public static final String COLUMN_RESOURCE_CODE = "RESOURCE_CODE";
	
	// 应用图标
	public static final String COLUMN_ICON_CONTEXT = "ICON_CONTEXT";
		
	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";
	
	// 预留字段1
	public static final String COLUMN_TEMP_FIELD1 = "TEMP_FIELD1";
	
	// 预留字段2
	public static final String COLUMN_TEMP_FIELD2 = "TEMP_FIELD2";
	

	/**
	 * 属性
	 */
	@Comment(label = "应用编码")
	@Column(name = COLUMN_RESOURCE_CODE, nullable = false, length = 50)
	private String resourceCode;

	@Comment(label = "应用名称")
	@Column(name = COLUMN_ICON_CONTEXT, nullable = false)
	private byte[] iconContext;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP)
	private Long updateTimeStamp;

	@Comment(label = "预留字段1")
	@Column(name = COLUMN_TEMP_FIELD1)
	private String tempField1;
	
	@Comment(label = "预留字段2")
	@Column(name = COLUMN_TEMP_FIELD2)
	private String tempField2;
	
	public String getResourceCode()
	{
		return resourceCode;
	}

	public void setResourceCode(String resourceCode)
	{
		this.resourceCode = resourceCode;
	}

	public byte[] getIconContext()
	{
		return iconContext;
	}

	public void setIconContext(byte[] iconContext)
	{
		this.iconContext = iconContext;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
	
	public String getTempField1()
	{
		return tempField1;
	}

	public void setTempField1(String tempField1)
	{
		this.tempField1 = tempField1;
	}
	
	public String getTempField2()
	{
		return tempField2;
	}

	public void setTempField2(String tempField2)
	{
		this.tempField2 = tempField2;
	}
}

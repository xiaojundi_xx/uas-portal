package com.scanywhere.modules.resource.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_RESOURCE_ACCOUNT")
@SuppressWarnings("serial")
public class ResourceAccount extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_RESOURCE_ACCOUNT";

	/**
	 * 列名常量
	 */
	// 应用系统ID
	public static final String COLUMN_RESOURCE_ID = "RESOURCE_ID";

	// 应用系统帐号别名
	public static final String COLUMN_ACCOUNT_ALIAS = "ACCOUNT_ALIAS";

	// 应用系统帐号
	public static final String COLUMN_ACCOUNT_NAME = "ACCOUNT_NAME";

	// 应用系统账号密码
	public static final String COLUMN_ACCOUNT_PASSWORD = "ACCOUNT_PASSWORD";

	// 应用系统账号状态
	public static final String COLUMN_ACCOUNT_STATUS = "ACCOUNT_STATUS";
	
	// 应用系统账号绑定模式
	public static final String COLUMN_BINDING_MODE = "BINDING_MODE";

	// 账号有效期
	public static final String COLUMN_EXPIRED_TIME = "EXPIRED_TIME";

	// 创建者名称
	public static final String COLUMN_CREATOR_NAME = "CREATOR_NAME";

	// 修改者名称
	public static final String COLUMN_MENDER_NAME = "MENDER_NAME";

	// 绑定的用户ID
	public static final String COLUMN_USER_ID = "USER_ID";

	// 绑定的用户账号
	public static final String COLUMN_LOGIN_NAME = "LOGIN_NAME";

	// 绑定的用户姓名
	public static final String COLUMN_USER_NAME = "USER_NAME";

	// 备注
	public static final String COLUMN_REMARK = "REMARK";
		
	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	/**
	 * 属性
	 */
	@Comment(label = "应用系统ID")
	@Column(name = COLUMN_RESOURCE_ID, nullable = false, length = 32)
	private String resourceId;

	@Comment(label = "应用系统帐号别名")
	@Column(name = COLUMN_ACCOUNT_ALIAS, nullable = false, length = 50)
	private String accountAlias;

	@Comment(label = "应用系统帐号")
	@Column(name = COLUMN_ACCOUNT_NAME, nullable = false, length = 50)
	private String accountName;

	@Comment(label = "应用系统账号密码")
	@Column(name = COLUMN_ACCOUNT_PASSWORD, nullable = false, length = 300)
	private String accountPassword;

	@Comment(label = "应用系统账号状态")
	@Column(name = COLUMN_ACCOUNT_STATUS, nullable = false)
	private Integer accountStatus;

	@Comment(label = "应用系统账号绑定模式")
	@Column(name = COLUMN_BINDING_MODE, nullable = false)
	private Integer bindingMode;
	
	@Comment(label = "账号有效期")
	@Column(name = COLUMN_EXPIRED_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date expiredTime;

	@Comment(label = "创建者名称")
	@Column(name = COLUMN_CREATOR_NAME, nullable = false, length = 50)
	private String creatorName;

	@Comment(label = "修改者名称")
	@Column(name = COLUMN_MENDER_NAME, length = 50)
	private String menderName;

	@Comment(label = "绑定的用户ID")
	@Column(name = COLUMN_USER_ID, length = 32)
	private String userId;

	@Comment(label = "绑定的用户账号")
	@Column(name = COLUMN_LOGIN_NAME, length = 50)
	private String loginName;

	@Comment(label = "绑定的用户姓名")
	@Column(name = COLUMN_USER_NAME, length = 50)
	private String userName;

	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK, length = 255)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getResourceId()
	{
		return resourceId;
	}

	public void setResourceId(String resourceId)
	{
		this.resourceId = resourceId;
	}

	public String getAccountAlias()
	{
		return accountAlias;
	}

	public void setAccountAlias(String accountAlias)
	{
		this.accountAlias = accountAlias;
	}

	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}

	public String getAccountPassword()
	{
		return accountPassword;
	}

	public void setAccountPassword(String accountPassword)
	{
		this.accountPassword = accountPassword;
	}

	public Integer getAccountStatus()
	{
		return accountStatus;
	}

	public void setAccountStatus(Integer accountStatus)
	{
		this.accountStatus = accountStatus;
	}

	public Integer getBindingMode()
	{
		return bindingMode;
	}

	public void setBindingMode(Integer bindingMode)
	{
		this.bindingMode = bindingMode;
	}
	
	public Date getExpiredTime()
	{
		return expiredTime;
	}

	public void setExpiredTime(Date expiredTime)
	{
		this.expiredTime = expiredTime;
	}

	public String getCreatorName()
	{
		return creatorName;
	}

	public void setCreatorName(String creatorName)
	{
		this.creatorName = creatorName;
	}

	public String getMenderName()
	{
		return menderName;
	}

	public void setMenderName(String menderName)
	{
		this.menderName = menderName;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getLoginName()
	{
		return loginName;
	}

	public void setLoginName(String loginName)
	{
		this.loginName = loginName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

package com.scanywhere.modules.resource.entity;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_RESOURCE")
@SuppressWarnings("serial")
public class AppResource extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_RESOURCE";

	/**
	 * 列名常量
	 */
	// 应用编码
	public static final String COLUMN_RESOURCE_CODE = "RESOURCE_CODE";

	// 应用名称
	public static final String COLUMN_RESOURCE_NAME = "RESOURCE_NAME";

	// 是否默认应用图标
	public static final String COLUMN_ICON_IS_DEFAULT = "ICON_IS_DEFAULT";

	// 应用类型
	public static final String COLUMN_RESOURCE_TYPE = "RESOURCE_TYPE";

	// 应用密钥
	public static final String COLUMN_RESOURCE_KEY = "RESOURCE_KEY";
		
	// 状态
	public static final String COLUMN_RESOURCE_STATUS = "RESOURCE_STATUS";

	// 排序
	public static final String COLUMN_RESOURCE_SORT = "RESOURCE_SORT";

	// 是否默认应用
	public static final String COLUMN_IS_DEFAULT = "IS_DEFAULT";

	// 认证策略
	public static final String COLUMN_AUTH_POLICY = "AUTH_POLICY";

	// 启用单点登录
	public static final String COLUMN_ENABLE_SSO = "ENABLE_SSO";

	// 单点登录类型
	public static final String COLUMN_SSO_TYPE = "SSO_TYPE";

	// 启用应用账号
	public static final String COLUMN_ENABLE_ACCOUNT = "ENABLE_ACCOUNT";
	
	// 应用账号密码必填
	public static final String COLUMN_REQUIRED_ACCOUNT_PASSWORD = "REQUIRED_ACCOUNT_PASSWORD";

	// 支持一用户多账号
	public static final String COLUMN_SUPPORT_MULTI_ACCOUNT = "SUPPORT_MULTI_ACCOUNT";

	// 允许外网用户访问
	public static final String COLUMN_ALLOW_VPN_USER = "ALLOW_VPN_USER";

	// 备注
	public static final String COLUMN_REMARK = "REMARK";
		
	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";
	
	// 预留字段1
	public static final String COLUMN_TEMP_FIELD1 = "TEMP_FIELD1";
	
	// 预留字段2
	public static final String COLUMN_TEMP_FIELD2 = "TEMP_FIELD2";
	
	// 预留字段3
	public static final String COLUMN_TEMP_FIELD3 = "TEMP_FIELD3";

	/**
	 * 属性
	 */
	@Comment(label = "应用编码")
	@Column(name = COLUMN_RESOURCE_CODE, nullable = false, length = 50)
	private String resourceCode;

	@Comment(label = "应用名称")
	@Column(name = COLUMN_RESOURCE_NAME, nullable = false, length = 50)
	private String resourceName;

	@Comment(label = "是否默认应用图标")
	@Column(name = COLUMN_ICON_IS_DEFAULT, nullable = false)
	private Integer iconIsDefault;

	@Comment(label = "应用类型")
	@Column(name = COLUMN_RESOURCE_TYPE, nullable = false)
	private Integer resourceType;
	
	@Comment(label = "应用密钥")
	@Column(name = COLUMN_RESOURCE_KEY, nullable = false, length = 32)
	private String resourceKey;

	@Comment(label = "状态")
	@Column(name = COLUMN_RESOURCE_STATUS, nullable = false)
	private Integer resourceStatus;

	@Comment(label = "排序")
	@Column(name = COLUMN_RESOURCE_SORT, nullable = false)
	private Integer resourceSort;

	@Comment(label = "是否默认应用")
	@Column(name = COLUMN_IS_DEFAULT, nullable = false)
	private Integer isDefault;

	@Comment(label = "认证策略")
	@Column(name = COLUMN_AUTH_POLICY)
	private Integer authPolicy;

	@Comment(label = "启用单点登录")
	@Column(name = COLUMN_ENABLE_SSO, nullable = false)
	private Integer enableSso;

	@Comment(label = "单点登录类型")
	@Column(name = COLUMN_SSO_TYPE)
	private String ssoType;

	@Comment(label = "启用应用账号")
	@Column(name = COLUMN_ENABLE_ACCOUNT, nullable = false)
	private Integer enableAccount;
	
	@Comment(label = "是否强制子账号")
	@Column(name = COLUMN_REQUIRED_ACCOUNT_PASSWORD, nullable = false)
	private Integer requiredAccountPassword;

	@Comment(label = "支持一用户多账号")
	@Column(name = COLUMN_SUPPORT_MULTI_ACCOUNT, nullable = false)
	private Integer supportMultiAccount;

	@Comment(label = "允许外网用户访问")
	@Column(name = COLUMN_ALLOW_VPN_USER, nullable = false)
	private Integer allowVpnUser;

	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK, length = 255)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	@Comment(label = "预留字段1")
	@Column(name = COLUMN_TEMP_FIELD1)
	private String tempField1;
	
	@Comment(label = "预留字段2")
	@Column(name = COLUMN_TEMP_FIELD2)
	private String tempField2;
	
	@Comment(label = "预留字段3")
	@Column(name = COLUMN_TEMP_FIELD3)
	private String tempField3;
	
	/**
	 * 单点登录参数配置
	 */
	@Transient
	private Map<String, Object> ssoParams;
	
	/**
	 * 是否允许访问, 校验内外网用户, 认证级别
	 */
	@Transient
	private int allowAccess = 0;

	public String getResourceCode()
	{
		return resourceCode;
	}

	public void setResourceCode(String resourceCode)
	{
		this.resourceCode = resourceCode;
	}

	public String getResourceName()
	{
		return resourceName;
	}

	public void setResourceName(String resourceName)
	{
		this.resourceName = resourceName;
	}

	public Integer getIconIsDefault()
	{
		return iconIsDefault;
	}

	public void setIconIsDefault(Integer iconIsDefault)
	{
		this.iconIsDefault = iconIsDefault;
	}

	public Integer getResourceType()
	{
		return resourceType;
	}

	public void setResourceType(Integer resourceType)
	{
		this.resourceType = resourceType;
	}

	public String getResourceKey()
	{
		return resourceKey;
	}

	public void setResourceKey(String resourceKey)
	{
		this.resourceKey = resourceKey;
	}
	
	public Integer getResourceStatus()
	{
		return resourceStatus;
	}

	public void setResourceStatus(Integer resourceStatus)
	{
		this.resourceStatus = resourceStatus;
	}

	public Integer getResourceSort()
	{
		return resourceSort;
	}

	public void setResourceSort(Integer resourceSort)
	{
		this.resourceSort = resourceSort;
	}

	public Integer getIsDefault()
	{
		return isDefault;
	}

	public void setIsDefault(Integer isDefault)
	{
		this.isDefault = isDefault;
	}

	public Integer getAuthPolicy()
	{
		return authPolicy;
	}

	public void setAuthPolicy(Integer authPolicy)
	{
		this.authPolicy = authPolicy;
	}

	public Integer getEnableSso()
	{
		return enableSso;
	}

	public void setEnableSso(Integer enableSso)
	{
		this.enableSso = enableSso;
	}

	public String getSsoType()
	{
		return ssoType;
	}

	public void setSsoType(String ssoType)
	{
		this.ssoType = ssoType;
	}

	public Integer getEnableAccount()
	{
		return enableAccount;
	}

	public void setEnableAccount(Integer enableAccount)
	{
		this.enableAccount = enableAccount;
	}
	
	public Integer getRequiredAccountPassword()
	{
		return requiredAccountPassword;
	}

	public void setRequiredAccountPassword(Integer requiredAccountPassword)
	{
		this.requiredAccountPassword = requiredAccountPassword;
	}

	public Integer getSupportMultiAccount()
	{
		return supportMultiAccount;
	}

	public void setSupportMultiAccount(Integer supportMultiAccount)
	{
		this.supportMultiAccount = supportMultiAccount;
	}

	public Integer getAllowVpnUser()
	{
		return allowVpnUser;
	}

	public void setAllowVpnUser(Integer allowVpnUser)
	{
		this.allowVpnUser = allowVpnUser;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
	
	public String getTempField1()
	{
		return tempField1;
	}

	public void setTempField1(String tempField1)
	{
		this.tempField1 = tempField1;
	}
	
	public String getTempField2()
	{
		return tempField2;
	}

	public void setTempField2(String tempField2)
	{
		this.tempField2 = tempField2;
	}

	public void setTempField3(String tempField3)
	{
		this.tempField3 = tempField3;
	}
	
	public String getTempField3()
	{
		return tempField3;
	}
	
	public Map<String, Object> getSsoParams()
	{
		return ssoParams;
	}

	public void setSsoParams(Map<String, Object> ssoParams)
	{
		this.ssoParams = ssoParams;
	}
	
	public int getAllowAccess()
	{
		return allowAccess;
	}

	public void setAllowAccess(int allowAccess)
	{
		this.allowAccess = allowAccess;
	}
}

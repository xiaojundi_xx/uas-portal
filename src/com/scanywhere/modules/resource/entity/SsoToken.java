package com.scanywhere.modules.resource.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_SSO_TOKEN")
@SuppressWarnings("serial")
public class SsoToken extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_SSO_TOKEN";

	/**
	 * 列名常量
	 */
	// 应用系统ID
	public static final String COLUMN_RESOURCE_ID = "RESOURCE_ID";

	// 用户ID
	public static final String COLUMN_USER_ID = "USER_ID";

	// 认证方式
	public static final String COLUMN_AUTH_MODE = "AUTH_MODE";
		
	// 单点票据
	public static final String COLUMN_ACCESS_TOKEN = "ACCESS_TOKEN";

	// 单点登录用户通行证
	public static final String COLUMN_SSO_PASSPORT = "SSO_PASSPORT";

	// 单点信息校验HASH
	public static final String COLUMN_SSO_VERIFY_HASH = "SSO_VERIFY_HASH";

	// EXPIRED_TIME
	public static final String COLUMN_EXPIRED_TIME = "EXPIRED_TIME";

	// CREATE_DATE_TIME
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	/**
	 * 属性
	 */
	@Comment(label = "应用系统ID")
	@Column(name = COLUMN_RESOURCE_ID, nullable = false, length = 32)
	private String resourceId;

	@Comment(label = "用户ID")
	@Column(name = COLUMN_USER_ID, nullable = false, length = 32)
	private String userId;

	@Comment(label = "认证方式")
	@Column(name = COLUMN_AUTH_MODE, nullable = false, length = 32)
	private String authMode;
	
	@Comment(label = "单点票据")
	@Column(name = COLUMN_ACCESS_TOKEN, nullable = false, length = 50)
	private String accessToken;

	@Comment(label = "单点登录用户通行证")
	@Column(name = COLUMN_SSO_PASSPORT, nullable = false, length = 50)
	private String ssoPassport;

	@Comment(label = "SSO_VERIFY_HASH")
	@Column(name = COLUMN_SSO_VERIFY_HASH, nullable = false, length = 50)
	private String ssoVerifyHash;

	@Comment(label = "EXPIRED_TIME")
	@Column(name = COLUMN_EXPIRED_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date expiredTime;

	@Comment(label = "单点信息校验HASH")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	public String getResourceId()
	{
		return resourceId;
	}

	public void setResourceId(String resourceId)
	{
		this.resourceId = resourceId;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getAuthMode()
	{
		return authMode;
	}

	public void setAuthMode(String authMode)
	{
		this.authMode = authMode;
	}
	
	public String getAccessToken()
	{
		return accessToken;
	}

	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}

	public String getSsoPassport()
	{
		return ssoPassport;
	}

	public void setSsoPassport(String ssoPassport)
	{
		this.ssoPassport = ssoPassport;
	}

	public String getSsoVerifyHash()
	{
		return ssoVerifyHash;
	}

	public void setSsoVerifyHash(String ssoVerifyHash)
	{
		this.ssoVerifyHash = ssoVerifyHash;
	}

	public Date getExpiredTime()
	{
		return expiredTime;
	}

	public void setExpiredTime(Date expiredTime)
	{
		this.expiredTime = expiredTime;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	
	
}

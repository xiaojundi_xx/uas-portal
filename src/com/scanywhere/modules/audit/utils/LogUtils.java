package com.scanywhere.modules.audit.utils;

import java.util.List;
import java.util.Map;

import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.audit.entity.SsoLog;
import com.scanywhere.modules.audit.entity.UserLog;
import com.scanywhere.modules.audit.service.SsoLogService;
import com.scanywhere.modules.audit.service.UserLogService;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.security.subject.UserSubject;

public class LogUtils
{
	private static volatile LogUtils logUtils;
	
	private UserLogService userLogService;
	
	private SsoLogService ssoLogService;
	
	private LogUtils()
	{
		userLogService = (UserLogService) SpringContextUtils.getBean("userLogService");
		ssoLogService = (SsoLogService) SpringContextUtils.getBean("ssoLogService");
	}
	
	public static LogUtils me()
	{
		if (logUtils == null)
		{
			synchronized (LogUtils.class)
			{
				if (logUtils == null)
				{
					logUtils = new LogUtils();
				}
			}
		}
		return logUtils;
	}
	
	public void saveUserLog(UserSubject userSubject, int actionType, String operObject, int operResult, String logContext)
	{
		User user = null;
		if (userSubject != null && userSubject.isAuthenticated())
		{
			user = (User) userSubject.getPrincipal().getInfo();
		}
		
		UserLog log = new UserLog();
		log.setUserId(user == null ? "-1" : user.getId());
		log.setLoginName(user == null ? "未知" : user.getLoginName());
		log.setUserName(user == null ? "未知" : user.getUserName());
		log.setOrgId(user == null ? "" : user.getOrgId());
		log.setOrgName(user == null ? "" : user.getOrgName());
		log.setActionType(actionType);
		log.setOperObject(operObject);
		log.setOperResult(operResult);
		log.setLogContext(logContext);
		log.setClientIp(StringUtils.isEmpty(userSubject.getClientIp()) ? "127.0.0.1" : userSubject.getClientIp());
		log.setOperDateTime(DateUtils.date());
//		log.setAuthCode(userSubject.getAuthCode());
		log.setBrowserType(userSubject.getBrowserType());
		userLogService.save(log);
	}
	
	public void saveUserLog(User user, int actionType, String operObject, int operResult, String clientIp, String logContext)
	{
		UserLog log = new UserLog();
		log.setUserId(user == null ? "-1" : user.getId());
		log.setLoginName(user == null ? "未知" : user.getLoginName());
		log.setUserName(user == null ? "未知" : user.getUserName());
		log.setOrgId(user == null ? "" : user.getOrgId());
		log.setOrgName(user == null ? "" : user.getOrgName());
		log.setActionType(actionType);
		log.setOperObject(operObject);
		log.setOperResult(operResult);
		log.setLogContext(logContext);
		log.setClientIp(clientIp);
		log.setOperDateTime(DateUtils.date());
		userLogService.save(log);
	}
	
	public void saveSsoLog(UserSubject userSubject, AppResource resource, int accessResult, String logContext)
	{
		User user = (User) userSubject.getPrincipal().getInfo();
		SsoLog log = new SsoLog();
		log.setUserId(user.getId());
		log.setLoginName(user.getLoginName());
		log.setUserName(user.getUserName());
		log.setOrgId(user.getOrgId());
		log.setOrgName(user.getOrgName());
		log.setResourceId(resource.getId());
		log.setResourceCode(resource.getResourceCode());
		log.setResourceName(resource.getResourceName());
		log.setAccessResult(accessResult);
		log.setLogContext(logContext);
		log.setClientIp(userSubject.getClientIp());
		log.setAccessDateTime(DateUtils.date());
		ssoLogService.save(log);
	}
	
	public void saveSsoLog(UserSubject userSubject, AppResource resource, int accessResult, String logContext, Map<String, String> others)
	{
		User user = (User) userSubject.getPrincipal().getInfo();
		SsoLog log = new SsoLog();
		log.setUserId(user.getId());
		log.setLoginName(user.getLoginName());
		log.setUserName(user.getUserName());
		log.setOrgId(user.getOrgId());
		log.setOrgName(user.getOrgName());
		log.setResourceId(resource.getId());
		log.setResourceCode(resource.getResourceCode());
		log.setResourceName(resource.getResourceName());
		log.setAccountName(others.get("accountName"));
		log.setAccessResult(accessResult);
		log.setLogContext(logContext);
		log.setClientIp(userSubject.getClientIp());
		log.setAccessDateTime(DateUtils.date());
		log.setAccessToken(others.get("accessToken"));
		ssoLogService.save(log);
	}
	
	public void updateSsoLog(List<Object> SsoLogList, String logContext)
	{
		SsoLog log = (SsoLog) SsoLogList.get(0);
		log.setLogContext(logContext);
		
		ssoLogService.update(log);
	}
}

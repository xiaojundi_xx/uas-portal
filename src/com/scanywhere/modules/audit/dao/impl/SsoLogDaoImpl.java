package com.scanywhere.modules.audit.dao.impl;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.audit.dao.SsoLogDao;
import com.scanywhere.modules.audit.entity.SsoLog;

@Repository("ssoLogDao")
public class SsoLogDaoImpl extends BaseDaoImpl<SsoLog> implements SsoLogDao
{

}

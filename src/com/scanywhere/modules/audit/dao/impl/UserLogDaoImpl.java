package com.scanywhere.modules.audit.dao.impl;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.audit.dao.UserLogDao;
import com.scanywhere.modules.audit.entity.UserLog;

@Repository("userLogDao")
public class UserLogDaoImpl extends BaseDaoImpl<UserLog> implements UserLogDao
{

}

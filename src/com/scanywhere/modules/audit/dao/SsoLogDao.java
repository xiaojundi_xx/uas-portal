package com.scanywhere.modules.audit.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.audit.entity.SsoLog;

public interface SsoLogDao extends BaseDao<SsoLog>
{

}

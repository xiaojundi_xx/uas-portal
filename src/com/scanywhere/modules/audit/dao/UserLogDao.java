package com.scanywhere.modules.audit.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.audit.entity.UserLog;

public interface UserLogDao extends BaseDao<UserLog>
{

}

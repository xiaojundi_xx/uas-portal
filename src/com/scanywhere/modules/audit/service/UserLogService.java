package com.scanywhere.modules.audit.service;

import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.audit.entity.UserLog;

public interface UserLogService extends BaseService<UserLog>
{

	/**
	 * 获取用户列表数据
	 * 
	 * @param reqObj
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> loadData(String reqObj) throws Exception;
}

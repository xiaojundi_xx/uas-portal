package com.scanywhere.modules.audit.service;

import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.audit.entity.SsoLog;

public interface SsoLogService extends BaseService<SsoLog>
{
	/**
	 * 获取列表数据
	 * 
	 * @param reqObj
	 * @return
	 */
	public Map<String, Object> loadData(String reqObj) throws Exception;
}

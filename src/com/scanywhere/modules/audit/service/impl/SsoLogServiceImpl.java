package com.scanywhere.modules.audit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.page.pojo.Page;
import com.scanywhere.core.page.pojo.QueryCondition;
import com.scanywhere.core.page.utils.TableUtils;
import com.scanywhere.core.page.xml.Table;
import com.scanywhere.modules.audit.dao.SsoLogDao;
import com.scanywhere.modules.audit.entity.SsoLog;
import com.scanywhere.modules.audit.service.SsoLogService;
import com.scanywhere.security.SecurityLoginUtils;

@Service("ssoLogService")
public class SsoLogServiceImpl extends BaseServiceImpl<SsoLog> implements SsoLogService
{
	@Resource
	private SsoLogDao ssoLogDao;
	
	@Resource(name = "ssoLogDao")
	public void setDao(BaseDao<SsoLog> dao)
	{
		super.setDao(dao);
	}
	
	public Map<String, Object> loadData(String reqObj) throws Exception
	{
		// 解析查询条件
		QueryCondition queryCondition = JSON.parseObject(reqObj, QueryCondition.class);
		
		// 获取Query配置
        Table table = TableUtils.getTable(queryCondition);
        
        // 分页信息
        Page page = TableUtils.getPage(queryCondition, table);
        
        // 查询条件
        Map<String, Object> params = queryCondition.getEqualConditionMap();
        
        // 设置当前用户
        params.put("userId", SecurityLoginUtils.getSubject().getPrincipal().getId());
        
        // 模糊查询条件
        Map<String, String> likeParams = queryCondition.getLikeConditionMap();
        
        // 时间查询条件
        Map<String, String> timeParams = queryCondition.getTimeConditionMap();
        
        // 查询数据
        Long totalRecord = super.findCount(params, likeParams, timeParams);
        if (totalRecord > 0L)
        {
        	List<SsoLog> dataList = super.find(params, likeParams, timeParams, "accessDateTime", page.getPageNo(), page.getPageSize(), false);
        	page.setTotalRecord(totalRecord.intValue());
            page.setDataList(dataList);
        }
        else
        {
        	page.setTotalRecord(CommonConstants.INT_VALUE_ZERO);
            page.setDataList(new ArrayList<SsoLog>());
        }
        
        // 返回数据
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(CommonConstants.MAP_KEY_TABLE, table);
        dataMap.put(CommonConstants.MAP_KEY_PAGE, page);
		return dataMap;
	}
}

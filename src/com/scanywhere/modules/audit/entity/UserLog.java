package com.scanywhere.modules.audit.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_USER_LOG")
@SuppressWarnings("serial")
public class UserLog extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_USER_LOG";

	/**
	 * 列名常量
	 */
	// 用户ID
	public static final String COLUMN_USER_ID = "USER_ID";
	
	// 用户帐号
	public static final String COLUMN_LOGIN_NAME = "LOGIN_NAME";

	// 用户姓名
	public static final String COLUMN_USER_NAME = "USER_NAME";

	// 组织机构ID
	public static final String COLUMN_ORG_ID = "ORG_ID";

	// 组织机构名称
	public static final String COLUMN_ORG_NAME = "ORG_NAME";

	// 行为类型
	public static final String COLUMN_ACTION_TYPE = "ACTION_TYPE";

	// 操作对象
	public static final String COLUMN_OPER_OBJECT = "OPER_OBJECT";

	// 操作结果
	public static final String COLUMN_OPER_RESULT = "OPER_RESULT";

	// 日志内容
	public static final String COLUMN_LOG_CONTEXT = "LOG_CONTEXT";

	// 客户端IP
	public static final String COLUMN_CLIENT_IP = "CLIENT_IP";

	// 操作时间
	public static final String COLUMN_OPER_DATE_TIME = "OPER_DATE_TIME";

	// 认证方式编码
//	public static final String COLUMN_AUTH_CODE = "AUTH_CODE";
	
	// 浏览器类型
	public static final String COLUMN_BROWSER_TYPE = "BROWSER_TYPE";
	/**
	 * 属性
	 */
	@Comment(label = "用户ID")
	@Column(name = COLUMN_USER_ID, nullable = false, length = 32)
	private String userId;
	
	@Comment(label = "用户帐号")
	@Column(name = COLUMN_LOGIN_NAME, nullable = false, length = 50)
	private String loginName;

	@Comment(label = "用户姓名")
	@Column(name = COLUMN_USER_NAME, nullable = false, length = 50)
	private String userName;

	@Comment(label = "组织机构ID")
	@Column(name = COLUMN_ORG_ID, length = 32)
	private String orgId;

	@Comment(label = "组织机构名称")
	@Column(name = COLUMN_ORG_NAME, length = 50)
	private String orgName;

	@Comment(label = "行为类型")
	@Column(name = COLUMN_ACTION_TYPE, nullable = false)
	private Integer actionType;

	@Comment(label = "操作对象")
	@Column(name = COLUMN_OPER_OBJECT, nullable = false, length = 50)
	private String operObject;

	@Comment(label = "操作结果")
	@Column(name = COLUMN_OPER_RESULT, nullable = false)
	private Integer operResult;

	@Comment(label = "日志内容")
	@Column(name = COLUMN_LOG_CONTEXT)
	private String logContext;

	@Comment(label = "客户端IP")
	@Column(name = COLUMN_CLIENT_IP, nullable = false)
	private String clientIp;

	@Comment(label = "操作时间")
	@Column(name = COLUMN_OPER_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date operDateTime;
	
//	@Comment(label = "认证方式编码")
//	@Column(name = COLUMN_AUTH_CODE, nullable = false)
//	private String authCode;
	
	@Comment(label = "浏览器类型")
	@Column(name = COLUMN_BROWSER_TYPE)
	private String browserType;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	
	public String getLoginName()
	{
		return loginName;
	}

	public void setLoginName(String loginName)
	{
		this.loginName = loginName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getOrgId()
	{
		return orgId;
	}

	public void setOrgId(String orgId)
	{
		this.orgId = orgId;
	}

	public String getOrgName()
	{
		return orgName;
	}

	public void setOrgName(String orgName)
	{
		this.orgName = orgName;
	}

	public Integer getActionType()
	{
		return actionType;
	}

	public void setActionType(Integer actionType)
	{
		this.actionType = actionType;
	}

	public String getOperObject()
	{
		return operObject;
	}

	public void setOperObject(String operObject)
	{
		this.operObject = operObject;
	}

	public Integer getOperResult()
	{
		return operResult;
	}

	public void setOperResult(Integer operResult)
	{
		this.operResult = operResult;
	}

	public String getLogContext()
	{
		return logContext;
	}

	public void setLogContext(String logContext)
	{
		this.logContext = logContext;
	}

	public String getClientIp()
	{
		return clientIp;
	}

	public void setClientIp(String clientIp)
	{
		this.clientIp = clientIp;
	}

	public Date getOperDateTime()
	{
		return operDateTime;
	}

	public void setOperDateTime(Date operDateTime)
	{
		this.operDateTime = operDateTime;
	}

//	public String getAuthCode() {
//		return authCode;
//	}
//
//	public void setAuthCode(String authCode) {
//		this.authCode = authCode;
//	}

	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}
	
	
	
}

package com.scanywhere.modules.audit.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_SSO_LOG")
@SuppressWarnings("serial")
public class SsoLog extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_SSO_LOG";

	/**
	 * 列名常量
	 */
	// 用户ID
	public static final String COLUMN_USER_ID = "USER_ID";

	// 用户帐号
	public static final String COLUMN_LOGIN_NAME = "LOGIN_NAME";

	// 用户姓名
	public static final String COLUMN_USER_NAME = "USER_NAME";

	// 组织机构ID
	public static final String COLUMN_ORG_ID = "ORG_ID";

	// 组织机构名称
	public static final String COLUMN_ORG_NAME = "ORG_NAME";

	// 应用系统ID
	public static final String COLUMN_RESOURCE_ID = "RESOURCE_ID";
	
	// 应用系统编码
	public static final String COLUMN_RESOURCE_CODE = "RESOURCE_CODE";

	// 应用系统名称
	public static final String COLUMN_RESOURCE_NAME = "RESOURCE_NAME";

	// 访问结果
	public static final String COLUMN_ACCESS_RESULT = "ACCESS_RESULT";

	// 日志内容
	public static final String COLUMN_LOG_CONTEXT = "LOG_CONTEXT";

	// 应用系统账号ID
	public static final String COLUMN_ACCOUNT_ID = "ACCOUNT_ID";

	// 应用系统帐号别名
	public static final String COLUMN_ACCOUNT_ALIAS = "ACCOUNT_ALIAS";

	// 应用系统账号
	public static final String COLUMN_ACCOUNT_NAME = "ACCOUNT_NAME";

	// 客户端IP
	public static final String COLUMN_CLIENT_IP = "CLIENT_IP";

	// 访问时间
	public static final String COLUMN_ACCESS_DATE_TIME = "ACCESS_DATE_TIME";
	
	// 访问时间
	public static final String COLUMN_ACCESS_TOKEN = "ACCESS_TOKEN";

	/**
	 * 属性
	 */
	@Comment(label = "用户ID")
	@Column(name = COLUMN_USER_ID, nullable = false, length = 32)
	private String userId;

	@Comment(label = "用户帐号")
	@Column(name = COLUMN_LOGIN_NAME, nullable = false, length = 50)
	private String loginName;

	@Comment(label = "用户姓名")
	@Column(name = COLUMN_USER_NAME, nullable = false, length = 50)
	private String userName;

	@Comment(label = "组织机构ID")
	@Column(name = COLUMN_ORG_ID, length = 32)
	private String orgId;

	@Comment(label = "组织机构名称")
	@Column(name = COLUMN_ORG_NAME, length = 50)
	private String orgName;

	@Comment(label = "应用系统ID")
	@Column(name = COLUMN_RESOURCE_ID, nullable = false, length = 32)
	private String resourceId;

	@Comment(label = "应用系统编码")
	@Column(name = COLUMN_RESOURCE_CODE, nullable = false, length = 50)
	private String resourceCode;
	
	@Comment(label = "应用系统名称")
	@Column(name = COLUMN_RESOURCE_NAME, nullable = false, length = 50)
	private String resourceName;

	@Comment(label = "访问结果")
	@Column(name = COLUMN_ACCESS_RESULT, nullable = false)
	private Integer accessResult;

	@Comment(label = "日志内容")
	@Column(name = COLUMN_LOG_CONTEXT)
	private String logContext;

	@Comment(label = "应用系统账号ID")
	@Column(name = COLUMN_ACCOUNT_ID, length = 32)
	private String accountId;

	@Comment(label = "应用系统帐号别名")
	@Column(name = COLUMN_ACCOUNT_ALIAS, length = 50)
	private String accountAlias;

	@Comment(label = "应用系统账号")
	@Column(name = COLUMN_ACCOUNT_NAME, length = 50)
	private String accountName;

	@Comment(label = "客户端IP")
	@Column(name = COLUMN_CLIENT_IP, nullable = false, length = 30)
	private String clientIp;

	@Comment(label = "访问时间")
	@Column(name = COLUMN_ACCESS_DATE_TIME)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date accessDateTime;
	
	@Comment(label = "应用系统账号")
	@Column(name = COLUMN_ACCESS_TOKEN, length = 50)
	private String accessToken;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getLoginName()
	{
		return loginName;
	}

	public void setLoginName(String loginName)
	{
		this.loginName = loginName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getOrgId()
	{
		return orgId;
	}

	public void setOrgId(String orgId)
	{
		this.orgId = orgId;
	}

	public String getOrgName()
	{
		return orgName;
	}

	public void setOrgName(String orgName)
	{
		this.orgName = orgName;
	}

	public String getResourceId()
	{
		return resourceId;
	}

	public void setResourceId(String resourceId)
	{
		this.resourceId = resourceId;
	}

	public String getResourceCode()
	{
		return resourceCode;
	}

	public void setResourceCode(String resourceCode)
	{
		this.resourceCode = resourceCode;
	}
	
	public String getResourceName()
	{
		return resourceName;
	}

	public void setResourceName(String resourceName)
	{
		this.resourceName = resourceName;
	}

	public Integer getAccessResult()
	{
		return accessResult;
	}

	public void setAccessResult(Integer accessResult)
	{
		this.accessResult = accessResult;
	}

	public String getLogContext()
	{
		return logContext;
	}

	public void setLogContext(String logContext)
	{
		this.logContext = logContext;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public void setAccountId(String accountId)
	{
		this.accountId = accountId;
	}

	public String getAccountAlias()
	{
		return accountAlias;
	}

	public void setAccountAlias(String accountAlias)
	{
		this.accountAlias = accountAlias;
	}

	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}

	public String getClientIp()
	{
		return clientIp;
	}

	public void setClientIp(String clientIp)
	{
		this.clientIp = clientIp;
	}

	public Date getAccessDateTime()
	{
		return accessDateTime;
	}

	public void setAccessDateTime(Date accessDateTime)
	{
		this.accessDateTime = accessDateTime;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	
}

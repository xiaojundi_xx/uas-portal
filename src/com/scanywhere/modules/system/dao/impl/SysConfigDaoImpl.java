package com.scanywhere.modules.system.dao.impl;

import org.springframework.stereotype.Repository;

import com.scanywhere.core.base.dao.impl.BaseDaoImpl;
import com.scanywhere.modules.system.dao.SysConfigDao;
import com.scanywhere.modules.system.entity.SysConfig;

@Repository("sysConfigDao")
public class SysConfigDaoImpl extends BaseDaoImpl<SysConfig> implements SysConfigDao
{

}

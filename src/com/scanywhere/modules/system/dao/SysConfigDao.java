package com.scanywhere.modules.system.dao;

import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.modules.system.entity.SysConfig;

public interface SysConfigDao extends BaseDao<SysConfig>
{

}

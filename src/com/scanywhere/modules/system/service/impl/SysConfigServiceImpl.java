package com.scanywhere.modules.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.base.dao.BaseDao;
import com.scanywhere.core.base.service.impl.BaseServiceImpl;
import com.scanywhere.modules.system.dao.SysConfigDao;
import com.scanywhere.modules.system.entity.SysConfig;
import com.scanywhere.modules.system.service.SysConfigService;

@Service("sysConfigService")
public class SysConfigServiceImpl extends BaseServiceImpl<SysConfig> implements SysConfigService
{
	@Resource
	private SysConfigDao sysConfigDao;
	
	@Resource(name = "sysConfigDao")
	public void setDao(BaseDao<SysConfig> dao)
	{
		super.setDao(dao);
	}

	public Map<String, Object> findSysConfig(String moduleCode)
	{
		if (StringUtils.isEmpty(moduleCode))
		{
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sysModuleCode", moduleCode);
		List<SysConfig> configList = sysConfigDao.find(params);
		if (CollectionUtils.isNotEmpty(configList))
		{
			Map<String, Object> returnMap = new HashMap<String, Object>();
			for (SysConfig sysConfig : configList)
			{
				returnMap.put(sysConfig.getScCode(), sysConfig.getScValue());
			}
			return returnMap;
		}
		return null;
	}

	public String findValueByCode(String moduleCode, String code)
	{
		if (StringUtils.isEmpty(code))
		{
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		if (StringUtils.isNotEmpty(moduleCode))
		{
			params.put("sysModuleCode", moduleCode);
		}
		params.put("scCode", code);
		List<SysConfig> configList = sysConfigDao.find(params);
		if (CollectionUtils.isNotEmpty(configList))
		{
			return configList.get(0).getScValue();
		}
		return null;
	}
}

package com.scanywhere.modules.system.service;

import java.util.Map;

import com.scanywhere.core.base.service.BaseService;
import com.scanywhere.modules.system.entity.SysConfig;

public interface SysConfigService extends BaseService<SysConfig>
{
	/**
	 * 根据系统模块编码查询系统配置
	 * 
	 * @param moduleCode
	 * @return
	 */
	public Map<String, Object> findSysConfig(String moduleCode);
	
	/**
	 * 根据配置编码查询
	 * 
	 * @param moduleCode
	 * @param code
	 * @throws ServiceLayerException
	 */
	public String findValueByCode(String moduleCode, String code);
}

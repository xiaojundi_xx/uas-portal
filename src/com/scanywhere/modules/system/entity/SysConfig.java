package com.scanywhere.modules.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.scanywhere.core.annotation.Comment;
import com.scanywhere.core.base.entity.BaseEntity;

@Entity
@Table(name = "UAS_SYS_CONFIG")
@SuppressWarnings("serial")
public class SysConfig extends BaseEntity
{
	/** 表名常量 */
	public static final String TABLE_NAME = "UAS_SYS_CONFIG";

	/**
	 * 列名常量
	 */
	// 系统模块编码
	public static final String COLUMN_SYS_MODULE_CODE = "SYS_MODULE_CODE";

	// 配置编码
	public static final String COLUMN_SC_CODE = "SC_CODE";

	// 配置名称
	public static final String COLUMN_SC_NAME = "SC_NAME";

	// 配置值
	public static final String COLUMN_SC_VALUE = "SC_VALUE";

	// 备注
	public static final String COLUMN_REMARK = "REMARK";

	// 创建时间
	public static final String COLUMN_CREATE_DATE_TIME = "CREATE_DATE_TIME";

	// 修改时间
	public static final String COLUMN_UPDATE_DATE_TIME = "UPDATE_DATE_TIME";

	// 修改时间戳
	public static final String COLUMN_UPDATE_TIME_STAMP = "UPDATE_TIME_STAMP";

	@Comment(label = "系统模块编码")
	@Column(name = COLUMN_SYS_MODULE_CODE, nullable = false, length = 50)
	private String sysModuleCode;

	@Comment(label = "配置编码")
	@Column(name = COLUMN_SC_CODE, nullable = false, length = 50)
	private String scCode;

	@Comment(label = "配置名称")
	@Column(name = COLUMN_SC_NAME, nullable = false, length = 50)
	private String scName;

	@Comment(label = "配置值")
	@Column(name = COLUMN_SC_VALUE)
	private String scValue;

	@Comment(label = "备注")
	@Column(name = COLUMN_REMARK, length = 255)
	private String remark;

	@Comment(label = "创建时间")
	@Column(name = COLUMN_CREATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDateTime;

	@Comment(label = "修改时间")
	@Column(name = COLUMN_UPDATE_DATE_TIME, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date updateDateTime;

	@Comment(label = "修改时间戳")
	@Column(name = COLUMN_UPDATE_TIME_STAMP, nullable = false)
	private Long updateTimeStamp;

	public String getSysModuleCode()
	{
		return sysModuleCode;
	}

	public void setSysModuleCode(String sysModuleCode)
	{
		this.sysModuleCode = sysModuleCode;
	}

	public String getScCode()
	{
		return scCode;
	}

	public void setScCode(String scCode)
	{
		this.scCode = scCode;
	}

	public String getScName()
	{
		return scName;
	}

	public void setScName(String scName)
	{
		this.scName = scName;
	}

	public String getScValue()
	{
		return scValue;
	}

	public void setScValue(String scValue)
	{
		this.scValue = scValue;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDateTime()
	{
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime)
	{
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public Long getUpdateTimeStamp()
	{
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Long updateTimeStamp)
	{
		this.updateTimeStamp = updateTimeStamp;
	}
}

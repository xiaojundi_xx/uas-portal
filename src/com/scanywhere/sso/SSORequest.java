package com.scanywhere.sso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;

import com.scanywhere.common.lang.StringUtils;

public class SSORequest implements java.io.Serializable
{
	private static final long serialVersionUID = 1377400740116989965L;

	public static final String UAS_SSO_REQUEST_KEY = "savedRequest";
	
    private String resourceCode;
    
	private String service;
    
    private String queryStr;
    
    public SSORequest(HttpServletRequest request)
	{
    	this.resourceCode = StringUtils.isEmpty(request.getParameter("resourceCode")) ? "" : request.getParameter("resourceCode");
		
		this.service = StringUtils.isEmpty(request.getParameter("service")) ? "" : request.getParameter("service");
		if (StringUtils.isNotEmpty(service) && (service.indexOf("/") > 0 || service.indexOf("?") > 0 || service.indexOf("=") > 0 || service.indexOf("&") > 0))
		{
			try 
			{
				service = URLEncoder.encode(service, "UTF-8");
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}
		}
		
		if (StringUtils.isNotEmpty(this.resourceCode))
		{
			queryStr = request.getQueryString(); 
			queryStr = StringUtils.isEmpty(queryStr) ? "" : StringEscapeUtils.unescapeHtml(queryStr);
			queryStr = queryStr.replaceAll("authCode=[^&]+&", "");
		}
	}
    
    public String getResourceCode()
	{
		return resourceCode;
	}

	public String getService()
	{
		return service;
	}

	public String getQueryStr()
	{
		return queryStr;
	}
}

package com.scanywhere.sso.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.scanywhere.common.support.Convert;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.user.entity.UserAccount;
import com.scanywhere.modules.user.service.UserAccountService;

@SuppressWarnings("serial")
public abstract class AbstractSSOHandler implements SSOHandler
{
	private UserAccountService userAccountService;
	
	// 单点配置参数
	private Map<String, Object> ssoParameter;

	/**
	 * 获取单点配置参数
	 */
	public String getParameter(String key)
	{
		if (ssoParameter.containsKey(key))
		{
			return Convert.toStr(ssoParameter.get(key), "");
		}
		return null;
	}

	/**
	 * 获取单点配置参数
	 */
	public String getParameter(String key, String defaultValue)
	{
		if (ssoParameter.containsKey(key))
		{
			return Convert.toStr(ssoParameter.get(key), defaultValue);
		}
		return null;
	}

	/**
	 * 获取单点配置参数
	 */
	public Integer getParameterToInt(String key)
	{
		if (ssoParameter.containsKey(key))
		{
			return Convert.toInt(ssoParameter.get(key));
		}
		return null;
	}

	/**
	 * 获取单点配置参数
	 */
	public Integer getParameterToInt(String key, Integer defaultValue)
	{
		if (ssoParameter.containsKey(key))
		{
			return Convert.toInt(ssoParameter.get(key), defaultValue);
		}
		return defaultValue;
	} 
	
	/**
	 * 获取所有单点配置参数
	 * 
	 * @return
	 */
	public Map<String, Object> getSSOParameter()
	{
		return ssoParameter;
	}

	/**
	 * 设置单点配置参数
	 * 
	 * @param ssoParameter
	 */
	public void setSSOParameter(Map<String, Object> ssoParameter)
	{
		this.ssoParameter = ssoParameter;
	}
	
	/**
	 * 获取应用子帐号
	 * 
	 * @param resourceId
	 * @param userId
	 * @return
	 */
	public List<Map<String, Object>> getUserAccount(String resourceId, String userId, int bindingMode)
	{
		userAccountService = (UserAccountService) SpringContextUtils.getBean("userAccountService");
		List<UserAccount> accountList = userAccountService.findUserAccount(resourceId, userId, bindingMode);
		List<Map<String, Object>> accountMap = new ArrayList<Map<String, Object>>();
		if (CollectionUtils.isNotEmpty(accountList))
		{
			for (UserAccount account : accountList)
			{
				Map<String, Object> accountObj = new HashMap<String, Object>();
				accountObj.put("accountType", "secondary");
				accountObj.put("accountId", account.getAccountId());
				accountObj.put("accountName", account.getAccountName());
				accountObj.put("accountAlias", account.getAccountAlias());
				accountObj.put("isDefault", account.getIsDefault());
				accountMap.add(accountObj);
			}
		}
		return accountMap;
	}
}

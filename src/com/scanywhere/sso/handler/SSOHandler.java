package com.scanywhere.sso.handler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.security.subject.UserSubject;
import com.scanywhere.sso.SSOResult;
import com.scanywhere.sso.exception.SSOException;
import com.scanywhere.sso.principal.SSOCredentials;

public interface SSOHandler
{
	/**
	 * 初始化
	 */
	public void init();
	
	/**
	 * 获取单点登录方式
	 * 
	 * @return
	 */
	public String getSSOType();
	
	/**
	 * 单点登录请求处理前调用
	 * 
	 * @param request
	 * @param response
	 * @param userSubject
	 * @param resource
	 * @throws SSOException
	 */
	public SSOCredentials preHandler(HttpServletRequest request, HttpServletResponse response, UserSubject userSubject, AppResource resource, String ssoAccount) throws SSOException;
	
	/**
	 * 处理单点登录请求
	 * 
	 * @param request
	 * @param response
	 * @param userSubject
	 * @param resource
	 * @param credentials
	 * @throws SSOException
	 */
	public SSOResult postHandle(HttpServletRequest request, HttpServletResponse response, UserSubject userSubject, AppResource resource, SSOCredentials credentials, String accountType) throws SSOException;
	
	/**
	 * 请求结束后处理
	 * 
	 * @param request
	 * @param response
	 * @throws SSOException
	 */
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response) throws SSOException;
	
	/**
	 * 验证单点方式是否支持此凭证
	 * 
	 * @param credentials
	 * @return
	 */
	public boolean supports(SSOCredentials credentials);
	
	/**
	 * 获取单点配置参数
	 */
	public String getParameter(String key);
	
	/**
	 * 获取单点配置参数
	 */
	public String getParameter(String key, String defaultValue);
	
	/**
	 * 获取单点配置参数
	 */
	public Integer getParameterToInt(String key);
	
	/**
	 * 获取单点配置参数
	 */
	public Integer getParameterToInt(String key, Integer defaultValue);
	
	/**
	 * 获取单点配置参数
	 */
	public Map<String, Object> getSSOParameter();
	
	/**
	 * 设置单点配置参数
	 * 
	 * @param ssoParameter
	 */
	public void setSSOParameter(Map<String, Object> ssoParameter);
}

package com.scanywhere.sso.handler.support;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scanywhere.common.codec.AESUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.entity.ResourceAccount;
import com.scanywhere.modules.resource.service.ResourceAccountService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.modules.user.service.UserService;
import com.scanywhere.security.subject.UserSubject;
import com.scanywhere.sso.SSOResult;
import com.scanywhere.sso.exception.SSOException;
import com.scanywhere.sso.handler.AbstractSSOHandler;
import com.scanywhere.sso.principal.DefaultSSOCredentials;
import com.scanywhere.sso.principal.SSOCredentials;

public class FormSSOHandler extends AbstractSSOHandler
{
	private UserService userService;
	private ResourceAccountService resourceAccountService;
	
	public void init()
	{
		userService = (UserService) SpringContextUtils.getBean("userService");
		resourceAccountService = (ResourceAccountService) SpringContextUtils.getBean("resourceAccountService");
	}

	public String getSSOType()
	{
		return "form_base";
	}

	public SSOCredentials preHandler(HttpServletRequest request, HttpServletResponse response, UserSubject userSubject, AppResource resource, String ssoAccount) throws SSOException
	{
		// 登录凭证
		DefaultSSOCredentials credentials = new DefaultSSOCredentials();
		
		// 获取当前登录用户
		User user = (User) userSubject.getPrincipal().getInfo();
		
		// 单点登录帐号
		if (StringUtils.isNotEmpty(ssoAccount))
		{
			credentials.setSsoAccount(ssoAccount);
		}
		
		List<Map<String, Object>> accountMap = new ArrayList<Map<String,Object>>();
		// 判断应用是否启用从账号
		if (resource.getEnableAccount() == CommonConstants.STATUS_ENABLED)
		{
			accountMap = super.getUserAccount(resource.getId(), user.getId(), (ProjectConstants.ACCOUNT_BINDING_MODE_USER | ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN));
			// 是否强制使用子帐号
			int forceResourceAccount = super.getParameterToInt("forceResourceAccount", CommonConstants.STATUS_1);

			if(forceResourceAccount == CommonConstants.STATUS_0){
				if(CollectionUtils.isEmpty(accountMap))
					return null;
			}else{
				Map<String, Object> accountObj = new HashMap<String, Object>();
				accountObj.put("accountType", "primary");
				accountObj.put("accountId", user.getId());
				accountObj.put("accountName", user.getLoginName());
				accountObj.put("accountAlias", user.getUserName());
				accountObj.put("isDefault", CommonConstants.STATUS_0);
				accountMap.add(accountObj);
			}
		}else{
			//不启用子账号，即使用主账号
			Map<String, Object> accountObj = new HashMap<String, Object>();
			accountObj.put("accountType", "primary");
			accountObj.put("accountId", user.getId());
			accountObj.put("accountName", user.getLoginName());
			accountObj.put("accountAlias", user.getUserName());
			accountObj.put("isDefault", CommonConstants.STATUS_0);
			accountMap.add(accountObj);
		}
		credentials.setAccountMap(accountMap);
		//未选择登录帐号，且只有一个应用帐号
		if (StringUtils.isEmpty(credentials.getSsoAccount()) && accountMap.size() == 1)
			credentials.setSsoAccount((String) accountMap.get(0).get("accountName"));
		return credentials;
	}

	public SSOResult postHandle(HttpServletRequest request, HttpServletResponse response, UserSubject userSubject, AppResource resource, SSOCredentials credentials, String accountType) throws SSOException
	{
		// 表单提交地址
		String fromAction = super.getParameter("fromAction");
		if (StringUtils.isEmpty(fromAction))
		{
			throw new SSOException("单点登录参数配置错误!");
		}
		
		// 表单用户名元素
		String usernameElement = super.getParameter("usernameElement");
		if (StringUtils.isEmpty(usernameElement))
		{
			throw new SSOException("单点登录参数配置错误!");
		}
		
		// 表单密码元素
		String passwordElement = super.getParameter("passwordElement");
		if (StringUtils.isEmpty(passwordElement))
		{
			throw new SSOException("单点登录参数配置错误!");
		}
		
		// 表单是否自定义登录函数
		int customLoginFunction = super.getParameterToInt("customLoginFunction", CommonConstants.STATUS_1);
		
		// 表单登录函数
		String loginFunction = super.getParameter("loginFunction");
		if (customLoginFunction == CommonConstants.STATUS_0 && StringUtils.isEmpty(loginFunction))
		{
			throw new SSOException("单点登录参数配置错误!");
		}
		
		// 单点登录信息
		String username = credentials.getSsoAccount();
		
		if(resource.getEnableAccount()==CommonConstants.STATUS_DISABLED)
		{
			accountType = "primary";
		}else{
			if(credentials.getAccountMap().size()==1 
					&& super.getParameterToInt("forceResourceAccount", CommonConstants.STATUS_1)==CommonConstants.STATUS_1)
				accountType = "primary";
		}

		String password;
		if("primary".equals(accountType)){
			User user = userService.findByLoginName(username);
			password = user.getFormPassword();
			if(StringUtils.isBlank(password)){
				throw new SSOException("用户密码未同步,可通过密码登录一次统一认证系统,即可完成密码同步，详情见常见问题!");
			}
			password = AESUtils.decode(password);
		}
		else
		{
			String accountId = null;
			for(Map<String,Object> account_ : credentials.getAccountMap())
			{
				if(account_.get("accountName").equals(username))
				{
					accountId = (String) account_.get("accountId");
				}
			}
			
			// 查询子帐号信息
			ResourceAccount account = resourceAccountService.getById(accountId);
			if (resource.getRequiredAccountPassword() == CommonConstants.STATUS_0 && StringUtils.isEmpty(account.getAccountPassword()))
			{
				throw new SSOException("未配置子帐号密码属性!");
			}

			password = AESUtils.decode(account.getAccountPassword());
		}
		
		StringBuffer form = new StringBuffer();
		form.append("<body> \n");
		form.append("<form id=\"presForm\" method=\"post\" action=\"" + fromAction + "\"> \n");
		
		// 帐号
		form.append("<input type=\"hidden\" name=\"" + usernameElement + "\" value=\"" + username + "\"/> \n");
		
		// 密码
		form.append("<input type=\"hidden\" name=\"" + passwordElement + "\" value=\"" + password + "\"/> \n");
		
		form.append("</form> \n");
		form.append("</body> \n");
		
		if (customLoginFunction == CommonConstants.STATUS_0)
		{
			if (StringUtils.isEmpty(loginFunction))
			{
				throw new SSOException("未配置登录函数!");
			}
			
			form.append("<script type=\"text/javascript\"> \n");
			form.append(loginFunction + "\n");
			form.append("</script> \n");
		}
		else
		{
			form.append("<script type=\"text/javascript\">");
			form.append("document.getElementById(\"presForm\").submit();");
			form.append("</script>");
		}
		
		try
		{
			response.setHeader("Content-Type", "text/html; charset=UTF-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().write(form.toString());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new SSOException("单点登录失败!");
		}
		
		SSOResult ssoResult = new SSOResult();
		ssoResult.setSuccess(true);
		ssoResult.setRedirectUrl(null);
		ssoResult.setFormInfo(form.toString());
		return ssoResult;
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response) throws SSOException
	{
		
	}

	public boolean supports(SSOCredentials credentials)
	{
		return credentials != null && DefaultSSOCredentials.class.isAssignableFrom(credentials.getClass());
	}
}

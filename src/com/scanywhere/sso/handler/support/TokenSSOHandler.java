package com.scanywhere.sso.handler.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scanywhere.common.codec.MD5Utils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.DateUtils;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.common.utils.RandomUtils;
import com.scanywhere.core.constant.CommonConstants;
import com.scanywhere.core.utils.SpringContextUtils;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.entity.SsoToken;
import com.scanywhere.modules.resource.service.SsoTokenService;
import com.scanywhere.modules.user.entity.User;
import com.scanywhere.security.subject.UserSubject;
import com.scanywhere.sso.SSORequest;
import com.scanywhere.sso.SSOResult;
import com.scanywhere.sso.exception.SSOException;
import com.scanywhere.sso.handler.AbstractSSOHandler;
import com.scanywhere.sso.principal.DefaultSSOCredentials;
import com.scanywhere.sso.principal.SSOCredentials;

public class TokenSSOHandler extends AbstractSSOHandler
{
	private SsoTokenService ssoTokenService;
	
	public void init()
	{
		ssoTokenService = (SsoTokenService) SpringContextUtils.getBean("ssoTokenService");
	}

	public String getSSOType()
	{
		return "plugin_token";
	}

	public SSOCredentials preHandler(HttpServletRequest request, HttpServletResponse response, UserSubject userSubject, AppResource resource, String ssoAccount) throws SSOException
	{
		// 登录凭证
		DefaultSSOCredentials credentials = new DefaultSSOCredentials();
		
		// 获取当前登录用户
		User user = (User) userSubject.getPrincipal().getInfo();
		
		// 单点登录帐号
		if (StringUtils.isNotEmpty(ssoAccount))
		{
			credentials.setSsoAccount(ssoAccount);
			return credentials;
		}
		
		List<Map<String, Object>> accountMap = new ArrayList<Map<String,Object>>();
		// 判断应用是否启用从账号
		if (resource.getEnableAccount() == CommonConstants.STATUS_ENABLED)
		{
			accountMap = super.getUserAccount(resource.getId(), user.getId(), ProjectConstants.ACCOUNT_BINDING_MODE_ADMIN);
			
			// 是否强制使用子帐号
			int forceResourceAccount = super.getParameterToInt("forceResourceAccount", CommonConstants.STATUS_1);
			
			if(forceResourceAccount == CommonConstants.STATUS_0){
				if(CollectionUtils.isEmpty(accountMap))
					return credentials;
			}else{
				Map<String, Object> accountObj = new HashMap<String, Object>();
				accountObj.put("accountType", "primary");
				accountObj.put("accountId", user.getId());
				accountObj.put("accountName", user.getLoginName());
				accountObj.put("accountAlias", user.getUserName());
				accountObj.put("isDefault", CommonConstants.STATUS_0);
				accountMap.add(accountObj);
			}
		}else{
			//不启用子账号，即使用主账号
			Map<String, Object> accountObj = new HashMap<String, Object>();
			accountObj.put("accountType", "primary");
			accountObj.put("accountId", user.getId());
			accountObj.put("accountName", user.getLoginName());
			accountObj.put("accountAlias", user.getUserName());
			accountObj.put("isDefault", CommonConstants.STATUS_0);
			accountMap.add(accountObj);
		}
		credentials.setAccountMap(accountMap);
		//未选择登录帐号，且只有一个应用帐号
		if (accountMap.size() == 1)
			credentials.setSsoAccount((String) accountMap.get(0).get("accountName"));
		
		return credentials;
	}

	public SSOResult postHandle(HttpServletRequest request, HttpServletResponse response, UserSubject userSubject, AppResource resource, SSOCredentials credentials, String accountType) throws SSOException
	{
		// 单点跳转地址
		String ssoUrl = super.getParameter("ssoUrl");
		if (StringUtils.isEmpty(ssoUrl))
		{
			throw new SSOException("单点登录参数配置错误!");
		}
		
		// 获取当前登录用户
		User user = (User) userSubject.getPrincipal().getInfo();
		
		// 单点登录信息
		String passport = super.getParameter("ssoAccount");
		if (StringUtils.isEmpty(passport))
		{
			passport = StringUtils.isEmpty(credentials.getSsoAccount())?user.getLoginName():credentials.getSsoAccount();
		}
		
		// 生成单点登录凭证
		SsoToken ssoToken = new SsoToken();
		// 应用系统ID
		ssoToken.setResourceId(resource.getId());
		// 用户ID
		ssoToken.setUserId(user.getId());
		// 认证方式
		ssoToken.setAuthMode(userSubject.getAuthCode());
		// 单点票据
		String accessToken = RandomUtils.generateString(32);
		ssoToken.setAccessToken(accessToken);
		// 单点登录用户信息
		ssoToken.setSsoPassport(passport);
		// 单点信息校验HASH
		ssoToken.setSsoVerifyHash(MD5Utils.md5(accessToken + "&" + ssoToken.getSsoPassport()));
		// 有效期
		ssoToken.setExpiredTime(DateUtils.getMinute(DateUtils.date(), 5));
		ssoToken.setCreateDateTime(DateUtils.date());
		ssoTokenService.save(ssoToken);
		
		if (ssoUrl.indexOf("?") == -1) 
		{
			ssoUrl = ssoUrl + "?token=" + accessToken + "";
		} 
		else 
		{
			ssoUrl = ssoUrl + "&token=" + accessToken + "";
		}
		
		SSORequest ssoRequest = new SSORequest(request);
		if (ssoRequest != null && StringUtils.isNotEmpty(ssoRequest.getService()))
		{
			ssoUrl = ssoUrl + "&service=" + ssoRequest.getService() + "";
		}
		
		SSOResult ssoResult = new SSOResult();
		ssoResult.setSuccess(true);
		ssoResult.setRedirectUrl(ssoUrl);
		ssoResult.setTokenInfo(ssoToken.getAccessToken());
		return ssoResult;
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response) throws SSOException
	{
		
	}

	public boolean supports(SSOCredentials credentials)
	{
		return credentials != null && DefaultSSOCredentials.class.isAssignableFrom(credentials.getClass());
	}
}

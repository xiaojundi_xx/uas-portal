package com.scanywhere.sso.principal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class DefaultSSOCredentials implements SSOCredentials
{
	private String ssoAccount;

	List<Map<String, Object>> accountMap = new ArrayList<Map<String, Object>>();
	
	public String getSsoAccount()
	{
		return ssoAccount;
	}

	public void setSsoAccount(String ssoAccount)
	{
		this.ssoAccount = ssoAccount;
	}
	
	public List<Map<String, Object>> getAccountMap()
	{
		return accountMap;
	}

	public void setAccountMap(List<Map<String, Object>> accountMap)
	{
		this.accountMap = accountMap;
	}
}

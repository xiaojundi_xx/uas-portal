package com.scanywhere.sso.principal;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface SSOCredentials extends Serializable
{
	/**
	 * 获取单点登录帐号
	 * 
	 * @return
	 */
	public String getSsoAccount();
	
	/**
	 * 获取单点登录帐号集合
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getAccountMap();
}

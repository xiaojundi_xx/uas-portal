package com.scanywhere.sso;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.authentication.exception.AuthenticationException;
import com.scanywhere.common.cache.CacheUtils;
import com.scanywhere.common.constant.ProjectConstants;
import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.CollectionUtils;
import com.scanywhere.core.constant.LogConstant;
import com.scanywhere.modules.audit.utils.LogUtils;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.modules.resource.service.ResourceService;
import com.scanywhere.security.SecurityLoginUtils;
import com.scanywhere.security.subject.UserSubject;
import com.scanywhere.sso.exception.SSOException;
import com.scanywhere.sso.handler.SSOHandler;
import com.scanywhere.sso.principal.SSOCredentials;

public class SSOManagerImpl implements SSOManager
{
	@Resource
	private ResourceService resourceService;
	
	public void doSSO(HttpServletRequest request, HttpServletResponse response, AppResource resource, String ssoAccount, String accountType) throws SSOException
	{
		// 获取单点实现
		SSOHandler handler = (SSOHandler) CacheUtils.get(ProjectConstants.SSO_CACHE, resource.getSsoType());
		if (handler == null)
		{
			throw new SSOException("单点登录实例获取失败, 请重新访问!");
		}
		
		// 设置单点登录配置参数
		handler.setSSOParameter(resource.getSsoParams());
		try
		{
			// 获取当前登录用户
			UserSubject userSubject = SecurityLoginUtils.getSubject();
			
			// 单点登录请求处理前调用
			SSOCredentials credentials = handler.preHandler(request, response, userSubject, resource,ssoAccount);
			
			if (credentials == null)
			{
				// 记录日志
				LogUtils.me().saveSsoLog(SecurityLoginUtils.getSubject(), resource, LogConstant.FAILURE, "单点登录信息为空, 请重新访问!");
				throw new SSOException("单点登录信息为空, 请重新访问!");
			}
			
			// 判断实例是否支持单点信息
			if (!handler.supports(credentials))
			{
				throw new AuthenticationException("认证实例不支持此认证信息, 请重新访问!");
			}
			
			String accountName = credentials.getSsoAccount();
			if(StringUtils.isEmpty(accountName))
			{
				throw new SSOException();
			}
			Map<String, String> logMap = new HashMap<String,String>();
			logMap.put("accountName", accountName);
			
			// 处理单点登录请求
			SSOResult ssoResult = handler.postHandle(request, response, userSubject, resource, credentials, accountType);
			if (!ssoResult.isSuccess())
			{
				// 记录日志
				LogUtils.me().saveSsoLog(SecurityLoginUtils.getSubject(), resource, LogConstant.FAILURE, ssoResult.getSsoError(),logMap);
				throw new SSOException(ssoResult.getSsoError());
			}
			
			// 请求结束后处理
			handler.afterCompletion(request, response);
			
			logMap.put("accessToken", ssoResult.getTokenInfo());
//			logMap.put("formInfo", ssoResult.getFormInfo());
			// 记录日志
			LogUtils.me().saveSsoLog(SecurityLoginUtils.getSubject(), resource, LogConstant.SUCCESS, "登录应用账号: "+accountName, logMap);
			// 是否需要重定向
			if (StringUtils.isNotEmpty(ssoResult.getRedirectUrl()))
			{
				response.sendRedirect(ssoResult.getRedirectUrl());
				return;
			}
		}
		catch (SSOException e)
		{
			e.printStackTrace();
			throw new SSOException(StringUtils.isEmpty(e.getMessage())?"系统异常":e.getMessage());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new SSOException("单点登录重定向失败!");
		}
	}

	@Override
	public void loadAccount(HttpServletRequest request, HttpServletResponse response, AppResource resource, JSONObject result) throws SSOException
	{
		// 获取单点实现
		SSOHandler handler = (SSOHandler) CacheUtils.get(ProjectConstants.SSO_CACHE, resource.getSsoType());
		if (handler == null)
		{
			throw new SSOException("单点登录实例获取失败, 请重新访问");
		}
		
		// 设置单点登录配置参数
		handler.setSSOParameter(resource.getSsoParams());
		// 获取当前登录用户
		UserSubject userSubject = SecurityLoginUtils.getSubject();

		// 单点登录请求处理前调用
		SSOCredentials credentials = handler.preHandler(request, response, userSubject, resource,"");
		if (credentials == null)
			throw new SSOException("单点登录信息为空, 请重新访问或联系管理员");
		
		if(CollectionUtils.isEmpty(credentials.getAccountMap()))
			throw new SSOException("获取应用帐号失败, 请刷新后访问");
		
		
		result.put("accountList", credentials.getAccountMap());
	}
}

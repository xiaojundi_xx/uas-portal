package com.scanywhere.sso;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.scanywhere.modules.resource.entity.AppResource;
import com.scanywhere.sso.exception.SSOException;

public interface SSOManager
{
	/**
	 * 单点登录
	 * 
	 * @param request
	 * @param response
	 * @param resource
	 * @throws SSOException
	 */
	public void doSSO(HttpServletRequest request, HttpServletResponse response, AppResource resource, String ssoAccount, String accountType) throws SSOException;

	/**
	 * 获取帐号
	 * 
	 * @param request
	 * @param response
	 * @param resource
	 * @param result
	 */
	public void loadAccount(HttpServletRequest request, HttpServletResponse response, AppResource resource, JSONObject result) throws SSOException;
}

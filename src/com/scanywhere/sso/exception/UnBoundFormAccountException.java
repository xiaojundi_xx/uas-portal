package com.scanywhere.sso.exception;

@SuppressWarnings("serial")
public class UnBoundFormAccountException extends SSOException
{
	public UnBoundFormAccountException() {
	}
	
	public UnBoundFormAccountException(String message)
	{
		super(message);
	}
	
	public UnBoundFormAccountException(Throwable cause)
	{
		super(cause);
	}
	  
	public UnBoundFormAccountException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

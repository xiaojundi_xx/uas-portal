package com.scanywhere.sso.exception;

@SuppressWarnings("serial")
public class UnBoundAccountException extends SSOException
{
	public UnBoundAccountException() {
	}
	
	public UnBoundAccountException(String message)
	{
		super(message);
	}
	
	public UnBoundAccountException(Throwable cause)
	{
		super(cause);
	}
	  
	public UnBoundAccountException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

package com.scanywhere.sso.exception;

@SuppressWarnings("serial")
public class SSOException extends RuntimeException
{
	public SSOException() {
	}
	
	public SSOException(String message)
	{
		super(message);
	}
	
	public SSOException(Throwable cause)
	{
		super(cause);
	}
	  
	public SSOException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

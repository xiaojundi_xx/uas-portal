package com.scanywhere.sso;

public class SSOResult
{
	private boolean success = false;
	
	private String redirectUrl;
	
	private String ssoError;

	private String tokenInfo;
	
	private String formInfo;
	
	

	public String getFormInfo() {
		return formInfo;
	}

	public void setFormInfo(String formInfo) {
		this.formInfo = formInfo;
	}

	public String getTokenInfo() {
		return tokenInfo;
	}

	public void setTokenInfo(String tokenInfo) {
		this.tokenInfo = tokenInfo;
	}
	
	public boolean isSuccess()
	{
		return success;
	}
	
	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}
	
	public String getSsoError()
	{
		return ssoError;
	}

	public void setSsoError(String ssoError)
	{
		this.ssoError = ssoError;
	}
}

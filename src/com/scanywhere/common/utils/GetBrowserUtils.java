package com.scanywhere.common.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class GetBrowserUtils {

	public static String getBrowserType(HttpServletRequest req){  
        Map<String,String> Sys= new HashMap<String, String>();  
        String ua = req.getHeader("User-Agent").toLowerCase();  
        String s;  
        String msieP = "msie ([\\d.]+)";  
        String ie11 = "\\) like gecko";
        String firefoxP = "firefox\\/([\\d.]+)";  
        String chromeP = "chrome\\/([\\d.]+)";  
        String safariP = "version\\/([\\d.]+).*safari";  
          
        Pattern pattern = Pattern.compile(msieP);  
        Matcher mat = pattern.matcher(ua);  
        if(mat.find()){  
            s=mat.group();  
            return "ie "+s.split(" ")[1];  
        }  
        pattern = Pattern.compile(ie11);  
        mat = pattern.matcher(ua);  
        if(mat.find()){  
            s=mat.group();  
            return "ie 11.0";  
        }
        pattern = Pattern.compile(firefoxP);  
        mat=pattern.matcher(ua);  
        if(mat.find()){  
            s=mat.group();  
            return "firefox "+s.split("/")[1];  
        }  
        pattern = Pattern.compile(chromeP);  
        mat=pattern.matcher(ua);  
        if(mat.find()){  
            s=mat.group();  
            return "chrom "+s.split("/")[1];  
        }   
       
        pattern = Pattern.compile(safariP);  
        mat=pattern.matcher(ua);  
        if(mat.find()){  
            s=mat.group();  
            return "safari "+s.split("/")[1].split(".")[0];  
        }   
        return "others";  
    }  
	
}

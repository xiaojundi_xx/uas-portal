package com.scanywhere.common.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.util.StringUtils;

import com.sca.cert.CertValid;
import com.sca.service.AgentServlet;
import com.sca.service.ScService;

public class CAUtils {

	public final static String CAROOTPATH = FileUtils.getAbsolutePath("../caroot.cer");
	public final static String CACRLPATH = FileUtils.getAbsolutePath("../ca.crl");
	
	private static final String RA_CLIENT_PROPERTIES = "config/ra_client.properties";
	private static ScService _service = null;

	private static ScService generate(){
		if(_service == null)
			return initRaService(RA_CLIENT_PROPERTIES);
		return _service;
	}
	
	public static ScService initRaService(String filePath){
		Properties properties = new Properties();
		InputStream in = AgentServlet.class.getClassLoader().getResourceAsStream(filePath);
		try {
			properties.load(in);
			return new ScService(properties);
		} catch (Exception e) {
			PrintUtils.printError("连接RA", e.getMessage());
		} finally{
			try{in.close();}catch(Exception ex){};
		}
		return null;
	}
	
	public static X509CertificateHolder initCARoot(){
		_service = generate();
		X509CertificateHolder ca = null;
		byte[] caRootBytes = null;
		boolean getCARoot = false;
		if(FileUtils.exist(new File(CAROOTPATH)))
			try {
				caRootBytes = FileUtils.readBytes(new File(CAROOTPATH));
			} catch (IOException e) {
				getCARoot = true;
			}
		else
			getCARoot = true;
		if(getCARoot && _service!=null){
			Map<String, Object> resultMap = new HashMap<String,Object>();
			try {
				resultMap = _service.getCAStr();
			} catch (Throwable e) {
				PrintUtils.printError("获取CA根证书", e.getMessage());
			}
			if(StringUtils.isEmpty(resultMap.get("success")))
				PrintUtils.printError("获取CA根证书", "CA服务器异常");
			else if(resultMap.get("success").equals("false"))
				PrintUtils.printError("获取CA根证书", (String) resultMap.get("message"));
			else {
				String certBase64 = ((String) resultMap.get("message"))
						.replace("-----BEGIN CERTIFICATE-----", "")
						.replace("-----END CERTIFICATE-----", "");
				caRootBytes = Base64.decode(certBase64);
				try {
					FileUtils.writeBytes(caRootBytes, CAROOTPATH);
				} catch (IOException e) {
					PrintUtils.printError("获取CA根证书", "生成文件失败");
				}
			}
		}
		if(caRootBytes != null)
			try {
				ca = CertValid.parseCertificate(caRootBytes);
			} catch (Throwable e) {
				PrintUtils.printError("获取CA根证书", "文件异常");
			}
		return ca;
	}
	
	public static X509CRLHolder initCRL(String issuer){
		X509CRLHolder crl = null;
		_service = generate();
		Map<String,Object> resultMap = new HashMap<String,Object>();
		InputStream crlInputStream = null;
		if(_service != null){
			try {
				resultMap = _service.getCRLStr(issuer);
			} catch (Throwable e) {
				PrintUtils.printError("获取CRL列表", e.getMessage());
			}

			if(StringUtils.isEmpty(resultMap.get("success")))
				PrintUtils.printError("获取CRL列表", "CA服务器异常");
			else if(resultMap.get("success").equals("false"))
				PrintUtils.printError("获取CRL列表", (String) resultMap.get("message"));
			else{
				byte[] crlBytes = Base64.decode((String) resultMap.get("message"));
				try {
					FileUtils.writeBytes(crlBytes, CACRLPATH);
				} catch (IOException e) {
					PrintUtils.printError("获取CRL列表", "生成文件异常");
				}
				crlInputStream = new ByteArrayInputStream(crlBytes);
			}
		}
		if(crlInputStream != null)
			try {
				crl = CertValid.parseCRL(crlInputStream);
			} catch (Throwable e) {
				PrintUtils.printError("获取CRL列表", "文件异常");
			}
		return crl;
	}
	
	
}

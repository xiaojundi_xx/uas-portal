package com.scanywhere.common.utils;

/**
 * 分页工具类
 */
public class PageUtils
{
	/**
	 * 将页数和每页条目数转换为开始位置和结束位置<br>
	 * 此方法用于不包括结束位置的分页方法<br>
	 * 例如：<br>
	 * 页码：1，每页10 -> [0, 10]<br>
	 * 页码：2，每页10 -> [10, 20]<br>
	 * <br>
	 * 
	 * @param pageNo 页码（从1计数）
	 * @param pageSize 每页条目数
	 * @return 第一个数为开始位置，第二个数为结束位置
	 */
	public static int[] transToStartEnd(int pageNo, int pageSize)
	{
		if (pageNo < 1)
		{
			pageNo = 1;
		}

		if (pageSize < 1)
		{
			pageSize = 0;
		}

		int start = (pageNo - 1) * pageSize;
		int end = start + pageSize;
		return new int[] { start, end };
	}

	/**
	 * 根据总数计算总页数
	 * 
	 * @param totalRecord 总数
	 * @param pageSize 每页数
	 * @return 总页数
	 */
	public static int totalPages(int totalRecord, int pageSize)
	{
		if (pageSize == 0)
		{
			return 0;
		}

		return totalRecord % pageSize == 0 ? (totalRecord / pageSize) : (totalRecord / pageSize + 1);
	}

	/**
	 * 计算当前页开始记录
	 * 
	 * @param pageNo 当前第几页
	 * @param pageSize 每页记录数
	 * @return 当前页开始记录号
	 */
	public static int countStartIndex(int pageNo, int pageSize)
	{
		int offset = pageSize * (pageNo - 1);
		return offset;
	}
}

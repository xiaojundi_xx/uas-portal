package com.scanywhere.common.utils;

import java.util.Date;

public class RandomUtils
{
	/**
	 * 返回一个定长的随机字符串(只包含大小写字母、数字)
	 * 
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static synchronized  String  generateString(int length)
	{
		try
		{
			// 指定字符内容为"0-9""a-z""A-Z"
			RandomGen.setCharset("a-z0-9A-Z");
			// 指定字符长度
			RandomGen.setLength(String.valueOf(length));
			// 生成随机数
			RandomGen.generateRandomObject();
			// 得到随机数
			String str = RandomGen.getRandom();
			return str;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 返回一个定长的随机字符串(只包含大小写字母、数字)
	 * 
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static String generateMinString(int length)
	{
		return generateString(length).toLowerCase();
	}

	/**
	 * 返回一个定长的随机字符串(只包含数字)
	 * 
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static synchronized String generateNumberString(int length)
	{
		try
		{
			// 指定字符内容为"0-9"
			RandomGen.setCharset("0-9");
			// 指定字符长度
			RandomGen.setLength(String.valueOf(length));
			// 生成随机数
			RandomGen.generateRandomObject();
			// 得到随机数
			String str = RandomGen.getRandom();
			return str;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 返回一个定长的随机纯字母字符串(只包含大小写字母)
	 * 
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static  synchronized String generateCharString(int length)
	{
		try
		{
			// 指定字符内容为"a-z""A-Z"
			RandomGen.setCharset("a-zA-Z");
			// 指定字符长度
			RandomGen.setLength(String.valueOf(length));
			// 生成随机数
			RandomGen.generateRandomObject();
			// 得到随机数
			String str = RandomGen.getRandom();
			return str;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 返回一个定长的随机纯小写字母字符串
	 * 
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static String generateLowerCharString(int length)
	{
		return generateCharString(length).toLowerCase();
	}

	/**
	 * 返回一个定长的随机纯大写字母字符串
	 * 
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static String generateUpperCharString(int length)
	{
		return generateCharString(length).toUpperCase();
	}

	/**
	 * 生成一个定长的纯0字符串
	 * 
	 * @param length 字符串长度
	 * @return 纯0字符串
	 */
	public static String generateZeroString(int length)
	{
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++)
		{
			sb.append('0');
		}
		return sb.toString();
	}

	/**
	 * 根据数字生成一个定长的字符串, 长度不够前面补0
	 * 
	 * @param num 数字
	 * @param fixdlenth 字符串长度
	 * @return 定长的字符串
	 */
	public static String toFixdLengthString(long num, int fixdlenth)
	{
		StringBuffer sb = new StringBuffer();
		String strNum = String.valueOf(num);
		if (fixdlenth - strNum.length() >= 0)
		{
			sb.append(generateZeroString(fixdlenth - strNum.length()));
		}
		else
		{
			throw new RuntimeException("将数字" + num + "转化为长度为" + fixdlenth + "的字符串发生异常!");
		}
		sb.append(strNum);
		return sb.toString();
	}

	/**
	 * 根据数字生成一个定长的字符串, 长度不够前面补0
	 * 
	 * @param num 数字
	 * @param fixdlenth 字符串长度
	 * @return 定长的字符串
	 */
	public static String toFixdLengthString(int num, int fixdlenth)
	{
		StringBuffer sb = new StringBuffer();
		String strNum = String.valueOf(num);
		if (fixdlenth - strNum.length() >= 0)
		{
			sb.append(generateZeroString(fixdlenth - strNum.length()));
		}
		else
		{
			throw new RuntimeException("将数字" + num + "转化为长度为" + fixdlenth + "的字符串发生异常!");
		}
		sb.append(strNum);
		return sb.toString();
	}
	
}



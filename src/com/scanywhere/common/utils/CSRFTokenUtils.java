package com.scanywhere.common.utils;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

public class CSRFTokenUtils
{
	public static String generate(HttpServletRequest request)
	{
		return UUID.randomUUID().toString();
	}
}

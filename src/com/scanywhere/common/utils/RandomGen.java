package com.scanywhere.common.utils;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.StringTokenizer;

/**
 * 生成随机数的工具类.
 */
public final class RandomGen
{
	static String randomstr;

	static boolean allchars = false;

	static Integer length = new Integer(8);

	static HashMap hmap;

	static ArrayList lower = null;

	static ArrayList upper = null;

	static char single[] = null;

	static int singlecount = 0;

	static boolean singles = false;

	static String algorithm = null;

	static String provider = null;

	static boolean secure = false;

	static Random random = null;

	static SecureRandom secrandom = null;

	public RandomGen()
	{
	}

	private static float getFloat()
	{
		if (random == null)
			return secrandom.nextFloat();
		else
			return random.nextFloat();
	}

	public static void generateRandomObject() throws Exception
	{
		if (secure)
			try
			{
				if (provider != null)
					random = SecureRandom.getInstance(algorithm, provider);
				else
					random = SecureRandom.getInstance(algorithm);
			}
			catch (NoSuchAlgorithmException ne)
			{
				throw new Exception(ne.getMessage());
			}
			catch (NoSuchProviderException pe)
			{
				throw new Exception(pe.getMessage());
			}
		else
			random = new Random();
	}

	private static void generaterandom()
	{
		if (allchars)
		{
			for (int i = 0; i < length.intValue(); i++)
				randomstr = (new StringBuilder()).append(randomstr)
						.append((new Character((char) (34 + (int) (getFloat() * 93F)))).toString()).toString();

		}
		else if (singles)
		{
			if (upper.size() == 3)
			{
				for (int i = 0; i < length.intValue(); i++)
					if ((int) (getFloat() * 100F) % 2 == 0)
					{
						if ((int) (getFloat() * 100F) % 2 == 0)
							randomstr = (new StringBuilder()).append(randomstr).append(randomSingle().toString())
									.toString();
						else
							randomstr = (new StringBuilder()).append(randomstr)
									.append(randomChar((Character) lower.get(2), (Character) upper.get(2)).toString())
									.toString();
					}
					else if ((int) (getFloat() * 100F) % 2 == 0)
						randomstr = (new StringBuilder()).append(randomstr)
								.append(randomChar((Character) lower.get(1), (Character) upper.get(1)).toString())
								.toString();
					else
						randomstr = (new StringBuilder()).append(randomstr)
								.append(randomChar((Character) lower.get(0), (Character) upper.get(0)).toString())
								.toString();

			}
			else if (upper.size() == 2)
			{
				for (int i = 0; i < length.intValue(); i++)
					if ((int) (getFloat() * 100F) % 2 == 0)
						randomstr = (new StringBuilder()).append(randomstr).append(randomSingle().toString())
								.toString();
					else if ((int) (getFloat() * 100F) % 2 == 0)
						randomstr = (new StringBuilder()).append(randomstr)
								.append(randomChar((Character) lower.get(1), (Character) upper.get(1)).toString())
								.toString();
					else
						randomstr = (new StringBuilder()).append(randomstr)
								.append(randomChar((Character) lower.get(0), (Character) upper.get(0)).toString())
								.toString();

			}
			else if (upper.size() == 1)
			{
				for (int i = 0; i < length.intValue(); i++)
					if (((int) getFloat() * 100) % 2 == 0)
						randomstr = (new StringBuilder()).append(randomstr).append(randomSingle().toString())
								.toString();
					else
						randomstr = (new StringBuilder()).append(randomstr)
								.append(randomChar((Character) lower.get(0), (Character) upper.get(0)).toString())
								.toString();

			}
			else
			{
				for (int i = 0; i < length.intValue(); i++)
					randomstr = (new StringBuilder()).append(randomstr).append(randomSingle().toString()).toString();

			}
		}
		else if (upper.size() == 3)
		{
			for (int i = 0; i < length.intValue(); i++)
				if ((int) (getFloat() * 100F) % 2 == 0)
					randomstr = (new StringBuilder()).append(randomstr)
							.append(randomChar((Character) lower.get(2), (Character) upper.get(2)).toString())
							.toString();
				else if ((int) (getFloat() * 100F) % 2 == 0)
					randomstr = (new StringBuilder()).append(randomstr)
							.append(randomChar((Character) lower.get(1), (Character) upper.get(1)).toString())
							.toString();
				else
					randomstr = (new StringBuilder()).append(randomstr)
							.append(randomChar((Character) lower.get(0), (Character) upper.get(0)).toString())
							.toString();

		}
		else if (upper.size() == 2)
		{
			for (int i = 0; i < length.intValue(); i++)
				if ((int) (getFloat() * 100F) % 2 == 0)
					randomstr = (new StringBuilder()).append(randomstr)
							.append(randomChar((Character) lower.get(1), (Character) upper.get(1)).toString())
							.toString();
				else
					randomstr = (new StringBuilder()).append(randomstr)
							.append(randomChar((Character) lower.get(0), (Character) upper.get(0)).toString())
							.toString();

		}
		else
		{
			for (int i = 0; i < length.intValue(); i++)
				randomstr = (new StringBuilder()).append(randomstr)
						.append(randomChar((Character) lower.get(0), (Character) upper.get(0)).toString()).toString();

		}
	}

	private static Character randomSingle()
	{
		return new Character(single[(int) (getFloat() * (float) singlecount - 1.0F)]);
	}

	private static Character randomChar(Character lower, Character upper)
	{
		char low = lower.charValue();
		char up = upper.charValue();
		int tempval = (int) ((float) low + getFloat() * (float) (up - low));
		return new Character((char) tempval);
	}

	@SuppressWarnings("unchecked")
	public static String getRandom()
	{
		randomstr = new String();
		generaterandom();
		if (hmap != null)
		{
			for (; hmap.containsKey(randomstr); generaterandom());
			hmap.put(randomstr, null);
		}
		return randomstr;
	}

	public static void setRanges(ArrayList low, ArrayList up)
	{
		lower = low;
		upper = up;
	}

	public static void setHmap(HashMap map)
	{
		hmap = map;
	}

	public static void setLength(String value)
	{
		length = new Integer(value);
	}

	public static void setAlgorithm(String value)
	{
		algorithm = value;
		secure = true;
	}

	public static void setProvider(String value)
	{
		provider = value;
	}

	public static void setAllchars(boolean value)
	{
		allchars = value;
	}

	public static void setSingle(char chars[], int value)
	{
		single = chars;
		singlecount = value;
		singles = true;
	}

	@SuppressWarnings("unchecked")
	public static void setCharset(String value)
	{
		boolean more = true;
		lower = new ArrayList(3);
		upper = new ArrayList(3);
		if (value.compareTo("all") == 0)
		{
			allchars = true;
			more = false;
		}
		else if (value.charAt(1) == '-' && value.charAt(0) != '\\')
			while (more && value.charAt(1) == '-' && value.charAt(0) != '\\')
			{
				lower.add(new Character(value.charAt(0)));
				upper.add(new Character(value.charAt(2)));
				if (value.length() <= 3)
					more = false;
				else
					value = value.substring(3);
			}
		if (more)
		{
			single = new char[30];
			for (StringTokenizer tokens = new StringTokenizer(value); tokens.hasMoreTokens();)
			{
				String token = tokens.nextToken();
				if (token.length() > 1)
					single[singlecount++] = '-';
				single[singlecount++] = token.charAt(0);
			}

		}
		if (lower == null && single == null)
			setCharset("a-zA-Z0-9");
	}

}

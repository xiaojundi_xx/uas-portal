package com.scanywhere.common.utils;


public class PrintUtils
{
	public static void printError(String result, String cause)
	{
		System.out.println("*************************************");
		System.out.println("失败: " + result);
		System.out.println("原因: " + cause);
		System.out.println("*************************************");
	}
}

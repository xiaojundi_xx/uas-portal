package com.scanywhere.common.support;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.scanywhere.common.lang.StringUtils;
import com.scanywhere.common.utils.BeanUtils;
import com.scanywhere.common.utils.CollectionUtils;

/**
 * JavaBean映射
 */
public class BeanInjector
{
	public static final <T> T inject(Class<T> beanClass, HttpServletRequest request)
	{
		try
		{
			return BeanUtils.mapToBeanIgnoreCase(getParameterMap(request), beanClass);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e.getMessage());
		}
	}

	public static final <T> T inject(Class<T> beanClass, String prefix, HttpServletRequest request)
	{
		try
		{
			Map<String, Object> map = injectPara(prefix, request);
			return BeanUtils.mapToBeanIgnoreCase(map, beanClass);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e.getMessage());
		}
	}

	public static final Map<String, Object> injectMaps(HttpServletRequest request)
	{
		return getParameterMap(request);
	}

	public static final Map<String, Object> injectMaps(String prefix, HttpServletRequest request)
	{
		Map<String, Object> map = injectPara(prefix, request);
		return map;
	}

	private static final Map<String, Object> injectPara(String prefix, HttpServletRequest request)
	{
		Map<String, String[]> paramMap = request.getParameterMap();
		Map<String, Object> map = new HashMap<>();
		String start = prefix.toLowerCase() + ".";
		String[] value = null;
		for (Entry<String, String[]> param : paramMap.entrySet())
		{
			if (!param.getKey().toLowerCase().startsWith(start))
			{
				continue;
			}
			value = param.getValue();
			Object o = null;
			if (CollectionUtils.isNotEmpty(value))
			{
				if (value.length > 1)
				{
					o = CollectionUtils.join(value, ",");
				}
				else
				{
					o = value[0];
				}
			}
			map.put(StringUtils.removePrefixIgnoreCase(param.getKey(), start).toLowerCase(), o);
		}
		return map;
	}

	private static final Map<String, Object> getParameterMap(HttpServletRequest request)
	{
		Map<String, String[]> paramMap = request.getParameterMap();
		Map<String, Object> map = new HashMap<>();
		String[] value = null;
		for (Entry<String, String[]> param : paramMap.entrySet())
		{
			value = param.getValue();
			Object o = null;
			if (CollectionUtils.isNotEmpty(value))
			{
				if (value.length > 1)
				{
					o = CollectionUtils.join(value, ",");
				}
				else
				{
					o = value[0];
				}
			}
			map.put(param.getKey(), o);
		}
		return map;
	}

}

package com.scanywhere.common.callback;

/**
 * 方法回调接口
 */
public interface MethodCallback
{
	Object execute(Object... params);
}
package com.scanywhere.common.constant;

public class ProjectConstants
{
	/** 系统配置 安全配置 security_config */
	public static final String SYS_CONFIG_MODULE_SECURITY = "security_config";
	
	/** 系统配置 证书系统配置 ca_config */
	public static final String SYS_CONFIG_MODULE_CA_SYS = "ca_config";
	
	/** 系统配置 认证网关配置 gateway_config */
	public static final String SYS_CONFIG_MODULE_GATEWAY_SYS = "gateway_config";
	
	/** 授权管理 访问策略 access_policy */
	public static final String SYS_CONFIG_MODULE_ACCESS_POLICY = "access_policy";
	
	/** 系统授权 lic_config */
	public static final String SYS_CONFIG_MODULE_LIC_CONFIG = "lic_config";
	
	/** 用户角色 内置角色 */
	public static final int USER_ROLE_BUILTIN = 0;
	
	/** 用户角色 基础角色 */
	public static final int USER_ROLE_BASIC = 1;
	
	/** 认证方式缓存标识 authentication_cache */
	public static final String AUTHENTICATION_CACHE = "authentication_cache";
	
	/** 单点登录缓存标识 sso_cache */
	public static final String SSO_CACHE = "sso_cache";
	
	/** 登录模式 内网 */
	public static final int LOGIN_MODE_INTRANET = 0;
	
	/** 登录模式 公网 */
	public static final int LOGIN_MODE_PUBLIC_NETWORK = 1;
	
	/** 应用子帐号绑定模式 用户 */
	public static final int ACCOUNT_BINDING_MODE_USER = 1;
	
	/** 应用子帐号绑定模式 用户 */
	public static final int ACCOUNT_BINDING_MODE_ADMIN = 2;
}

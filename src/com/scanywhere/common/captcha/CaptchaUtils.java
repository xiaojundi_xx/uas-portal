package com.scanywhere.common.captcha;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

public class CaptchaUtils
{
	// 验证码随机字符数组
	private static final String[] strArr = { "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H",
			"J", "K", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y" };

	/**
	 * 生成4位随机数
	 * 
	 * @return
	 */
	public static String generateCaptcha()
	{
		Random random = new Random();
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < 4; i++)
		{
			String r = String.valueOf(strArr[random.nextInt(strArr.length)]);
			s.append(r);
		}
		return s.toString();
	}

	public static BufferedImage generateImage(String captcha, int width, int height)
	{
		try
		{
			// 生成随机类
			Random random = new Random();

			// 在内存中创建图象
			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

			// 获取图形上下文
			Graphics g = image.getGraphics();

			// 设定背景色
			g.setColor(new Color(0xDCDCDC));
			g.fillRect(0, 0, width, height);

			// 画边框
			g.setColor(Color.black);
			g.drawRect(0, 0, width - 1, height - 1);

			// 将认证码显示到图象中
			g.setColor(Color.black);

			// 设定字体
			g.setFont(new Font("Atlantic Inline", Font.PLAIN, 18));

			// 画蛋蛋, 有蛋的生活才精彩
			/*Color color;
			for (int i = 0; i < 8; i++)
			{
				color = getRandColor(120, 200);
				g.setColor(color);
				g.drawOval(random.nextInt(width), random.nextInt(height), 5 + random.nextInt(10),
						5 + random.nextInt(10));
				color = null;
			}*/

			String str = captcha.substring(0, 1);
			g.drawString(str, 3, 23);

			str = captcha.substring(1, 2);
			g.drawString(str, 17, 21);

			str = captcha.substring(2, 3);
			g.drawString(str, 32, 24);

			str = captcha.substring(3, 4);
			g.drawString(str, 46, 21);

			// 随机产生20个干扰点，使图象中的认证码不易被其它程序探测到
			for (int i = 0; i < 10; i++)
			{
				int x = random.nextInt(width);
				int y = random.nextInt(height);
				g.drawOval(x, y, random.nextInt(5), random.nextInt(5));
			}

			g.dispose();
			return image;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	/*
	 * 给定范围获得随机颜色
	 */
	private static Color getRandColor(int fc, int bc)
	{
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}
}
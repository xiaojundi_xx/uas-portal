package com.scanywhere.common.cache;

/**
 * 数据重载
 */
public interface ILoader
{
	Object load();
}
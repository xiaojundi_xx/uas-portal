package com.scanywhere.common.cache;

import java.util.List;

/**
 * 通用缓存接口
 */
public interface ICache
{
	/**
	 * 向指定缓存中添加数据
	 * 
	 * @param cacheName
	 * @param key
	 * @param value
	 */
	public void put(String cacheName, Object key, Object value);

	/**
	 * 
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public <T> T get(String cacheName, Object key);

	/**
	 * 
	 * @param cacheName
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getKeys(String cacheName);

	/**
	 * 
	 * @param cacheName
	 * @param key
	 */
	public void remove(String cacheName, Object key);

	/**
	 * 
	 * @param cacheName
	 */
	public void removeAll(String cacheName);

	/**
	 * 
	 * @param cacheName
	 * @param key
	 * @param iLoader
	 * @return
	 */
	public <T> T get(String cacheName, Object key, ILoader iLoader);

	/**
	 * 
	 * @param cacheName
	 * @param key
	 * @param iLoaderClass
	 * @return
	 */
	public <T> T get(String cacheName, Object key, Class<? extends ILoader> iLoaderClass);

}

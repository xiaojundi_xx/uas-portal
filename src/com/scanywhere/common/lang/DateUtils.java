package com.scanywhere.common.lang;

import java.lang.management.ManagementFactory;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

/**
 * 日期工具类, 继承org.apache.commons.lang.time.DateUtils类
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils
{
	/** 毫秒 */
	public final static long MS = 1;

	/** 每秒钟的毫秒数 */
	public final static long SECOND_MS = MS * 1000;

	/** 每分钟的毫秒数 */
	public final static long MINUTE_MS = SECOND_MS * 60;

	/** 每小时的毫秒数 */
	public final static long HOUR_MS = MINUTE_MS * 60;

	/** 每天的毫秒数 */
	public final static long DAY_MS = HOUR_MS * 24;

	/** 标准日期格式 */
	public final static String NORM_DATE_PATTERN = "yyyy-MM-dd";

	/** 标准时间格式 */
	public final static String NORM_TIME_PATTERN = "HH:mm:ss";

	/** 标准日期时间格式,精确到分 */
	public final static String NORM_DATE_MINUTE_PATTERN = "yyyy-MM-dd HH:mm";

	/** 标准日期时间格式,精确到秒 */
	public final static String NORM_DATE_SEC_PATTERN = "yyyy-MM-dd HH:mm:ss";

	/** 标准日期时间格式,精确到毫秒 */
	public final static String NORM_DATE_MS_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

	/** HTTP头中日期时间格式 */
	public final static String HTTP_HEADER_DATE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss z";

	private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");

	private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");

	private final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");

	private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final static SimpleDateFormat sdfmsTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	private final static SimpleDateFormat allTime = new SimpleDateFormat("yyyyMMddHHmmss");

	private static String[] parsePatterns = { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH",
			"yyyy-MM", "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM/dd HH", "yyyy/MM",
			"yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM.dd HH", "yyyy.MM", "yyyy年MM月dd日",
			"yyyy年MM月dd日 HH时mm分ss秒", "yyyy年MM月dd日 HH时mm分", "yyyy年MM月dd日 HH时", "yyyy年MM月", "yyyy" };

	/** HTTP日期时间格式化器 */
	private static ThreadLocal<SimpleDateFormat> HTTP_HEADER_DATE_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		synchronized protected SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat(HTTP_HEADER_DATE_PATTERN, Locale.US);
		};
	};

	/**
	 * 得到日期字符串 ，转换格式（yyyy-MM-dd）
	 */
	public static String formatDate(Date date)
	{
		return formatDate(date, "yyyy-MM-dd");
	}

	/**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(long dateTime, String pattern)
	{
		return formatDate(new Date(dateTime), pattern);
	}

	/**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(Date date, String pattern)
	{
		String formatDate = null;
		if (date != null)
		{
			if (StringUtils.isBlank(pattern))
			{
				pattern = "yyyy-MM-dd";
			}
			formatDate = FastDateFormat.getInstance(pattern).format(date);
		}
		return formatDate;
	}

	/**
	 * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String formatDateTime(Date date)
	{
		return formatDate(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd）
	 */
	public static String getDate()
	{
		return getDate("yyyy-MM-dd");
	}

	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String getDate(String pattern)
	{
		// return DateFormatUtils.format(new Date(), pattern);
		return FastDateFormat.getInstance(pattern).format(new Date());
	}

	/**
	 * 得到当前日期前后多少天，月，年的日期字符串
	 * 
	 * @param pattern 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 * @param amont 数量，前为负数，后为正数
	 * @param type
	 *            类型，可参考Calendar的常量(如：Calendar.HOUR、Calendar.MINUTE、Calendar.SECOND)
	 * @return
	 */
	public static String getDate(String pattern, int amont, int type)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(type, amont);
		// return DateFormatUtils.format(calendar.getTime(), pattern);
		return FastDateFormat.getInstance(pattern).format(calendar.getTime());
	}

	/**
	 * 得到当前时间字符串 格式（HH:mm:ss）
	 */
	public static String getCurrentTime()
	{
		return formatDate(new Date(), "HH:mm:ss");
	}

	/**
	 * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String getCurrentDateTime()
	{
		return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前年份字符串 格式（yyyy）
	 */
	public static String getCurrentYear()
	{
		return formatDate(new Date(), "yyyy");
	}

	/**
	 * 得到当前月份字符串 格式（MM）
	 */
	public static String getCurrentMonth()
	{
		return formatDate(new Date(), "MM");
	}

	/**
	 * 得到当天字符串 格式（dd）
	 */
	public static String getCurrentDay()
	{
		return formatDate(new Date(), "dd");
	}

	/**
	 * 得到当前星期字符串 格式（E）星期几
	 */
	public static String getWeek()
	{
		return formatDate(new Date(), "E");
	}

	/**
	 * 日期型字符串转化为日期 格式 see to DateUtils#parsePatterns
	 */
	public static Date parseDate(Object str)
	{
		if (str == null)
		{
			return null;
		}
		try
		{
			return parseDate(str.toString(), parsePatterns);
		}
		catch (ParseException e)
		{
			return null;
		}
	}

	/**
	 * 获取过去的天数
	 * 
	 * @param date
	 * @return
	 */
	public static long pastDays(Date date)
	{
		long t = System.currentTimeMillis() - date.getTime();
		return t / (24 * 60 * 60 * 1000);
	}

	/**
	 * 获取过去的小时
	 * 
	 * @param date
	 * @return
	 */
	public static long pastHour(Date date)
	{
		long t = System.currentTimeMillis() - date.getTime();
		return t / (60 * 60 * 1000);
	}

	/**
	 * 获取过去的分钟
	 * 
	 * @param date
	 * @return
	 */
	public static long pastMinutes(Date date)
	{
		long t = System.currentTimeMillis() - date.getTime();
		return t / (60 * 1000);
	}

	/**
	 * 获取两个日期之间的天数
	 * 
	 * @param before
	 * @param after
	 * @return
	 */
	public static double getDistanceOfTwoDate(Date before, Date after)
	{
		long beforeTime = before.getTime();
		long afterTime = after.getTime();
		return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
	}

	/**
	 * 获取某月有几天
	 * 
	 * @param date 日期
	 * @return 天数
	 */
	public static int getMonthHasDays(Date date)
	{
		// String yyyyMM = new SimpleDateFormat("yyyyMM").format(date);
		String yyyyMM = FastDateFormat.getInstance("yyyyMM").format(date);
		String year = yyyyMM.substring(0, 4);
		String month = yyyyMM.substring(4, 6);
		String day31 = ",01,03,05,07,08,10,12,";
		String day30 = "04,06,09,11";
		int day = 0;
		if (day31.contains(month))
		{
			day = 31;
		}
		else if (day30.contains(month))
		{
			day = 30;
		}
		else
		{
			int y = Integer.parseInt(year);
			if ((y % 4 == 0 && (y % 100 != 0)) || y % 400 == 0)
			{
				day = 29;
			}
			else
			{
				day = 28;
			}
		}
		return day;
	}

	/**
	 * 获取日期是当年的第几周
	 * 
	 * @param date
	 * @return
	 */
	public static int getWeekOfYear(Date date)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 获取一天的开始时间（如：2015-11-3 00:00:00.000）
	 * 
	 * @param date 日期
	 * @return
	 */
	public static Date getOfDayFirst(Date date)
	{
		if (date == null)
		{
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 获取一天的最后时间（如：2015-11-3 23:59:59.999）
	 * 
	 * @param date 日期
	 * @return
	 */
	public static Date getOfDayLast(Date date)
	{
		if (date == null)
		{
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	/**
	 * 获取服务器启动时间
	 * 
	 * @param date
	 * @return
	 */
	public static Date getServerStartDate()
	{
		long time = ManagementFactory.getRuntimeMXBean().getStartTime();
		return new Date(time);
	}

	/**
	 * 格式化为日期范围字符串
	 * 
	 * @param beginDate 2018-01-01
	 * @param endDate 2018-01-31
	 * @return 2018-01-01 ~ 2018-01-31
	 * @author ThinkGem
	 */
	public static String formatDateBetweenString(Date beginDate, Date endDate)
	{
		String begin = DateUtils.formatDate(beginDate);
		String end = DateUtils.formatDate(endDate);
		if (StringUtils.isNoneBlank(begin, end))
		{
			return begin + " ~ " + end;
		}
		return null;
	}

	/**
	 * 解析日期范围字符串为日期对象
	 * 
	 * @param dateString 2018-01-01 ~ 2018-01-31
	 * @return new Date[]{2018-01-01, 2018-01-31}
	 * @author ThinkGem
	 */
	public static Date[] parseDateBetweenString(String dateString)
	{
		Date beginDate = null;
		Date endDate = null;
		if (StringUtils.isNotBlank(dateString))
		{
			String[] ss = StringUtils.split(dateString, "~");
			if (ss != null && ss.length == 2)
			{
				String begin = StringUtils.trim(ss[0]);
				String end = StringUtils.trim(ss[1]);
				if (StringUtils.isNoneBlank(begin, end))
				{
					beginDate = DateUtils.parseDate(begin);
					endDate = DateUtils.parseDate(end);
				}
			}
		}
		return new Date[] { beginDate, endDate };
	}

	/**
	 * 获取YYYY格式
	 * 
	 * @return
	 */
	public static String getYear()
	{
		return sdfYear.format(new Date());
	}

	/**
	 * 获取YYYY格式
	 * 
	 * @return
	 */
	public static String getYear(Date date)
	{
		return sdfYear.format(date);
	}

	/**
	 * 获取YYYY-MM-DD格式
	 * 
	 * @return
	 */
	public static String getDay()
	{
		return sdfDay.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD格式
	 * 
	 * @return
	 */
	public static String getDay(Date date)
	{
		return sdfDay.format(date);
	}

	/**
	 * 获取YYYYMMDD格式
	 * 
	 * @return
	 */
	public static String getDays()
	{
		return sdfDays.format(new Date());
	}

	/**
	 * 获取YYYYMMDD格式
	 * 
	 * @return
	 */
	public static String getDays(Date date)
	{
		return sdfDays.format(date);
	}

	/**
	 * 获取YYYY-MM-DD HH:mm:ss格式
	 * 
	 * @return
	 */
	public static String getTime()
	{
		return sdfTime.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD HH:mm:ss.SSS格式
	 * 
	 * @return
	 */
	public static String getMsTime()
	{
		return sdfmsTime.format(new Date());
	}

	/**
	 * 获取YYYYMMDDHHmmss格式
	 * 
	 * @return
	 */
	public static String getAllTime()
	{
		return allTime.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD HH:mm:ss格式
	 * 
	 * @return
	 */
	public static String getTime(Date date)
	{
		return sdfTime.format(date);
	}

	/**
	 * @Title: compareDate
	 * @Description:(日期比较，如果s>=e 返回true 否则返回false)
	 * @param s
	 * @param e
	 * @return boolean
	 * @throws @author luguosui
	 */
	public static boolean compareDate(String s, String e)
	{
		if (parseDate(s) == null || parseDate(e) == null)
		{
			return false;
		}
		return parseDate(s).getTime() >= parseDate(e).getTime();
	}

	/**
	 * 格式化日期
	 * 
	 * @return
	 */
	public static Date parseDate(String date)
	{
		try
		{
			return sdfDay.parse(date);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 格式化日期
	 * 
	 * @return
	 */
	public static Date parseTime(String date)
	{
		try
		{
			return sdfTime.parse(date);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 格式化日期
	 * 
	 * @return
	 */
	public static Date parse(String date, String pattern)
	{
		DateFormat fmt = new SimpleDateFormat(pattern);
		try
		{
			return fmt.parse(date);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 把日期转换为Timestamp
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp format(Date date)
	{
		return new java.sql.Timestamp(date.getTime());
	}

	/**
	 * 校验日期是否合法
	 * 
	 * @return
	 */
	public static boolean isValidDate(String s)
	{
		try
		{
			sdfTime.parse(s);
			return true;
		}
		catch (Exception e)
		{
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			return false;
		}
	}

	/**
	 * 校验日期是否合法
	 * 
	 * @return
	 */
	public static boolean isValidDate(String s, String pattern)
	{
		DateFormat fmt = new SimpleDateFormat(pattern);
		try
		{
			fmt.parse(s);
			return true;
		}
		catch (Exception e)
		{
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			return false;
		}
	}

	public static int getDiffYear(String startTime, String endTime)
	{
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			int years = (int) (((fmt.parse(endTime).getTime() - fmt.parse(startTime).getTime()) / (1000 * 60 * 60 * 24))
					/ 365);
			return years;
		}
		catch (Exception e)
		{
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			return 0;
		}
	}

	/**
	 * <li>功能描述：时间相减得到天数
	 * 
	 * @param beginDateStr
	 * @param endDateStr
	 * @return long
	 * @author Administrator
	 */
	public static long getDaySub(String beginDateStr, String endDateStr)
	{
		long day = 0;
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.util.Date beginDate = null;
		java.util.Date endDate = null;

		try
		{
			beginDate = format.parse(beginDateStr);
			endDate = format.parse(endDateStr);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
		return day;
	}

	/**
	 * 得到n天之后的日期
	 * 
	 * @param days
	 * @return
	 */
	public static String getAfterDayDate(String days)
	{
		int daysInt = Integer.parseInt(days);

		Calendar canlendar = Calendar.getInstance(); // java.util包
		canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
		Date date = canlendar.getTime();

		SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = sdfd.format(date);

		return dateStr;
	}

	/**
	 * 得到n天之后是周几
	 * 
	 * @param days
	 * @return
	 */
	public static String getAfterDayWeek(String days)
	{
		int daysInt = Integer.parseInt(days);

		Calendar canlendar = Calendar.getInstance(); // java.util包
		canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
		Date date = canlendar.getTime();

		SimpleDateFormat sdf = new SimpleDateFormat("E");
		String dateStr = sdf.format(date);

		return dateStr;
	}

	/**
	 * 当前时间,格式 yyyy-MM-dd HH:mm:ss
	 * 
	 * @return 当前时间的标准形式字符串
	 */
	public static String now()
	{
		return formatDate(new Date());
	}

	/**
	 * 当前时间long
	 * 
	 * @param isNano 是否为高精度时间
	 * @return 时间
	 */
	public static long current(boolean isNano)
	{
		return isNano ? System.nanoTime() : System.currentTimeMillis();
	}

	/**
	 * 当前日期,格式 yyyy-MM-dd
	 * 
	 * @return 当前日期的标准形式字符串
	 */
	public static String today()
	{
		return formatDate(new Date());
	}

	/**
	 * @return 当前月份
	 */
	public static int thisMonth()
	{
		return month(date());
	}

	/**
	 * @return 今年
	 */
	public static int thisYear()
	{
		return year(date());
	}

	/**
	 * @return 当前时间
	 */
	public static Date date()
	{
		return new Date();
	}

	/**
	 * Long类型时间转为Date
	 * 
	 * @param date Long类型Date（Unix时间戳）
	 * @return 时间对象
	 */
	public static Date date(long date)
	{
		return new Date(date);
	}

	/**
	 * 转换为Calendar对象
	 * 
	 * @param date 日期对象
	 * @return Calendar对象
	 */
	public static Calendar toCalendar(Date date)
	{
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	/**
	 * 获得月份,从1月开始计数
	 * 
	 * @param date 日期
	 * @return 月份
	 */
	public static int month(Date date)
	{
		return toCalendar(date).get(Calendar.MONTH) + 1;
	}

	/**
	 * 获得年
	 * 
	 * @param date 日期
	 * @return 年
	 */
	public static int year(Date date)
	{
		return toCalendar(date).get(Calendar.YEAR);
	}

	/**
	 * 获得季节
	 * 
	 * @param date 日期
	 * @return 第几个季节
	 */
	public static int season(Date date)
	{
		return toCalendar(date).get(Calendar.MONTH) / 3 + 1;
	}

	/**
	 * 获得指定日期年份和季节<br>
	 * 格式：[20131]表示2013年第一季度
	 * 
	 * @param date 日期
	 * @return Season ,类似于 20132
	 */
	public static String yearAndSeason(Date date)
	{
		return yearAndSeason(toCalendar(date));
	}

	/**
	 * 获得指定日期区间内的年份和季节<br>
	 * 
	 * @param startDate 其实日期（包含）
	 * @param endDate 结束日期（包含）
	 * @return Season列表 ,元素类似于 20132
	 */
	public static LinkedHashSet<String> yearAndSeasons(Date startDate, Date endDate)
	{
		final LinkedHashSet<String> seasons = new LinkedHashSet<String>();
		if (startDate == null || endDate == null)
		{
			return seasons;
		}

		final Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		while (true)
		{
			// 如果开始时间超出结束时间,让结束时间为开始时间,处理完后结束循环
			if (startDate.after(endDate))
			{
				startDate = endDate;
			}

			seasons.add(yearAndSeason(cal));

			if (startDate.equals(endDate))
			{
				break;
			}

			cal.add(Calendar.MONTH, 3);
			startDate = cal.getTime();
		}

		return seasons;
	}

	/**
	 * 根据特定格式格式化日期
	 * 
	 * @param date 被格式化的日期
	 * @param format 格式
	 * @return 格式化后的字符串
	 */
	public static String format(Date date, String format)
	{
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * 格式化为Http的标准日期格式
	 * 
	 * @param date 被格式化的日期
	 * @return HTTP标准形式日期字符串
	 */
	public static String formatHttpDate(Date date)
	{
		if (null == date)
		{
			return null;
		}
		return HTTP_HEADER_DATE_FORMAT.get().format(date);
	}

	/**
	 * 构建Date对象
	 * 
	 * @param dateStr Date字符串
	 * @param simpleDateFormat 格式化器
	 * @return Date对象
	 */
	public static Date parse(String dateStr, SimpleDateFormat simpleDateFormat)
	{
		try
		{
			return simpleDateFormat.parse(dateStr);
		}
		catch (Exception e)
		{
		}
		return null;
	}

	/**
	 * 格式：<br>
	 * 1、yyyy-MM-dd HH:mm:ss<br>
	 * 2、yyyy-MM-dd<br>
	 * 3、HH:mm:ss<br>
	 * 4、yyyy-MM-dd HH:mm 5、yyyy-MM-dd HH:mm:ss.SSS
	 * 
	 * @param dateStr 日期字符串
	 * @return 日期
	 */
	public static Date parse(String dateStr)
	{
		if (null == dateStr)
		{
			return null;
		}
		dateStr = dateStr.trim();
		int length = dateStr.length();
		try
		{
			if (length == NORM_DATE_SEC_PATTERN.length())
			{
				return parseTime(dateStr);
			}
			else if (length == NORM_DATE_PATTERN.length())
			{
				return parseDate(dateStr);
			}
			else if (length == NORM_TIME_PATTERN.length())
			{
				return parse(dateStr, NORM_TIME_PATTERN);
			}
			else if (length == NORM_DATE_MINUTE_PATTERN.length())
			{
				return parse(dateStr, NORM_DATE_MINUTE_PATTERN);
			}
			else if (length >= NORM_DATE_MS_PATTERN.length() - 2)
			{
				return parse(dateStr, NORM_DATE_MS_PATTERN);
			}
		}
		catch (Exception e)
		{
		}
		return null;
	}

	/**
	 * 获取某天的开始时间
	 * 
	 * @param date 日期
	 * @return 某天的开始时间
	 */
	public static Date getBeginTimeOfDay(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 获取某天的结束时间
	 * 
	 * @param date 日期
	 * @return 某天的结束时间
	 */
	public static Date getEndTimeOfDay(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	/**
	 * 昨天
	 * 
	 * @return 昨天
	 */
	public static Date yesterday()
	{
		return offsiteDay(new Date(), -1);
	}

	/**
	 * 上周
	 * 
	 * @return 上周
	 */
	public static Date lastWeek()
	{
		return offsiteWeek(new Date(), -1);
	}

	/**
	 * 上个月
	 * 
	 * @return 上个月
	 */
	public static Date lastMouth()
	{
		return offsiteMonth(new Date(), -1);
	}

	/**
	 * 偏移天
	 * 
	 * @param date 日期
	 * @param offsite 偏移天数,正数向未来偏移,负数向历史偏移
	 * @return 偏移后的日期
	 */
	public static Date offsiteDay(Date date, int offsite)
	{
		return offsiteDate(date, Calendar.DAY_OF_YEAR, offsite);
	}

	/**
	 * 偏移周
	 * 
	 * @param date 日期
	 * @param offsite 偏移周数,正数向未来偏移,负数向历史偏移
	 * @return 偏移后的日期
	 */
	public static Date offsiteWeek(Date date, int offsite)
	{
		return offsiteDate(date, Calendar.WEEK_OF_YEAR, offsite);
	}

	/**
	 * 偏移月
	 * 
	 * @param date 日期
	 * @param offsite 偏移月数,正数向未来偏移,负数向历史偏移
	 * @return 偏移后的日期
	 */
	public static Date offsiteMonth(Date date, int offsite)
	{
		return offsiteDate(date, Calendar.MONTH, offsite);
	}

	/**
	 * 获取指定日期偏移指定时间后的时间
	 * 
	 * @param date 基准日期
	 * @param calendarField 偏移的粒度大小（小时、天、月等）使用Calendar中的常数
	 * @param offsite 偏移量,正数为向后偏移,负数为向前偏移
	 * @return 偏移后的日期
	 */
	public static Date offsiteDate(Date date, int calendarField, int offsite)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendarField, offsite);
		return calendar.getTime();
	}

	/**
	 * 判断两个日期相差的时长<br/>
	 * 返回 minuend - subtrahend 的差
	 * 
	 * @param subtrahend 减数日期
	 * @param minuend 被减数日期
	 * @param diffField 相差的选项：相差的天、小时
	 * @return 日期差
	 */
	public static long diff(Date subtrahend, Date minuend, long diffField)
	{
		long diff = minuend.getTime() - subtrahend.getTime();
		return diff / diffField;
	}

	/**
	 * 计时,常用于记录某段代码的执行时间,单位：纳秒
	 * 
	 * @param preTime 之前记录的时间
	 * @return 时间差,纳秒
	 */
	public static long spendNt(long preTime)
	{
		return System.nanoTime() - preTime;
	}

	/**
	 * 计时,常用于记录某段代码的执行时间,单位：毫秒
	 * 
	 * @param preTime 之前记录的时间
	 * @return 时间差,毫秒
	 */
	public static long spendMs(long preTime)
	{
		return System.currentTimeMillis() - preTime;
	}

	/**
	 * 格式化成yyMMddHHmm后转换为int型
	 * 
	 * @param date 日期
	 * @return int
	 */
	public static int toIntSecond(Date date)
	{
		return Integer.parseInt(format(date, "yyMMddHHmm"));
	}

	/**
	 * 计算指定指定时间区间内的周数
	 * 
	 * @param start 开始时间
	 * @param end 结束时间
	 * @return 周数
	 */
	public static int weekCount(Date start, Date end)
	{
		final Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(start);
		final Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(end);

		final int startWeekofYear = startCalendar.get(Calendar.WEEK_OF_YEAR);
		final int endWeekofYear = endCalendar.get(Calendar.WEEK_OF_YEAR);

		int count = endWeekofYear - startWeekofYear + 1;

		if (Calendar.SUNDAY != startCalendar.get(Calendar.DAY_OF_WEEK))
		{
			count--;
		}

		return count;
	}

	/**
	 * 生日转为年龄,计算法定年龄
	 * 
	 * @param birthDay 生日,标准日期字符串
	 * @return 年龄
	 * @throws Exception
	 */
	public static int ageOfNow(String birthDay)
	{
		return ageOfNow(parse(birthDay));
	}

	/**
	 * 生日转为年龄,计算法定年龄
	 * 
	 * @param birthDay 生日
	 * @return 年龄
	 * @throws Exception
	 */
	public static int ageOfNow(Date birthDay)
	{
		return age(birthDay, date());
	}

	/**
	 * 计算相对于dateToCompare的年龄,长用于计算指定生日在某年的年龄
	 * 
	 * @param birthDay 生日
	 * @param dateToCompare 需要对比的日期
	 * @return 年龄
	 * @throws Exception
	 */
	public static int age(Date birthDay, Date dateToCompare)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateToCompare);

		if (cal.before(birthDay))
		{
			throw new IllegalArgumentException("Birthday is after date {" + formatDate(dateToCompare) + "}!");
		}

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

		cal.setTime(birthDay);
		int age = year - cal.get(Calendar.YEAR);

		int monthBirth = cal.get(Calendar.MONTH);
		if (month == monthBirth)
		{
			int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
			if (dayOfMonth < dayOfMonthBirth)
			{
				// 如果生日在当月,但是未达到生日当天的日期,年龄减一
				age--;
			}
		}
		else if (month < monthBirth)
		{
			// 如果当前月份未达到生日的月份,年龄计算减一
			age--;
		}

		return age;
	}

	/**
	 * 获得指定日期年份和季节<br>
	 * 格式：[20131]表示2013年第一季度
	 * 
	 * @param cal 日期
	 */
	private static String yearAndSeason(Calendar cal)
	{
		return new StringBuilder().append(cal.get(Calendar.YEAR)).append(cal.get(Calendar.MONTH) / 3 + 1).toString();
	}

	public static Object getLastMilliSecond(Object date) throws SecurityException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime((Date) date);
		cal.add(5, 1);
		cal.add(14, -1);
		Constructor constructor = date.getClass().getConstructor(new Class[] { Long.TYPE });
		Object newDate = constructor.newInstance(new Object[] { Long.valueOf(cal.getTimeInMillis()) });
		return newDate;
	}
	
	/**
	 * 获得多少分钟以前（负数）或以后（正数）的时间,格式化成yyyy-MM-dd HH:mm:ss
	 * 
	 * @throws ParseException 
	 */
	public static Date getMinute(Date time, int minute)
	{
		// 日历对象
		Calendar calendar = Calendar.getInstance();
		// 设置当前日期
		calendar.setTime(time);
		// 为负减, 为正加
		calendar.add(Calendar.MINUTE, minute);
		return calendar.getTime();
	}
	
	/**
	 * 获得多少小时以前（负数）或以后（正数）的时间,格式化成yyyy-MM-dd HH:mm:ss
	 * 
	 * @throws ParseException 
	 */
	public static Date getHour(Date time, int hour)
	{
		// 日历对象
		Calendar calendar = Calendar.getInstance();
		// 设置当前日期
		calendar.setTime(time);
		// 为负减, 为正加
		calendar.add(Calendar.HOUR, hour);
		return calendar.getTime();
	}
	
	/**
	 * 获得多少天以前（负数）或以后（正数）的时间,格式化成yyyy-MM-dd HH:mm:ss
	 * 
	 * @throws ParseException 
	 */
	public static Date getDate(Date time, int date)
	{
		// 日历对象
		Calendar calendar = Calendar.getInstance();
		// 设置当前日期
		calendar.setTime(time);
		// 为负减, 为正加
		calendar.add(Calendar.DATE, date);
		return calendar.getTime();
	}
	
	/**
	 * 获得多少月以前（负数）或以后（正数）的时间,格式化成yyyy-MM-dd HH:mm:ss
	 * 
	 * @throws ParseException 
	 */
	public static Date getMonth(Date time, int month)
	{
		// 日历对象
		Calendar calendar = Calendar.getInstance();
		// 设置当前日期
		calendar.setTime(time);
		// 为负减, 为正加
		calendar.add(Calendar.MONTH, month);
		return calendar.getTime();
	}
	
	/**
	 * 获得多少年以前（负数）或以后（正数）的时间,格式化成yyyy-MM-dd HH:mm:ss
	 * 
	 * @throws ParseException 
	 */
	public static Date getYear(Date time, int year)
	{
		// 日历对象
		Calendar calendar = Calendar.getInstance();
		// 设置当前日期
		calendar.setTime(time);
		// 为负减, 为正加
		calendar.add(Calendar.YEAR, year);
		return calendar.getTime();
	}
	
	/**
	 * 根据传入的毫秒数获取到时间
	 */
	public static Date getDateByMills(String timeMills) 
	{
		if (timeMills != null && timeMills.length() > 0) 
		{
			// 日历对象
			Calendar cal = Calendar.getInstance();
			// 设置毫秒
			cal.setTimeInMillis(Long.valueOf(timeMills));
			return cal.getTime();
		}
		return null;
	}
	
	/**
	 * PIN码临时锁定 允许N分钟后登录
	 * @param lastTime
	 * @param now
	 * @return
	 */
	public static String getTimeDifference(Date lastTime, Date now)
	{
		long l = lastTime.getTime() - now.getTime();
		long day = l / (24 * 60 * 60 * 1000);
		long hour = (l / (60 * 60 * 1000) - day * 24);
		long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
		long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		return min + "分" + s + "秒";
	}
}

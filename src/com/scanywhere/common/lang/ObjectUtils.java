package com.scanywhere.common.lang;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.internal.CriteriaImpl.Subcriteria;
import org.springframework.beans.BeanUtils;

import com.scanywhere.common.io.IOUtils;

/**
 * 对象操作工具类, 继承org.apache.commons.lang3.ObjectUtils类
 */
public class ObjectUtils extends org.apache.commons.lang3.ObjectUtils
{
	/**
	 * 转换为Double类型
	 */
	public static Double toDouble(final Object val)
	{
		if (val == null)
		{
			return 0D;
		}
		try
		{
			return NumberUtils.toDouble(StringUtils.trim(val.toString()));
		}
		catch (Exception e)
		{
			return 0D;
		}
	}

	/**
	 * 转换为Float类型
	 */
	public static Float toFloat(final Object val)
	{
		return toDouble(val).floatValue();
	}

	/**
	 * 转换为Long类型
	 */
	public static Long toLong(final Object val)
	{
		return toDouble(val).longValue();
	}

	/**
	 * 转换为Integer类型
	 */
	public static Integer toInteger(final Object val)
	{
		return toLong(val).intValue();
	}

	/**
	 * 转换为Boolean类型 'true', 'on', 'y', 't', 'yes' or '1' (case insensitive) will
	 * return true. Otherwise, false is returned.
	 */
	public static Boolean toBoolean(final Object val)
	{
		if (val == null)
		{
			return false;
		}
		return BooleanUtils.toBoolean(val.toString()) || "1".equals(val.toString());
	}

	/**
	 * 转换为字符串
	 * 
	 * @param obj
	 * @return
	 */
	public static String toString(final Object obj)
	{
		return toString(obj, StringUtils.EMPTY);
	}

	/**
	 * 如果对象为空，则使用defaultVal值
	 * 
	 * @param obj
	 * @param defaultVal
	 * @return
	 */
	public static String toString(final Object obj, final String defaultVal)
	{
		return obj == null ? defaultVal : obj.toString();
	}

	/**
	 * 空转空字符串（"" to "" ; null to "" ; "null" to "" ; "NULL" to "" ; "Null" to
	 * ""）
	 * 
	 * @param val 需转换的值
	 * @return 返回转换后的值
	 */
	public static String toStringIgnoreNull(final Object val)
	{
		return ObjectUtils.toStringIgnoreNull(val, StringUtils.EMPTY);
	}

	/**
	 * 空对象转空字符串 （"" to defaultVal ; null to defaultVal ; "null" to defaultVal ;
	 * "NULL" to defaultVal ; "Null" to defaultVal）
	 * 
	 * @param val 需转换的值
	 * @param defaultVal 默认值
	 * @return 返回转换后的值
	 */
	public static String toStringIgnoreNull(final Object val, String defaultVal)
	{
		String str = ObjectUtils.toString(val);
		return !"".equals(str) && !"null".equals(str.trim().toLowerCase()) ? str : defaultVal;
	}

	/**
	 * 拷贝一个对象（但是子对象无法拷贝）
	 * 
	 * @param source
	 * @param ignoreProperties
	 */
	public static Object copyBean(Object source, String... ignoreProperties)
	{
		if (source == null)
		{
			return null;
		}
		Object target = BeanUtils.instantiate(source.getClass());
		BeanUtils.copyProperties(source, target, ignoreProperties);
		return target;
	}

	/**
	 * 注解到对象复制，只复制能匹配上的方法。 硕正组件用。
	 * 
	 * @param annotation
	 * @param object
	 */
	public static void annotationToObject(Object annotation, Object object)
	{
		if (annotation != null && object != null)
		{
			Class<?> annotationClass = annotation.getClass();
			Class<?> objectClass = object.getClass();
			for (Method m : objectClass.getMethods())
			{
				if (StringUtils.startsWith(m.getName(), "set"))
				{
					try
					{
						String s = StringUtils.uncapitalize(StringUtils.substring(m.getName(), 3));
						Object obj = annotationClass.getMethod(s).invoke(annotation);
						if (obj != null && !"".equals(obj.toString()))
						{
							// if (object == null){
							// object = objectClass.newInstance();
							// }
							m.invoke(object, obj);
						}
					}
					catch (Exception e)
					{
						// 忽略所有设置失败方法
					}
				}
			}
		}
	}

	/**
	 * 序列化对象
	 * 
	 * @param object
	 * @return
	 */
	public static byte[] serialize(Object object)
	{
		if (object == null)
		{
			return null;
		}
		byte[] bytes = null;
		ObjectOutputStream oos = null;
		ByteArrayOutputStream baos = null;
		try
		{
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			bytes = baos.toByteArray();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			IOUtils.closeQuietly(oos);
			IOUtils.closeQuietly(baos);
		}
		return bytes;
	}

	/**
	 * 反序列化对象
	 * 
	 * @param bytes
	 * @return
	 */
	public static Object unserialize(byte[] bytes)
	{
		if (bytes == null)
		{
			return null;
		}
		Object object = null;
		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;
		try
		{
			if (bytes.length > 0)
			{
				bais = new ByteArrayInputStream(bytes);
				ois = new ObjectInputStream(bais);
				object = ois.readObject();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			IOUtils.closeQuietly(ois);
			IOUtils.closeQuietly(bais);
		}
		return object;
	}

	/**
	 * 根据属性名获取getter setter方法名
	 *
	 * @param field 字段名
	 * @param prefix 前缀
	 * @return
	 */
	public static String methodName(String field, String prefix)
	{

		if (field == null)
			return null;
		if (field.length() > 1 && Character.isUpperCase(field.charAt(1)))
			return prefix + field;
		else
			return prefix + field.substring(0, 1).toUpperCase() + field.substring(1);
	}

	/**
	 * 根据属性名获取值
	 *
	 * @param obj 对象
	 * @param field 字段名
	 * @return
	 */
	public static Object getValueByKey(Object obj, String field)
	{

		try
		{
			Method method = null;
			if (obj == null || StringUtils.isBlank(field))
				return null;
			String[] fieldArr = field.split("[.]");
			for (String str : fieldArr)
			{
				method = obj.getClass().getMethod(methodName(str, "get"));
				obj = method.invoke(obj);
			}
			return obj;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
	 * 将对象object特定方法的返回值（
	 *
	 * @param obj 对象
	 * @param field 方法
	 * @param format 格式
	 * @return
	 */
	public static String ObjectToString(Object obj, String field, String format) throws Exception
	{

		try
		{
			Method method = null;
			if (obj == null || StringUtils.isBlank(field))
				return null;
			String[] fieldArr = field.split("[.]");
			for (String str : fieldArr)
			{
				if (method != null)
					obj = method.invoke(obj);
				method = obj.getClass().getMethod(methodName(str, "get"));
			}
			return ObjectToString(obj, method, format);

		}
		catch (Exception e)
		{
			return null;
		}

	}

	public static Object ObjectSetValue(Object obj, String field, Object value) throws Exception
	{

		try
		{
			Object retObject = obj;
			Method getMethod = null;
			Method setMethod = null;
			if (obj == null || StringUtils.isBlank(field))
				return null;
			String[] fieldArr = field.split("[.]");
			for (String str : fieldArr)
			{
				if (getMethod != null)
				{
					Object childObject = getMethod.getReturnType().newInstance();
					setMethod.invoke(obj, childObject);
					obj = childObject;
				}
				getMethod = obj.getClass().getMethod(methodName(str, "get"));
				setMethod = obj.getClass().getMethod(methodName(str, "set"), getMethod.getReturnType());
			}
			if (value == null || StringUtils.isEmpty(value.toString()))
				value = null;
			else
			{
				if (getMethod.getReturnType().equals(Integer.class))
				{
					value = Integer.valueOf(value.toString());
				}
				else if (getMethod.getReturnType().equals(Double.class))
				{
					value = Double.valueOf(value.toString());
				}
				else if (getMethod.getReturnType().equals(Float.class))
				{
					value = Float.valueOf(value.toString());
				}
				else if (getMethod.getReturnType().equals(Date.class))
				{
					value = DateUtils.parseDate(value);
				}
				setMethod.invoke(obj, value);
			}

			return retObject;
			// return (obj, method, format);

		}
		catch (Exception e)
		{
			throw new RuntimeException(e.getMessage());
		}

	}

	/**
	 * 将对象object特定方法的返回值（主要是get方法）按照format格式转化为字符串类型
	 *
	 * @param object 对象
	 * @param method 方法
	 * @param format 格式
	 * @return
	 */
	public static String ObjectToString(Object object, Method method, String format) throws Exception
	{

		if (object == null || method == null)
			return null;
		// 时间类型
		if (method.getReturnType().getName().equals(Date.class.getName()))
		{
			if (StringUtils.isEmpty(format))
				return DateUtils.formatDate((Date) method.invoke(object));
			else
				return DateUtils.format((Date) method.invoke(object), format);
		}

		return method.invoke(object).toString();
	}

	public static DetachedCriteria getCriteriaWithAlias(DetachedCriteria criteria, String columnName)
	{

		if (columnName.indexOf(".") == -1)
			return criteria;
		String[] nameArr = columnName.split("[.]");
		for (int index = 0; index < nameArr.length - 1; index++)
		{
			String str = nameArr[index];
			if (index > 0 && !isExistAlias((DetachedCriteria) criteria, "" + nameArr[index - 1] + "." + str + ""))
			{
				criteria.createAlias("" + nameArr[index - 1] + "." + str + "", "" + str + "",
						DetachedCriteria.LEFT_JOIN);
			}
			if (index == 0 && !isExistAlias((DetachedCriteria) criteria, str))
			{
				criteria.createAlias("" + str + "", "" + str + "", DetachedCriteria.LEFT_JOIN);
			}
		}
		return criteria;
	}

	public static boolean isExistAlias(DetachedCriteria impl, String path)
	{
		try
		{
			Field field = DetachedCriteria.class.getDeclaredField("impl");
			field.setAccessible(true);
			CriteriaImpl criteriaImpl = (CriteriaImpl) field.get(impl);
			Iterator iterator = criteriaImpl.iterateSubcriteria();
			for (; iterator.hasNext();)
			{
				Subcriteria subcriteria = (Subcriteria) iterator.next();
				if (subcriteria.getPath().equals(path))
				{
					return true;
				}
			}
			return false;
		}
		catch (Exception e)
		{
			return false;
		}
	}
}

package com.scanywhere.common.lang;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;

import com.scanywhere.common.codec.EncodeUtils;
import com.scanywhere.common.collect.ListUtils;
import com.scanywhere.common.support.StrFormatter;
import com.scanywhere.common.utils.CharsetUtils;
import com.scanywhere.common.utils.CollectionUtils;

/**
 * 字符串工具类, 继承org.apache.commons.lang3.StringUtils类
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils
{
	public static final char C_SPACE = ' ';

	public static final char C_TAB = '	';

	public static final char C_DOT = '.';

	public static final char C_SLASH = '/';

	public static final char C_BACKSLASH = '\\';

	public static final char C_CR = '\r';

	public static final char C_LF = '\n';

	public static final char C_UNDERLINE = '_';

	public static final char C_COMMA = ',';

	public static final char C_DELIM_START = '{';

	public static final char C_DELIM_END = '}';

	public static final String SPACE = " ";

	public static final String TAB = "	";

	public static final String DOT = ".";

	public static final String SLASH = "/";

	public static final String BACKSLASH = "\\";

	public static final String EMPTY = "";

	public static final String CR = "\r";

	public static final String LF = "\n";

	public static final String CRLF = "\r\n";

	public static final String UNDERLINE = "_";

	public static final String COMMA = ",";

	public static final String HTML_NBSP = "&nbsp;";

	public static final String HTML_AMP = "&amp";

	public static final String HTML_QUOTE = "&quot;";

	public static final String HTML_LT = "&lt;";

	public static final String HTML_GT = "&gt;";

	public static final String EMPTY_JSON = "{}";

	private static final char SEPARATOR = '_';

	private static final String CHARSET_NAME = "UTF-8";

	/**
	 * Determines whether the String is null or of length 0.
	 *
	 * @param string the string to check
	 * @return true if its null or length of 0, false otherwise.
	 */
	public static boolean isEmpty(final String string)
	{
		return string == null || string.length() == 0;
	}

	/**
	 * * Check that the given CharSequence is neither <code>null</code> nor of
	 * length 0. * Note: Will return <code>true</code> for a CharSequence that
	 * purely consists of whitespace. *
	 * <p>
	 * 
	 * <pre>
	 *      * StringUtils.hasLength(null) = false     * StringUtils.hasLength(&quot;&quot;) = false     * StringUtils.hasLength(&quot; &quot;) = true     * StringUtils.hasLength(&quot;Hello&quot;) = true     *
	 * </pre>
	 * 
	 * *
	 * 
	 * @param str the CharSequence to check (may be <code>null</code>) *
	 * @return <code>true</code> if the CharSequence is not null and has length
	 *         *
	 * @see #hasText(String)
	 */
	public static boolean hasLength(CharSequence str)
	{
		return (str != null && str.length() > 0);
	}

	/**
	 * * Check that the given String is neither <code>null</code> nor of length
	 * 0. * Note: Will return <code>true</code> for a String that purely
	 * consists of whitespace. *
	 * 
	 * @param str the String to check (may be <code>null</code>) *
	 * @return <code>true</code> if the String is not null and has length *
	 * @see #hasLength(CharSequence)
	 */
	public static boolean hasLength(String str)
	{
		return hasLength((CharSequence) str);
	}

	/**
	 * * Check whether the given CharSequence has actual text. * More
	 * specifically, returns <code>true</code> if the string not
	 * <code>null</code>, * its length is greater than 0, and it contains at
	 * least one non-whitespace character. *
	 * <p>
	 * 
	 * <pre>
	 *      * StringUtils.hasText(null) = false     * StringUtils.hasText(&quot;&quot;) = false     * StringUtils.hasText(&quot; &quot;) = false     * StringUtils.hasText(&quot;12345&quot;) = true     * StringUtils.hasText(&quot; 12345 &quot;) = true     *
	 * </pre>
	 * 
	 * *
	 * 
	 * @param str the CharSequence to check (may be <code>null</code>) *
	 * @return <code>true</code> if the CharSequence is not <code>null</code>, *
	 *         its length is greater than 0, and it does not contain whitespace
	 *         only *
	 * @see java.lang.Character#isWhitespace
	 */
	public static boolean hasText(CharSequence str)
	{
		if (!hasLength(str))
		{
			return false;
		}
		int strLen = str.length();
		for (int i = 0; i < strLen; i++)
		{
			if (!Character.isWhitespace(str.charAt(i)))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * * Check whether the given String has actual text. * More specifically,
	 * returns <code>true</code> if the string not <code>null</code>, * its
	 * length is greater than 0, and it contains at least one non-whitespace
	 * character. *
	 * 
	 * @param str the String to check (may be <code>null</code>) *
	 * @return <code>true</code> if the String is not <code>null</code>, its
	 *         length is * greater than 0, and it does not contain whitespace
	 *         only *
	 * @see #hasText(CharSequence)
	 */
	public static boolean hasText(String str)
	{
		return hasText((CharSequence) str);
	}

	/**
	 * * Count the occurrences of the substring in string s. *
	 * 
	 * @param str string to search in. Return 0 if this is null. *
	 * @param sub string to search for. Return 0 if this is null.
	 */
	public static int countOccurrencesOf(String str, String sub)
	{
		if (str == null || sub == null || str.length() == 0 || sub.length() == 0)
		{
			return 0;
		}
		int count = 0;
		int pos = 0;
		int idx;
		while ((idx = str.indexOf(sub, pos)) != -1)
		{
			++count;
			pos = idx + sub.length();
		}
		return count;
	}

	/**
	 * * Tokenize the given String into a String array via a StringTokenizer. *
	 * Trims tokens and omits empty tokens. *
	 * <p>
	 * The given delimiters string is supposed to consist of any number of *
	 * delimiter characters. Each of those characters can be used to separate *
	 * tokens. A delimiter is always a single character; for multi-character *
	 * delimiters, consider using <code>delimitedListToStringArray</code> *
	 * 
	 * @param str the String to tokenize *
	 * @param delimiters the delimiter characters, assembled as String * (each
	 *            of those characters is individually considered as delimiter).
	 *            *
	 * @return an array of the tokens *
	 * @see java.util.StringTokenizer *
	 * @see java.lang.String#trim() *
	 * @see #delimitedListToStringArray
	 */
	public static String[] tokenizeToStringArray(String str, String delimiters)
	{
		return tokenizeToStringArray(str, delimiters, true, true);
	}

	/**
	 * * Tokenize the given String into a String array via a StringTokenizer. *
	 * <p>
	 * The given delimiters string is supposed to consist of any number of *
	 * delimiter characters. Each of those characters can be used to separate *
	 * tokens. A delimiter is always a single character; for multi-character *
	 * delimiters, consider using <code>delimitedListToStringArray</code> *
	 * 
	 * @param str the String to tokenize *
	 * @param delimiters the delimiter characters, assembled as String * (each
	 *            of those characters is individually considered as delimiter) *
	 * @param trimTokens trim the tokens via String's <code>trim</code> *
	 * @param ignoreEmptyTokens omit empty tokens from the result array * (only
	 *            applies to tokens that are empty after trimming;
	 *            StringTokenizer * will not consider subsequent delimiters as
	 *            token in the first place). *
	 * @return an array of the tokens (<code>null</code> if the input String *
	 *         was <code>null</code>) *
	 * @see java.util.StringTokenizer *
	 * @see java.lang.String#trim() *
	 * @see #delimitedListToStringArray
	 */
	public static String[] tokenizeToStringArray(String str, String delimiters, boolean trimTokens,
			boolean ignoreEmptyTokens)
	{
		if (str == null)
		{
			return null;
		}
		StringTokenizer st = new StringTokenizer(str, delimiters);
		List<String> tokens = new ArrayList<String>();
		while (st.hasMoreTokens())
		{
			String token = st.nextToken();
			if (trimTokens)
			{
				token = token.trim();
			}
			if (!ignoreEmptyTokens || token.length() > 0)
			{
				tokens.add(token);
			}
		}
		return toStringArray(tokens);
	}

	/**
	 * * Copy the given Collection into a String array. * The Collection must
	 * contain String elements only. *
	 * 
	 * @param collection the Collection to copy *
	 * @return the String array (<code>null</code> if the passed-in * Collection
	 *         was <code>null</code>)
	 */
	public static String[] toStringArray(Collection<String> collection)
	{
		if (collection == null)
		{
			return null;
		}
		return collection.toArray(new String[collection.size()]);
	}

	/**
	 * 转换为字节数组
	 * 
	 * @param str
	 * @return
	 */
	public static byte[] getBytes(String str)
	{
		if (str != null)
		{
			try
			{
				return str.getBytes(CHARSET_NAME);
			}
			catch (UnsupportedEncodingException e)
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	/**
	 * 转换为字节数组
	 * 
	 * @param str
	 * @return
	 */
	public static String toString(byte[] bytes)
	{
		try
		{
			return new String(bytes, CHARSET_NAME);
		}
		catch (UnsupportedEncodingException e)
		{
			return EMPTY;
		}
	}

	/**
	 * 是否包含字符串
	 * 
	 * @param str 验证字符串
	 * @param strs 字符串组
	 * @return 包含返回true
	 */
	public static boolean inString(String str, String... strs)
	{
		if (str != null && strs != null)
		{
			for (String s : strs)
			{
				if (str.equals(trim(s)))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 是否包含字符串
	 * 
	 * @param str 验证字符串
	 * @param strs 字符串组
	 * @return 包含返回true
	 */
	public static boolean inStringIgnoreCase(String str, String... strs)
	{
		if (str != null && strs != null)
		{
			for (String s : strs)
			{
				if (str.equalsIgnoreCase(trim(s)))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 替换掉HTML标签方法
	 */
	public static String stripHtml(String html)
	{
		if (isBlank(html))
		{
			return "";
		}
		// html.replaceAll("\\&[a-zA-Z]{0,9};", "").replaceAll("<[^>]*>", "");
		String regEx = "<.+?>";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(html);
		String s = m.replaceAll("");
		return s;
	}

	/**
	 * 替换为手机识别的HTML，去掉样式及属性，保留回车。
	 * 
	 * @param html
	 * @return
	 */
	public static String toMobileHtml(String html)
	{
		if (html == null)
		{
			return "";
		}
		return html.replaceAll("<([a-z]+?)\\s+?.*?>", "<$1>");
	}

	/**
	 * 对txt进行HTML编码，并将\n转换为&gt;br/&lt;、\t转换为&nbsp; &nbsp;
	 * 
	 * @param txt
	 * @return
	 */
	public static String toHtml(String txt)
	{
		if (txt == null)
		{
			return "";
		}
		return replace(replace(EncodeUtils.encodeHtml(trim(txt)), "\n", "<br/>"), "\t", "&nbsp; &nbsp; ");
	}

	/**
	 * 缩略字符串（不区分中英文字符）
	 * 
	 * @param str 目标字符串
	 * @param length 截取长度
	 * @return
	 */
	public static String abbr(String str, int length)
	{
		if (str == null)
		{
			return "";
		}
		try
		{
			StringBuilder sb = new StringBuilder();
			int currentLength = 0;
			for (char c : stripHtml(StringEscapeUtils.unescapeHtml4(str)).toCharArray())
			{
				currentLength += String.valueOf(c).getBytes("GBK").length;
				if (currentLength <= length - 3)
				{
					sb.append(c);
				}
				else
				{
					sb.append("...");
					break;
				}
			}
			return sb.toString();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		return "";
	}

	// 缩略字符串替换Html正则表达式预编译
	private static Pattern p1 = Pattern.compile("<([a-zA-Z]+)[^<>]*>");

	/**
	 * 缩略字符串（适应于与HTML标签的）
	 * 
	 * @param param 目标字符串
	 * @param length 截取长度
	 * @return
	 */
	public static String htmlAbbr(String param, int length)
	{
		if (param == null)
		{
			return "";
		}
		StringBuffer result = new StringBuffer();
		int n = 0;
		char temp;
		boolean isCode = false; // 是不是HTML代码
		boolean isHTML = false; // 是不是HTML特殊字符,如&nbsp;
		for (int i = 0; i < param.length(); i++)
		{
			temp = param.charAt(i);
			if (temp == '<')
			{
				isCode = true;
			}
			else if (temp == '&')
			{
				isHTML = true;
			}
			else if (temp == '>' && isCode)
			{
				n = n - 1;
				isCode = false;
			}
			else if (temp == ';' && isHTML)
			{
				isHTML = false;
			}
			try
			{
				if (!isCode && !isHTML)
				{
					n += String.valueOf(temp).getBytes("GBK").length;
				}
			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}

			if (n <= length - 3)
			{
				result.append(temp);
			}
			else
			{
				result.append("...");
				break;
			}
		}
		// 取出截取字符串中的HTML标记
		String tempResult = result.toString().replaceAll("(>)[^<>]*(<?)", "$1$2");
		// 去掉不需要结素标记的HTML标记
		tempResult = tempResult.replaceAll("</?(AREA|BASE|BASEFONT|BODY|BR|COL|COLGROUP|DD|DT|FRAME|HEAD|HR|"
				+ "HTML|IMG|INPUT|ISINDEX|LI|LINK|META|OPTION|P|PARAM|TBODY|TD|TFOOT|TH|THEAD|TR|area|base|"
				+ "basefont|body|br|col|colgroup|dd|dt|frame|head|hr|html|img|input|isindex|li|link|meta|"
				+ "option|p|param|tbody|td|tfoot|th|thead|tr)[^<>]*/?>", "");
		// 去掉成对的HTML标记
		tempResult = tempResult.replaceAll("<([a-zA-Z]+)[^<>]*>(.*?)</\\1>", "$2");
		// 用正则表达式取出标记
		Matcher m = p1.matcher(tempResult);
		List<String> endHTML = ListUtils.newArrayList();
		while (m.find())
		{
			endHTML.add(m.group(1));
		}
		// 补全不成对的HTML标记
		for (int i = endHTML.size() - 1; i >= 0; i--)
		{
			result.append("</");
			result.append(endHTML.get(i));
			result.append(">");
		}
		return result.toString();
	}

	/**
	 * 首字母大写
	 */
	public static String cap(String str)
	{
		return capitalize(str);
	}

	/**
	 * 首字母小写
	 */
	public static String uncap(String str)
	{
		return uncapitalize(str);
	}

	/**
	 * 驼峰命名法工具
	 * 
	 * @return camelCase("hello_world") == "helloWorld"
	 *         capCamelCase("hello_world") == "HelloWorld"
	 *         uncamelCase("helloWorld") = "hello_world"
	 */
	public static String camelCase(String s)
	{
		if (s == null)
		{
			return null;
		}

		s = s.toLowerCase();

		StringBuilder sb = new StringBuilder(s.length());
		boolean upperCase = false;
		for (int i = 0; i < s.length(); i++)
		{
			char c = s.charAt(i);

			if (c == SEPARATOR)
			{
				upperCase = true;
			}
			else if (upperCase)
			{
				sb.append(Character.toUpperCase(c));
				upperCase = false;
			}
			else
			{
				sb.append(c);
			}
		}

		return sb.toString();
	}

	/**
	 * 驼峰命名法工具
	 * 
	 * @return camelCase("hello_world") == "helloWorld"
	 *         capCamelCase("hello_world") == "HelloWorld"
	 *         uncamelCase("helloWorld") = "hello_world"
	 */
	public static String capCamelCase(String s)
	{
		if (s == null)
		{
			return null;
		}
		s = camelCase(s);
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	/**
	 * 驼峰命名法工具
	 * 
	 * @return camelCase("hello_world") == "helloWorld"
	 *         capCamelCase("hello_world") == "HelloWorld"
	 *         uncamelCase("helloWorld") = "hello_world"
	 */
	public static String uncamelCase(String s)
	{
		if (s == null)
		{
			return null;
		}

		StringBuilder sb = new StringBuilder();
		boolean upperCase = false;
		for (int i = 0; i < s.length(); i++)
		{
			char c = s.charAt(i);

			boolean nextUpperCase = true;

			if (i < (s.length() - 1))
			{
				nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
			}

			if ((i > 0) && Character.isUpperCase(c))
			{
				if (!upperCase || !nextUpperCase)
				{
					sb.append(SEPARATOR);
				}
				upperCase = true;
			}
			else
			{
				upperCase = false;
			}

			sb.append(Character.toLowerCase(c));
		}

		return sb.toString();
	}

	/**
	 * 转换为JS获取对象值，生成三目运算返回结果
	 * 
	 * @param objectString 对象串 例如：row.user.id
	 *            返回：!row?'':!row.user?'':!row.user.id?'':row.user.id
	 */
	public static String jsGetVal(String objectString)
	{
		StringBuilder result = new StringBuilder();
		StringBuilder val = new StringBuilder();
		String[] vals = split(objectString, ".");
		for (int i = 0; i < vals.length; i++)
		{
			val.append("." + vals[i]);
			result.append("!" + (val.substring(1)) + "?'':");
		}
		result.append(val.substring(1));
		return result.toString();
	}

	/**
	 * 获取随机字符串
	 * 
	 * @param count
	 * @return
	 */
	public static String getRandomStr(int count)
	{
		char[] codeSeq = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
				'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		Random random = new Random();
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < count; i++)
		{
			String r = String.valueOf(codeSeq[random.nextInt(codeSeq.length)]);
			s.append(r);
		}
		return s.toString();
	}

	/**
	 * 获取随机数字
	 * 
	 * @param count
	 * @return
	 */
	public static String getRandomNum(int count)
	{
		char[] codeSeq = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		Random random = new Random();
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < count; i++)
		{
			String r = String.valueOf(codeSeq[random.nextInt(codeSeq.length)]);
			s.append(r);
		}
		return s.toString();
	}

	/**
	 * 字符串是否为空白 空白的定义如下： <br>
	 * 1、为null <br>
	 * 2、为不可见字符（如空格）<br>
	 * 3、""<br>
	 * 
	 * @param str 被检测的字符串
	 * @return 是否为空
	 */
	public static boolean isBlank(String str)
	{
		int length;

		if ((str == null) || ((length = str.length()) == 0))
		{
			return true;
		}

		for (int i = 0; i < length; i++)
		{
			// 只要有一个非空字符即为非空字符串
			if (false == Character.isWhitespace(str.charAt(i)))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * 字符串是否为非空白 空白的定义如下： <br>
	 * 1、不为null <br>
	 * 2、不为不可见字符（如空格）<br>
	 * 3、不为""<br>
	 * 
	 * @param str 被检测的字符串
	 * @return 是否为非空
	 */
	public static boolean notBlank(String str)
	{
		return false == isBlank(str);
	}

	/**
	 * 是否包含空字符串
	 * 
	 * @param strs 字符串列表
	 * @return 是否包含空字符串
	 */
	public static boolean hasBlank(String... strs)
	{
		if (CollectionUtils.isEmpty(strs))
		{
			return true;
		}

		for (String str : strs)
		{
			if (isBlank(str))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * 给定所有字符串是否为空白
	 * 
	 * @param strs 字符串
	 * @return 所有字符串是否为空白
	 */
	public static boolean isAllBlank(String... strs)
	{
		if (CollectionUtils.isEmpty(strs))
		{
			return true;
		}

		for (String str : strs)
		{
			if (notBlank(str))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * 字符串是否为非空白 空白的定义如下： <br>
	 * 1、不为null <br>
	 * 2、不为""<br>
	 * 
	 * @param str 被检测的字符串
	 * @return 是否为非空
	 */
	public static boolean isNotEmpty(String str)
	{
		return false == isEmpty(str);
	}

	/**
	 * 当给定字符串为null时，转换为Empty
	 * 
	 * @param str 被转换的字符串
	 * @return 转换后的字符串
	 */
	public static String nullToEmpty(String str)
	{
		return nullToDefault(str, EMPTY);
	}

	/**
	 * 如果字符串是<code>null</code>，则返回指定默认字符串，否则返回字符串本身。
	 * 
	 * <pre>
	 * nullToDefault(null, &quot;default&quot;)  = &quot;default&quot;
	 * nullToDefault(&quot;&quot;, &quot;default&quot;)    = &quot;&quot;
	 * nullToDefault(&quot;  &quot;, &quot;default&quot;)  = &quot;  &quot;
	 * nullToDefault(&quot;bat&quot;, &quot;default&quot;) = &quot;bat&quot;
	 * </pre>
	 * 
	 * @param str 要转换的字符串
	 * @param defaultStr 默认字符串
	 * 
	 * @return 字符串本身或指定的默认字符串
	 */
	public static String nullToDefault(String str, String defaultStr)
	{
		return (str == null) ? defaultStr : str;
	}

	/**
	 * 当给定字符串为空字符串时，转换为<code>null</code>
	 * 
	 * @param str 被转换的字符串
	 * @return 转换后的字符串
	 */
	public static String emptyToNull(String str)
	{
		return isEmpty(str) ? null : str;
	}

	/**
	 * 是否包含空字符串
	 * 
	 * @param strs 字符串列表
	 * @return 是否包含空字符串
	 */
	public static boolean hasEmpty(String... strs)
	{
		if (CollectionUtils.isEmpty(strs))
		{
			return true;
		}

		for (String str : strs)
		{
			if (isEmpty(str))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * 是否全部为空字符串
	 * 
	 * @param strs 字符串列表
	 * @return 是否全部为空字符串
	 */
	public static boolean isAllEmpty(String... strs)
	{
		if (CollectionUtils.isEmpty(strs))
		{
			return true;
		}

		for (String str : strs)
		{
			if (isNotEmpty(str))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * 除去字符串头尾部的空白，如果字符串是<code>null</code>，依然返回<code>null</code>。
	 * 
	 * <p>
	 * 注意，和<code>String.trim</code>不同，此方法使用<code>Character.isWhitespace</code>
	 * 来判定空白， 因而可以除去英文字符集之外的其它空白，如中文空格。
	 * 
	 * <pre>
	 * trim(null)          = null
	 * trim(&quot;&quot;)            = &quot;&quot;
	 * trim(&quot;     &quot;)       = &quot;&quot;
	 * trim(&quot;abc&quot;)         = &quot;abc&quot;
	 * trim(&quot;    abc    &quot;) = &quot;abc&quot;
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param str 要处理的字符串
	 * 
	 * @return 除去空白的字符串，如果原字串为<code>null</code>，则返回<code>null</code>
	 */
	public static String trim(String str)
	{
		return (null == str) ? null : trim(str, 0);
	}

	/**
	 * 给定字符串数组全部做去首尾空格
	 * 
	 * @param strs 字符串数组
	 */
	public static void trim(String[] strs)
	{
		if (null == strs)
		{
			return;
		}
		String str;
		for (int i = 0; i < strs.length; i++)
		{
			str = strs[i];
			if (null != str)
			{
				strs[i] = str.trim();
			}
		}
	}

	/**
	 * 除去字符串头部的空白，如果字符串是<code>null</code>，则返回<code>null</code>。
	 * 
	 * <p>
	 * 注意，和<code>String.trim</code>不同，此方法使用<code>Character.isWhitespace</code>
	 * 来判定空白， 因而可以除去英文字符集之外的其它空白，如中文空格。
	 * 
	 * <pre>
	 * trimStart(null)         = null
	 * trimStart(&quot;&quot;)           = &quot;&quot;
	 * trimStart(&quot;abc&quot;)        = &quot;abc&quot;
	 * trimStart(&quot;  abc&quot;)      = &quot;abc&quot;
	 * trimStart(&quot;abc  &quot;)      = &quot;abc  &quot;
	 * trimStart(&quot; abc &quot;)      = &quot;abc &quot;
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param str 要处理的字符串
	 * 
	 * @return 除去空白的字符串，如果原字串为<code>null</code>或结果字符串为<code>""</code>，则返回
	 *         <code>null</code>
	 */
	public static String trimStart(String str)
	{
		return trim(str, -1);
	}

	/**
	 * 除去字符串尾部的空白，如果字符串是<code>null</code>，则返回<code>null</code>。
	 * 
	 * <p>
	 * 注意，和<code>String.trim</code>不同，此方法使用<code>Character.isWhitespace</code>
	 * 来判定空白， 因而可以除去英文字符集之外的其它空白，如中文空格。
	 * 
	 * <pre>
	 * trimEnd(null)       = null
	 * trimEnd(&quot;&quot;)         = &quot;&quot;
	 * trimEnd(&quot;abc&quot;)      = &quot;abc&quot;
	 * trimEnd(&quot;  abc&quot;)    = &quot;  abc&quot;
	 * trimEnd(&quot;abc  &quot;)    = &quot;abc&quot;
	 * trimEnd(&quot; abc &quot;)    = &quot; abc&quot;
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param str 要处理的字符串
	 * 
	 * @return 除去空白的字符串，如果原字串为<code>null</code>或结果字符串为<code>""</code>，则返回
	 *         <code>null</code>
	 */
	public static String trimEnd(String str)
	{
		return trim(str, 1);
	}

	/**
	 * 除去字符串头尾部的空白符，如果字符串是<code>null</code>，依然返回<code>null</code>。
	 * 
	 * @param str 要处理的字符串
	 * @param mode <code>-1</code>表示trimStart，<code>0</code>表示trim全部，
	 *            <code>1</code>表示trimEnd
	 * 
	 * @return 除去指定字符后的的字符串，如果原字串为<code>null</code>，则返回<code>null</code>
	 */
	public static String trim(String str, int mode)
	{
		if (str == null)
		{
			return null;
		}

		int length = str.length();
		int start = 0;
		int end = length;

		// 扫描字符串头部
		if (mode <= 0)
		{
			while ((start < end) && (Character.isWhitespace(str.charAt(start))))
			{
				start++;
			}
		}

		// 扫描字符串尾部
		if (mode >= 0)
		{
			while ((start < end) && (Character.isWhitespace(str.charAt(end - 1))))
			{
				end--;
			}
		}

		if ((start > 0) || (end < length))
		{
			return str.substring(start, end);
		}

		return str;
	}

	/**
	 * 是否以指定字符串开头
	 * 
	 * @param str 被监测字符串
	 * @param prefix 开头字符串
	 * @param isIgnoreCase 是否忽略大小写
	 * @return 是否以指定字符串开头
	 */
	public static boolean startWith(String str, String prefix, boolean isIgnoreCase)
	{
		if (isIgnoreCase)
		{
			return str.toLowerCase().startsWith(prefix.toLowerCase());
		}
		else
		{
			return str.startsWith(prefix);
		}
	}

	/**
	 * 是否以指定字符串结尾
	 * 
	 * @param str 被监测字符串
	 * @param suffix 结尾字符串
	 * @param isIgnoreCase 是否忽略大小写
	 * @return 是否以指定字符串结尾
	 */
	public static boolean endWith(String str, String suffix, boolean isIgnoreCase)
	{
		if (isIgnoreCase)
		{
			return str.toLowerCase().endsWith(suffix.toLowerCase());
		}
		else
		{
			return str.endsWith(suffix);
		}
	}

	/**
	 * 是否包含特定字符，忽略大小写，如果给定两个参数都为<code>null</code>，返回true
	 * 
	 * @param str 被检测字符串
	 * @param testStr 被测试是否包含的字符串
	 * @return 是否包含
	 */
	public static boolean containsIgnoreCase(String str, String testStr)
	{
		if (null == str)
		{
			// 如果被监测字符串和
			return null == testStr;
		}
		return str.toLowerCase().contains(testStr.toLowerCase());
	}

	/**
	 * 获得set或get方法对应的标准属性名<br/>
	 * 例如：setName 返回 name
	 * 
	 * @param getOrSetMethodName
	 * @return 如果是set或get方法名，返回field， 否则null
	 */
	public static String getGeneralField(String getOrSetMethodName)
	{
		if (getOrSetMethodName.startsWith("get") || getOrSetMethodName.startsWith("set"))
		{
			return removePreAndLowerFirst(getOrSetMethodName, 3);
		}
		return null;
	}

	/**
	 * 生成set方法名<br/>
	 * 例如：name 返回 setName
	 * 
	 * @param fieldName 属性名
	 * @return setXxx
	 */
	public static String genSetter(String fieldName)
	{
		return upperFirstAndAddPre(fieldName, "set");
	}

	/**
	 * 生成get方法名
	 * 
	 * @param fieldName 属性名
	 * @return getXxx
	 */
	public static String genGetter(String fieldName)
	{
		return upperFirstAndAddPre(fieldName, "get");
	}

	/**
	 * 去掉首部指定长度的字符串并将剩余字符串首字母小写<br/>
	 * 例如：str=setName, preLength=3 -> return name
	 * 
	 * @param str 被处理的字符串
	 * @param preLength 去掉的长度
	 * @return 处理后的字符串，不符合规范返回null
	 */
	public static String removePreAndLowerFirst(String str, int preLength)
	{
		if (str == null)
		{
			return null;
		}
		if (str.length() > preLength)
		{
			char first = Character.toLowerCase(str.charAt(preLength));
			if (str.length() > preLength + 1)
			{
				return first + str.substring(preLength + 1);
			}
			return String.valueOf(first);
		}
		else
		{
			return str;
		}
	}

	/**
	 * 去掉首部指定长度的字符串并将剩余字符串首字母小写<br/>
	 * 例如：str=setName, prefix=set -> return name
	 * 
	 * @param str 被处理的字符串
	 * @param prefix 前缀
	 * @return 处理后的字符串，不符合规范返回null
	 */
	public static String removePreAndLowerFirst(String str, String prefix)
	{
		return lowerFirst(removePrefix(str, prefix));
	}

	/**
	 * 原字符串首字母大写并在其首部添加指定字符串 例如：str=name, preString=get -> return getName
	 * 
	 * @param str 被处理的字符串
	 * @param preString 添加的首部
	 * @return 处理后的字符串
	 */
	public static String upperFirstAndAddPre(String str, String preString)
	{
		if (str == null || preString == null)
		{
			return null;
		}
		return preString + upperFirst(str);
	}

	/**
	 * 大写首字母<br>
	 * 例如：str = name, return Name
	 * 
	 * @param str 字符串
	 * @return 字符串
	 */
	public static String upperFirst(String str)
	{
		if (isBlank(str))
		{
			return str;
		}
		return Character.toUpperCase(str.charAt(0)) + subSuf(str, 1);
	}

	/**
	 * 小写首字母<br>
	 * 例如：str = Name, return name
	 * 
	 * @param str 字符串
	 * @return 字符串
	 */
	public static String lowerFirst(String str)
	{
		if (isBlank(str))
		{
			return str;
		}
		return Character.toLowerCase(str.charAt(0)) + subSuf(str, 1);
	}

	/**
	 * 去掉指定前缀
	 * 
	 * @param str 字符串
	 * @param prefix 前缀
	 * @return 切掉后的字符串，若前缀不是 preffix， 返回原字符串
	 */
	public static String removePrefix(String str, String prefix)
	{
		if (isEmpty(str) || isEmpty(prefix))
		{
			return str;
		}

		if (str.startsWith(prefix))
		{
			return subSuf(str, prefix.length());// 截取后半段
		}
		return str;
	}

	/**
	 * 忽略大小写去掉指定前缀
	 * 
	 * @param str 字符串
	 * @param prefix 前缀
	 * @return 切掉后的字符串，若前缀不是 prefix， 返回原字符串
	 */
	public static String removePrefixIgnoreCase(String str, String prefix)
	{
		if (isEmpty(str) || isEmpty(prefix))
		{
			return str;
		}

		if (str.toLowerCase().startsWith(prefix.toLowerCase()))
		{
			return subSuf(str, prefix.length());// 截取后半段
		}
		return str;
	}

	/**
	 * 去掉指定后缀
	 * 
	 * @param str 字符串
	 * @param suffix 后缀
	 * @return 切掉后的字符串，若后缀不是 suffix， 返回原字符串
	 */
	public static String removeSuffix(String str, String suffix)
	{
		if (isEmpty(str) || isEmpty(suffix))
		{
			return str;
		}

		if (str.endsWith(suffix))
		{
			return subPre(str, str.length() - suffix.length());// 截取前半段
		}
		return str;
	}

	/**
	 * 去掉指定后缀，并小写首字母
	 * 
	 * @param str 字符串
	 * @param suffix 后缀
	 * @return 切掉后的字符串，若后缀不是 suffix， 返回原字符串
	 */
	public static String removeSufAndLowerFirst(String str, String suffix)
	{
		return lowerFirst(removeSuffix(str, suffix));
	}

	/**
	 * 忽略大小写去掉指定后缀
	 * 
	 * @param str 字符串
	 * @param suffix 后缀
	 * @return 切掉后的字符串，若后缀不是 suffix， 返回原字符串
	 */
	public static String removeSuffixIgnoreCase(String str, String suffix)
	{
		if (isEmpty(str) || isEmpty(suffix))
		{
			return str;
		}

		if (str.toLowerCase().endsWith(suffix.toLowerCase()))
		{
			return subPre(str, str.length() - suffix.length());
		}
		return str;
	}

	/**
	 * 如果给定字符串不是以prefix开头的，在开头补充 prefix
	 * 
	 * @param str 字符串
	 * @param prefix 前缀
	 * @return 补充后的字符串
	 */
	public static String addPrefixIfNot(String str, String prefix)
	{
		if (isEmpty(str) || isEmpty(prefix))
		{
			return str;
		}
		if (false == str.startsWith(prefix))
		{
			str = prefix + str;
		}
		return str;
	}

	/**
	 * 如果给定字符串不是以suffix结尾的，在尾部补充 suffix
	 * 
	 * @param str 字符串
	 * @param suffix 后缀
	 * @return 补充后的字符串
	 */
	public static String addSuffixIfNot(String str, String suffix)
	{
		if (isEmpty(str) || isEmpty(suffix))
		{
			return str;
		}
		if (false == str.endsWith(suffix))
		{
			str += suffix;
		}
		return str;
	}

	/**
	 * 清理空白字符
	 * 
	 * @param str 被清理的字符串
	 * @return 清理后的字符串
	 */
	public static String cleanBlank(String str)
	{
		if (str == null)
		{
			return null;
		}

		return str.replaceAll("\\s*", EMPTY);
	}

	/**
	 * 切分字符串<br>
	 * a#b#c -> [a,b,c] <br>
	 * a##b#c -> [a,"",b,c]
	 * 
	 * @param str 被切分的字符串
	 * @param separator 分隔符字符
	 * @return 切分后的集合
	 */
	/*
	 * public static List<String> split(String str, char separator) { return
	 * split(str, separator, 0); }
	 */

	/**
	 * 切分字符串
	 * 
	 * @param str 被切分的字符串
	 * @param separator 分隔符字符
	 * @param limit 限制分片数
	 * @return 切分后的集合
	 */
	/*
	 * public static List<String> split(String str, char separator, int limit) {
	 * if (str == null) { return null; } List<String> list = new
	 * ArrayList<String>(limit == 0 ? 16 : limit); if (limit == 1) {
	 * list.add(str); return list; }
	 * 
	 * boolean isNotEnd = true; // 未结束切分的标志 int strLen = str.length();
	 * StringBuilder sb = new StringBuilder(strLen); for (int i = 0; i < strLen;
	 * i++) { char c = str.charAt(i); if (isNotEnd && c == separator) {
	 * list.add(sb.toString()); // 清空StringBuilder sb.delete(0, sb.length());
	 * 
	 * // 当达到切分上限-1的量时，将所剩字符全部作为最后一个串 if (limit != 0 && list.size() == limit -
	 * 1) { isNotEnd = false; } } else { sb.append(c); } }
	 * list.add(sb.toString());// 加入尾串 return list; }
	 */

	/**
	 * 切分字符串<br>
	 * from jodd
	 * 
	 * @param str 被切分的字符串
	 * @param delimiter 分隔符
	 * @return 字符串
	 */
	public static String[] split(String str, String delimiter)
	{
		if (str == null)
		{
			return null;
		}
		if (str.trim().length() == 0)
		{
			return new String[] { str };
		}

		int dellen = delimiter.length(); // del length
		int maxparts = (str.length() / dellen) + 2; // one more for the last
		int[] positions = new int[maxparts];

		int i, j = 0;
		int count = 0;
		positions[0] = -dellen;
		while ((i = str.indexOf(delimiter, j)) != -1)
		{
			count++;
			positions[count] = i;
			j = i + dellen;
		}
		count++;
		positions[count] = str.length();

		String[] result = new String[count];

		for (i = 0; i < count; i++)
		{
			result[i] = str.substring(positions[i] + dellen, positions[i + 1]);
		}
		return result;
	}

	/**
	 * 改进JDK subString<br>
	 * index从0开始计算，最后一个字符为-1<br>
	 * 如果from和to位置一样，返回 "" <br>
	 * 如果from或to为负数，则按照length从后向前数位置，如果绝对值大于字符串长度，则from归到0，to归到length<br>
	 * 如果经过修正的index中from大于to，则互换from和to example: <br>
	 * abcdefgh 2 3 -> c <br>
	 * abcdefgh 2 -3 -> cde <br>
	 * 
	 * @param string String
	 * @param fromIndex 开始的index（包括）
	 * @param toIndex 结束的index（不包括）
	 * @return 字串
	 */
	public static String sub(String string, int fromIndex, int toIndex)
	{
		int len = string.length();

		if (fromIndex < 0)
		{
			fromIndex = len + fromIndex;
			if (fromIndex < 0)
			{
				fromIndex = 0;
			}
		}
		else if (fromIndex > len)
		{
			fromIndex = len;
		}

		if (toIndex < 0)
		{
			toIndex = len + toIndex;
			if (toIndex < 0)
			{
				toIndex = len;
			}
		}
		else if (toIndex > len)
		{
			toIndex = len;
		}

		if (toIndex < fromIndex)
		{
			int tmp = fromIndex;
			fromIndex = toIndex;
			toIndex = tmp;
		}

		if (fromIndex == toIndex)
		{
			return EMPTY;
		}

		char[] strArray = string.toCharArray();
		char[] newStrArray = Arrays.copyOfRange(strArray, fromIndex, toIndex);
		return new String(newStrArray);
	}

	/**
	 * 切割前部分
	 * 
	 * @param string 字符串
	 * @param toIndex 切割到的位置（不包括）
	 * @return 切割后的字符串
	 */
	public static String subPre(String string, int toIndex)
	{
		return sub(string, 0, toIndex);
	}

	/**
	 * 切割后部分
	 * 
	 * @param string 字符串
	 * @param fromIndex 切割开始的位置（包括）
	 * @return 切割后的字符串
	 */
	public static String subSuf(String string, int fromIndex)
	{
		if (isEmpty(string))
		{
			return null;
		}
		return sub(string, fromIndex, string.length());
	}

	/**
	 * 给定字符串是否被字符包围
	 * 
	 * @param str 字符串
	 * @param prefix 前缀
	 * @param suffix 后缀
	 * @return 是否包围，空串不包围
	 */
	public static boolean isSurround(String str, String prefix, String suffix)
	{
		if (isBlank(str))
		{
			return false;
		}
		if (str.length() < (prefix.length() + suffix.length()))
		{
			return false;
		}

		return str.startsWith(prefix) && str.endsWith(suffix);
	}

	/**
	 * 给定字符串是否被字符包围
	 * 
	 * @param str 字符串
	 * @param prefix 前缀
	 * @param suffix 后缀
	 * @return 是否包围，空串不包围
	 */
	public static boolean isSurround(String str, char prefix, char suffix)
	{
		if (isBlank(str))
		{
			return false;
		}
		if (str.length() < 2)
		{
			return false;
		}

		return str.charAt(0) == prefix && str.charAt(str.length() - 1) == suffix;
	}

	/**
	 * 重复某个字符
	 * 
	 * @param c 被重复的字符
	 * @param count 重复的数目
	 * @return 重复字符字符串
	 */
	public static String repeat(char c, int count)
	{
		char[] result = new char[count];
		for (int i = 0; i < count; i++)
		{
			result[i] = c;
		}
		return new String(result);
	}

	/**
	 * 重复某个字符串
	 * 
	 * @param str 被重复的字符
	 * @param count 重复的数目
	 * @return 重复字符字符串
	 */
	public static String repeat(String str, int count)
	{

		// 检查
		final int len = str.length();
		final long longSize = (long) len * (long) count;
		final int size = (int) longSize;
		if (size != longSize)
		{
			throw new ArrayIndexOutOfBoundsException("Required String length is too large: " + longSize);
		}

		final char[] array = new char[size];
		str.getChars(0, len, array, 0);
		int n;
		for (n = len; n < size - n; n <<= 1)
		{// n <<= 1相当于n *2
			System.arraycopy(array, 0, array, n, n);
		}
		System.arraycopy(array, 0, array, n, size - n);
		return new String(array);
	}

	/**
	 * 比较两个字符串（大小写敏感）。
	 * 
	 * <pre>
	 * equals(null, null)   = true
	 * equals(null, &quot;abc&quot;)  = false
	 * equals(&quot;abc&quot;, null)  = false
	 * equals(&quot;abc&quot;, &quot;abc&quot;) = true
	 * equals(&quot;abc&quot;, &quot;ABC&quot;) = false
	 * </pre>
	 * 
	 * @param str1 要比较的字符串1
	 * @param str2 要比较的字符串2
	 * 
	 * @return 如果两个字符串相同，或者都是<code>null</code>，则返回<code>true</code>
	 */
	public static boolean equals(String str1, String str2)
	{
		if (str1 == null)
		{
			return str2 == null;
		}

		return str1.equals(str2);
	}

	/**
	 * 比较两个字符串（大小写不敏感）。
	 * 
	 * <pre>
	 * equalsIgnoreCase(null, null)   = true
	 * equalsIgnoreCase(null, &quot;abc&quot;)  = false
	 * equalsIgnoreCase(&quot;abc&quot;, null)  = false
	 * equalsIgnoreCase(&quot;abc&quot;, &quot;abc&quot;) = true
	 * equalsIgnoreCase(&quot;abc&quot;, &quot;ABC&quot;) = true
	 * </pre>
	 * 
	 * @param str1 要比较的字符串1
	 * @param str2 要比较的字符串2
	 * 
	 * @return 如果两个字符串相同，或者都是<code>null</code>，则返回<code>true</code>
	 */
	public static boolean equalsIgnoreCase(String str1, String str2)
	{
		if (str1 == null)
		{
			return str2 == null;
		}

		return str1.equalsIgnoreCase(str2);
	}

	/**
	 * 格式化文本, {} 表示占位符<br>
	 * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
	 * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
	 * 例：<br>
	 * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
	 * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
	 * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
	 * 
	 * @param template 文本模板，被替换的部分用 {} 表示
	 * @param params 参数值
	 * @return 格式化后的文本
	 */
	public static String format(String template, Object... params)
	{
		if (CollectionUtils.isEmpty(params) || isBlank(template))
		{
			return template;
		}
		return StrFormatter.format(template, params);
	}

	/**
	 * 格式化文本，使用 {varName} 占位<br>
	 * map = {a: "aValue", b: "bValue"} format("{a} and {b}", map) ----> aValue
	 * and bValue
	 * 
	 * @param template 文本模板，被替换的部分用 {key} 表示
	 * @param map 参数值对
	 * @return 格式化后的文本
	 */
	public static String format(String template, Map<?, ?> map)
	{
		if (null == map || map.isEmpty())
		{
			return template;
		}

		for (Entry<?, ?> entry : map.entrySet())
		{
			template = template.replace("{" + entry.getKey() + "}", utf8Str(entry.getValue()));
		}
		return template;
	}

	/**
	 * 编码字符串
	 * 
	 * @param str 字符串
	 * @param charset 字符集，如果此字段为空，则解码的结果取决于平台
	 * @return 编码后的字节码
	 */
	public static byte[] bytes(String str, String charset)
	{
		return bytes(str, isBlank(charset) ? Charset.defaultCharset() : Charset.forName(charset));
	}

	/**
	 * 编码字符串
	 * 
	 * @param str 字符串
	 * @param charset 字符集，如果此字段为空，则解码的结果取决于平台
	 * @return 编码后的字节码
	 */
	public static byte[] bytes(String str, Charset charset)
	{
		if (str == null)
		{
			return null;
		}

		if (null == charset)
		{
			return str.getBytes();
		}
		return str.getBytes(charset);
	}

	/**
	 * 将对象转为字符串<br>
	 * 1、Byte数组和ByteBuffer会被转换为对应字符串的数组 2、对象数组会调用Arrays.toString方法
	 * 
	 * @param obj 对象
	 * @return 字符串
	 */
	public static String utf8Str(Object obj)
	{
		return str(obj, CharsetUtils.CHARSET_UTF_8);
	}

	/**
	 * 将对象转为字符串<br>
	 * 1、Byte数组和ByteBuffer会被转换为对应字符串的数组 2、对象数组会调用Arrays.toString方法
	 * 
	 * @param obj 对象
	 * @param charsetName 字符集
	 * @return 字符串
	 */
	public static String str(Object obj, String charsetName)
	{
		return str(obj, Charset.forName(charsetName));
	}

	/**
	 * 将对象转为字符串<br>
	 * 1、Byte数组和ByteBuffer会被转换为对应字符串的数组 2、对象数组会调用Arrays.toString方法
	 * 
	 * @param obj 对象
	 * @param charset 字符集
	 * @return 字符串
	 */
	public static String str(Object obj, Charset charset)
	{
		if (null == obj)
		{
			return null;
		}

		if (obj instanceof String)
		{
			return (String) obj;
		}
		else if (obj instanceof byte[] || obj instanceof Byte[])
		{
			return str((Byte[]) obj, charset);
		}
		else if (obj instanceof ByteBuffer)
		{
			return str((ByteBuffer) obj, charset);
		}
		else if (CollectionUtils.isArray(obj))
		{
			return CollectionUtils.toString(obj);
		}

		return obj.toString();
	}

	/**
	 * 将byte数组转为字符串
	 * 
	 * @param bytes byte数组
	 * @param charset 字符集
	 * @return 字符串
	 */
	public static String str(byte[] bytes, String charset)
	{
		return str(bytes, isBlank(charset) ? Charset.defaultCharset() : Charset.forName(charset));
	}

	/**
	 * 解码字节码
	 * 
	 * @param data 字符串
	 * @param charset 字符集，如果此字段为空，则解码的结果取决于平台
	 * @return 解码后的字符串
	 */
	public static String str(byte[] data, Charset charset)
	{
		if (data == null)
		{
			return null;
		}

		if (null == charset)
		{
			return new String(data);
		}
		return new String(data, charset);
	}

	/**
	 * 将编码的byteBuffer数据转换为字符串
	 * 
	 * @param data 数据
	 * @param charset 字符集，如果为空使用当前系统字符集
	 * @return 字符串
	 */
	public static String str(ByteBuffer data, String charset)
	{
		if (data == null)
		{
			return null;
		}

		return str(data, Charset.forName(charset));
	}

	/**
	 * 将编码的byteBuffer数据转换为字符串
	 * 
	 * @param data 数据
	 * @param charset 字符集，如果为空使用当前系统字符集
	 * @return 字符串
	 */
	public static String str(ByteBuffer data, Charset charset)
	{
		if (null == charset)
		{
			charset = Charset.defaultCharset();
		}
		return charset.decode(data).toString();
	}

	/**
	 * 字符串转换为byteBuffer
	 * 
	 * @param str 字符串
	 * @param charset 编码
	 * @return byteBuffer
	 */
	public static ByteBuffer byteBuffer(String str, String charset)
	{
		return ByteBuffer.wrap(bytes(str, charset));
	}

	/**
	 * 以 conjunction 为分隔符将多个对象转换为字符串
	 * 
	 * @param conjunction 分隔符
	 * @param objs 数组
	 * @return 连接后的字符串
	 */
	public static String join(String conjunction, Object... objs)
	{
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Object item : objs)
		{
			if (isFirst)
			{
				isFirst = false;
			}
			else
			{
				sb.append(conjunction);
			}
			sb.append(item);
		}
		return sb.toString();
	}

	/**
	 * 将驼峰式命名的字符串转换为下划线方式。如果转换前的驼峰式命名的字符串为空，则返回空字符串。</br>
	 * 例如：HelloWorld->hello_world
	 *
	 * @param camelCaseStr 转换前的驼峰式命名的字符串
	 * @return 转换后下划线大写方式命名的字符串
	 */
	public static String toUnderlineCase(String camelCaseStr)
	{
		if (camelCaseStr == null)
		{
			return null;
		}

		final int length = camelCaseStr.length();
		StringBuilder sb = new StringBuilder();
		char c;
		boolean isPreUpperCase = false;
		for (int i = 0; i < length; i++)
		{
			c = camelCaseStr.charAt(i);
			boolean isNextUpperCase = true;
			if (i < (length - 1))
			{
				isNextUpperCase = Character.isUpperCase(camelCaseStr.charAt(i + 1));
			}
			if (Character.isUpperCase(c))
			{
				if (!isPreUpperCase || !isNextUpperCase)
				{
					if (i > 0)
						sb.append(UNDERLINE);
				}
				isPreUpperCase = true;
			}
			else
			{
				isPreUpperCase = false;
			}
			sb.append(Character.toLowerCase(c));
		}
		return sb.toString();
	}

	/**
	 * 将下划线方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。</br>
	 * 例如：hello_world->HelloWorld
	 *
	 * @param name 转换前的下划线大写方式命名的字符串
	 * @return 转换后的驼峰式命名的字符串
	 */
	public static String toCamelCase(String name)
	{
		if (name == null)
		{
			return null;
		}
		if (name.contains(UNDERLINE))
		{
			name = name.toLowerCase();

			StringBuilder sb = new StringBuilder(name.length());
			boolean upperCase = false;
			for (int i = 0; i < name.length(); i++)
			{
				char c = name.charAt(i);

				if (c == '_')
				{
					upperCase = true;
				}
				else if (upperCase)
				{
					sb.append(Character.toUpperCase(c));
					upperCase = false;
				}
				else
				{
					sb.append(c);
				}
			}
			return sb.toString();
		}
		else
			return name;
	}

	/**
	 * 包装指定字符串
	 * 
	 * @param str 被包装的字符串
	 * @param prefix 前缀
	 * @param suffix 后缀
	 * @return 包装后的字符串
	 */
	public static String wrap(String str, String prefix, String suffix)
	{
		return format("{}{}{}", prefix, str, suffix);
	}

	/**
	 * 指定字符串是否被包装
	 * 
	 * @param str 字符串
	 * @param prefix 前缀
	 * @param suffix 后缀
	 * @return 是否被包装
	 */
	public static boolean isWrap(String str, String prefix, String suffix)
	{
		return str.startsWith(prefix) && str.endsWith(suffix);
	}

	/**
	 * 指定字符串是否被同一字符包装（前后都有这些字符串）
	 * 
	 * @param str 字符串
	 * @param wrapper 包装字符串
	 * @return 是否被包装
	 */
	public static boolean isWrap(String str, String wrapper)
	{
		return isWrap(str, wrapper, wrapper);
	}

	/**
	 * 指定字符串是否被同一字符包装（前后都有这些字符串）
	 * 
	 * @param str 字符串
	 * @param wrapper 包装字符
	 * @return 是否被包装
	 */
	public static boolean isWrap(String str, char wrapper)
	{
		return isWrap(str, wrapper, wrapper);
	}

	/**
	 * 指定字符串是否被包装
	 * 
	 * @param str 字符串
	 * @param prefixChar 前缀
	 * @param suffixChar 后缀
	 * @return 是否被包装
	 */
	public static boolean isWrap(String str, char prefixChar, char suffixChar)
	{
		return str.charAt(0) == prefixChar && str.charAt(str.length() - 1) == suffixChar;
	}

	/**
	 * 补充字符串以满足最小长度 StrKit.padPre("1", 3, '0');//"001"
	 * 
	 * @param str 字符串
	 * @param minLength 最小长度
	 * @param padChar 补充的字符
	 * @return 补充后的字符串
	 */
	public static String padPre(String str, int minLength, char padChar)
	{
		if (str.length() >= minLength)
		{
			return str;
		}
		StringBuilder sb = new StringBuilder(minLength);
		for (int i = str.length(); i < minLength; i++)
		{
			sb.append(padChar);
		}
		sb.append(str);
		return sb.toString();
	}

	/**
	 * 补充字符串以满足最小长度 StrKit.padEnd("1", 3, '0');//"100"
	 * 
	 * @param str 字符串
	 * @param minLength 最小长度
	 * @param padChar 补充的字符
	 * @return 补充后的字符串
	 */
	public static String padEnd(String str, int minLength, char padChar)
	{
		if (str.length() >= minLength)
		{
			return str;
		}
		StringBuilder sb = new StringBuilder(minLength);
		sb.append(str);
		for (int i = str.length(); i < minLength; i++)
		{
			sb.append(padChar);
		}
		return sb.toString();
	}

	/**
	 * 创建StringBuilder对象
	 * 
	 * @return StringBuilder对象
	 */
	public static StringBuilder builder()
	{
		return new StringBuilder();
	}

	/**
	 * 创建StringBuilder对象
	 * 
	 * @return StringBuilder对象
	 */
	public static StringBuilder builder(int capacity)
	{
		return new StringBuilder(capacity);
	}

	/**
	 * 创建StringBuilder对象
	 * 
	 * @return StringBuilder对象
	 */
	public static StringBuilder builder(String... strs)
	{
		final StringBuilder sb = new StringBuilder();
		for (String str : strs)
		{
			sb.append(str);
		}
		return sb;
	}

	/**
	 * 获得StringReader
	 * 
	 * @param str 字符串
	 * @return StringReader
	 */
	public static StringReader getReader(String str)
	{
		return new StringReader(str);
	}

	/**
	 * 获得StringWriter
	 * 
	 * @return StringWriter
	 */
	public static StringWriter getWriter()
	{
		return new StringWriter();
	}

	/**
	 * 统计指定内容中包含指定字符串的数量
	 * 
	 * @param content 内容
	 * @param strForSearch 被统计的字符串
	 * @return 包含数量
	 */
	public static int count(String content, String strForSearch)
	{
		int contentLength = content.length();
		if (null == content || null == strForSearch || strForSearch.length() > contentLength)
		{
			return 0;
		}

		return contentLength - content.replace(strForSearch, EMPTY).length();
	}

	/**
	 * 统计指定内容中包含指定字符的数量
	 * 
	 * @param content 内容
	 * @param charForSearch 被统计的字符
	 * @return 包含数量
	 */
	public static int count(String content, char charForSearch)
	{
		int count = 0;
		int contentLength = content.length();
		for (int i = 0; i < contentLength; i++)
		{
			if (charForSearch == content.charAt(i))
			{
				count++;
			}
		}
		return count;
	}

	/**
	 * 获得字符串对应byte数组
	 * 
	 * @param str 字符串
	 * @param charset 编码，如果为<code>null</code>使用系统默认编码
	 * @return bytes
	 */
	public static byte[] getBytes(String str, Charset charset)
	{
		if (null == str)
		{
			return null;
		}
		return null == charset ? str.getBytes() : str.getBytes(charset);
	}
}

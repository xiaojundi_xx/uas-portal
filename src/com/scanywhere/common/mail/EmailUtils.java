package com.scanywhere.common.mail;

import org.apache.commons.mail.HtmlEmail;

/**
 * 发送电子邮件
 */
public class EmailUtils
{
	/**
	 * 发送邮件
	 * 
	 * @param toAddress 接收地址
	 * @param subject 标题
	 * @param content 内容
	 * @return
	 */
	public static boolean sendEmail(String fromAddress, String fromPassword, String fromHostName, String sslOnConnect,
			String sslSmtpPort, String toAddress, String subject, String content)
	{
		try
		{
			HtmlEmail htmlEmail = new HtmlEmail();
			// 发送地址
			htmlEmail.setFrom(fromAddress);
			// 密码校验
			htmlEmail.setAuthentication(fromAddress, fromPassword);
			// 发送服务器协议
			htmlEmail.setHostName(fromHostName);

			// SSL
			if ("true".equals(sslOnConnect))
			{
				htmlEmail.setSSLOnConnect(true);
				htmlEmail.setSslSmtpPort(sslSmtpPort);
			}

			// 接收地址
			htmlEmail.addTo(toAddress);

			// 标题
			htmlEmail.setSubject(subject);
			// 内容
			htmlEmail.setMsg(content);

			// 其他信息
			htmlEmail.setCharset("utf-8");

			// 发送
			htmlEmail.send();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
}
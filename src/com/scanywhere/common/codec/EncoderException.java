package com.scanywhere.common.codec;

@SuppressWarnings("serial")
public class EncoderException extends Exception
{
	/**
	 * Constructs an exception with the supplied message.
	 *
	 * @param string the message
	 */
	public EncoderException(final String string)
	{
		super(string);
	}

	/**
	 * Constructs an exception with the supplied message and chained throwable.
	 *
	 * @param string the message
	 * @param throwable the original exception
	 */
	public EncoderException(final String string, final Throwable throwable)
	{
		super(string, throwable);
	}

	/**
	 * Constructs an exception with the chained throwable.
	 * 
	 * @param throwable the original exception.
	 */
	public EncoderException(final Throwable throwable)
	{
		super(throwable);
	}
}

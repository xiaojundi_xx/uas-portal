package com.scanywhere.common.codec;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;

public class Norm3DESUtils
{
	private String plainText;

	private String cipherText;

	/**
	 * 用指定的密钥加密数据.
	 * 
	 * @param plainText 加密数据
	 * @param secretKey 加密密钥
	 * @param iv IV
	 * 
	 * @return
	 */
	public boolean encrypt(String plainText, String secretKey, String iv) throws EncoderException
	{
		try
		{
			TripleDES des = new TripleDES();
			byte[] encrypted = des.encrypt(plainText.getBytes("GBK"), secretKey.getBytes(), iv.getBytes());
			this.cipherText =  Base64.encodeBase64String(encrypted);
			if (this.cipherText != null && this.cipherText.trim().length() > 0)
			{
				Pattern p = Pattern.compile("\\s*|\t|\r|\n");
				Matcher m = p.matcher(this.cipherText);
				this.cipherText = m.replaceAll("");
			}
		}
		catch (InvalidKeyException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (InvalidKeySpecException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (NoSuchPaddingException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (InvalidAlgorithmParameterException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (IllegalBlockSizeException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (BadPaddingException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		return true;
	}

	/**
	 * 用指定的密钥解密数据.
	 * 
	 * @param cipherText 密文数据
	 * @param secretKey 解密密钥
	 * @param iv IV
	 * 
	 * @return
	 */
	public boolean decrypt(String cipherText, String secretKey, String iv) throws EncoderException
	{
		try
		{
			byte[] data = Base64.decodeBase64(cipherText);
			TripleDES des = new TripleDES();
			byte[] decrypted = des.decrypt(data, secretKey.getBytes(), iv.getBytes());

			this.plainText = new String(decrypted, "GBK");
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (InvalidKeyException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (InvalidKeySpecException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (NoSuchPaddingException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (InvalidAlgorithmParameterException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (IllegalBlockSizeException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		catch (BadPaddingException e)
		{
			e.printStackTrace();
			throw new EncoderException(e.getMessage());
		}
		return true;
	}

	/**
	 * 取出解密后的数据
	 * 
	 * @return 数据原文
	 */
	public String getPlainText()
	{
		return this.plainText;
	}

	/**
	 * 取出加密后的数据
	 * 
	 * @return 数据密文
	 */
	public String getCipherText()
	{
		return this.cipherText;
	}
}

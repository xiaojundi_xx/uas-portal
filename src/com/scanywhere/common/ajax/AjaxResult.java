package com.scanywhere.common.ajax;

public class AjaxResult
{
	// 返回状态码 (默认0:成功, 1:失败)
	private int code = 0;

	// 返回的中文消息
	private String message;

	// 成功时携带的数据
	private Object data;

	public int getCode()
	{
		return code;
	}

	public AjaxResult setCode(int code)
	{
		this.code = code;
		return this;
	}

	public String getMessage()
	{
		return message;
	}

	public AjaxResult setMessage(String message)
	{
		this.message = message;
		return this;
	}

	public Object getData()
	{
		return data;
	}

	public AjaxResult setData(Object data)
	{
		this.data = data;
		return this;
	}

	public AjaxResult success()
	{
		this.code = 0;
		this.message = null;
		this.data = null;
		return this;
	}
	
	public AjaxResult success(String message)
	{
		this.code = 0;
		this.message = message;
		this.data = null;
		return this;
	}
	
	public AjaxResult success(Object data)
	{
		this.code = 0;
		this.message = "操作成功!";
		this.data = data;
		return this;
	}
	
	public AjaxResult success(String message, Object data)
	{
		this.code = 0;
		this.message = message;
		this.data = data;
		return this;
	}
	
	public AjaxResult error()
	{
		this.code = 1;
		this.message = "操作失败!";
		this.data = null;
		return this;
	}
	
	public AjaxResult error(String message)
	{
		this.code = 1;
		this.message = message;
		this.data = null;
		return this;
	}

	public AjaxResult error(Object data)
	{
		this.code = 1;
		this.message = "操作失败!";
		this.data = data;
		return this;
	}
	
	public AjaxResult error(String message, Object data)
	{
		this.code = 1;
		this.message = message;
		this.data = data;
		return this;
	}
	
}

package com.scanywhere.common.json;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class JSONUtils
{
	public static String toJson(Object object)
	{
		return JSONObject.toJSONStringWithDateFormat(object, "yyyy-MM-dd HH:mm:ss",
				SerializerFeature.WriteDateUseDateFormat);
	}

	public static JSONObject parse(String text)
	{
		return JSONObject.parseObject(text);
	}

	public static <T> T parse(String text, Class<T> clazz)
	{
		return JSONObject.parseObject(text, clazz);
	}

	public static void ajaxJson(String jsonString, HttpServletResponse response)
	{
		ajax(jsonString, "application/json", response);
	}

	public static void ajax(String content, String type, HttpServletResponse response)
	{
		try
		{
			response.setContentType(type + ";charset=UTF-8");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.getWriter().write(content);
			response.getWriter().flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

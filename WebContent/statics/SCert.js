//encoding:utf-8

function OnBase64Encode(form) {
  var Sign = GetControl();
  try {
    form.children.result.value = Sign.Base64Encode(form.children.msg.value);
  } catch (e) {
    alert(e);
  }
  return false;
}

function OnBase64Decode(form) {
  var Sign = GetControl();
  try {
    form.children.result.value = Sign.Base64Decode(form.children.msg.value);
  } catch (e) {
    alert(e);
  }
  return false;
}
//----------------------------------------------------------------------------
function ComponetCheck(onResult) {
  var iTimerID = null;

  function LookupResult(nRet) {
    if (iTimerID) {
      window.clearTimeout(iTimerID);
      iTimerID = null;
    }
    if (typeof onResult == "function") return onResult(nRet);

    if (nRet == 0) {
      alert("USB KEY尚未插入,请插入USB KEY");
      return true;
    } else if (nRet == -1) {
      alert("USB KEY驱动尚未按安装,需要重新安装");
      return false;
    } else if (nRet == -2) {
      alert("你尚未安装插件(add-on.crx),请安装插件后使用");
    } else if (nRet == -3) {
      alert("浏览器扩展组件尚未安装");
    }
  }

  iTimerID = window.setTimeout(function() {
    LookupResult(-2);
  }, 2000);

  var Sign = GetControl();
  try {
    Sign.Lookup("", LookupResult);
  } catch (e) {
    LookupResult(-3);
  }
}

function OnHashData(form) {
  function DigestResult(msg) {
    form.children.hashed.value = msg;
  }

  var Sign = GetControl();
  try {
    Sign.Digest(
      Sign.Base64Encode(form.children.msg.value),
      form.children.alg.value,
      DigestResult
    );
  } catch (e) {
    if (window.console && console.log) console.log(e);
    alert(e);
  }
  return false;
}

function OnFindCertificates(form) {
  function FindResult(certs) {
    certs = certs.split(";");
    var list = document.getElementById("certs");

    while (list.length) list.remove(0);

    for (var i = 0; i < certs.length; i++) {
      list.add(new Option(certs[i], certs[i]));
    }
  }

  var Sign = GetControl();
  try {
    Sign.FindCertificates(
      Sign.Base64Encode(form.children.msg.value),
      true,
      FindResult
    );
  } catch (e) {
    alert(e);
  }
  return false;
}

var cert;

function OnSelectCertificate(form) {
  function SelectResult(c) {
    form.children.result.value = c;
    cert = c;
  }

  var Sign = GetControl();
  try {
    Sign.SelectCertificate(
      Sign.Base64Encode(form.children.msg.value),
      true,
      SelectResult
    );
  } catch (e) {
    alert(e);
  }
  return false;
}

function OnViewCertificate() {
  function viewResult() {
    if (window.console && console.log) console.log("viewResult");
  }
  var Sign = GetControl();
  Sign.ViewCertificate(cert, viewResult);
}

function OnExportCertificate(form) {
  var info = document.getElementById("info");

  function ExportResult(crt) {
    info.value = crt;
  }
  var Sign = GetControl();
  try {
    Sign.ExportCertificate(cert, ExportResult);
  } catch (e) {
    alert(e);
  }
  return false;
}

function OnGetInfo(form) {
  var i = 0;
  var info = document.getElementById("info");

  function GetInfoResult(msg) {
    info.value = info.value + i + ": " + Sign.Base64Decode(msg) + "\n";
  }

  var Sign = GetControl();
  try {
    info.value = "";
    for (i = 0; i < 9; i++) {
      Sign.GetCertInfo(cert, i, GetInfoResult);
    }
  } catch (e) {
    alert(e);
  }
  return false;
}

function OnFreeCertificate(form) {
  function FreeResult() {
    if (window.console && console.log) console.log("FreeResult");
    cert = 0;
    alert("证书已释放");
  }
  var Sign = GetControl();
  Sign.FreeCertificate(cert, FreeResult);

  return false;
}

function OnSignData(form) {
  function SignResult(sig) {
    form.children.result.value = sig;
  }
  var Sign = GetControl();
  try {
    Sign.SignData(
      cert,
      form.children.alg.value,
      Sign.Base64Encode(form.children.msg.value),
      SignResult
    );
  } catch (e) {
    alert(e);
  }
  return false;
}

function OnVerifySignature(form) {
  function VerifyResult(result) {
    form.children.result.value = result;
  }
  var Sign = GetControl();
  try {
    Sign.VerifySignature(
      cert,
      form.children.alg.value,
      Sign.Base64Encode(form.children.msg.value),
      form.children.sig.value,
      VerifyResult
    );
  } catch (e) {
    alert(e);
  }
  return false;
}

function OnPkcs7Sign(form) {
  function SignResult(sig) {
    form.children.result.value = sig;
  }
  var Sign = GetControl();
  try {
    Sign.PKCS7Sign(
      cert,
      Sign.Base64Encode(form.children.msg.value),
      form.children.alg.value,
      SignResult
    );
  } catch (e) {
    alert(e);
  }
  return false;
}

function OnPkcs7Verify(form) {
  var Sign = GetControl();

  try {
    var flags = form.children.alg.value;
    var sCert = flags & 0x2 ? cert : "";
    var sMsg = flags & 0x1 ? Sign.Base64Encode(form.children.msg.value) : "";

    var result = false;

    function VerifyResult(result) {
      form.children.result.value = result;
      if (result) {
        if (flags & 1) {
          form.children.msg.value = sMsg;
        }
        if (flags & 0x2) {
          form.children.sCert.value = sCert;
        }
      }
    }

    Sign.PKCS7Verify(
      form.children.pkcs7.value,
      sMsg,
      sCert,
      flags,
      VerifyResult
    );
  } catch (e) {
    alert(e);
  }
  return false;
}

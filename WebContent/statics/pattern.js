/*纯数字*/
function isNum(str){
	return /^\d+$/.test(str);
}
/*纯字母*/
function isLet(str){
	return /^[A-Za-z]+$/.test(str);
}
/*数字和字母*/
function isNum_Let(str){
	if (!/^[0-9A-Za-z]+$/.test(str))  return false;
	return /[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/.test(str);
}
/*数字和特殊字符(!@#$-)*/
function isNum_Char(str){
	if(/^(?=.*\d)(?=.*[!@#$-])[\d!@#$-]{4,}$/.exec(str) == null) return false;
	return true;
}
/*字母和特殊字符(!@#$-)*/
function isLet_Char(str){
	if(/^(?=.*[a-zA-Z])(?=.*[!@#$-])[a-zA-Z!@#$-]{4,}$/.exec(str) == null) return false;
	return true;
}
/*数字、字母和特殊字符(!@#$-)*/
function isNum_Let_Char(str){
	if(/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$-])[\da-zA-Z!@#$-]{4,}$/.exec(str) == null) return false;
	return true;
}
/*包含数字*/
function containNum(str){
	return /\d+/.test(str);
}
/*包含字母*/
function containLet(str){
	return /[a-zA-Z]+/.test(str);
}

/*检查格式*/
function checkPattern(str,id,minLength,maxLength,pwdComplexity){
	// 判断长度
	if(str.length>0 && str.length<minLength) {
		$("#"+id).html("密码不能小于" + minLength + "个字符, 请检查");
		return false;
	}

	if(str.length > maxLength) {
		$("#"+id).html("密码不能大于" + maxLength + "个字符, 请检查");
		return false;
	}

	if(pwdComplexity == 1) {
		// 至少包含数字
		if(!containNum(str)){
			$("#"+id).html("密码必须包含数字, 请检查");
			return false;
		}
		if(!isNum(str) && !isNum_Let(str) && !isNum_Char(str) && !isNum_Let_Char(str)){
			if(!isNum_Let(str))
				$("#"+id).html("密码含有非法字符, 请检查");
			return false;
		}
	}
	else if (pwdComplexity == 2) {	
		// 至少包含字母和数字
		if(!containLet(str) || !containNum(str)){
			$("#"+id).html("密码必须包含数字和字母, 请检查");
			return false;
		}
		if(!isNum_Let(str) && !isNum_Let_Char(str)){
			$("#"+id).html("密码含有非法字符, 请检查");
			return false;
		}
	}
	else if(pwdComplexity == 3) {	
		// 至少包含数字、字母和特殊符号(!@#$-)
		if(!isNum_Let_Char(str)){
			$("#"+id).html("密码必须包含数字、字母和特殊符号, 请检查");
			return false;
		}
	}
	return true;
}

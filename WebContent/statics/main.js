// default options for bootstrapValidator

//true default	When a field has multiple validators, all of them will be checked respectively. If errors occur in multiple validators, all of them will be displayed to the user
//false	when a field has multiple validators, validation for this field will be terminated upon the first encountered error. Thus, only the very first error message related to this field will be displayed to the user
$.fn.bootstrapValidator.DEFAULT_OPTIONS.verbose = true;

//enabled default	The plugin validates fields as soon as they are changed
//disabled	Disable the live validating. The error messages are only shown after the form is submitted
//submitted	The live validating is enabled after the form is submitted
$.fn.bootstrapValidator.DEFAULT_OPTIONS.live = "enabled";

// default options for BootstrapDialog.alert
$.portal = {}
$.portal.alertOptions = {
  closable: true,
  title: "提示信息",
  onshown: function(dlg) {
    clearInterval(dlg.getData('hideInterval'));
    var i = dlg.getData('ttl');
    dlg.getModalBody().html(
      '<h4>' + dlg.getData('message') + "</h4><h4>" + i + "秒后自动关闭</h4>"
    );
    dlg.setData('ttl', i - 1);

    dlg.setData('hideInterval', setInterval(function() {
      i = dlg.getData('ttl');
      if (i == 0) {
        clearInterval(dlg.getData('hideInterval'));
        dlg.close();
      } else {
        dlg.getModalBody().html(
          '<h4>' + dlg.getData('message') + "</h4><h4>" + i + "秒后自动关闭</h4>"
        );
        dlg.setData('ttl', i - 1);
      }
    }, 1000));
  }
}

function ShowAlert(message, ttl) {
  if (ttl == null || ttl == undefined)
    ttl = 4;
  var alertOptions = $.extend(true, $.portal.alertOptions, {
    message: '<h4>' + message + '</h4>',
    data: {
      message: message,
      ttl: ttl
    }
  });
  BootstrapDialog.alert(alertOptions);
}

$(document).ready(function() {
  $("#dialog").dialog({
    autoOpen: false
  });
});

function open_home() {
  window.location.reload(true);
}

function open_mainboard() {
  $("#context").load("ctx/main.html");
}

function open_accountmgr() {
  $("#context").load("ctx/account.html");
}

function open_userinfo() {
  $("#context").load("ctx/userinfo.html", function() {
    load_userinfo();
  });
}

function open_changepwd() {
  $("#context").load("ctx/changepassword.html");
}

function open_faq() {
  if ($("#context").length > 0)
    $("#context").load("ctx/faq.html");
  else
    $("#main").load("ctx/faq.html");
}

function open_userLogInfo(id) {
  //	$("#context").load("ctx/logInfo.html");
  $("#context").load("ctx/userLogInfo.html", function() {
    load_userLogInfo(id);
  });
}

function open_loginLogInfo(id) {
  //	$("#context").load("ctx/logInfo.html");
  $("#context").load("ctx/loginLogInfo.html", function() {
    load_loginLogInfo(id);
  });
}

function add_favorite() {
  try {
    window.external.AddFavorite(location.href, document.title);
  } catch (e) {
    console.log("AddFavorite:", e);
  }
}

function add_hmepage(obj) {
  try {
    obj.style.behavior = 'url(#default#homepage)';
    obj.setHomePage(window.location);
  } catch (e) {
    console.log("setHomePage:", e)
  }
}

function do_logout() {
  BootstrapDialog.confirm({
    title: "操作确认",
    message: '您确认要注销当前会话吗?',
    btnOKLabel: "确认",
    btnCancelLabel: "取消",
    callback: function(result) {
      console.log('result:', result);
      if (result) {
        $.ajax({
          url: "logout",
          type: "GET",
          dataType: "json",
          cache: false,
          success: function(result) {
            if (result.code == 0) {
              logined = false;
              window.clearTimeout(certtimer);
              certtimer = null;
              $('#main').load('ctx/login.html');
            } else {
              ShowAlert(result.message || "注销操作失败");
            }
          }
        });
      }
    }
  });
}

var node;
var timestamp;

function getTimestamp() {
  timestamp = (new Date()).valueOf();
}

function node_onclick(i) {
  var strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes";

  if (node == undefined)
    ShowAlert("请重新刷新浏览器!");
  else {
    var n = node[i];
    console.log(n);
    if(n.cueMsg == "no_trust_site"){
      faq_point = 'question-fifteen';
      
      var ptions = {
        closable: true,
        title: "提示信息",
        onshown: function(dlg) {
          dlg.getModalBody().html(
            "<h4>访问该应用需要设置信任域, 详细步骤请参阅使用帮助--<br/><br/><a href='javascript:void(0);' onclick ='open_faq()' style='color:red;text-decoration: underline;'>“OA系统”登录失败怎么办</a></h4>"            	    
          );
          trust_dlg = dlg;
        }
      }
      BootstrapDialog.alert(ptions);
      
      //ShowAlert("访问该应用需要设置信任域, 详细步骤请参阅使用帮助--<br/><a href='javascript:void(0);' onclick ='open_faq()' style='color:red;'>“OA系统”登录失败怎么办</a>");
    }else if (n.allowAccess == 0) {
      if (n.tempField1 == 'usableAccount') {
        if (n.tempField2 == 'singleAccount') {
          if (n.ssoType == 'form_base') document.cookie = "AppName=" + n.resourceCode;
          getTimestamp();
          n.opened = window.open("sso?resourceCode=" + n.resourceCode + "&ssoAccount=" + n.tempField3 + "&R" + timestamp + "=sso", n.resourceName, "");
          if (n.opened && n.opened.open && !n.opened.closed)
            n.opened.focus();
        } else {
          load_ssoAccount(n);
        }
      } else {
        if (n.ssoType == 'form_base') {
          do_account_add(n.id, n.resourceName, 1);
        } else if (n.ssoType == 'plugin_token') {
          ShowAlert("无子账号,该应用需要通过过管理员配置账号,请联系管理员");
        } else
          ShowAlert("系统异常,请联系管理员");
      }
    } else if (n.allowAccess == 1) {
      ShowAlert("该应用安全级别较高,请使用其他认证方式");
    } else if (n.allowAccess == 2) {
      ShowAlert("该应用是内网应用,禁止公网访问");
    } else
      ShowAlert("系统异常,请联系管理员");
  }
}

function set_userinfo(user) {
  $(("#portaluserinfo")).html($.tmpl($("#portaluserinfo").html(), user));
}

//正常色
var nomalColor = "white";
var normalTextColor = 'black';

//提示色
var cueColor = "white";
//警告色
var warnColor = "#white";
var grayTextColor = 'black';
//正常边框色
var nomalBorder = "#878FB4";
//提示边框色
var cueBorder = "#878FB4";
//警告边框色
var warnBorder = "#878FB4";

function set_mainboard(data) {
  if (data == undefined || data.length == 0) {
    $("#mainboard").html("<h1>没有任何授权应用，请联系管理开通!</h1>");
    return;
  }
  for (i = 0; i < data.length; i++) {
    var n = data[i];
    n.i = i;
    n.textColor = grayTextColor;
    n.typeCls = "disabled";
    if(n.iconIsDefault == 1){
  	  n.imgUrl = "appIcon?resourceCode=" + n.resourceCode + "&" + iconTime;
    }else{
//      n.imgUrl = "images/app-Icon/" + n.resourceCode + ".png";
      n.imgUrl = "images/default.png";
    }
    if(!be_trustedIE && $.inArray(n.resourceCode,TrustedIE_arr) >= 0){
      n.ssoStatus = "请添加信任域";
      n.color = cueColor;
      n.border = cueBorder;
      n.cueMsg = "no_trust_site";
    }else if (n.allowAccess == 0) {
      if (n.tempField1 == 'usableAccount') {
        n.ssoStatus = "";
        n.color = nomalColor;
        n.border = nomalBorder;
        n.typeCls = "normal";
        n.textColor = normalTextColor;
      } else {
        if (n.ssoType == 'form_base') {
          n.ssoStatus = "未配置帐号";
          n.color = nomalColor;
          n.border = nomalBorder;
        } else if (n.ssoType == 'plugin_token') {
          n.ssoStatus = "管理员未授权";
          n.color = cueColor;
          n.border = cueBorder;
        } else {
          n.ssoStatus = "系统异常";
          n.color = warnColor;
          n.border = warnBorder;
        }
      }
    } else if (n.allowAccess == 1) {
      n.ssoStatus = "请更换认证方式";
      n.color = cueColor;
      n.border = cueBorder;
    } else if (n.allowAccess == 2) {
      n.ssoStatus = "禁止公网访问";
      n.color = cueColor;
      n.border = cueBorder;
    } else {
      n.ssoStatus = "系统异常";
      n.color = warnColor;
      n.border = warnBorder;
    }
    if (n.ssoStatus == '')
      n.display = 'none';
    else
      n.display = 'flow'
  }
  node = data;
  var warning = '<span class="glyphicon glyphicon-info-sign" style="color:#EF5350;display:${display}" aria-hidden="true"></span> ';
  var anode =
    '<div class="col-xs-4 pb30 node${typeCls}">' +
    ' <a href="javascript:node_onclick(${i});" title="${resourceName}">' +
    '  <div class="media res-list" style="background-color:${color};border-color:${border}">' +
    '   <span class="help-block pull-right">' + warning + '${ssoStatus}</span>' +
    '   <div class="media-left media-middle">' +
    '    <img class="media-object res-list" src="${imgUrl}">' +
    "   </div>" +
    '   <div class="media-body media-middle ${typeCls}">' +
    '    <h4 class="media-title" style="color:${textColor};">${resourceName}</h4>' +
    "   </div>" +
    "  </div>" +
    " </a>" +
    "</div>";
  $.tmpl(anode, data).appendTo("#mainboard");
}

function set_accountList(data) {
  if (data == undefined || data.length == 0) {
    $("#account_list").html("<h1>没有发现可用帐号!</h1>");
    return;
  }
  for (i = 0; i < data.length; i++) {
    var n = data[i];
    n.i = i;
    if (i == 0)
      n.checked = "checked";
    else
      n.checked = "";
    if (n.accountType == 'primary') {
      n.labelName = '（主账号）';
    } else {
      n.labelName = '（子账号）';
    }
    console.log(n);
  }

  var anode =
    '<div class="form-group">' +
    ' <label class="control-label" for="accountList_${i}">${labelName}</label>' +
    ' <input id="accountList_${i}" name="ssoAccount" type="radio" ${checked} accountType="${accountType}" value="${accountName}"/>' +
    ' <label class="control-label select-account" for="accountList_${i}">&emsp;${accountName}</label>' +
    '</div>';
  $.tmpl(anode, data).appendTo("#account_list");
}

function load_ssoAccount(resource) {
  $.ajax({
    url: 'loadAccount',
    data: {
      resourceCode: resource.resourceCode
    },
    type: 'post',
    dataType: 'json',
    success: function(data) {
      console.log('ssoAccount:', data);
      if (data.code == 0) {
        do_account_select(data.data.accountList, resource);
      } else {
        ShowAlert(data.message);
      }
    },
    error: function() {
      ShowAlert("系统异常，请联系管理员");
    }
  });
}

var iconTime;
function load_mainboard() {
  $.ajax({
    url: 'main',
    type: 'get',
    dataType: 'json',
    success: function(data) {
      console.log("mainboard", data);
      if (data.code == 0) {
    	iconTime = data.data.iconTime;
        set_userinfo(data.data.user);
        set_mainboard(data.data.resourceList);
        var task = data.data.task;
        if (task != null) {
          var tips;
          if (task.expirePrompt) {
            if (task.distanceDay == 0)
              tips = '当前密码今日到期，请及时<a href="javascript:open_changepwd();" style="color:#1296DB;">&lt;修改密码&gt;</a>';
            else
              tips = '当前密码有效期剩余' + task.distanceDay + '天，请及时<a href="javascript:open_changepwd();" style="color:#1296DB;">&lt;修改密码&gt;</a>';
          } else
            tips = "";
          $("#portaltips").html(tips);
        }
      } else {
        ShowAlert(data.message);
      }
    }
  });
}

function load_dataTable(id, url, columns) {
  $(id).DataTable({
    ajax: {
      url: url,
      type: "post",
      dataSrc: "data"
    },
    destroy: true,
    ordering: false,
    searching: false,
    processing: true,
    serverSide: false,
    paging: false,
    searching: false,
    //pagingType: "full_numbers",
    language: {
      decimal: "",
      emptyTable: "无数据",
      info: "显示从 _START_ 到 _END_ (共 _TOTAL_ 条记录)",
      infoEmpty: "无数据",
      infoFiltered: "(过滤 _MAX_ 条记录)",
      infoPostFix: "",
      thousands: ",",
      lengthMenu: "显示 _MENU_ 条记录",
      loadingRecords: "正在加载...",
      processing: "正在处理...",
      search: "搜索:",
      zeroRecords: "不存在满足条件的记录",
      paginate: {
        first: "|<",
        last: ">|",
        previous: "<",
        next: ">"
      }
    },
    columns: columns
  });
}

var account_dialog_callback;

function on_account_dialog_show() {
  account_dialog_callback();
}

function do_account_select(accountList, resource) {
  var dialog = BootstrapDialog.show({
    closable: false,
    title: "选择登录应用帐号",
    message: $("<div></div>").load("ctx/accountselect.html")
  });
  account_dialog_callback = function() {
    set_accountList(accountList);
    dialog.getModalBody().find('#resourceName').html("选定的帐号用于登录应用系统: " + resource.resourceName);
    var form = $("#account_select");
    form.on('reset', function() {
      dialog.close();
    });
    form.on('submit', function() {
    	var ssoAccount = $("input[name='ssoAccount']:checked").val();
    	var accountType = $("input[name='ssoAccount']:checked").attr('accountType');
      if (typeof(ssoAccount) != "undefined" || typeof(accountType) != "undefined") {
        dialog.close();
        if (resource.ssoType == 'form_base') document.cookie = "AppName=" + resource.resourceCode;
        getTimestamp();
        window.open("sso?resourceCode=" + resource.resourceCode + "&ssoAccount=" + ssoAccount + "&accountType=" + accountType + "&R" + timestamp + "=sso", resource.resourceCode);
      }
      return false;
    });
  }
}

function do_account_add(resourceId, resourceName, board) {
  var dialog = BootstrapDialog.show({
    closable: false,
    title: "代填应用添加子帐号",
    message: $("<div></div>").load("ctx/accountadd.html")
  });
  account_dialog_callback = function() {
    var form = $("#account_add");
    form.on('reset', function() {
      dialog.close();
    });
    $('#resourceName').html("增加的子帐号用于登录应用系统:" + resourceName);

    form.bootstrapValidator({
      message: '输入数据非法',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        accountName: {
          notEmpty: {
            message: '用户子帐号不能为空'
          }
        },
        accountPassword: {
          validators: {
            stringLength: {
              min: 4,
              max: 20,
              message: '密码长度必须在4到20位之间'
            },
            regexp: {
              regexp: /^[a-zA-Z0-9~!@#$%^&*()_:;<>?,./]+$/,
              message: '密码只能包含大写、小写、数字和字符'
            }
          }
        },
        confirmAccountPassword: {
          validators: {
            identical: {
              field: 'accountPassword',
              message: '两次输入密码不一致'
            }
          }
        }
      }
    }).on('success.form.bv', function(e) {
      // Prevent form submission
      e.preventDefault();
      // Get the form instance
      var form = $(e.target);
      // Get the BootstrapValidator instance

      var post = {
        accountStatus: "0",
        resourceId: resourceId,
        accountName: form.find("input[name='accountName']").val(),
        accountPassword: form.find("input[name='accountPassword']").val(),
        confirmAccountPassword: form.find("input[name='confirmAccountPassword']").val()
      }
      $.ajax({
        data: {
          "formData": JSON.stringify(post)
        },
        url: "personal/account/save",
        dataType: "json",
        method: "post",
        success: function(obj) {
          console.log(obj);
          if (obj.code != 0) {
            ShowAlert(obj.message);
          } else {
            dialog.close();
            ShowAlert("子帐号注册成功");
            if (board == 1)
              open_mainboard();
            else
              open_accountmgr();
          }
        }
      });
      return false;
    });
  }
}

function do_account_edit(resourceId, resourceName, id, accountName) {
  var dialog = BootstrapDialog.show({
    closable: false,
    title: "编辑应用子帐号",
    message: $("<div></div>").load("ctx/accountadd.html")
  });
  account_dialog_callback = function() {
    var form = $("#account_add");
    form.on('reset', function() {
      dialog.close();
    });
    $('#resourceName').html("被编辑的子帐号用于登录应用系统:" + resourceName);
    form.find("input[name='accountName']").val(accountName);
    form.bootstrapValidator({
      message: '输入数据非法',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        accountName: {
          notEmpty: {
            message: '用户子帐号不能为空'
          }
        },
        accountPassword: {
          validators: {
            stringLength: {
              min: 4,
              max: 20,
              message: '密码长度必须在4到20位之间'
            },
            regexp: {
              regexp: /^[a-zA-Z0-9~!@#$%^&*()_:;<>?,./]+$/,
              message: '密码只能包含大写、小写、数字和字符'
            }
          }
        },
        confirmAccountPassword: {
          validators: {
            identical: {
              field: 'accountPassword',
              message: '两次输入密码不一致'
            }
          }
        }
      }
    }).on('success.form.bv', function(e) {
      // Prevent form submission
      e.preventDefault();
      // Get the form instance
      var form = $(e.target);
      // Get the BootstrapValidator instance

      var post = {
        accountStatus: "0",
        id: id,
        resourceId: resourceId,
        accountStatus: "0",
        accountName: form.find("input[name='accountName']").val(),
        accountPassword: form.find("input[name='accountPassword']").val(),
        confirmAccountPassword: form.find("input[name='confirmAccountPassword']").val()
      }
      $.ajax({
        data: {
          "formData": JSON.stringify(post)
        },
        url: "personal/account/update",
        dataType: "json",
        method: "post",
        success: function(obj) {
          console.log(obj);
          if (obj.code != 0) {
            ShowAlert(obj.message);
          } else {
            dialog.close();
            ShowAlert("子帐号编辑成功");
            open_accountmgr();
          }
        }
      });
      return false;
    });
  }
}

function do_account_remove(id) {
  BootstrapDialog.confirm({
    title: "操作确认",
    message: '您确认解除当前绑定吗?',
    btnCancelLabel: "取消",
    closable: false,
    btnOKLabel: '解绑', // <-- Default value is 'OK',
    callback: function(result) {
      if (result) {
        $.ajax({
          type: "POST",
          url: "personal/account/deleteAuthorizeUser",
          data: {
            id: id
          },
          dataType: "json",
          success: function(data) {
            if (data.code == 0) {
              ShowAlert("删除子帐号成功");
              open_accountmgr();
            } else {
              ShowAlert(data.message);
            }
          }
        });
      }
    }
  });
}

function load_dataTable_applist_proxy() {
  var columns = [{
      data: "resourceName",
      width: "30%"
    },
    {
      data: "accountName",
      width: "30%"
    },
    {
      data: "bindingMode",
      width: "20%",
      render: function(data, type, row) {
        if (type == "display") {
          if (data == "1") {
            return "用户映射";
          } else {
            return "管理员配置";
          }
        }
        return data;
      }
    },
    {
      data: "action",
      width: "20%",
      render: function(data, type, row) {
        if (type == 'display') {
          if (row.bindingMode == 1) {
            var s = '<div class="row">';
            if (row.accountName == null || row.accountName == "") {
              s = s + "<a href='javascript:do_account_add(\"" + row.resourceId + "\",\"" + row.resourceName + "\", 0);' class='btn btn-primary' role='button'>添加</a>";
            } else {
              s = s + "<a href='javascript:do_account_edit(\"" + row.resourceId + "\",\"" + row.resourceName + "\",\"" + row.accountId + "\",\"" + row.accountName + "\");' class='btn btn-primary' role='button'>编辑</a>";
              s = s + ' ';
              s = s + "<a href='javascript:do_account_remove(\"" + row.accountId + "\");' class='btn btn-primary' role='button'>解绑</a>";
            }
            s = s + '</div>';
            return s;
          } else if (row.bindingMode == 2) {
            if (row.accountName == null)
              return "尚未配置";
            else
              return "管理员维护";
          }
        }
        return data;
      }
    }
  ]
  load_dataTable("#appview", "personal/account/list?bindingMode=1", columns)
}

function on_accountload() {
  load_dataTable_applist_proxy();
  load_dataTable_applist_plugin();
  $('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
    console.log(e.target.hash);
    //if(e.target.hash="#idplugin")
    //	load_dataTable_applist_plugin();
    //if(e.target.hash="#idproxy")
    //	load_dataTable_applist_proxy();

  });
}
//显示状态为图片
function fnRenderResult(value, type, rowObj) {
  if (value == 0) {
    return "<span class='label label-success'>成功</span>";
  } else if (value == 1) {
    return "<span class='label label-danger'>失败</span>"
  }
}

function load_access_log_list() {
  var config = {
    dataUrl: "personal/log/loadData"
  }
  var logTable = new CommonTable("access_log_list", "searchDiv", config);
}

function load_user_log_list() {
  var config = {
    dataUrl: "personal/log/userLoadData"
  }
  var logTable = new CommonTable("user_log_list", "searchDiv", config);
}

function on_logload() {
  console.log("log page load!");

  load_access_log_list();
  var log_load = false;

  $('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
    console.log(e.target.hash);
    if (e.target.hash == "#idplugin" && !log_load) {

      load_user_log_list();
      log_load = false;
    }
    //e.target; // newly activated tab
    //e.relatedTarget; // previous active tab
  });
  //loadSSOLog('http://localhost:8080/uas-portal/personal/log/loadData',{});

}

function load_dataTable_applist_plugin() {
  var columns = [{
      data: "resourceName",
      width: "30%"
    },
    {
      data: "accountName",
      width: "30%",
      render: function(data, type, row) {
        if (type == "display") {
          if (data == null || data == "") {
            return "(空)";
          }
        }
        return data;
      }
    },
    {
      data: "bindingMode",
      width: "20%",
      render: function(data, type, row) {
        if (type == "display") {
          if (data == "1") {
            return "用户映射";
          } else if (data == null) {
            return "管理员未配置";
          } else {
            return "管理员配置";
          }
        }
        return data;
      }
      /*},
      {
        data: "action",
        width: "20%",
        align: "center",
        render: function(data, type, row) {
          if (type == 'display') {
            if (row.bindingMode == 1) {
              var s = '<div class="btn-group btn-group-justified" role="group>';
              if (row.accountName == null) {
                s = s + "<a href='#' class='btn btn-primary'>添加</button>";
              } else {
                s = s + "<a href='javascript:do_accountedit();' class='btn btn-primary'>编辑</button>";
                s = s + "<a href='javascript:do_accountunbind(" + row.accountId + ");' class='btn btn-primary'>解绑</button>";
              }
              s = s + '</div>';
              return s;
            } else if (row.bindingMode == 2) {
              if (row.accountName == null)
                return "管理员未配置";
              else
                return "管理员维护";
            }
          }
          return data;
        }*/
    }
  ]
  load_dataTable("#logview", "personal/account/list?bindingMode=2", columns)
}

function load_change_userpassword() {
  var info;
  $.ajax({
    url: "personal/info/password",
    dataType: "json",
    method: "GET",
    cache: false,
    success: function(result) {
      if (result.code == 0) {
        info = result.data;
        if (info.authCode == 'password_auth') {
          $('#title').html('修改国密统一认证系统口令');
          var form = $('#change_userpwd');
          form.show();
          form.bootstrapValidator({
            message: '输入数据非法',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
              chg_oldpwd: {
                notEmpty: {
                  message: '用户原密码不能为空'
                }
              },
              chg_passwd: {
                validators: {
                  stringLength: {
                    min: info.minLength,
                    max: info.maxLength,
                    message: '新密码长度必须在' + info.minLength + '到' + info.maxLength + '位之间'
                  },
                  regexp: {
                    regexp: /^[a-zA-Z0-9~!@#$%^&*()_:;<>?,./]+$/,
                    message: '新密码只能包含大写、小写、数字和字符'
                  }
                }
              },
              chg_passwd1: {
                validators: {
                  identical: {
                    field: 'chg_passwd',
                    message: '两次输入密码不一致'
                  }
                }
              }
            }
          }).on('success.form.bv', function(e) {
            // Get the form instance
            e.preventDefault();

            var form = $(e.target);
            // Get the BootstrapValidator instance
            form.find("input[type='submit']").prop('disabled', true);

            var old_pwd = form.find("input[name='chg_oldpwd']").val();
            var new_pwd = form.find("input[name='chg_passwd']").val();
            var new_pwd1 = form.find("input[name='chg_passwd1']").val();
            if (new_pwd != new_pwd1) {
              ShowAlert("俩次输入的密码不一致");
              return false;
            }

            var encrypt_old_pwd = encrypt(info.loginName, old_pwd);
            var encrypt_new_pwd = encrypt(info.loginName, new_pwd);
            if(encrypt_old_pwd=='' || encrypt_new_pwd==''){
              ShowAlert("数据处理失败, 请刷新后重试");
              return false;
            }
            $.ajax({
              data: {
                old_pwd: encrypt_old_pwd,
                new_pwd: encrypt_new_pwd
              },
              url: "personal/info/updatePasswordByPWD",
              dataType: "json",
              method: "post",
              success: function(obj) {
                console.log(obj);
                if (obj.code != 0) {
                  ShowAlert(obj.message);
                } else {
                  ShowAlert("口令修改成功");
                  open_mainboard();
                }
                return false;
              }
            });
            return false;
          });
        } else if (info.authCode == 'certificate_auth') {
          $('#title').html('修改UKey证书口令');

          var form = $('#change_ukeypwd');
          form.show();
          form.bootstrapValidator({
            message: '输入数据非法',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
              chg_oldpwd: {
                notEmpty: {
                  message: '用户原证书密码不能为空'
                },
                validators: {
                  stringLength: {
                    min: 6,
                    max: 15,
                    message: '用户原证书密码长度必须在 6 到 15 位之间'
                  }
                }
              },
              chg_passwd: {
                validators: {
                  stringLength: {
                    min: 6,
                    max: 15,
                    message: '用户新证书密码长度必须在 6 到 15 位之间'
                  },
                  regexp: {
                    regexp: /^[a-zA-Z0-9_]+$/,
                    message: '用户新证书密码只能包含大写、小写、数字和下划线'
                  }
                }
              },
              chg_passwd1: {
                validators: {
                  identical: {
                    field: 'chg_passwd',
                    message: '两次输入证书密码不一致'
                  }
                }
              }
            }
          }).on('success.form.bv', function(e) {
            // Get the form instance
            e.preventDefault();

            var form = $(e.target);
            // Get the BootstrapValidator instance
            form.find("input[type='submit']").prop('disabled', true);

            var old_pwd = form.find("input[name='chg_oldpwd']").val();
            var new_pwd = form.find("input[name='chg_passwd']").val();
            var new_pwd1 = form.find("input[name='chg_passwd1']").val();
            if (new_pwd != new_pwd1) {
              ShowAlert("俩次输入的证书密码不一致");
              return false;
            }

            ChangePIN(info.loginName, old_pwd, new_pwd, function(result) {
              result = 0 + result;
              var msg = "";
              if (result > 0) {
                msg = "原证书密码错误，剩余" + result + "次机会";
              } else if (result == 0) {
                msg = "证书密码修改成功";
              } else
                msg = "修改证书密码失败: " + result;
              ShowAlert(msg);
              if (result == 0)
                open_mainboard();
              return false;
            });
            return false;
          });
        }

      }
    }
  });
}

function load_userinfo() {
  $.ajax({
    url: "personal/info/view",
    dataType: "json",
    method: "GET",
    cache: false,
    success: function(data) {
      if (data.code == 0) {
        $('#u_loginName').html(data.data.loginName);
        $('#u_userName').html(data.data.userName);
        $('#u_orgName').html(data.data.orgName);
        $('#u_idNumber').html(data.data.idNumber);
        $('#u_hireTime').html(data.data.hireTime == null || data.data.hireTime == "" ? '' : (new Date(data.data.hireTime)).Format('yyyy-mm-dd'));
        $('#mobilePhone').val(data.data.mobilePhone);
        $('#officePhone').val(data.data.officePhone);
        $('#birthday').val(data.data.birthday == null ? '' : (new Date(data.data.birthday)).Format('yyyy-mm-dd'));
        $('#userinfo_form').find("input[name='id']").val(data.data.id);
        $('#userinfo_form').find("input[name='updateTimeStamp']").val(data.data.updateTimeStamp);
        /*$('#birthday').datepicker('setDate', new Date(data.data.birthday));*/
      } else {
        ShowAlert(data.message);
      }

      if (data.data.birthday != null)
        $("#birthday").datepicker('setDate', new Date(data.data.birthday));

      $('#userinfo_form').bootstrapValidator({
        message: '输入数据非法',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          birthday: {
            validators: {
              regexp: {
                regexp: /^\d{4,4}-\d{2,2}-\d{2,2}$/,
                message: '出生日期格式不正确，如1999-09-09'
              }
            }
          },
          mobilePhone: {
            validators: {
              regexp: {
                regexp: /^1[3|4|5|7|8][0-9]{9}$/,
                message: '手机号格式不正确'
              }
            }
          },
          officePhone: {
            validators: {
              regexp: {
                regexp: /^(\d3,4|\d{3,4}-)?\d{7,8}$/,
                message: '办公电话格式不正确!'
              }
            }
          }
        }
      }).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();
        // Get the form instance
        var form = $(e.target);
        // Get the BootstrapValidator instance
        var data = {
          birthday: $('#birthday').datepicker('getDate') == null ? '' : $('#birthday').datepicker('getDate').getTime(),
          mobilePhone: form.find("input[name='mobilePhone']").val(),
          officePhone: form.find("input[name='officePhone']").val(),
          id: form.find("input[name='id']").val()
        };
        $.ajax({
          data: {
            formData: JSON.stringify(data)
          },
          url: "personal/info/updateInfo",
          dataType: "json",
          method: "post",
          success: function(obj) {
            console.log(obj);
            if (obj.code != 0) {
              ShowAlert(obj.message);
            } else {
              ShowAlert("身份信息修改成功");
              open_mainboard();
            }
          }
        });
        return false;
      });
    }
  });
}

function load_userLogInfo(id) {
  $.ajax({
    url: "personal/log/view/" + id,
    dataType: "json",
    method: "GET",
    cache: false,
    success: function(data) {
      if (data.code == 0) {
        $('#log_id').html(data.data.id);
        $('#log_loginName').html(data.data.loginName);
        $('#log_userName').html(data.data.userName);
        $('#log_orgName').html(data.data.orgName);
        /*$('#log_actionType').html(data.data.actionType);*/

        $('#log_operObject').html(data.data.operObject);
        if (data.data.operResult == 0) {
          $('#log_operResult').html("成功");
        } else {
          $('#log_operResult').html("失败");
        }

        $('#log_clientIp').html(data.data.clientIp);
        $('#log_operDateTime').html(data.data.operDateTime == null ? '' : (new Date(data.data.operDateTime)).Format('yyyy-mm-dd hh:ii:ss'));
        $('#log_logContext').html(data.data.logContext);
      } else {
        ShowAlert(data.message);
      }
    }
  });
}

function load_loginLogInfo(id) {
  $.ajax({
    url: "personal/log/viewLogin/" + id,
    dataType: "json",
    method: "GET",
    cache: false,
    success: function(data) {
      if (data.code == 0) {
        $('#log_id').html(data.data.id);
        $('#log_loginName').html(data.data.loginName);
        $('#log_userName').html(data.data.userName);
        $('#log_orgName').html(data.data.orgName);
        $('#log_resourceName').html(data.data.resourceName);

        $('#log_accountAlias').html(data.data.accountAlias);
        $('#log_accountName').html(data.data.accountName);
        $('#log_clientIp').html(data.data.clientIp);
        if (data.data.accessResult == 0) {
          $('#log_accessResult').html("成功");
        } else {
          $('#log_accessResult').html("失败");
        }

        $('#log_accessDateTime').html(data.data.accessDateTime == null ? '' : (new Date(data.data.accessDateTime)).Format('yyyy-mm-dd hh:ii:ss'));
        $('#log_logContext').html(data.data.logContext);
      } else {
        ShowAlert(data.message);
      }
    }
  });
}


function open_loglist() {
  $("#context").load("ctx/log.html");

  $("#dialog").on("dialogcreate", function(event, ui) {
    console.log(event);
    console.log(ui);

    columns = [{
        data: "id"
      },
      {
        data: "operator"
      },
      {
        data: "operate"
      },
      {
        data: "target"
      },
      {
        data: "result"
      },
      {
        data: "timestamp",
        render: function(data, type, row) {
          if (type == "display") {
            var d = new Date(data * 1000);
            return d
              .toISOString()
              .substring(0, 19)
              .replace("T", " ");
          }
          return data;
        }
      },
      {
        data: "status",
        render: function(data, type, row) {
          if (type == "display") {
            if (data == "") {
              return "正常";
            } else {
              return "已审计";
            }
          }
          return data;
        }
      }
    ];
  });
}

function open_loginlog() {
  $("#context").load("ctx/loginlog.html");

  $("#dialog").on("dialogcreate", function(event, ui) {
    console.log(event);
    console.log(ui);

    columns = [{
        data: "id"
      },
      {
        data: "operator"
      },
      {
        data: "operate"
      },
      {
        data: "target"
      },
      {
        data: "result"
      },
      {
        data: "timestamp",
        render: function(data, type, row) {
          if (type == "display") {
            var d = new Date(data * 1000);
            return d
              .toISOString()
              .substring(0, 19)
              .replace("T", " ");
          }
          return data;
        }
      },
      {
        data: "status",
        render: function(data, type, row) {
          if (type == "display") {
            if (data == "") {
              return "正常";
            } else {
              return "已审计";
            }
          }
          return data;
        }
      }
    ];
  });
}

function open_userecord() {
  $("#context").load("ctx/userecord.html");

  $("#dialog").on("dialogcreate", function(event, ui) {
    console.log(event);
    console.log(ui);

    columns = [{
        data: "id"
      },
      {
        data: "operator"
      },
      {
        data: "operate"
      },
      {
        data: "target"
      },
      {
        data: "result"
      },
      {
        data: "timestamp",
        render: function(data, type, row) {
          if (type == "display") {
            var d = new Date(data * 1000);
            return d
              .toISOString()
              .substring(0, 19)
              .replace("T", " ");
          }
          return data;
        }
      },
      {
        data: "status",
        render: function(data, type, row) {
          if (type == "display") {
            if (data == "") {
              return "正常";
            } else {
              return "已审计";
            }
          }
          return data;
        }
      }
    ];
  });
}

// 递归删除空属性防止把null变成空值
function deleteEmptyProp(obj) {
  for (var a in obj) {
    if (typeof(obj[a]) == "object" && obj[a] != null) {
      deleteEmptyProp(obj[a]);
    } else {
      if (!obj[a]) {
        delete obj[a];
      }
    }
  }
  return obj;
}

function ajaxPost(url, params, callback) {
  var result = null;

  // console.log("before remove empty prop:");
  // console.log(params);
  if (params && typeof params == "object") {
    params = deleteEmptyProp(params);
  }
  // console.log("after remove empty prop:");
  // console.log(params);

  jQuery.ajax({
    type: 'post',
    async: false,
    url: url,
    data: params,
    dataType: 'json',
    success: function(data, status) {
      result = data;
      if (data && data.code && data.code == '1') {
        alert("操作失败, 具体错误：" + data.message);
        return false;
      }
      if (callback) {
        callback.call(this, data, status);
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      // console.log("ajaxPost发生异常，请仔细检查请求url是否正确，如下面错误信息中出现success，则表示csrftoken更新，请忽略");
      // console.log(err.responseText);
      if (jqXHR && jqXHR.readyState && jqXHR.readyState == '4') {
        var sessionStatus = jqXHR.getResponseHeader("session-status");
        // console.log(err);
        // console.log(err1);
        // console.log(err2);
        if (sessionStatus == "timeout") {
          // 如果超时就处理 ，指定要跳转的页面
          window.location.href = contextPath + "/";
        } else if (textStatus == "parsererror") {
          // csrf异常
          var responseBody = jqXHR.responseText;
          if (responseBody) {
            responseBody = "{'retData':" + responseBody;
            var resJson = eval('(' + responseBody + ')');
            jQuery("#csrftoken").val(resJson.csrf.CSRFToken);
            this.success(resJson.retData, 200);
          }
          return;
        } else {
          BootstrapDialog.alert({
            text: JSON.stringify(jqXHR) + '<br/>textStatus:' + JSON.stringify(textStatus) + '<br/>errorThrown:' + JSON.stringify(errorThrown),
            large: true
          });
          return;
        }
      }

      BootstrapDialog.alert({
        text: JSON.stringify(jqXHR) + '<br/>textStatus:' + JSON.stringify(textStatus) + '<br/>errorThrown:' + JSON.stringify(errorThrown),
        large: true
      });
    }
  });

  return result;
}

/**
 * 格式化日期
 */
function formatDate(date, format) {
  if (!date) return date;
  date = (typeof date == "number") ? new Date(date) : date;
  return date.Format(format);
}

Date.prototype.Format = function(fmt) {
  var o = {
    "m+": this.getMonth() + 1, // 月份
    "d+": this.getDate(), // 日
    "h+": this.getHours(), // 小时
    "i+": this.getMinutes(), // 分
    "s+": this.getSeconds(), // 秒
    "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
    "S": this.getMilliseconds()
    // 毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

/**
 * 比较两个时间的大小 d1>d2 返回大于0
 * 
 * @param d1
 * @param d2
 * @returns {number}
 * @constructor
 */
function DateDiff(d1, d2) {
  var result = Date.parse(d1.replace(/-/g, "/")) - Date.parse(d2.replace(/-/g, "/"));
  return result;
}

/**
 * 字符串转日期
 * 
 * @returns {number}
 */
String.prototype.strToDate = function() {
  if (this && this != "") {
    return Date.parse(this.replace(/-/g, "/"));
  } else
    return "";
}
/**
 * 将map类型[name,value]的数据转化为对象类型
 */
function getObjectFromMap(aData) {
  var map = {};
  for (var i = 0; i < aData.length; i++) {
    var item = aData[i];
    if (!map[item.name]) {
      map[item.name] = item.value;
    }
  }
  return map;
}


/**
 * 获取下一个编码 000001，000001000006，6 得到结果 000001000007
 */
function getNextCode(prefix, maxCode, length) {
  if (maxCode == null) {
    var str = "";
    for (var i = 0; i < length - 1; i++) {
      str += "0";
    }
    return prefix + str + 1;
  } else {
    var str = "";
    var sno = parseInt(maxCode.substring(prefix.length)) + 1;
    for (var i = 0; i < length - sno.toString().length; i++) {
      str += "0";
    }
    return prefix + str + sno;
  }

}

String.prototype.startWith = function(s) {
  if (s == null || s == "" || this.length == 0 || s.length > this.length)
    return false;
  if (this.substr(0, s.length) == s)
    return true;
  else
    return false;
  return true;
}

String.prototype.replaceAll = function(s1, s2) {
  return this.replace(new RegExp(s1, "gm"), s2);
}

String.prototype.format = function() {
  if (arguments.length == 0) return this;
  for (var s = this, i = 0; i < arguments.length; i++)
    s = s.replace(new RegExp("\\{" + i + "\\}", "g"), arguments[i]);
  return s;
};

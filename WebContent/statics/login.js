function encrypt(keyInit, password) {
  if (keyInit.length > 16) {
    keyInit = keyInit.substring(0, 16);
  }
  if (keyInit.length < 16) {
    while (keyInit.length < 16) {
      keyInit = keyInit + "0";
    }
  }
  if (password == "" || password == undefined) {
    return '';
  }
  password = password.replace(/^\s+|\s+$/g, "");

  var encrypted;
  try {
    var key = CryptoJS.enc.Utf8.parse(keyInit);
    var srcs = CryptoJS.enc.Utf8.parse(password);
    encrypted = CryptoJS.AES.encrypt(srcs, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    encrypted = encrypted.toString();
  } catch (e) {
    return '';
  }
  return encrypted;
}

var load_firstchange_userpassword;

function first_force_changepassword(data) {
  var dialog = BootstrapDialog.show({
    closable: true,
    title: "首次登录修改口令",
    message: $("<div></div>").load("ctx/firstchange.html")
  });
  load_firstchange_userpassword = function() {
    var form = $('#change_userpwd');
    form.on('reset', function() {
      dialog.close();
    });

    form.bootstrapValidator({
      message: '输入数据非法',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        chg_oldpwd: {
          notEmpty: {
            message: '用户原密码不能为空'
          }
        },
        chg_passwd: {
          validators: {
            stringLength: {
              min: data.minLength,
              max: data.maxLength,
              message: '新密码长度必须在' + data.minLength + '到' + data.maxLength + '位之间'
            },
            regexp: {
              regexp: /^[a-zA-Z0-9~!@#$%^&*()_:;<>?,./]+$/,
              message: '新密码只能包含大写、小写、数字和字符'
            }
          }
        },
        chg_passwd1: {
          validators: {
            identical: {
              field: 'chg_passwd',
              message: '两次输入密码不一致'
            }
          }
        }
      }
    }).on('success.form.bv', function(e) {
      // Prevent form submission
      e.preventDefault();
      // Get the form instance
      var form = $(e.target);
      // Get the BootstrapValidator instance

      var old_pwd = form.find("input[name='chg_oldpwd']").val();
      var new_pwd = form.find("input[name='chg_passwd']").val();
      var new_pwd1 = form.find("input[name='chg_passwd1']").val();
      if (new_pwd != new_pwd1) {
        ShowAlert("俩次输入的密码不一致");
        return false;
      }
      var encrypt_old_pwd = encrypt(data.username, old_pwd);
      var encrypt_new_pwd = encrypt(data.username, new_pwd);
      if (encrypt_old_pwd == '' || encrypt_new_pwd == '') {
        ShowAlert("数据处理失败, 请刷新后重试");
        return false;
      }
      $.ajax({
        data: {
          loginName: data.username,
          old_pwd: encrypt_old_pwd,
          new_pwd: encrypt_new_pwd
        },
        url: "updatePwd",
        dataType: "json",
        method: "post",
        success: function(obj) {
          if (obj.code != 0) {
            ShowAlert(obj.message);
          } else {
            dialog.close();
            ShowAlert("口令修改成功,请重新登录");
          }
          $('#main').load('ctx/login.html');
        }
      });
      return false;
    });
  }
}

var verifyRandom;

function loadVerifyRandom() {
  $.ajax({
    url: "loadVerifyRandom",
    dataType: "json",
    method: "GET",
    cache: false,
    success: function(data) {
      if (data.code != 0) {
        ShowAlert(data.message);
        return;
      }
      verifyRandom = data.message;
    }
  });
}

function loadLoginPage(uid, cid) {
  $.ajax({
    url: "loadAuth",
    dataType: "json",
    method: "GET",
    cache: false,
    success: function(data) {
      if (data.code != 0) {
        $('#login').hide();
        BootstrapDialog.alert({
          title: '提示信息',
          message: data.message,
          closeable: true,
          buttonLabel: "确定"
        });
        return false;
      }
      if (data.data.noAuthCode == "certificate_auth") {
        user_pwd_form_valid(uid);
        $('#auth_cert').click(function() {
          ShowAlert("平台未启用证书认证方式");
          return false;
        });
      } else if (data.data.noAuthCode == "password_auth") {
        certificate_form_valid(cid);
        $("#li_auth_pwd").removeClass("active");
        $("#home").removeClass("active");
        $("#li_auth_cert").addClass("active");
        $("$profile").addClass("active");
        $('#auth_pwd').click(function() {
          ShowAlert("平台未启用密码认证方式");
          return false;
        });
      } else {
        user_pwd_form_valid(uid);
        certificate_form_valid(cid);
        if (data.data.authCode == "certificate_auth") {
          $("#li_auth_pwd").removeClass("active");
          $("#home").removeClass("active");
          $("#li_auth_cert").addClass("active");
          $("#profile").addClass("active");
          RunOnLoad();
        }
      }
    }
  });
}

function user_pwd_form_valid(id) {
  $.ajax({
    url: "login?authCode=password_auth",
    dataType: "json",
    method: "GET",
    cache: false,
    success: function(data) {
      if (data.code != 0) {
        ShowAlert(data.message);
        return;
      }
      data = data.data;
      if (data.enableCaptcha == 0) {
        $("#captchasection").show();
      } else
        $("#captchasection").hide();

      if (data.username != null && data.username != 'null')
        $('#pwd_username').val(data.username);

      $(id).bootstrapValidator({
        message: "This value is not valid",
        feedbackIcons: {
          valid: "glyphicon glyphicon-ok",
          invalid: "glyphicon glyphicon-remove",
          validating: "glyphicon glyphicon-refresh"
        },
        fields: {
          username: {
            message: "用户名验证失败",
            validators: {
              notEmpty: {
                message: "用户名不能为空"
              },
              stringLength: {
                min: 2,
                max: 25,
                message: "用户名长度必须在2到25位之间"
              },
              regexp: {
                regexp: /^[a-zA-Z0-9~!@#$%^&*()_:;<>?,./]+$/,
                message: "用户名包含非法字符"
              }
            }
          },
          password: {
            message: "输入的用户密码不正确",
            validators: {
              notEmpty: {
                message: "用户密码不能为空"
              },
              stringLength: {
                min: 4,
                max: 20,
                message: "用户密码长度必须在4到20位之间"
              },
              regexp: {
                regexp: /^[a-zA-Z0-9~!@#$%^&*()_:;<>?,./]+$/,
                message: "用户密码包含非法字符"
              }
            }
          },
          captcha: {
            validators: {
              notEmpty: {
                message: "验证码不能为空"
              },
              stringLength: {
                min: 4,
                max: 4,
                message: "验证码必须为4位字符"
              },
              remote: {
                url: "captchacheck"
              }
            }
          }
        }
      }).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();
        // Get the form instance
        var form = $(e.target);
        // Get the BootstrapValidator instance
        var username = form.find("input[name='username']").val();
        var password = form.find("input[name='password']").val();
        var captcha = form.find("input[name='captcha']").val();

        var encrypt_password = encrypt(username, password);
        if (encrypt_password == '') {
          ShowAlert("数据处理失败, 请刷新后重新登录");
          return false;
        }
        $.ajax({
          method: "post",
          data: {
            username: username,
            password: encrypt_password,
            captcha: captcha,
            authCode: "password_auth",
            verifyRandom: verifyRandom
          },
          url: "login",
          dataType: "json",
          success: function(obj) {
            if (obj.code == 0) {
              if (obj.data.authError != undefined)
                ShowAlert(obj.data.authError);
              else
                $("#main").load("portal.html");
            } else {
              if (obj.data.errorCode == "changePwd") {
                $("#captchasection").hide();
                data.username = username;
                first_force_changepassword(data);
              } else {
                $('#pwd_captcha').val('');
                $(id).data('bootstrapValidator').updateStatus('captcha', 'NOT_VALIDATED');
                ShowAlert(obj.data.authError);
                loadVerifyRandom();
                $.ajax({
                  url: "login?authCode=password_auth",
                  dataType: "json",
                  method: "GET",
                  cache: false,
                  async: false,
                  success: function(data) {
                    if (data.code != 0) {
                      ShowAlert(data.message);
                      return;
                    }
                    data = data.data;
                    if (data.enableCaptcha == 0) {
                      $("#captchasection").show();
                      $("#capimg").click();
                    } else
                      $("#captchasection").hide();
                  }
                })
              }
            }
          }
        });
        return false;
      });
    }
  });
}

function certificate_form_valid(id) {
  $(id).bootstrapValidator({
    message: "This value is not valid",
    feedbackIcons: {
      valid: "glyphicon glyphicon-ok",
      invalid: "glyphicon glyphicon-remove",
      validating: "glyphicon glyphicon-refresh"
    },
    fields: {
      username: {
        message: "选择的证书验证失败",
        validators: {
          notEmpty: {
            message: "必须选择用户证书"
          }
        }
      },
      pin: {
        message: "输入的证书密码不正确",
        validators: {
          notEmpty: {
            message: "用户证书密码不能为空"
          }
        }
      }
    }
  }).on('success.form.bv', function(e) {
    $("#profile button").addClass('disabled');
    setTimeout(function() {
      var bok = false;
      try {
        var cn = $("#cert_user").val();
        var pin = $("#cert_pin").val();

        if (cn == null || cn == "") {
          showError("未发现可用证书");
          return false;
        }

        if (pin == null || pin == "") {
          showError('证书密码不能为空');
          return false;
        }

        if (pin.length < 6) {
          showError('密码长度不能小于 6');
          return false;
        }

        if (pin.length > 15) {
          showError('密码长度不能大于 15');
          return false;
        }
        bok = true;
        signSM2(cn, pin, verifyRandom, function(success, msg) {
          $("#profile button").removeClass('disabled');
          onResult(success, msg);
        })
      } finally {
        if (bok == false) {
          $("#profile button").removeClass('disabled');
        }
      }
    }, 1);

    return false;
  });
}

var hasUkey = false;

function RunOnLoad() {
  if (RunOnLoaded) return;
  RunOnLoaded = true;
  //  var filter = "issuer:DemoSM2CA";
  var usercerts = $('#cert_user');


  function onCertResult(certs) {
    if (logined) {
      return;
    }
    var clear = true;
    var cn = usercerts.val();
    if (cn == null) {
      cn = '';
    }
    if (certtimer != undefined && certtimer != null) window.clearTimeout(certtimer);
    certtimer = window.setTimeout(function() {
      MonitorCert(onCertResult);
    }, cn == '' ? 2000 : 6000);

    //所有可选的用户证书
    certs = certs.split(";");
    var options = new Array();

    for (i = 0; i < certs.length; i++) {
      if (certs[i] != '') {
        var v = certs[i].split(',');
        v = v[0].substring(3);
        if (v != '') {
          if (v != cn) {
            v = "<option>" + v + "</option>";
          } else
            v = "<option selected>" + v + "</option>";
          options.push(v);
        }
      }
    }
    if (cn == '' && options.length == 0)
      clear = false;

    //删除所有未被选择的选项
    if (clear)
      usercerts.find('option').remove();

    if (options.length == 0) {
      if (clear)
        usercerts.append("<option value=''>请插入USB KEY</option>");

      if (hasUkey) {
        $('#cform').data('bootstrapValidator').updateStatus($('#cert_user'), "INVALID");
        hasUkey = false;
      }
    } else {
      if (!hasUkey) {
        $('#cform').data('bootstrapValidator').updateStatus($('#cert_user'), "VALID");
        hasUkey = true;
      }
      for (i = 0; i < options.length; i++)
        usercerts.append(options[i]);
    }
  }

  function MonitorCert(FindResult) {
    var Sign = GetControl();
    try {
      Sign.FindCertificates(
        Sign.Base64Encode(filter),
        true,
        FindResult
      );
    } catch (e) {
      onCheck(-3);
      console.log(e);
    }
  }

  //组件及USBKEY检查回掉函数
  //nRet 为0，时表示KEY没有插入，循环等待，2秒一次
  //不要在Timerout之外onCheck进行递归调用
  function onCheck(nRet) {
    if (nRet == 0) {
      //监控USB KEY插入,等待KEY插入
      /*
      window.setTimeout(function() {
        ComponetCheck(onCheck);
      }, 2000);
      */
      MonitorCert(onCertResult);
      return false;
    } else if (nRet == -1) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    } else if (nRet == -2) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    } else if (nRet == -3) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    } else if (nRet == -4) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    }
    console.log("USBKEY count:", nRet);
    //监控USB KEY插入,载入证书
    MonitorCert(onCertResult);
  }

  ComponetCheck(onCheck);
}
var logined = false;

function AutoLogout() {
  //  var filter = "issuer:DemoSM2CA";

  function onCertResult(certs) {
    if (logined == false) true;
    var logout = true;
    if (certtimer != undefined && certtimer != null) window.clearTimeout(certtimer);
    certtimer = window.setTimeout(function() {
      MonitorCert(onCertResult);
    }, 3000);

    //所有可选的用户证书
    certs = certs.split(";");
    for (i = 0; i < certs.length; i++) {
      if (certs[i] != '') {
        var v = certs[i].split(',');
        v = v[0].substring(3);
        if (v != '') {
          logout = false;
        }
      }
    }
    //删除所有未被选择的选项
    if (logout) {
      $.ajax({
        url: "logout",
        type: "GET",
        dataType: "json",
        cache: false,
        success: function(result) {
          if (result.code == 0) {
            logined = false;
            window.clearTimeout(certtimer);
            certtimer = null;
            $('#main').load('ctx/login.html');
          } else {
            ShowAlert(result.message || "注销操作失败");
          }
        }
      });
    }
  }

  function MonitorCert(FindResult) {
    var Sign = GetControl();
    try {
      Sign.FindCertificates(
        Sign.Base64Encode(filter),
        true,
        FindResult
      );
    } catch (e) {
      onCheck(-3);
      console.log(e);
    }
  }

  //组件及USBKEY检查回掉函数
  //nRet 为0，时表示KEY没有插入，循环等待，2秒一次
  //不要在Timerout之外onCheck进行递归调用
  function onCheck(nRet) {
    if (nRet == 0) {
      //监控USB KEY插入,等待KEY插入
      MonitorCert(onCertResult);
      return false;
    } else if (nRet == -1) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    } else if (nRet == -2) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    } else if (nRet == -3) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    } else if (nRet == -4) {
      setConfirm('国密安全组件下载提示', '国密安全组件未安装,确认下载？');
      return false;
    }
    console.log("USBKEY count:", nRet);
    //监控USB KEY插入,载入证书
    MonitorCert(onCertResult);
  }

  ComponetCheck(onCheck);
}

function showError(msg) {
  ShowAlert(msg);
  return false;
}

function certSubmit(result, onResult) {
  $.ajax({
    type: "POST",
    dataType: "json",
    url: "login",
    data: {
      cn: result.cn,
      signature: result.singR,
      cert: result.cert,
      verifyRandom: verifyRandom,
      authCode: "certificate_auth"
    },
    cache: false,
    async: false,
    success: function(obj) {
      if (obj.code == 0) {
        hasUkey = false;
        $("#main").load("portal.html");
      } else {
        onResult(false, obj.data.authError);
        loadVerifyRandom();
      }
    }
  });
}

//证书认证接口回调函数
//success 为 true 表示成功, false 表示失败
function onResult(success, msg) {
  if (success) {
    return true;
  } else {
    showError(msg);
    return false;
  }
}

function setConfirm(title, message) {
  BootstrapDialog.confirm({
    title: title,
    message: message,
    type: BootstrapDialog.TYPE_WARNING,
    closable: true,
    draggable: true,
    btnCancelLabel: '取消',
    btnOKLabel: '下载', // <-- Default value is 'OK',
    btnOKClass: 'btn-warning',
    callback: function(result) {
      if (result) {
        window.location = "downloadClient";
      }
    }
  });
}


function ChangePIN(name, opin, spin, onResult) {
  var Sign = GetControl();
  try {
    Sign.SelectCertificate(Sign.Base64Encode(name), true, function(cert) {
      Sign.ChangePIN(cert, opin, spin, onResult);
    });
  } catch (e) {
    onResult(e);
  }
}

function signSM2(name, password, randomR, onResult) {
  var singR;
  var Sign = GetControl();
  var result = {};
  result.cn = name;
  result.randomR = randomR;
  try {
    //选择得到证书
    function onSelectCertificate(cert) {
      if (window.console && console.log)
        console.log('select cert:' + cert);
      if (cert == 0 || cert == "" || cert == null) {
        onResult(false, "未发现可用证书");
        return false;
      }

      //导出用户证书
      function onCertExport(Export) {
        if (Export == null || Export == "") {
          onResult(false, "内部错误");
          return false;
        }
        result.cert = Export;
        Sign.FreeCertificate(cert, function() {
          cert = 0;
        });
        certSubmit(result, onResult);
      }

      //得到签名结果
      function onSignData(singR) {
        if (singR == null || singR == "") {
          $('#pin').val('');
          $('#pin').focus();
          onResult(false, "用户证书密码验证错误");
          return false;
        }
        result.singR = singR;
        Sign.ExportCertificate(cert, onCertExport);
      }

      function onChecked(nRet) {
        if (nRet == 0) {
          Sign.SignData(cert, 3, Sign.Base64Encode(randomR), onSignData);
        } else if (nRet > 0) {
          $('#pin').val('');
          $('#pin').focus();
          onResult(false, "用户证书密码错误，您还可以尝试" + nRet + "次登录");
          return false;
        } else {
          $('#pin').val('');
          onResult(false, "用户证书被锁定，请联系管理员");
          return false;
        }
      }
      Sign.CheckPIN(cert, Sign.Base64Encode(password), onChecked);
    }
    Sign.SelectCertificate(Sign.Base64Encode(name), true, onSelectCertificate);

    return false;
  } catch (e) {
    onResult(false, e.message);
  } finally {}
  return false;
}

//encoding:utf-8

navigator.sayswho = (function() {
  var ua = navigator.userAgent,
    tem,
    M =
      ua.match(
        /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
      ) || [];
  if (/trident/i.test(M[1])) {
    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    return "IE " + (tem[1] || "");
  }
  if (M[1] === "Chrome") {
    tem = ua.match(/\bOPR\/(\d+)/);
    if (tem != null) return "Opera " + tem[1];
  }
  M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, "-?"];
  if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
  return M.join(" ");
})();

/**
 *
 *  Base64 encode / decode
 *  http://www.webtoolkit.info/
 *
 **/
var Base64 = {
  // private property
  _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

  // public method for encoding
  encode: function(input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = Base64._utf8_encode(input);

    while (i < input.length) {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output =
        output +
        this._keyStr.charAt(enc1) +
        this._keyStr.charAt(enc2) +
        this._keyStr.charAt(enc3) +
        this._keyStr.charAt(enc4);
    }

    return output;
  },

  // public method for decoding
  decode: function(input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    while (i < input.length) {
      enc1 = this._keyStr.indexOf(input.charAt(i++));
      enc2 = this._keyStr.indexOf(input.charAt(i++));
      enc3 = this._keyStr.indexOf(input.charAt(i++));
      enc4 = this._keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
        output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
        output = output + String.fromCharCode(chr3);
      }
    }

    output = Base64._utf8_decode(output);

    return output;
  },

  // private method for UTF-8 encoding
  _utf8_encode: function(string) {
    string = string.replace(/\r\n/g, "\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      } else if (c > 127 && c < 2048) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
    }

    return utftext;
  },

  // private method for UTF-8 decoding
  _utf8_decode: function(utftext) {
    var string = "";
    var i = 0;
    var c = (c1 = c2 = 0);

    while (i < utftext.length) {
      c = utftext.charCodeAt(i);

      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      } else if (c > 191 && c < 224) {
        c2 = utftext.charCodeAt(i + 1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      } else {
        c2 = utftext.charCodeAt(i + 1);
        c3 = utftext.charCodeAt(i + 2);
        string += String.fromCharCode(
          ((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)
        );
        i += 3;
      }
    }

    return string;
  }
};
//////////////////////////////////////////////////////////////////////////////////
// Global variables
//////////////////////////////////////////////////////////////////////////////////

var m_objScaLuancher = null;

var m_retryCount = 0;
var m_retryMax = 100; // 100x500ms = 50s
var m_strSessId = "";

//////////////////////////////////////////////////////////////////////////////////
// Page functions (originally done in pages)
//////////////////////////////////////////////////////////////////////////////////

var ContentName = "com.sca.content";
function SCANativeLauncher() {
  var callback = {};
  var explorer = navigator.sayswho;
  var ie = explorer.indexOf("IE") > -1;
  var sign = null;
  if (ie) sign = document.getElementById("SCert");
  else {
    //handle OnMessageBackground
    this.backgroundListener = function(event) {
      if (event.source != window) return;
      var res = event.data;
      if (res.success) {
        if (callback[res.func] != null) {
          callback[res.func](event.data.ret);
        } else {
          alert("不支持的方法:" + res.func);
        }
      } else {
        if (window.console && console.log) console.log(res);
      }
    };
    window.addEventListener("message", this.backgroundListener, false);
  }

  this.CheckPIN = function(cert,password, cb) {
    if (ie) {
      cb(sign.CheckPIN(cert,password));
    } else {
      var req = { SCA:true, func: "CheckPIN", params: [cert,password] };
      window.postMessage(req, "*");
      callback.CheckPIN = cb;
    }
  };

  this.ChangePIN = function(cert,opin, spin, cb) {
    if (ie) {
      cb(sign.ChangePIN(cert,opin, spin));
    } else {
      var req = { SCA:true, func: "ChangePIN", params: [cert,opin, spin] };
      window.postMessage(req, "*");
      callback.ChangePIN = cb;
    }
  };

  this.Lookup = function(which, cb) {
    if (ie) {
      cb(sign.Lookup(which));
    } else {
      var req = { SCA:true, func: "Lookup", params: [] };
      window.postMessage(req, "*");
      callback.Lookup = cb;
    }
  };

  this.Base64Encode = function(s) {
    return Base64.encode(s);
  };
  this.Base64Decode = function(s) {
    return Base64.decode(s);
  };
  /*
  this.Base64Encode = function(msg, cb) {
    var req = {func: "Base64Encode", params: [msg]};
    window.postMessage(req, "*");
    callback.Base64Encode = cb;
  };
  
  this.Base64Decode = function(msg, cb) {
    var req = {func: "Base64Decode", params: [msg]};
    window.postMessage(req, "*");
    callback.Base64Decode = cb;
  };
  */

  this.Digest = function(msg, alg, cb) {
    if (ie) {
      cb(sign.Digest(msg, alg));
    } else {
      var req = { SCA:true, func: "Digest", params: [msg, alg] };
      window.postMessage(req, "*");
      callback.Digest = cb;
    }
  };

  this.FindCertificates = function(filter, onlysign, cb) {
    if (ie) {
      var val = sign.FindCertificates(filter, onlysign);
      console.log(new Date()+' XX:'+val);
      cb(val);
    } else {
      var id = window.setTimeout(function(n){
        cb(n);
      }, 3000)
      var req = { SCA:true, func: "FindCertificates", params: [filter, onlysign] };
      window.postMessage(req, "*");
      callback.FindCertificates = function(n){ window.clearTimeout(id); cb(n);};
    }
  };

  this.SelectCertificate = function(msg, onlysign, cb) {
    if (ie) {
      cb(sign.SelectCertificate(msg, onlysign));
    } else {
      var req = { SCA:true, func: "SelectCertificate", params: [msg, onlysign] };
      window.postMessage(req, "*");
      callback.SelectCertificate = cb;
    }
  };
  this.ViewCertificate = function(hCert, cb) {
    if (ie) {
      sign.ViewCertificate(hCert);
      cb();
    } else {
      var req = { SCA:true, func: "ViewCertificate", params: [hCert] };
      window.postMessage(req, "*");
      callback.ViewCertificate = cb;
    }
  };
  this.FreeCertificate = function(hCert, cb) {
    if (ie) {
      cb(sign.FreeCertificate(hCert));
    } else {
      var req = { SCA:true, func: "FreeCertificate", params: [hCert] };
      window.postMessage(req, "*");
      callback.FreeCertificate = cb;
    }
  };

  this.GetCertInfo = function(hCert, i, cb) {
    if (ie) {
      cb(sign.GetCertInfo(hCert, i));
    } else {
      var req = { SCA:true, func: "GetCertInfo", params: [hCert, i] };
      window.postMessage(req, "*");
      callback.GetCertInfo = cb;
    }
  };

  this.ExportCertificate = function(hCert, cb) {
    if (ie) {
      cb(sign.ExportCertificate(hCert));
    } else {
      var req = { SCA:true, func: "ExportCertificate", params: [hCert] };
      window.postMessage(req, "*");
      callback.ExportCertificate = cb;
    }
  };

  this.SignData = function(hCert, alg, msg, cb) {
    if (ie) {
      cb(sign.SignData(hCert, alg, msg));
    } else {
      var req = { SCA:true, func: "SignData", params: [hCert, alg, msg] };
      window.postMessage(req, "*");
      callback.SignData = cb;
    }
  };

  this.VerifySignature = function(hCert, alg, msg, sig, cb) {
    if (ie) {
      cb(sign.VerifySignature(hCert, alg, msg, sig));
    } else {
      var req = { SCA:true, func: "VerifySignature", params: [hCert, alg, msg, sig] };
      window.postMessage(req, "*");
      callback.VerifySignature = cb;
    }
  };

  this.PKCS7Sign = function(hCert, msg, flags, cb) {
    if (ie) {
      cb(sign.PKCS7Sign(hCert, msg, flags));
    } else {
      var req = { SCA:true, func: "PKCS7Sign", params: [hCert, msg, flags] };
      window.postMessage(req, "*");
      callback.PKCS7Sign = cb;
    }
  };

  this.PKCS7Verify = function(p7, msg, cert, flags, cb) {
    cert = flags & 0x2 ? cert : "";
    msg = flags & 0x1 ? msg : "";
    if (ie) {
      cb(sign.PKCS7Verify(p7, msg, cert, flags));
    } else {
      var req = { SCA:true, func: "PKCS7Verify", params: [p7, cert, msg, flags] };
      window.postMessage(req, "*");
      callback.PKCS7Verify = cb;
    }
  };
}

//////////////////////////////////////////////////////////////////////////////////
var m_objScaLuancher = null;

function GetControl() {
  try {
    if (m_objScaLuancher == null) {
      m_objScaLuancher = new SCANativeLauncher();
    }
    return m_objScaLuancher;
  } catch (e) {
    var explorer = navigator.sayswho;
    if (window.console && console.log) console.log(e);
    alert(
      "浏览器" +
        explorer +
        "不被支持\n" +
        "请使用 IE8 或 Chrome 47 及以上版本的浏览器"
    );
  }
  return null;
}

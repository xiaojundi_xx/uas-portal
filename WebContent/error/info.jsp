<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html;" charset="utf-8" />
  <title>页面出错</title>
  <style>  
        body{font-size: 14px;font-family: 'helvetica neue',tahoma,arial,'hiragino sans gb','microsoft yahei','Simsun',sans-serif; background-color:#fff; color:#808080;}  
        .wrap{
          margin:200px auto;
          width: 60%;
          position:absolute;
          left:20%;
        }
        table{margin:0px auto;}
        td{text-align:left; padding:2px 10px;}  
        td.header{font-size:22px; padding-bottom:10px; color:#000;}  
        td.check-info{padding-top:20px;}  
        a{color:#328ce5; text-decoration:none;}  
        a:hover{text-decoration:underline;}  
    </style>  
</head>
<body>
	<div class="wrap">
		<table>
			<tr>
				<td rowspan="5" style=""><img src="images/error.png" alt=""/></td>
				<td class="header">很抱歉!${errorName}</td>
			</tr>
			<tr>
				<td>错误原因：${message}</td>
			</tr>
		</table>
	</div>
</body>
</html>